# PhaseUnwrapper

### A C library interfacing with Python via Cython to unwrap wrapped phase images.


Phase unwrapping is a problem that occurs when handling complex valued data,
for example in Interferometric Synthetic Aperture Radar (IFSAR), 
which it was originally developed for. It is also used in Magnetic Resonance Imaging (MRI), 
wavefront distortion measurement [1], [2] or - as is the case here - in diffraction imaging. 
The data of a phase always lies in the interval $(-\pi, \pi]$ 
and values that are lower or higher than the boundaries will be wrapped, like 
value overflow for small datatypes like `uint8`.

### Generating the library
To be able to use the Python functions you will first have to build the library 
file. This requires [Cython 0.24](http://cython.org/#download) or higher to be installed.

After that you need to run the following command in the directory where `setup.py` is located: <br>
`python setup.py build_ext --inplace` <br>
or simply run the shell script provided in this repository: <br>
`sh run_setup.sh` <br>
which does the same thing. <p>

This is only required the first time you use this. After the build directory is created,
you can automatically recompile before calling your `main()` function using the
`subprocess` Python module:

```python
import subprocess
print "starting self compilation!"
try:
    subprocess.check_call(["./run_setup.sh"])
    print "Compilation finished"
    main()
except subprocess.CalledProcessError:
    print "There were compilation errors"
```

When the library file (`PhaseUnwrapper_C.so` for unix-based systems) is generated 
a phase image can be unwrapped by calling the `unwrap_phase` function:
```python
from PhaseUnwrapper_C import unwrap_phase
import numpy as np
wrapped = np.load(‘my_phase_array.npy‘)
algorithm = ‘flynn‘
k = 3
unwrapped = unwrap_phase(wrapped, algorithm=algorithm, quality_map_mode=‘variance‘, filter_size=k)
```


### References
[1] [D. C. Ghiglia and M. D. Pritt, 
    "Two-Dimensional Phase Unwrapping: Theory, Algorithms and Software."
    Wiley Blackwell, May 1998.
    http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html]
    
[2] [L. Ying, 
    “Phase unwrapping,” 
    in Wiley encyclopedia of biomedical engineering, 
    M. Akay, Ed. Wiley Interscience, 2006, pp. 266–290. 
    https://dx.doi.org/10.1002/9780471740360.ebs1356]
    

