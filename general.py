# # -*- coding: utf-8 -*-
#
# !/usr/bin/env python
#
# :brief:    module implementing the general purpose tools/utilities for phase unwrapping
#
#            Implementation of phase unwrapping specific code
#            is a more or less modified version of the one found in
#            D. C. Ghiglia and M. D. Pritt, Two-Dimensional Phase Unwrapping:
#            Theory, Algorithms and Software. Wiley Blackwell, May 1998.
#            http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html
#
# :date:     15.06.2016
# :author:   Tim Elberfeld

import types
import warnings
import matplotlib.pyplot as plt
import numpy as np
import numpy.fft as nft
import pyfftw.builders as fftw              # faster FFT than numpy
import logger
from itertools import izip
from PhaseUnwrapper_C import floodfill, point_in_image

_PI = np.pi
_NPI = -np.pi
_TWOPI = 2*np.pi
customLog = logger.get_custom_logger("phase_unwrapper.txt")


def threshold(array, thresholdval, comparetype=None):
    """
    :brief:                 simple thresholding operation
    :param array:           array to perform thresholding on
    :param thresholdval:    value to compare against
    :param comparetype:     type of comparison. If nothing is given here, the default
                            comparison is >= or 'ge'
    :return:                binary array of same size as input with the result
    :date:                  15.06.2016
    :author:                Tim Elberfeld
    """
    if comparetype == 'ge' or comparetype is None:    # greater equal
        wherethresh = np.where(array >= thresholdval)
    elif comparetype == 'g':                          # greater
        wherethresh = np.where(array > thresholdval)
    elif comparetype == 'l':                          # lower
        wherethresh = np.where(array < thresholdval)
    elif comparetype == 'le':                         # lower equal
        wherethresh = np.where(array <= thresholdval)
    else:
        raise ValueError('unknown comparison type')

    retval = np.zeros_like(array)
    retval[wherethresh] = 1

    return retval


def wrapping_operator(array):
    """
    :brief:         function to wrap the phase values to the interval [-pi, pi]
    :param array:   array to wrap
    :return:        wrapped array
    :date:          15.06.2016
    :author:        Tim Elberfeld
    """
    result = np.mod(array + np.pi, 2*np.pi) - np.pi
    return result


def map_to_range(array, minval, maxval):
    """
    :brief:         maps the values in an array to the specified range
    :param array:   array to map
    :param minval:  new minimum value
    :param maxval:  new maximum value
    :return:        mapped array
    :date:          16.06.2016
    :author:        Tim Elberfeld
    """
    return normalize(array) * (maxval - minval) + minval


def normalize(array, minval=None, maxval=None):
    """
    :brief:         normalize an array to range [0,1]
    :param array:   array to normalize
    :param minval:  optional parameter: minimum value to use for the normalization.
                    If minval is specified maxval must also be specified
    :param maxval:  optional parameter: maximum value to use for the normalization.
                    If maxval is specified minval must also be specified
    :return:        normalized array
    :date:          16.06.2016
    :author:        Tim Elberfeld
    """
    if minval is None:
        return ((array.astype(np.float64) - array.min()) / float(array.ptp()))
    else:
        valrange = maxval - minval
        return ((array.astype(np.float64) - minval) / valrange)


def imfilter(array, mode="gaussian", **kwargs):
    """
    :brief:         filter function for images or 2d arrays
    :param array:   array that should be filtered
    :param mode:    filter mode. Can be one of
                    -   "gaussian"      - default!
                            gaussian smoothing / blur
                            requires one float value keyword
                            sigma=< myvalue >

                    -   "mean"
                            mean smoothing filter
                            requires an integer tuple keyword
                            size=< (myfilterwidth , myfilterheight) >

                    -   "binomial"
                            2d binomial smoothing filter (discrete approximation of gaussian)
                            requires an integer keyword
                            order=< myinteger >

                            order is the iteration of pascal's triangle that is used for the computation of the filter
                            mask. This produces the 1d filter mask first and computes the 2d version using the outer
                            product of this 1d version with itself
                            note that order 0 is [1] so the first "useful" filter that can be constructed
                            would be of order 2 (producing 1.0/4.0*[1,2,1]).
                            If order would produce an even row of pascals triangle the order will be incremented by 1
                            (for example order = 3 would produce [1, 3, 3, 1]. This will be fixed by using order = 4,
                            which produces [1, 4, 6, 4, 1])

                    -   "derivative"
                            derivative filter
                            requires a string keyword
                            direction=<directionstring>
                            directionstring can be one of
                            -   "x"         -> x direction derivative
                            -   "y"         -> y direction derivative
                            -   "xy" / "yx" -> both direction derivative

    :param kwargs:  variable arguments depending on chosen filter mode
                    see :param mode: for requirements for each filter
    :return:        filtered array
    :date:          22.06.2016
    :author:        Tim Elberfeld
    """
    # todo      test this method, this is code that has never been run!
    assert(len(array.shape) == 2)       # make sure this is a 2d array

    # parse keywords
    if mode == "gaussian":
        if "sigma" in kwargs:
            return np.real(gaussian_filter(array, kwargs["sigma"]))
        else:
            warnings.warn("gaussian filter called witout sigma keyword. Using 1.0 as sigma value")
            return np.real(gaussian_filter(array, 1.0))

    elif mode == "mean":
        if "size" in kwargs:
            filtersize = kwargs["size"]
            if len(filtersize) != 2:
                raise ValueError("\"size\" must be a tuple specifying an integer height and width")
            return np.real(mean_filter(array, kwargs["size"]))
        else:
            raise ValueError("Mean filter requires the \"size\" keyword to be specified")

    elif mode == "binomial":
        if "order" in kwargs:
            return np.real(binomial_filter(array, kwargs["order"]))
        else:
            raise ValueError("Binomial filter requires the \"order\" keyword to be specified")

    elif mode == "derivative":
        if "direction" in kwargs:
            direction = kwargs["direction"]
            return np.real(derivative_filter(array, mode=direction))
        else:
            raise ValueError("Derivative filter requires the \"direction\" keyword to be specified!")
    else:
        raise ValueError("unknown mode {0}".format(mode))


def binomial_filter(array, order):
    # todo      test this method, this is code that has never been run! Also write a description
    if order % 2 != 0:              # make sure we get a square with odd side length
        warnings.warn("order {0} is odd -> order = {1}".format(order, order + 1))
        order += 1
    filter_1d = np.asarray(pascals_triangle(order))
    filter_2d = np.outer(filter_1d, filter_1d)

    return conv_fourier(array, filter_2d)


def mean_filter(array, filtersize):
    # todo      test this method, this is code that has never been run!
    """
    :brief:             a mean filter is a convolution with a 2d rect function!
    :param array:       array to filter
    :param filtersize:  size of the rectangle used as mean filter
    :return:            filtered array
    :date:              22.06.2016
    :author:            Tim Elberfeld
    """
    assert(len(filtersize) == 2)  # assure that we deal with 2d filter!
    rect = np.ones(filtersize) * (1.0 / np.prod(filtersize))
    return conv_fourier(array, rect)


def derivative_filter(array, mode):
    """
    :brief:         derivative filter
    :param array:   array to filter
    :param mode:    mode of the filter. Can be one of
                    "x" -> only x direction derivative
                    "y" -> only y direction derivative
                    "xy" or "yx" -> both directions
                    this is not case sensitive
    :return:        filtered version of array
    :date:          22.06.2016
    :author:        Tim Elberfeld
    """
    # todo      test this method, this is code that has never been run!
    if mode.lower() == "x":
        mask = 1.0/2.0 * np.array([[0, 0, 0], [-1, 0, 1], [0, 0, 0]])  # mask = np.array([-1, 1])
    elif mode.lower() == "y":
        mask = 1.0/2.0 * np.array([[0, -1, 0], [0, 0, 0], [0, 1, 0]])  # mask = np.array([[-1], [1]])
    elif mode.lower() == "xy" or mode.lower() == "yx":
        mask = 1.0/4.0 * np.array([[0, 1, 0], [1, 0, -1], [0, -1, 0]])  # mask = np.array([[1, -1], [-1, 1])
    else:
        raise ValueError("mode {0} not recognized".format(mode))

    return conv_fourier(array, mask)


def conv_fourier(array1, array2, windowtype="hamming"):
    """
    :brief:             multiply two arrays in Fourier space -> convolution
    :param array1:      first array
    :param array2:      second array
    :param windowtype:  type of window to use. Pass None to not use a window
    :return:            convolution of both arrays, computed in fourier space
    :date:              22.06.2016
    :author:            Tim Elberfeld
    """
    size1 = array1.shape
    size2 = array2.shape

    # todo: figure out which one is bigger, and which one must be padded. Right now size2 must fit into size1!
    assert(size1[1] >= size2[1] and size1[0] >= size2[0])

    embed = np.zeros_like(array1)     # embed the secondary array in an array that is the same size as the main array
    embed[0:size2[0], 0:size2[1]] = array2
    embed = np.roll(embed, -size2[0] // 2, axis=0)  # shift mask to center
    embed = np.roll(embed, -size2[1] // 2, axis=1)

    if windowtype is not None:
        embed = apply_window_function(embed, type=windowtype)
        array1 = apply_window_function(array1, type=windowtype)

    f_transfer = fftw.fft2(embed)                   # this is the transfer function of the filter
    f_array = fftw.fft2(nft.fftshift(array1))
    f_prod = np.multiply(f_transfer(), f_array())   # convolution is multiplication in reciprocal space

    norm_ft = array1.size                           # this should be the normalization factor for the fft
    product = fftw.ifft2(f_prod)() / norm_ft

    return product


def gaussian_filter(array, sigma):
    """
    :brief:         apply gaussian blur to an array
    :param array:   array to blur
    :param sigma:   sigma value of the gaussian function to use to blur
    :return:        blurred array
    :date:          16.06.2016
    :author:        Tim Elberfeld
    :todo:          Need to rework the normalization here. there is somehting weird going on with fftw
    """
    minval = array.min()
    maxval = array.max()

    fval = fftw.fft2(nft.fftshift(array))() #fval = f()
    gauss = fftw.fft2(nft.fftshift(gaussian_window_2(array.shape, sigma)))()  # gauss = gauss()

    ft_smooth = np.multiply(fval, gauss)
    ift_smooth = fftw.ifft2(ft_smooth)

    # manual normalization is necessary because FFTW does not normalize
    ift_smooth_norm = map_to_range(ift_smooth(), minval, maxval)

    return np.real(nft.fftshift(ift_smooth_norm))


def base_grid_2(start, stop, gridsize):
    """
    :brief:             make 2D base grid
    :param start:       start value of the target interval
    :param stop:        stop value of the target interval
    :param gridsize:    size of the grid in sampling points (will be square)
    :return:            the grid
    :date:              25.04.2016
    :author:            Tim Elberfeld
    """
    stepx = (stop - start) * 1.0 / gridsize[1] * 1.0
    stepy = (stop - start) * 1.0 / gridsize[0] * 1.0
    x = np.arange(start, stop, stepx)
    y = np.arange(start, stop, stepy)
    xx, yy = np.meshgrid(x, y)
    return xx, yy


def gaussian_window_2(imagesize, sigma):
    """
    :brief:             Make a 2D window function following a gaussian profile
    :param imagesize:   Size of the resulting window
    :param sigma:       Sigma of the gaussian function.
                        The coordinate interval is set to [-1, 1] regardless of imageSize
    :return:            ndarray containing the gaussian
    :date:              25.04.2016
    :author:            Tim Elberfeld
    """
    x, y = base_grid_2(start=-1, stop=1, gridsize=imagesize)
    window = np.exp(-(x**2 + y**2) / (2*(sigma**2)))
    return window


def pascals_triangle(order):
    """
    :brief:         Compute the line of pascal's triangle with order 'order'
                    |       line          | order |
                    |---------------------|-------|
                    |         1           |   0   |
                    |       1   1         |   1   |
                    |     1   2   1       |   2   |
                    |    1   3   3   1    |   3   |
                    | 1   4   6   4   1   |   4   |
    :param order:   order of the line in pascal's triangle (starting with 0, which returns just [1])
    :return:        a list of the pascal's triangle line of order 'order'
    :date:          22.06.2016
    :author:        Tim Elberfeld
    """
    line = [1]
    for k in xrange(order):
        line.append(line[k] * (order - k) / (k + 1))
    return line


def crop_image(image, topleft, bottomright):
	"""
	:brief:             crop an image 
    :param image:	    the image
	:param topleft:		tuple representing the topleft point of the cropped image
	:param bottomright:	tuple representing the bottomright point of the croppped image
    :return:            cropped image
    :date:              10.10.2016
    :author:            Tim Elberfeld
	"""
	if (point_in_image(topleft[1], topleft[0], image.shape[0], image.shape[1]) and
		point_in_image(topleft[1], topleft[0], image.shape[0], image.shape[1])):
		
		return(image[topleft[0]:bottomright[0], topleft[1]:bottomright[1]])
	else:
		raise ValueError("Points for cropping must lie within the image")
	

	
def plotsurface(surface, path=None):
    """
    :brief:             plot a perlin noise surface as wireframe
    :param surface:     the surface to plot
    :param path:        optional parameter, if a valid path is given this renders an .eps of the plot
                        at this location
    :return:            nothing
    :date:              16.06.2016
    :author:            Tim Elberfeld
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x, y = base_grid_2(-1, 1, surface.shape)
    ax.plot_wireframe(x, y, surface, rstride=10, cstride=10)

    plt.show()

    if path is not None:
        fig.tight_layout()                      # reduce the margins on the sides
        plt.rc('text', usetex=True)             # enable TeX
        plt.rc('font', family='serif')          # change font

        plt.title("Example terrain generated by Perlin noise",
                  fontsize=16, color='gray')
        ax = plt.gca(projection='3d')
        ax.set_zlabel(r'terrain height', fontsize=16)
        ax.set_zticks([2*np.pi, np.pi, 0, -np.pi, -2*np.pi],
                      [r'$2\pi$', r'$\pi$', r'$0$', r'$-\pi$', r'$-2\pi$'])

        elevation_angle = -155.0
        azimuthal_angle = -119.0
        ax.view_init(elev=elevation_angle, azim=azimuthal_angle)

        x, y = base_grid_2(-1, 1, surface.shape)
       # ax.plot_surface(x, y, 1, rstride=1, cstride=1, facecolors=normalize(surface))

        plt.savefig(path + '.eps', format='eps', dpi=1000,  transparent=True)



def number_sign(num):
    """
    :brief:         function returning the sign of a number
    :param num:     number to test
    :return:        -1 if negative number, if 0 or positive returns 1
    :note:          This function would not be necessary if np.sign would
                    not have a special case for 0
    :date:          17.06.2016
    :author:        Tim Elberfeld
    """
    if num < 0:
        return -1
    if num >= 0:
        return 1


def euclidean_distance(p0, p1):
    """
    :brief:         simple euclidean distance. p0 and p1 can be numpy arrays, lists or tuples
                    In case of numpy arrays the points can also be 2xn to calculate several distances
                    at once.
    :param p0:      first point
    :param p1:      second point
    :return:        distance(s)
    :date:          19.06.2016
    :author:        Tim Elberfeld
    """
    return np.sqrt(np.sum((np.asarray(p0) - np.asarray(p1)) ** 2))


def print_log(msg, logger=None):
    """
    :brief:         custom logging and printing function
    :param msg:     message to log and print
    :param logger:  optional parameter. You can pass a custom logger here that will then be used for logging
    :return:        nothing
    :date:          01.07.2016
    :author:        Tim Elberfeld
    """
    print msg
    if logger is not None:
        logger.debug(msg)
    else:
        customLog.debug(msg)


def offset_list(k, center=None):
    """
    :brief:         construct a list of all offsets for a k by k environment centered around (0,0)
    :param k:       size of the environment
    :param center:  center point for the offsets. Coordinates returned will
                    be centered around this point, if it is not None.
                    This parameter is optional
    :return:        coordinates, same format as np.where()
                    when offsets[0] is multiplied with x position
                    and offsets[1] with y position the result can be directly
                    used to index ndarrays
    :date:          22.06.2016
    :author:        Tim Elberfeld
    """
    xoff = list()
    yoff = list()
    khalf = k // 2
    lower = -khalf
    upper = khalf

    if k % 2 != 0:
        upper += 1

    for koff in xrange(lower, upper):
        xoff.append(k*[koff])
        yoff.append(koff)

    xoff = np.asarray([inner for outer in xoff for inner in outer])  # unpack list of lists
    yoff = np.asarray(k * yoff)

    if center is not None:
        yoff += center[0]
        xoff += center[1]

    return tuple((yoff, xoff))


def border_offset_list(k, center=None, imshape=None):
    """
    :brief:         construct a list off the offsets of the border of a k by k environment centered at (0,0)
    :param k:       size of the environment
    :param center:  center point for the offsets. Coordinates returned will
                    be centered around this point, if it is not None.
                    This parameter is optional
    :param imshape: shape of the image (width and height)- if this parameter is given, only points within the image
                    are returned
    :return:        coordinates, same format as np.where()
                    when offsets[0] is multiplied with x position
                    and offsets[1] with y position the result can be directly
                    used to index ndarrays
    :date:          23.06.2016
    :author:        Tim Elberfeld
    """
    if k < 3:
        coordinates = offset_list(k, center)
    else:
        khalf = k // 2
        kmin2half = (k - 2) // 2
        lower_bound = -khalf
        upper_bound = khalf
        lowermin2 = -kmin2half
        uppermin2 = kmin2half

        if k % 2 != 0:
            upper_bound += 1                          # include khalf in the interval
            uppermin2 += 1                      # include khalfmin2 in the inteval

            lurange = range(lower_bound, upper_bound)
            lumin2range = sorted(2 * range(lowermin2, uppermin2))
            yoff = k * [-khalf] + lumin2range + k * [khalf]
            xoff = lurange + (k - 2) * [-khalf, khalf] + lurange

        else:
            lurange = range(lower_bound, upper_bound)
            lumin2range = sorted(2 * range(lowermin2, uppermin2))
            yoff = k * [-khalf] + lumin2range + k * [(khalf-1)]
            xoff = lurange + (k - 2) * [-khalf, (khalf-1)] + lurange

        if center is not None and imshape is not None:
            height, width = imshape
            xoff_m =list()
            yoff_m = list()
            for xx, yy in izip(xoff, yoff):
                yy_m = yy + center[0]
                xx_m = xx + center[1]
                if point_in_image(xx_m, yy_m, height, width):
                    xoff_m.append(xx)
                    yoff_m.append(yy)

            return tuple([np.asarray(xoff_m), np.asarray(yoff_m)])
        elif center is not None:
            coordinates = [np.asarray(xoff), np.asarray(yoff)]
            coordinates[0] += center[0]
            coordinates[1] += center[1]
            return tuple(coordinates)

        else:
            return tuple([np.asarray(xoff), np.asarray(yoff)])


def get_neighborhood(pos, neighborhood=4):
    """
    :brief:                 return a list of all neighboring pixels of pos
    :param pos:             position that the neighborhood is centered on
    :param neighborhood:    neighborhood specifier. Can have 2 values:
                            4 - only top, left, right, bottom
                            8 - top left, top, top right, right, left, bottom left, bottom, bottom right
    :return:                list of neighbor pixel positions
    :date:                  01.07.2016
    :author:                Tim Elberfeld
    """
    if neighborhood == 4:
        plist = [(pos[0] - 1, pos[1]),          # top
                 (pos[0] + 1, pos[1]),          # bottom
                 (pos[0], pos[1] + 1),          # right
                 (pos[0], pos[1] - 1)]          # left

        return plist
    elif neighborhood == 8:
        plist = [(pos[0] - 1, pos[1]),          # top
                 (pos[0] - 1, pos[1] + 1),      # top right
                 (pos[0] - 1, pos[1] - 1),      # top left
                 (pos[0] + 1, pos[1]),          # bottom
                 (pos[0] + 1, pos[1] + 1),      # bottom right
                 (pos[0] + 1, pos[1] - 1),      # bottom left
                 (pos[0],     pos[1] + 1),      # right
                 (pos[0],     pos[1] - 1)]      # left

        return plist
    else:
        raise ValueError("invalid neighborhood specifier - can be 4 or 8")

def set_border(img, value, isflag=False):
    """
    :brief:         set the border pixels of an array to 'value'
    :param img:     the array
    :param value:   the new values for the border
    :param isflag:  if set to true value is treated as a flag and will be bitwise or'ed with img
    :return:        the original array with updated border pixels
    :date:          01.07.2016
    :author:        Tim Elberfeld
    """
    if isflag:
        img[:, -1] |= value
        img[-1, :] |= value
        img[:, 0] |= value
        img[0, :] |= value
    else:
        img[:, -1] = value
        img[-1, :] = value
        img[:, 0] = value
        img[0, :] = value

    return img


def squared_diff_error(groundtruth, test):
    """
    :brief:             calculate the squared difference error of an image to its ground truth
    :param groundtruth: the ground truth
    :param test:        test data that should be the same as ground truth
    :return:            squared difference error
    :date:              04.07.2016
    :author:            Tim Elberfeld
    """
    diff = np.subtract(groundtruth, test)**2
    return np.sum(diff)


def apply_window_function(array, type, beta=None):
    """
    :brief:         apply a window function to an array. Step for before the Fourier transform
    :param array:   array to apply the window to
    :param type:    type of the window. Can be one of the following
                    - "bartlett"
                    - "blackman"
                    - "hamming"
                    - "hanning"
                    - "kaiser", has an additional parameter beta
    :return:        array with window applied
    :date:          05.07.2016
    :author:        Tim Elberfeld
    :mod:           19.07.2016  -   changed name of the function from apply_hamming()
                                    to apply_window_function() and added parameter "type"
                                    for a more general purpose functionality for "windowing".
                                    For the Kaiser window the additional parameter beta was also added
    """
    height, width = array.shape
    typei = type.lower()                        # make type lowercase

    """ assign window function as function object"""
    if typei == "bartlett":
        windowfunction = np.bartlett
    elif typei == "blackman":
        windowfunction = np.blackman
    elif typei == "hamming":
        windowfunction = np.hamming
    elif typei == "hanning":
        windowfunction = np.hanning
    elif typei == "kaiser":
        if beta is not None:
            windowfunction = lambda M: (np.kaiser(M, beta=beta))  # this function has "beta" already in it, when called
        else:
            raise ValueError("For mode \"kaiser\", beta needs to be specified!")
    else:
        raise ValueError("type {0} not recognized".format(type))

    windowy = windowfunction(height)
    windowx = windowfunction(width)
    window = np.sqrt(np.outer(windowy, windowx))  # make the window 2d by taking the outer product and then normalize

    return np.multiply(array.copy(), window)


def floodfill_list(img, pos, bordercolor):
    """
    :brief:             simple floodfill algorithm (non recursive)
                        This generates a list of points first and then fills in a second
                        pass. A more efficient routine is available in the PhaseUnwrapper_C.pyx file
    :param img:         image to fill
    :param pos:         seed position for the flood fill
    :param bordercolor: color of the border that will stop the filling
    :return:            np.wherer() like coordinates, of where to fill
    :date:              06.07.2016
    :author:            Tim Elberfeld
    """
    fillcolor = -1
    filled = floodfill(img, pos[1], pos[0], bordercolor, fillcolor)
    return np.where(filled == fillcolor), np.where(filled != fillcolor)


def rgb2gray(imgrgb):
    """
    :brief:         turn rgb image into gray image by simply computing the mean over the color values
    :param imgrgb:  rgb image
    :return:        gray image
    :date:          08.07.2016
    :author:        Tim Elberfeld
    """
    assert(len(imgrgb.shape) == 3)
    return np.mean(imgrgb, axis=2)


def compute_quality_map(phaseimg, k=3):
    """
    :brief:             compute a quality map using the phase derivative variance method
                        The formula to compute the pixel m,n as spedified by Ghiglia et al. is :
                        z_{m, n} = \frac{\sqrt{\sum(\Delta^{x}_{i, j} - \overline{\Delta}^{x}_{m, n}})^{2}} +
                                         \sqrt{\sum(\Delta^{y}_{i, j} - \overline{\Delta}^{y}_{m, n}})^{2}}}
                                         {k^{2}}
    :param phaseimg:    phase image
    :param k:           size of the evaluation window. Ghiglia et al. recommend using k=3, which is the default
    :return:            a quality map of same size as phaseimg. Depending on k the border pixels of a
                        k//2 border around the image will be zero
    :date:              22.06.2016
    :author:            Tim Elberfeld
    """
    #rawmap = res.raw_residue_map(phaseimg=phaseimg)
    x_deriv = phase_grad(phaseimg, "dx")
    y_deriv = phase_grad(phaseimg, "dy")

    filtersize = (k, k)
    avg_x = imfilter(x_deriv, mode="mean", size=filtersize)
    avg_y = imfilter(y_deriv, mode="mean", size=filtersize)

    offsets = offset_list(k)          # k by k window offsets

    height, width = phaseimg.shape
    result = np.zeros_like(phaseimg)
    for yy in range(1, height - 1):
        for xx in range(1, width - 1):
            cur_offsets = tuple((offsets[0]+yy, offsets[1]+xx))
            xderiv_sub = (x_deriv[cur_offsets] - avg_x[yy, xx]) ** 2
            yderiv_sub = (y_deriv[cur_offsets] - avg_y[yy, xx]) ** 2
            firstsum = np.sqrt(np.sum(xderiv_sub))
            secondsum = np.sqrt(np.sum(yderiv_sub))
            result[yy, xx] = firstsum + secondsum / (k**2)

    return normalize(result)


def phasediff_single(val1, val2):
    """
    :brief:          calculate phase difference of two adjacent phase values
    :param val1:     first value
    :param val2:     second value
    :return:         phase difference value
    :date:           23.06.2016
    :author          Tim Elberfeld
    :mod:            07.07.2016 -   In the original code the comparison was done with 0.5 and -0.5 and 1.0 was
                                    subtracted or added to/from r.
                                    Because the phase data is assumed to be in [-pi, pi] in this code, it was changed.
    """
    r = val1 - val2
    if r > _PI:
        r -= _TWOPI
    if r < _NPI:
        r += _TWOPI

    return r


def phasediff_matrix(arr1, arr2):
    """
    :brief:         vectorized version of phasediff_single
    :param arr1:    array with the first set of values, arr1 must have same shape as arr2!
    :param arr2:    array with second set of values, arr1 must have same shape as arr2!
    :return:        array_like with same shape of arr1 and arr2
    :date:          19.07.2016
    :author:        Tim Elberfeld
    """
    assert(arr1.shape == arr2.shape)
    r = arr1 - arr2

    return wrap_phase_diff(r)


def wrap_phase_diff(diff):
    diff[np.where(diff > _PI)] -= _TWOPI
    diff[np.where(diff < _NPI)] += _TWOPI

    return diff


def phase_grad(phaseimg, mode="dx"):
    """
    :brief:             compute phase gradient
    :param phaseimg:    phase image
    :param mode:        can be "dx" or "dy" for x or y direction respectiely
    :return:            gradient image
    :date:              19.07.2016
    :author:            Tim Elberfeld
    """
    if mode.lower() == "dx":
        shift1 = ((0, 0), (1, 1))
        shift2 = ((0, 0), (0, 2))
    elif mode.lower() == "dy":
        shift1 = ((1, 1), (0, 0))
        shift2 = ((0, 2), (0, 0))
    else:
        raise ValueError("mode {0} not recognized".format(mode))

    phaseimg_shift2 = np.pad(phaseimg, shift1, "constant")
    phaseimg_shift1 = np.pad(phaseimg, shift2, "constant")
    grad = phasediff_matrix(phaseimg_shift1, phaseimg_shift2)

    if mode.lower() == "dx":
        grad = grad[:, 1:-1]
    else:
        grad = grad[1:-1, :]

    return grad