# # -*- coding: utf-8 -*-
#
# !/usr/bin/env python
#
# :brief:    several functions to generate test data for the phase unwrapping code
#
#            Implementation of phase unwrapping specific code
#            is a more or less modified version of the one found in
#            D. C. Ghiglia and M. D. Pritt, Two-Dimensional Phase Unwrapping:
#            Theory, Algorithms and Software. Wiley Blackwell, May 1998.
#            http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html
#
# :date:     23.06.2016
# :author:   Tim Elberfeld

import matplotlib.image as matimg
import numpy as np
import numpy.matlib
import numpy.random as rng
from scipy.interpolate import griddata
import logger
import general as gen

from PhaseUnwrapper_C import    bresenham, \
                                point_in_image, \
                                get_nearest_border_pixel, \
                                save_as_bytecode

customLog = logger.get_custom_logger("phase_unwrapper.txt")


def goldstein_example_phase():
    """
    :brief:     return a simple 4x4 example that can be found in
                Goldstein's original paper Goldstein, Zebker and Werner,
                Satellite radar interferometry: Two-dimensional phase unwrapping
                1998
                This example has exactly one (positive) residue, that is the middle 2x2 area
                ... 0.0, 0.3, ...
                ... 0.8, 0.6, ...
    :return:    the grid multiplied by 2pi for easier handling with the other algorithms
    :date:      17.06.2016
    :author:    Tim Elberfeld
    """
    result = [[0.0, 0.1, 0.2, 0.3],
              [0.0, 0.0, 0.3, 0.4],
              [0.9, 0.8, 0.6, 0.5],
              [0.8, 0.8, 0.7, 0.6]]

    return 2*np.pi*np.asarray(result)


def gen_ramp_phase(shape, startval, endval, mode):
    rampimg = np.zeros(shape)
    valrange = endval - startval
    height, width = shape

    step = valrange / height
    ramp = np.arange(start=startval, stop=endval, step=step)

    if mode.lower() == "single":
        rampimg = np.matlib.repmat(ramp, width, 1).T
    elif mode.lower() == "sheared":
        rampimg[:, 0:width//2] = np.matlib.repmat(ramp, width//2, 1).T

    elif mode.lower() == "double sheared":
        rampimg[:, 0:width // 2] = np.matlib.repmat(ramp, width // 2, 1).T
        rampimg[:, width // 2:] = np.matlib.repmat(ramp[::-1], width // 2, 1).T
    else:
        raise ValueError("Invalid mode {0}".format(mode))

    return rampimg


def gen_random_surface(shape, valrange=4*np.pi, seed=None):
    """
    :brief:             generates a random heightmap with shape 'shape'
    :param shape:       2d tuple, specifying width and height of the surface
    :param valrange:    range of the values. Values will be centered, meaning
                        that if taking the default of 4*np.pi the values will range
                        from -2*np.pi to 2*np.pi
    :param seed:        optional parameter to generate reproducable results
    :return:            numpy array with specified dimensions filled with values
                        in the specified range
    :date:              16.06.2016
    :author:            Tim Elberfeld
    """
    octaves = 8             # parameter for the Perlin noise, this effects the maximum frequency in the surface
    startf = (3, 3)         # starting spatial frequency for x and y coordinates

    interptype = 'cubic'    # interpolation type can be changed, smoothest surfaces are generated using 'cubic'
    surface = perlin(imshape=shape,
                     octaves=octaves,
                     startfreq=startf,
                     valrange=valrange,
                     seed=seed,
                     interp=interptype)
    return surface


def perlin(imshape, octaves=8, startfreq=(3, 3), valrange=2*np.pi, seed=None, interp='linear'):
    """
    :brief:             perlin noise implementation
    :param imshape:     shape of the result array
    :param octaves:     number of octaves for the noise
    :param startfreq:   initial noise frequency
    :param valrange:    range of the values in the result array (will be shifted by half to include origin
    :param seed:        seed for the random number generator, optional
    :param interp:      type of interpolation, optional parameter.
                        can be 'linear', 'nearest' or 'cubic'. Default is 'linear'
    :return:            float array of shape imshape with values in range valrange
    :date:              15.06.2016
    :author:            Tim Elberfeld
    """
    cur_freq_y = startfreq[1]
    cur_freq_x = startfreq[0]

    if cur_freq_x < 2 or cur_freq_y < 2:
        raise ValueError('Frequency can not be lower than 2')

    cur_range = valrange
    increment = 2                               # must be an integer!

    result = np.zeros(imshape)

    for oc in range(octaves):
        if oc > 0:
            cur_freq_y *= increment
            cur_freq_x *= increment
            cur_range /= increment

        # generate random points according to the current frequency
        randval = gen_random_field((cur_freq_y, cur_freq_x), cur_range, 0, seed=seed)

        # linear interpolation on the random values
        x = np.linspace(0, 1, imshape[1])
        y = np.linspace(0, 1, imshape[0])
        target_grid_x, target_grid_y = np.meshgrid(x, y)

        points = np.transpose(np.asarray(np.where(randval)))
        points = points.astype(float)

        stepx = 1.0 / (cur_freq_x-1)
        points[:, 0] *= stepx

        stepy = 1.0 / (cur_freq_y - 1)
        points[:, 1] *= stepy

        rand_reshaped = np.reshape(randval, (randval.size, 1))
        cur_octave = np.squeeze(griddata(points, rand_reshaped, (target_grid_y, target_grid_x), method=interp))

        result = np.add(result, cur_octave)

        if seed is not None:
            seed /= increment                       # modify the seed to not get the same values every iteration

    result = np.squeeze(result)

    return gen.map_to_range(result, -valrange/2, valrange/2)


def gen_random_field(arrshape, sigma, mu, seed=None):
    """
    :brief:             generate a simple random field
    :param arrshape:    shape of the field
    :param sigma:       standard deviation of the values
    :param mu:          mean
    :param seed:        seed for rng, optional
    :return:            random field of given shape
    :date:              15.06.2016
    :author:            Tim Elberfeld
    """
    if seed is not None:
        rng.seed(seed)                             # initialize the random number generator

    randomfield = rng.normal(mu, sigma, arrshape)  # generate field with given sigma and mu that has same shape as input
    return randomfield


def add_random_noise(array, sigma, mu, seed=None):
    """
    :brief:         add random gaussian noise to an array
    :param array:   array that should be modified
    :param sigma:   standard deviation of the noise
    :param mu:      mean of the noise
    :param seed:    optional seed to be able to reproduce noise
    :return:        array with noise added
    :date:          15.06.2016
    :author:        Tim Elberfeld
    """
    if seed is not None:
        rng.seed(seed)                          # initialize the random number generator

    arrshape = array.shape
    noise = rng.normal(mu, sigma, arrshape)     # generate field with given sigma and mu that has same shape as input

    return array + noise


def random_residue_map(imsize, numres, balanced=False, seed=None):
    """
    :brief:             generate a random residueamp with specified number of residues
                        to test and debug balance behaviour
    :param imsize:      size of the map
    :param numres:      number of residues to put in
    :param balanced:    if True, the map will contain an even number of positive and negative residues
                        if False the map will be unbalanced
    :param seed:        seed for the random number generator. if left to be None, the normal initialization
                        of the rng will be used
    :return:            the residuemap
    :date:              20.06.2016
    :author:            Tim Elberfeld
    """
    if seed is not None:
        rng.seed(seed)

    if balanced != (numres % 2 == 0):
        numres += 1  # make sure there the number is odd when balanced is False and even when balanced is True

    residuemap = np.zeros(imsize)
    if numres >= np.sqrt(residuemap.size):  # this is an arbitrary limit!
        raise ValueError("number of residues should be < sqrt(residuemap.size)")

    plist = list()
    charge = -1                                 # start with negative charge, then toggle for each new residue
    for res in range(numres):
        py = rng.randint(0, imsize[0])
        px = rng.randint(0, imsize[1])
        gen.print_log("residue x: {0}, y: {1}".format(px, py))
        p = (py, px)

        if p not in plist:
            plist.append(p)                     # append current residue point to list
            residuemap[p] = charge
            charge = -charge                    # toggle charge for next residue

        else:
            res -= 1                            # try again, we don't want the same point twice

    customLog.debug("placed residues at {0}".format(plist))
    return residuemap


def gen_spiral_shear(shape, valrange, a):
    """
    :brief:             spiral shear example phase
    :param shape:       shape of the image
    :param valrange:    value range of the ramp in the spiral
    :param a:           parameter for the spiral (how many rotations the spiral does)
    :return:            example phase image
    :date:              05.07.2016
    :author:            Tim Elberfeld
    """
    result = np.zeros(shape)
    center = (shape[0] // 2, shape[1] // 2)     # center of the image
    prevpos1 = center                           # prevpos is to store the last painted position
    prevpos2 = center
    paint1 = True                               # the routine paints 2 spirals that are shifted by 180 deg
    paint2 = True                               # of one of the spirals hits the border, it will not be painted further

    color = 1.0                                 # color of the spiral

    for t in np.arange(0, 10*np.pi, 0.1):
        x1 = int(np.floor(center[1] + t * a * np.cos(t)))
        y1 = int(np.floor(center[0] + t * a * np.sin(t)))

        x2 = int(np.floor(center[1] + t * a * np.cos(t+np.pi)))
        y2 = int(np.floor(center[0] + t * a * np.sin(t+np.pi)))

        if paint1:
            if point_in_image(y1, x1, *result.shape):  # gen.pointinimage((y1, x1), result):
                linelist = bresenham(prevpos1[1], prevpos1[0], x1, y1)          # for better painting do line segments
                result[linelist] = color
                prevpos1 = (y1, x1)
            else:
                dist, px = get_nearest_border_pixel(result, prevpos1)
                linelist = bresenham(px[1], px[0], prevpos1[1], prevpos1[0])    # for better painting do line segments
                result[linelist] = color
                paint1 = False

        if paint2:
            if point_in_image(y2, x2, *result.shape):  # gen.pointinimage((y2, x2), result):
                linelist = bresenham(prevpos2[1], prevpos2[0], x2, y2)          # for better painting do line segments
                result[linelist] = color
                prevpos2 = (y2, x2)
            else:
                dist, px = get_nearest_border_pixel(result, prevpos2)
                linelist = bresenham(px[1], px[0], prevpos2[1], prevpos2[0])    # for better painting do line segments
                result[linelist] = color
                paint2 = False

        if not paint1 and not paint2:
            break                               # finish the loop prematurely if both spirals touch the wall

    # now fill one of the spiral arms with a ramp
    plist1, plist2 = gen.floodfill_list(result, (1, 1), bordercolor=color)
    maxval = valrange/2
    ramp = gen_ramp_phase(shape, -valrange/2, valrange/2, mode="single")
    result[plist1] = ramp[plist1]
    result[plist2] = (maxval - ramp)[plist2]

    return result


def get_wrapped_lena(valrange):
    """
    :brief:             example image with lena extended to valrange and then wrapped
    :param valrange:    new value range for lena (before wrapping)
    :return:            extended value image and wrapped version
    :date:              08.07.2016
    :author:            Tim Elberfeld
    """
    imgrgb = matimg.imread("lena.png")
    img = gen.rgb2gray(imgrgb)
    img_mapped = gen.map_to_range(img, -valrange/2, valrange/2)
    wrapped = gen.wrapping_operator(img_mapped)
    return img, wrapped


def gen_sample_img():
    """
    :brief:     Generation of sample images for testing of the C code. Will be saved as bytecode
    """
    numiter = 10
    imshape = [512, 512]
    radius = 10
    valrangemult = 4
    mu = 0.0
    sigma = 0.05
    seed = 1234
    # srcpath = "/Users/Tim/Dropbox/Uni/MasterThesis/test_data/"
    # img_ramp512 = datagen.gen_ramp_phase([512, 512], -(valrange / 2.0), valrange / 2.0, mode="double sheared")

    for x in range(numiter):
        """
        img_ramp = gen_ramp_phase(imshape, -(valrange / 2.0), valrange / 2.0, mode="double sheared")
        img_wrapped_ramp = gen.wrapping_operator(img_ramp)
        img_wrapped_ramp = gen.normalize(img_wrapped_ramp)
        save_as_bytecode(img_ramp, "ramp_{0}x{1}.surf".format(imshape[0], imshape[1]))
        save_as_bytecode(img_wrapped_ramp, "ramp_{0}x{1}.phase".format(imshape[0], imshape[1]))

        img_spiral = datagen.gen_spiral_shear(imshape, valrange, radius)
        img_wrapped_spiral = gen.wrapping_operator(img_spiral)
        img_wrapped_spiral = gen.normalize(img_wrapped_spiral)
        save_as_bytecode(img_spiral, "spiral_{0}x{1}.surf".format(imshape[0], imshape[1]))
        save_as_bytecode(img_wrapped_spiral, "spiral_{0}x{1}.phase".format(imshape[0], imshape[1]))
        """

        valrangestring = "{0}pi".format(int(valrangemult))
        img_perlin = gen_random_surface(imshape, np.pi * valrangemult, seed)
        img_perlin = add_random_noise(img_perlin, sigma, mu, seed=451267389406)
        img_wrapped_perlin = gen.wrapping_operator(img_perlin)
        img_wrapped_perlin = gen.normalize(img_wrapped_perlin)
        save_as_bytecode(img_perlin,
                         "perlinrange_{0}x{1}_rge{2}.surf".format(imshape[0], imshape[1], valrangestring))
        save_as_bytecode(img_wrapped_perlin,
                         "perlinrange_{0}x{1}_rge{2}.phase".format(imshape[0], imshape[1], valrangestring))
        customLog.debug("perlinrange_{0}x{1}_rge{2}".format(imshape[0], imshape[1], valrangestring))

        """
        img_ramp_noise = add_random_noise(img_ramp512, sigma, mu)
        img_wrapped_noise = gen.wrapping_operator(img_ramp_noise)
        img_wrapped_noise = gen.normalize(img_wrapped_noise)
        sigmastring = "{0}".format(sigma).replace(".", "_")
        save_as_bytecode(img_ramp_noise, "rampnoise_{0}x{1}_{2}.surf".format(512, 512, sigmastring))
        save_as_bytecode(img_wrapped_noise, "rampnoise_{0}x{1}_{2}.phase".format(512, 512, sigmastring))

        imshape[0] += 128
        imshape[1] += 128
        sigma += 0.02
        """
        valrangemult += 2
        radius += 10


def get_test_surface(imsize):
    smoothed = gen_ramp_phase(imsize, 0, 8*np.pi, mode="double sheared")
    wrapped = gen.wrapping_operator(smoothed)                      # wrap it to 2pi to simulate phase wrapping

    return wrapped, smoothed
