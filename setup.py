import os
import numpy as np
from distutils.core import setup
from distutils.extension import Extension
import Cython
from Cython.Distutils import build_ext
from stat import S_ISDIR
import warnings

join_path = os.path.join     # rename function


def search_source_files(path, extension=".c"):
    """
    :brief:             Search the working directory for a directory named "source".
                        Scan for the files in this directory and add all files with the ending
                        *.c to the list of source dependencies
    :param path:        the path to search
    :param extension:   extension of the files that should be searched for
    :return:            list of paths to the files that were found
    :date:              22.07.2016
    :author:            Tim Elberfeld
    """
    source_files = list()

    if dir_exists(path):
        for file in os.listdir(path):
            if file.endswith(extension):
                print "found file \"{0}\"".format(file)
                source_files.append(file)

    return [os.path.join(path, filename) for filename in source_files]


def dir_exists(path):
    """
    :brief:         check if the specified path is a directory
    :param path:    path to the directory
    :return:        True, if the path is a directory, False otherwise
    :date:          22.07.2016
    :author:        Tim Elberfeld
    """
    return S_ISDIR(os.stat(path).st_mode)


def delete_file(filename):
    """
    :brief:             delete a file
    :param filename:    path to the file
    :date:              05.08.2016
    :author:            Tim Elberfeld
    """
    if(os.path.isfile(filename)):
        print "Removing {0}".format(filename)
        os.remove(filename)


extra_compile_flags = [ '-Wno-unused-function',                 # disable warnings by cython
                        '-Wno-unused-variable',
                        '-O2']                                  # make sure we always compile with optimizations

if Cython.__version__ == '0.24':                                # cython version 0.24 uses deprecated numpy API
    msg = "Cython version {0} uses a deprecated Numpy API. Disabling all compiler warnings".format(Cython.__version__)
    warnings.warn(msg, UserWarning)
    extra_compile_flags.append("-w")                            # disable all warnings, only display errors

home_dir = os.path.dirname(os.path.abspath(__file__))   # setup.py dir
include_dirs = [join_path(home_dir, "include"),         # .h dir for own c functions
                np.get_include()]                       # .h dir for numpy

# delete_file(join_path(home_dir, "PhaseUnwrapper_C.so"))

source_path = join_path(home_dir, "source")
sources_c = search_source_files(source_path, extension=".c")    # find c files
sources_pyx = search_source_files(home_dir, extension=".pyx")   # find pyx files
sources = sources_c + sources_pyx

ext = Extension("PhaseUnwrapper_C",                             # module name
                sources=sources,                                # source files
                language="c",                                   # force compilation in c
                include_dirs=include_dirs,                      # header file directories
                extra_compile_args=extra_compile_flags)         # extra compiler flags

setup(
    name = "PhaseUnwrapper",  # project name, used for example when uninstalling with 'pip uninstall PhaseUnwrapper'
    ext_modules=[ext],
    cmdclass={'build_ext' : build_ext}
)
