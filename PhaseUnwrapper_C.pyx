cimport cython
cimport numpy as cnp

import math
import logger
import numpy as np
import general
import array
from itertools import izip
from collections import deque
from cpython cimport array
import logger

from PhaseUnwrapper_C cimport   MakeQualityMap, \
                                abs, \
                                GoldsteinUnwrap,\
                                ScanFileForNumBytes, \
                                ReadBytesPreAlloc_double, \
                                ReadBytesPreAlloc_float, \
                                ReadBytesPreAlloc_uint8, \
                                SaveAsByteCode

cdef unsigned char flag_pos_res = 0x01                         # 1st bit
cdef unsigned char flag_neg_res = 0x02                         # 2nd bit
cdef unsigned char flag_visited = 0x04                         # 3rd bit
cdef unsigned char flag_active = 0x08                          # 4th bit
cdef unsigned char flag_branch_cut = 0x10                      # 5th bit
cdef unsigned char flag_border = 0x20                          # 6th bit
cdef unsigned char flag_unwrapped = 0x40                       # 7th bit

cdef unsigned char flag_processed = (flag_visited | flag_active)
cdef unsigned char flag_neg_pos = (flag_neg_res | flag_pos_res)
cdef unsigned char flag_ignore = flag_border | flag_branch_cut | flag_unwrapped
cdef unsigned char flag_avoid = (flag_branch_cut | flag_border)

cdef double _PI = np.pi
cdef double _NPI = -np.pi
cdef double _TWOPI = 2*np.pi

quality_map_modes = {   "variance"  :   0x00,
                        "pseudocorr":   0x01,
                        "gradient"  :   0x02}

def unwrap_phase(phaseimg, algorithm='flynn', **kwargs):
    """
    :brief:             main function for phase unwrapping
                        The first two arguments are used for both implemented algorithms
                        The ones after that depend on the algorithm.
    :param phaseimg:    image containing the wrapped phase
    :param algorithm:   algorithm to use as a string. Options are
                        'goldstein', 'flynn'
                        String arguments are case insensitive!
    :param kwargs:      The rest of the arguments depend on the algorithm that was chosen.
                        'goldstein' requires:
                            remove_dipoles      - boolean argument, should dipoles be removed or not?
                            maxradius           - maximum search radius for the branch cut placement
                        'flynn'
                            quality_map_mode    - string specifying the quality map to use:
                                                    "variance"      - phase derivative variance map
                                                    "pseudocorr"    - pseudo correlation coefficient map
                                                    "gradient"      - maximum phase gradient map
                                                    "none"          - don't use a quality map
                            filter_size         - integer number to specify the quality map filter size
    :date:              28.07.2016
    :author:            Tim Elberfeld
    """

    # parse keyword arguments
    if algorithm.lower() == "goldstein":
        remove_dipoles = kwargs.get('remove_dipoles', True)
        maxradius = kwargs.get('maxradius', max(phaseimg.shape)/2)

        unwrapped = goldstein_unwrap(phaseimg, remove_dipoles, maxradius)

        return unwrapped
    elif algorithm.lower() == "flynn":
        mode = kwargs.get('quality_map_mode', "variance")
        print mode
        filter_size = kwargs.get('filter_size', 3)

        unwrapped = flynn_unwrap(phaseimg, mode, filter_size)
        return unwrapped
    else:
        raise ValueError("{0} is not a valid algorithm. Try \"goldstein\" or \"flynn\" ")


def goldstein_unwrap(phaseimg, remove_dipoles, maxradius):
    """
    :brief:                     unwrap the phase using goldsteins branch cut algorithm
    :param remove_dipoles:      boolean argument, should dipoles be removed or not?
    :param maxradius:           maximum search radius for the branch cut placement
    :date:                      28.07.2016
    :author:                    Tim Elberfeld
    """
    cdef:
        int nHeight = phaseimg.shape[0]
        int nWidth = phaseimg.shape[1]
        int nMaxCutLength = maxradius
        unsigned char bRemoveDipoles = remove_dipoles
        double[::1] pdSolution
        double[::1] pdPhase

    pdPhase = np.array(phaseimg.astype(np.float64), copy=True, dtype=np.float64, order='C').reshape(-1)
    pdSolution = np.zeros(shape=(phaseimg.size,), dtype=np.float64)

    GoldsteinUnwrap(&pdPhase[0], nWidth, nHeight,
                    nMaxCutLength, bRemoveDipoles, &pdSolution[0]);

    return np.asarray(pdSolution).reshape(phaseimg.shape)


def flynn_unwrap(phaseimg, quality_map_mode, filter_size):
    """
    :brief:                 wrapper function to call the C code to compute flynn phase unwrapping
    :param phaseimg:        the wrapped phase
    :param qualmode:        mode for the quality map
                            options are:
                                "variance"      : 0
                                "pseudocorr"    : 1
                                "gradient"      : 2
    :param filter_size:     size of the window for the computation of the variance
    :date:                  22.08.2016
    :author:                Tim Elberfeld
    """

    cdef:
        int nHeight = phaseimg.shape[0]
        int nWidth = phaseimg.shape[1]
        double[::1] pdQualityMap
        double[::1] pdSolution
        double[::1] pdPhase
        int mode = quality_map_modes[quality_map_mode] # the mode of the quality map

    pdPhase = np.array(phaseimg.astype(np.float64), copy=True, dtype=np.float64, order='C').reshape(-1)
    pdQualityMap = np.zeros(shape=(phaseimg.size,), dtype=np.float64)
    pdSolution = np.zeros(shape=(phaseimg.size,), dtype=np.float64)

    print "computing quality map {}".format(quality_map_mode)
    MakeQualityMap(&pdPhase[0], mode, nWidth, nHeight, filter_size, &pdQualityMap[0])

    print "starting unwrapping"
    FlynnUnwrap(&pdPhase[0], nWidth, nHeight, &pdQualityMap[0], &pdSolution[0])

    print "unwrapping finished"
    return np.asarray(pdSolution).reshape(phaseimg.shape)

	
def compute_quality_map_c(phaseimg, qualmode, k):
    """
    :brief:             wrapper function to call the C code to compute the quality map
    :param phaseimg:    the wrapped phase
    :param qualmode:    mode for the quality map
                        options are:
                            "variance"      : 0
                            "pseudocorr"    : 1
                            "gradient"      : 2
    :param k:           size of the window for the computation of the variance
    :date:              22.07.2016
    :author:            Tim Elberfeld
    """
    cdef int height = phaseimg.shape[0]
    cdef int width = phaseimg.shape[1]

    cdef:
        double[::1] phase_c
        double[::1] qual_map

    phase_c = np.array(phaseimg, copy=False, dtype=np.float64, order='C').reshape(-1)
    qual_map = np.zeros(shape=(phase_c.size,), dtype=np.float64)
    cdef int mode = quality_map_modes[qualmode]

    MakeQualityMap(&phase_c[0], mode, width, height, k, &qual_map[0]);

    return np.asarray(qual_map).reshape(phaseimg.shape)     # return as same shape as input phaseimg


def load_bytefile(file_path, dtype):
    """
    :brief:     load a bytefile -   opens a filedialog
    :date:      31.08.2016
    :author:    Tim Elberfeld
    """
    if(dtype == "uint8"):
        return read_uint8(file_path)

    elif(dtype == "float"):
       return read_float(file_path)

    elif(dtype == "double"):
        print("reading file {0}".format(file_path))
        return read_double(file_path)

    else:
        raise ValueError("invalid datatype {}".format(dtype))


def read_uint8(file_path):
    cdef int rnBytes = ScanFileForNumBytes(bytes(file_path)); # read number of bytes in the file
    cdef unsigned char[:] pArrayView = np.zeros((rnBytes, ), dtype=np.uint8)

    ReadBytesPreAlloc_uint8(bytes(file_path), &pArrayView[0], rnBytes)

    imshape = factor_int(rnBytes)
    return np.asarray(pArrayView).reshape(imshape)


def read_float(file_path):
    cdef int rnBytes = ScanFileForNumBytes(bytes(file_path)); # read number of bytes in the file
    cdef float[:] pArrayView = np.zeros((rnBytes//4, ), dtype=np.float32)

    ReadBytesPreAlloc_float(bytes(file_path), &pArrayView[0], rnBytes)

    imshape = factor_int(rnBytes//4)
    return np.asarray(pArrayView).reshape(imshape)


def read_double(file_path):
    cdef int rnBytes = ScanFileForNumBytes(bytes(file_path)); # read number of bytes in the file
    cdef double[:] pArrayView = np.zeros((rnBytes//8, ), dtype=np.float64)

    ReadBytesPreAlloc_double(bytes(file_path), &pArrayView[0], rnBytes)

    imshape = factor_int(rnBytes//8)
    return np.asarray(pArrayView).reshape(imshape)


def factor_int(n):
    """
    :brief:     factor an integer to something as close to a square, when regarding the factors as edge lengths
                This can be regarded as searching for a point (a, b) on a cartesian grid that is made up
                of only integer components and has the minimal distance to the point (sqrt(n), sqrt(n))
    :param n:   the integer to convert into nearly square factors
    :date:      31.08.2016
    :author:    Tim Elberfeld
    """
    nsqrt = math.ceil(math.sqrt(n))
    solution = False
    factors = [nsqrt, nsqrt]

    while True:
        factors[1] = int(n//factors[0])
        result = factors[0] * factors[1]
        if result == n:
            return factors
        else:
            factors[0] -= 1

def save_as_bytecode(data, file_path):
    print "saving {0} as bytecode".format(file_path)

    cdef double[::1] pdArray
    cdef void* pdVoidArray

    pdArray = np.array(data, copy=True, dtype=np.float64, order='C').reshape(-1)

    pdVoidArray = <void*>&pdArray[0]

    SaveAsByteCode(pdVoidArray, data.size, data.size*8, bytes(file_path));


class GotoException(Exception):
    pass


def bresenham(int x, int y, int x2, int y2):
    cdef int steep = 0
    cdef int dx = abs(x2 - x)
    cdef int dy = abs(y2 - y)
    cdef int sx, sy, d, i
    coordsx = []
    coordsy = []
    if (x2 - x) > 0:
        sx = 1
    else:
        sx = -1

    if (y2 - y) > 0:
        sy = 1
    else:
        sy = -1

    if dy > dx:
        steep = 1
        x,y = y,x
        dx,dy = dy,dx
        sx,sy = sy,sx

    d = (2 * dy) - dx
    for i in range(dx):
        if steep:
            coordsx.append(y)
            coordsy.append(x)
        else:
            coordsx.append(x)
            coordsy.append(y)

        while d >= 0:
            y = y + sy
            d = d - (2 * dx)

        x = x + sx
        d = d + (2 * dy)

    coordsx.append(x2)
    coordsy.append(y2)
    return (np.asarray(coordsy, dtype=np.int32), np.asarray(coordsx, dtype=np.int32))


def bresenham_k(int x, int y, int x2, int y2, width):
    cdef int steep = 0
    cdef int dx = abs(x2 - x)
    cdef int dy = abs(y2 - y)
    cdef int sx, sy, d, i
    coords = []
    if (x2 - x) > 0:
        sx = 1
    else:
        sx = -1

    if (y2 - y) > 0:
        sy = 1
    else:
        sy = -1

    if dy > dx:
        steep = 1
        x,y = y,x
        dx,dy = dy,dx
        sx,sy = sy,sx

    d = (2 * dy) - dx
    for i in range(dx):
        if steep:
            k = x * width + y
            #coordsx.append(y)
            #coordsy.append(x)
        else:
            k = y * width + x
            #coordsx.append(x)
            #coordsy.append(y)

        coords.append(k)
        while d >= 0:
            y = y + sy
            d = d - (2 * dx)

        x = x + sx
        d = d + (2 * dy)

    coords.append(y2 * width + x2)
    return (np.asarray(coords))


def floodfill(img, x, y, bordercolor, fillcolor):
    """
    :brief:             simplest possible recursive floodfill
    :param img:         image containing the shape
    :param x:           x of start position
    :param y:           y of start position
    :param bordercolor: color that is a border, so that we know when to stop
    :param fillcolor:   color to fill the image with
    :return:            filled image
    :date:              06.07.2016
    :author:            Tim Elberfeld
    """
    height, width = img.shape
    retimg = np.zeros_like(img)

    to_fill = set()
    to_fill.add((x,y))

    while len(to_fill):
        (x,y) = to_fill.pop()

        if x > 0 and x < width and y > 0 and y < height:
            pxval = img[y, x]
            if pxval != bordercolor and retimg[y, x] != fillcolor:
                retimg[y,x] = fillcolor
                to_fill.add((x - 1, y))
                to_fill.add((x + 1, y))
                to_fill.add((x,     y - 1))
                to_fill.add((x,     y + 1))

    return retimg


def point_in_image(x, y, height, width):
    if x < 0 or y < 0 or x >= width or y >= height:
        return False
    else:
        return True


def test_flag(value, flags):
    """
    :brief:         test if in a value a flag is set
    :param value:   value to test
    :param flags:   flags to test for
    :return:        True if flags are set
    :date:          19.06.2016
    :author:        Tim Elberfeld
    """
    return (value & flags) != 0


def get_nearest_border_pixel(img, pos):
    """
    :brief:         search for the nearest border pixel of a position and
                    return the distance to that point as well as the point
    :param img:     array like (numpy array). Only used for img.shape
    :param pos:     position to test
    :return:        distance and position, in this order!
    :date:          19.06.2016
    :author:        Tim Elberfeld
    """
    height, width = img.shape                   # BUG_TE_08072016: swapped height and width

    xidx = 1
    yidx = 0

    p0 = 0, pos[xidx]                           # ┌── p0 ───┐
    p1 = (height-1), pos[xidx]                  # p3 ─┼──── p2
    p2 = pos[yidx], (width-1)                   # │   │     │
    p3 = pos[yidx], 0                           # └── p1 ───┘

    if pos[xidx] < width / 2:                   # choices are p0, p1, p3 -> left half
        if pos[yidx] < height / 2:              # p0 or p3 -> upper left quarter
            if pos[xidx] < pos[yidx]:
                retdist = pos[xidx]             # distance to p3
                retpos = p3
            else:
                retdist = pos[yidx]             # distance to p0
                retpos = p0
        else:                                   # p3 or p1 -> lower left quarter
            dist1 = height - pos[yidx]          # distance to p1
            if pos[xidx] < dist1:
                retdist = pos[xidx]             # distance to p3
                retpos = p3
            else:
                retdist = dist1
                retpos = p1
    else:                                       # choices are p0, p1, p2 -> right half
        dist0 = width - pos[xidx]               # distance to p2

        if pos[yidx] < height / 2:              # p0 or p2 -> upper right quarter
            if dist0 < pos[yidx]:
                retdist = dist0
                retpos = p2
            else:
                retdist = pos[yidx]             # distance to p0
                retpos = p0
        else:                                   # p1 or p2 -> lower right quarter
            dist1 = height - pos[yidx]          # distance to p1
            if dist0 < dist1:
                retdist = dist0
                retpos = p2
            else:
                retdist = dist1
                retpos = p1

    return retdist, retpos


def looped_branch_cuts_c(residuemap, maxradius, numres, initialcutmap=None):
    """
    :brief:                 direct translation of the C code from Ghiglia et al. -> now ported to cython
    :param residuemap:      image containing the residues
    :param maxradius:       maximum search radius (and also maximum branch cut length if multiplied by sqrt(2)
    :param numres:          number of resiudes in the residuemap
    :param initialcutmap:   if there were dipoles, the result of the removal can be plugged in here. If left empty,
                            the map is constructed from scratch
    :return:                array like with same dimensions as residuemap containing the flag_branch_cut value
    :date:                  07.07.2016
    :author:                Tim Elberfeld
    """
    print("starting looped branch cut algorithm using c language")

    cdef int charge = 0
    cdef int max_active = numres + 15
    cdef int counter = 0
    cdef float thresh = 0.05
    cdef float step = 0.05
    cdef int yy = 0
    cdef int xx = 0
    cdef int k = 0
    cdef char pxl = 0
    cdef float progress = 0
    cdef int boxctr_i = 0
    cdef int boxctr_j = 0
    cdef char boxpxl = 0
    cdef int wk = 0

    imshape = residuemap.shape
    height, width = imshape

    print("initializing flag values")
    flags = np.zeros_like(residuemap, dtype=np.uint8)
    where_positive = np.where(residuemap == 1)
    where_negative = np.where(residuemap == -1)

    flags[where_positive] |= flag_pos_res
    flags[where_negative] |= flag_neg_res

    if initialcutmap is not None:
        flags[np.where(initialcutmap != 0)] |= flag_branch_cut
        print("filling preprocessed cutmap values")

    flags = np.reshape(flags, (residuemap.size, 1))
    active_indices = np.zeros(max_active, dtype=int)

    window_range = xrange(3, 2*maxradius, 2)      # 2 * radius  TE_27062016_BUG

    for yy in range(height):
        for xx in range(width):
            pxl = flags[k]
            counter += 1
            progress = counter / float(width*height)

            if progress >= thresh:
                print("Progress {0:05.2f}%".format(progress * 100))

                thresh += step
                if thresh > 1.0:
                    thresh = 1.0

            if (pxl & flag_neg_pos) != 0 and (pxl & flag_visited) == 0:
                flags[k] |= flag_processed

                if (pxl & flag_pos_res) != 0:
                    charge = 1
                elif (pxl & flag_neg_res) != 0:
                    charge = -1

                print("residue with charge {0} found at x{1}, y{2}".format(charge, xx, yy))
                num_active = 0

                print("num_active = {0}".format(num_active))
                active_indices[num_active] = k
                num_active += 1

                if num_active > max_active:
                    num_active = (max_active - 1)

                print("starting radius search")

                for w in window_range:
                    try:
                        searched = 0        # for keeping track of how many pixels we searched

                        for ka in range(num_active):
                            boxctr_i = active_indices[ka] % width
                            boxctr_j = active_indices[ka] // width  # integer division!
                            center = (boxctr_j, boxctr_i)

                            border_coordinates = general.border_offset_list(w, center=center, imshape=imshape)

                            xcoord = (border_coordinates[1])
                            ycoord = (border_coordinates[0])

                            for ii, jj in izip(xcoord, ycoord):     # loop over the "new" pixels with box edge lenght w

                                if jj < 0 or jj >= height or ii < 0 or ii >= width:
                                    print("pixel outside image encountered x {0}, y {1}".format(ii, jj))
                                    continue
                                else:
                                    wk = jj * width + ii
                                    boxpxl = flags[wk]

                                    if ii == 0 or ii == width-1 or jj == 0 or jj == height-1:
                                        print("border pixel found after {0} pixels, placing cut".format(searched))
                                        charge = 0
                                        dist, border = get_nearest_border_pixel(residuemap, center)

                                        print("placing cut from {0} to {1}".format(border, center))
                                        line_coords = bresenham_k(  border[1],
                                                                    border[0],
                                                                    center[1],
                                                                    center[0],
                                                                    width)
                                        flags[line_coords] |= flag_branch_cut

                                    elif (boxpxl & flag_neg_pos) != 0 and (boxpxl & flag_active) == 0:
                                        if (boxpxl & flag_visited) == 0:
                                            if (boxpxl & flag_pos_res) != 0:
                                                charge += 1
                                            else:
                                                charge += -1

                                            msg = "new residue found at {0}, {1}  after {2}px - new charge = {3}"\
                                                .format(ii, jj, searched, charge)
                                            print(msg)
                                            flags[wk] |= flag_visited  # we don't have to visit this pixel again

                                        active_indices[num_active] = wk

                                        num_active += 1
                                        if num_active > max_active:
                                            num_active = (max_active - 1)

                                        flags[wk] |= flag_active    # set pixel active
                                        pos = (jj, ii)

                                        print("placing cut from {0} to {1}".format(pos, center))
                                        line_coords = bresenham_k(  pos[1],
                                                                    pos[0],
                                                                    center[1],
                                                                    center[0],
                                                                    width)
                                        flags[line_coords] |= flag_branch_cut

                                    if charge == 0:
                                        """ This is NOT a good way to program but necessary
                                            to copy the C code identically!
                                            In the original code, the goto keyword is used """
                                        raise GotoException('jump outside loop')

                                # endelse (if jj < 0 or jj >= height or ii < 0 or ii >= width)
                            # endfor ii, jj in izip(xcoord, ycoord)
                        # endfor (ka in range(num_active))
                    except GotoException as e:
                        print("{0}: charge is balanced".format(e.message))
                        break           # jump to next loop iteration without going into the code below
                # endfor (w in np.arange(start=3, stop=maxradius, step=2))

                if charge != 0:                 # connect branch cuts to border
                    print("loop finished but charge unbalanced - searching nearest border pixel for last cut")
                    min_dist = width + height   # large value
                    nearestpxl = (-1, -1)
                    borderpxl = (-1, -1)

                    for ka in range(num_active):
                        ii = active_indices[ka] % width
                        jj = active_indices[ka] / width
                        dist, nearestborder = get_nearest_border_pixel(residuemap, (ii, jj))

                        if dist < min_dist:
                            min_dist = dist
                            nearestpxl = (ii, jj)
                            borderpxl = nearestborder

                    print("placing cut from {0} to {1}".format(nearestpxl, borderpxl))
                    line_coords = bresenham_k(  nearestpxl[1],
                                                nearestpxl[0],
                                                borderpxl[1],
                                                borderpxl[0],
                                                width)  # this calls the cython function!
                    flags[line_coords] |= flag_branch_cut

                print("resetting all active pixels")
                where_active = active_indices[np.where(active_indices != 0)]
                flags[where_active] &= ~flag_active     # unset the active flag
                active_indices[0:num_active] = 0        # clear the list of active pixels
                print("continue searching")

            # endif (test_flag(pxl, flag_neg_res) or test_flag(pxl,flag_pos_res)) and not test_flag(pxl, flag_visited):
            k += 1
        # endfor xx in range(width):
    # endfor yy in range(height):

    print("search finished")
    flags = np.reshape(flags, residuemap.shape)     # reshape flag array into its original shape again
    return flags


def unwrap_pixel_c(phaseimg, solution, sourcepos, neighborpos, width):
    """
    :brief:             unwrap the pixel value based on previous estimations
    :param phaseimg:    the original, wrapped phase
    :param solution:    the solution array, that stores all the pixels that have been unwrapped so far
    :param sourcepos:   the position that was the source for the method to be called
    :param neighborpos: neighbor of sourcepos that will be unwrapped now
    :param width:       width of the array. This is needed to transform 2d coordinates into a linear index
    :return:            the new solution array, filled with the unwrapped value for neighborpos
    :date:              05.07.2016
    :author:            Tim Elberfeld
    """
    cdef int k = neighborpos[0] * width + neighborpos[1]
    cdef int l = sourcepos[0] * width + sourcepos[1]

    cdef double grad = phaseimg[neighborpos] - phaseimg[sourcepos]

    if grad > _PI:
        grad -= (_TWOPI)
    if grad < _NPI:
        grad += (_TWOPI)

    if k < l:                              # decide which sign the gradient must have
        solution[neighborpos] = solution[sourcepos] - grad
    else:
        solution[neighborpos] = solution[sourcepos] + grad


def integrate_phase_c(phaseimg, where_branch):
    """
    :brief:                 integrate the phase along a path that does not cross branch cuts
    :param phaseimg:        original phase image
    :param where_branch:    coordinate list of branch cuts. Must be formatted like the return value of np.where()
    :return:                unwrapped phase
    :date:                  17.06.2016
    :author:                Tim Elberfeld
    """
    print("setting up flag array")

    result = np.zeros_like(phaseimg) # phaseimg.copy()
    imshape = phaseimg.shape
    height, width = imshape

    cdef size_t kk = 0

    flags = np.zeros_like(phaseimg, dtype=np.uint8)  # if flags is passed as None, we do not have branch cuts
    if where_branch is not None:
        flags[where_branch] |= flag_branch_cut

    general.set_border(flags, flag_border, isflag=True)

    print("unwrapping pixels")
    cdef int counter = 0
    cdef int to_go = height*width

    cdef int step = 5
    cdef float thresh = 0.0
    neighborhood = general.get_neighborhood((0,0))  # offsets for the 4 neighborhood

    seedpos = [1, 1]                            # find first wrapped position that is neither branch cut nor border

    cdef int increaseaxis = 0
    while (flags[seedpos[0], seedpos[1]] & flag_ignore) != 0:  # while the pixel is invalid, increase the position
        counter += 1
        seedpos[increaseaxis] += 1

        if seedpos[increaseaxis] > imshape[increaseaxis]:
            seedpos[increaseaxis] = 0
            increaseaxis = (not increaseaxis)

        if counter > to_go:
            raise IndexError("Tested all indices and found no pixels to unwrap")

    pdeque = deque()            # use deque because it has a constant time pop() and append()
    append = pdeque.append
    pop = pdeque.pop

    cdef size_t yy = 1
    cdef size_t xx = 1
    cdef float progress = 0.0

    for kk in xrange(width + 1, (height - 1) * (width - 1)):              # this is essentially a region labelling algorithm
        yy = kk % width
        xx = kk // width

        if (flags[yy, xx] & flag_ignore) == 0:  # enter the "fillling" algorithm if we found a valid pixel
            # update_list_c(phaseimg, result, flags, pdeque, neighborhood)
            curpos = (yy, xx)            # first point in the deque is the seed position

            for p in neighborhood:
                npos = (p[0] + curpos[0], p[1] + curpos[1])

                if (flags[npos] & flag_ignore) == 0:
                    unwrap_pixel_c(phaseimg, result, curpos, npos, width)
                    append(npos)
                    flags[npos] |= flag_unwrapped

            while pdeque:
                progress = 100 * (counter / float(to_go))

                #update_list_c(phaseimg, result, flags, pdeque, neighborhood)
                curpos = pop()

                for p in neighborhood:
                    npos = (p[0] + curpos[0], p[1] + curpos[1])
                    if (flags[npos] & flag_ignore) == 0:
                        unwrap_pixel_c(phaseimg, result, curpos, npos, width)
                        append(npos)
                        flags[npos] |= flag_unwrapped

                counter += 1

                if progress >= thresh:
                    print("Progress {0:05.2f}%".format(progress))

                    thresh += step
                    if thresh > 100.0:
                        thresh = 100.0

    if where_branch is not None:
        print("unwrapping branch cuts")

        for jj, ii in izip(*where_branch):          # unwrap the branch cut pixels
            curpos = (jj, ii)
            neighborpos = unwrapped_neighbor_c(curpos, flags, height, width)  # get unwrapped neighbor

            if neighborpos is not None:
                unwrap_pixel_c(phaseimg, result, curpos, neighborpos, width)

    result = result[1:-1, 1:-1] # cut the borders
    print("finished unwrapping")
    return result


def unwrapped_neighbor_c(pos, flags, height, width):
    """
    :brief:         check a position for unwrapped neighbor pixels
    :param pos:     position of the pixel which neighbors should be checked
    :param flags:   memory view to the flag values
    :return:        the FIRST neighbor position that has flag_unwrapped set.
                    Starting with the lowest y value going up to the highest.
                    If there is no such neighbor, None is returned
    :date:          29.06.2016
    :author:        Tim Elberfeld
    """
    plist = general.get_neighborhood(pos)

    for p in plist:
        if point_in_image(p[1], p[0], height, width):
            if (flags[p] & flag_unwrapped) != 0:
                return p
    return None