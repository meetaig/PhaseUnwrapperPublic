#include "imageprocessing.h"
/**
 @file      imageprocessing.c
 @author    Tim Elberfeld
 @date      27.07.2016
 @brief     collection of image processsing functions
 */

/**
 @brief             Simple breadth first non-recursive floodfill
 @param[inout]      pcImg           -   image containing the shape
 @param[in]         nDimension      -   height and width of the image
 @param[in]         ptSeed          -   seed/starting point
 @param[in]         cBorderColor    -   color that is a border, so that we know when to stop filling
 @param[in]         cFillColor:     -   color to fill the image with
 @date              06.07.2016
 @author            Tim Elberfeld
 */
void FloodFill(uint8* pcImg, ArrayDim nDimension, Point ptSeed, uint8 cBorderColor, uint8 cFillColor)
{
    Stack* ToFill = NewStack((nDimension.w * nDimension.h) / 2);
    int32 nIndex = ptSeed.y * nDimension.w + ptSeed.x;
    PushStack(&ToFill, nIndex);
    
    while(ToFill->size > 0)
    {
        nIndex = PopStack(ToFill);
        
        if(IsValidIndex(nIndex, nDimension))
        {
            if(pcImg[nIndex] != cBorderColor && pcImg[nIndex] != cFillColor)
            {
                pcImg[nIndex] = cFillColor;
                PushStack(&ToFill, nIndex - 1);             /* left */
                PushStack(&ToFill, nIndex + 1);             /* right */
                PushStack(&ToFill, nIndex - nDimension.w);  /* up */
                PushStack(&ToFill, nIndex + nDimension.w);  /* down */
            }
        }
    }

    FreeStack(ToFill);
}

/**
 @brief             Same as FloodFill() but returns the list of indices that are to be filled
 @param[inout]      pcImg           -   image containing the shape
 @param[in]         nDimension      -   height and width of the image
 @param[in]         ptSeed          -   seed/starting point
 @param[in]         cBorderColor    -   color that is a border, so that we know when to stop filling
 @return            <Stack*>        -   pointer to Stack structure containing the indices of the pixels to be filled
 @date              06.07.2016
 @author            Tim Elberfeld
 */
Stack* FloodFillList(uint8* pcImg, ArrayDim nDimension, Point ptSeed, uint8 cBorderColor)
{
    Stack* ToFill = NewStack((nDimension.w * nDimension.h) / 2);
    Stack* ReturnStack = NewStack((nDimension.w * nDimension.h) / 2);
    int32 nIndex = ptSeed.y * nDimension.w + ptSeed.x;
    PushStack(&ToFill, nIndex);
    
    while(ToFill->size > 0)
    {
        nIndex = PopStack(ToFill);
        
        if(IsValidIndex(nIndex, nDimension))
        {
            if(pcImg[nIndex] != cBorderColor)
            {
                PushStack(&ReturnStack, nIndex);            /* instead of filling, append to stack */
                PushStack(&ToFill, nIndex - 1);             /* left */
                PushStack(&ToFill, nIndex + 1);             /* right */
                PushStack(&ToFill, nIndex - nDimension.w);  /* up */
                PushStack(&ToFill, nIndex + nDimension.w);  /* down */
            }
        }
    }
    FreeStack(ToFill);
    return ReturnStack;
}

/**
@brief          Naive implementation of a 1D median filter.
@note           A better alternative would be the algorithm described in: <br>
                Simon Perreault and Patrick Hebert, "Median Filtering in Constant Time"
                https://nomis80.org/ctmf.pdf <br>
                But this takes too much time now...
@param[in]      pImg        -   the image to filter
@param[in]      nDimension  -   height and width of the image
@param[in]      nDirection  -   should the filter work in x or y direction?
@param[inout]   pResult     -   result image with same size as original
@date           02.09.2016
@author         Tim Elberfeld
*/
void MedianFilter1D(uint8* pImg, ArrayDim nDimension, Direction nDirection, uint8* pResult)
{
    int32 a, b, c;
    int32 median = 0;
    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {
            if(nDirection == DIR_X)
            {
                a = ((xx-1) < 0 || (xx-1)>= nDimension.w)   ? -1 : pImg[yy * nDimension.w + xx - 1];
                b = (xx < 0     ||  xx >= nDimension.w)     ? -1 : pImg[yy * nDimension.w + xx];
                c = ((xx+1) < 0 || (xx+1) >= nDimension.w)  ? -1 : pImg[yy * nDimension.w + xx + 1];
                median = MedianOfThree(a, b, c);
            }
            else if(nDirection == DIR_Y)
            {
                a = ((yy-1) < 0 || (xx-1)>= nDimension.w)   ? -1 : pImg[yy * nDimension.w + xx - 1];
                b = (yy < 0     ||  xx >= nDimension.w)     ? -1 : pImg[yy * nDimension.w + xx];
                c = ((yy+1) < 0 || (xx+1) >= nDimension.w)  ? -1 : pImg[yy * nDimension.w + xx + 1];
                median = MedianOfThree(a, b, c);
            }
            else
            {
                QuitWithError("Direction not supported", ERR_INVALID_PARAM);
            }
            
            pResult[yy * nDimension.w + xx] = (unsigned char) median;
        }
    }
}

/**
@brief          Compute 2d gradient filter in x or y direction.
@param[in]		pdArray		-	wrapped phase array
@param[in]		nDimension	-	width and height of the array
@param[in] 		nDirection	-	direction of the filter. See enum Direction
@param[inout]	pdGradient	-	array to store the filter result. Will not be allocated in the function!
@date			25.07.2016
@author			Tim Elberfeld
*/
void GradientFilter(double* pdArray, ArrayDim nDimension, Direction nDirection, bool bIsWrapped, double* pdGradient)
{
	double dGradVal = 0;
    int32 nIndex1 = 0;
    	
    for(int32 yy = 0; yy < nDimension.h; yy++)
	{
		for(int32 xx = 0; xx < nDimension.w; xx++)
		{
			nIndex1 = yy * nDimension.w + xx;

      		if(nDirection == DIR_X)				/* gradient filter in x direction (horizontal) */
      		{
                if (xx < (nDimension.w - 1))
                {    
                    dGradVal = GradientNormalized(pdArray[nIndex1+1], pdArray[nIndex1], 1);
                }
                else 
                {
                    dGradVal = GradientNormalized(pdArray[nIndex1-1], pdArray[nIndex1], 1);
                }

          	}
      		else if(nDirection == DIR_Y)		/* gradient filter in y direction (vertical) */
      		{
                if (yy < (nDimension.h - 1))
                {
                    dGradVal = GradientNormalized(pdArray[nIndex1 + nDimension.w], pdArray[nIndex1], 1);
                }
                else 
                {
                    dGradVal = GradientNormalized(pdArray[nIndex1 - nDimension.w], pdArray[nIndex1], 1);
                }

          	}
      		else								/* invalid direction */
      		{
      			QuitWithError("GradientFilter() - Direction parameter invalid!\n", ERR_INVALID_PARAM);
      		}
            
      		pdGradient[nIndex1] = dGradVal;
		}	
	}
}

/**
@brief			Compute the variance filter of an array
@param[in]		pdArray		-	the original array to apply the filter on
@param[in]      nDimension  -   width and height of the array
@param[in]      nBoxSize    -   the size of the window for the variance filter (nBoxSize x nBoxSize)
@param[in]      bIsAdditive -   If the pdVariance array is already filled with information, this parameter can be set to 
                                TRUE (1) and the new values will be added to the array instead of overwriting them.
@param[inout]   pdVariance  -	array to hold the result of the filter operation
@date           25.07.2016 
@author         Tim Elberfeld
*/
void VarianceFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdVariance)
{
	int32 nBoxIndex;
  	double  dResult, dAverage, dAvgSqr;
  	int32 nNumel = nDimension.w * nDimension.h;
    double dVariance;

  	if(nBoxSize < 3)							/* arbitrary minimum box size (better for quality!) */
  	{
        WriteToLogFile(Bytes(LOGBUFF), "nBoxSize %d too small,setting to minimum value 3", nBoxSize);
        nBoxSize = 3;
  	}

	int32 nHalfBox = nBoxSize / 2;				/* integer division! */
	int32 nBoxPixels = nBoxSize * nBoxSize;		/* number of pixels in the box */
	double dOneOverN = Reciprocal((double)nBoxPixels);

  	for (int32 yy = 0; yy < nDimension.h; yy++)
  	{
    	for (int32 xx = 0; xx < nDimension.w; xx++)
    	{
     		dAverage = dAvgSqr = 0.0;
      		
      		for (int32 bb = yy - nHalfBox; bb <= yy + nHalfBox; bb++)
      		{	
				for (int32 aa = xx - nHalfBox; aa <= xx + nHalfBox; aa++)
          		{
                    nBoxIndex = WrapIndex2D(aa, bb, nDimension); /*  wbb * nDimension.w + waa; */

                    if(nBoxIndex >= 0 && nBoxIndex < nNumel)
                    {
                        dResult = pdArray[nBoxIndex];
                        dAverage += dResult;        /* update the average ... */
                        dAvgSqr += dResult*dResult; /* ... and the squared average */
                    }
                    else
                    {
                        QuitWithError("Index out of bounds!", ERR_INDEX_OUT_OF_BOUNDS); /* memory access error */
                    }
        		}
          	}
	      	
    	  	dAverage *= dOneOverN;
      		dAvgSqr *= dOneOverN;
            dVariance = dAvgSqr - dAverage * dAverage;

      		if (bIsAdditive)					/* compute actual variance. Can be either added to array or overwritten! */
        	{
        		pdVariance[yy * nDimension.w + xx] += dVariance;
        	}	
      		else
      		{
        		pdVariance[yy * nDimension.w + xx] = dVariance;
        	}
    	}
  	}   
}

/**
@brief          Compute the maximum filter of an array
@param[in]      pdArray     -   the original array to apply the filter on
@param[in]      nDimension  -   width and height of the array
@param[in]      nBoxSize    -   the size of the window for the variance filter (nBoxSize x nBoxSize)
@param[in]      bIsAdditive -   If the @p pdMaximum array is already filled with information, this parameter can be set to
                                @c TRUE (1) and the new values will be added to the array instead of overwriting them.
@param[inout]   pdMaximum   -   array to hold the result of the filter operation
@date           25.07.2016 
@author         Tim Elberfeld
*/
void MaximumFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdMaximum)
{
    int32 nHeight = nDimension.h;
    int32 nWidth = nDimension.w;
    double dCurVal;
    double dMaxVal;
    int32 nHalfBox = nBoxSize / 2;              /* integer division! */
    int32 nNumel = nWidth * nHeight;
    int32 nBoxIndex;

    if (nBoxSize < 3)                           /* just simply add the arrays or copy */
    {
        if(bIsAdditive)
        {
            for (int ii=0; ii < nNumel; ii++)
            {
                pdMaximum[ii] = pdMaximum[ii] + pdArray[ii];
            }
        }
        else
        {
            memcpy(pdMaximum, pdArray, nNumel*sizeof(double));
        }
    }

    for (int32 yy = 0; yy < nHeight; yy++)
    {
        for (int32 xx = 0; xx < nWidth; xx++)
        {            
            dMaxVal = 0.0;
            for (int32 bb = yy - nHalfBox; bb <= yy + nHalfBox; bb++)
            {   
                if( (bb >= 0) &&
                    (bb < nHeight))
                {
                    for (int32 aa = xx - nHalfBox; aa <= xx + nHalfBox; aa++)
                    {
                        if( (aa >= 0) &&        /* maximum can't be wrapped. This would produce artifacts */
                            (aa < nWidth))
                        {
                            nBoxIndex = bb * nWidth + aa;
                            
                            if(nBoxIndex < nNumel)
                            {
                                dCurVal = fabs(pdArray[nBoxIndex]);
                                dMaxVal = (dMaxVal < dCurVal) ? dCurVal : dMaxVal;
                            }
                            else
                            {
                                QuitWithError("Index out of bounds!", ERR_INDEX_OUT_OF_BOUNDS);  /* memory access error */
                            }
                        }
                    }    
                }
            }

            if(bIsAdditive)
            {
                pdMaximum[yy * nWidth + xx] += dMaxVal;
            }   
            else
            {
                pdMaximum[yy * nWidth + xx] = dMaxVal;
            }
        }
    }
}


/**
@brief          Compute the average filter of an array
@param[in]      pdArray     -   the original array to apply the filter on
@param[in]      nDimension  -   width and height of the array
@param[in]      nBoxSize    -   the size of the window for the average filter (nBoxSize x nBoxSize)
@param[in]      bIsAdditive -   If the @p pdAverage array is already filled with information, this parameter can be set to
                                @c TRUE (1) and the new values will be added to the array instead of overwriting them.
@param[inout]   pdAverage   -   array to hold the result of the filter operation
@date           25.07.2016 
@author         Tim Elberfeld
*/
void AverageFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdAverage)
{
    double dCurVal;
    double dAvg;
    int32 nHalfBox = nBoxSize / 2;                    /* integer division! */
    int32 nBoxPixels = nBoxSize * nBoxSize;       /* number of pixels in the box */
    double dOneOverN = Reciprocal((double)nBoxPixels);
    int32 nNumel = nDimension.w * nDimension.h;
    int32 nBoxIndex;

    if (nBoxPixels < 3)                         /* just simply add the arrays or copy */
    {
        WriteToLogFile(Bytes(LOGBUFF), "nBoxSize %d too small,setting to minimum value 3", nBoxSize);
        nBoxSize = 3;
    }

    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {            
            dAvg = 0.0;
            for (int32 bb = yy - nHalfBox; bb <= yy + nHalfBox; bb++)
            {   
                for (int32 aa = xx - nHalfBox; aa <= xx + nHalfBox; aa++)
                {
                    nBoxIndex = WrapIndex2D(aa, bb, nDimension); /*  wbb * nDimension.w + waa; */
                    if (nBoxIndex < nNumel)
                    {
                        dCurVal = pdArray[nBoxIndex];
                        dAvg += dCurVal;
                    }
                    else
                    {
                        QuitWithError("Index out of bounds!", ERR_INDEX_OUT_OF_BOUNDS); /* memory access error */
                    }
                }
            }
            dAvg *= dOneOverN;

            if(bIsAdditive)
            {
                pdAverage[yy * nDimension.w + xx] += dAvg;
            }   
            else
            {
                pdAverage[yy * nDimension.w + xx] = dAvg;
            }
        }
    }
}

/**
@brief          Compute the squared average filter of an array
@param[in]      pdArray     -   the original array to apply the filter on
@param[in]      nDimension  -   width and height of the array
@param[in]      nBoxSize    -   the size of the window for the squared variance filter (nBoxSize x nBoxSize)
@param[in]      bIsAdditive -   If the @p pdAverage array is already filled with information, this parameter can be set to
                                @c TRUE (1) and the new values will be added to the array instead of overwriting them.
@param[inout]   pdAverage   -   array to hold the result of the filter operation
@date           25.07.2016 
@author         Tim Elberfeld
*/
void SquaredAverageFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdAverage)
{
    double dCurVal;
    double dAvg;
    int32 nHalfBox = nBoxSize / 2;                    /* integer division! */
    int32 nBoxPixels = nBoxSize * nBoxSize;       /* number of pixels in the box */
    double dOneOverN = Reciprocal((double)nBoxPixels);
    int32 nNumel = nDimension.w * nDimension.h;
    int32 nBoxIndex;
    
    if (nBoxPixels < 3)                         /* just simply add the arrays or copy */
    {
        WriteToLogFile(Bytes(LOGBUFF), "nBoxSize %d too small,setting to minimum value 3", nBoxSize);
        nBoxSize = 3;
    }

    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {            
            dAvg = 0.0;

            for (int32 bb = yy - nHalfBox; bb <= yy + nHalfBox; bb++)
            {   
                for (int32 aa = xx - nHalfBox; aa <= xx + nHalfBox; aa++)
                {
                    nBoxIndex = WrapIndex2D(aa, bb, nDimension); /* wbb * nDimension.w + waa; */
                    
                    if(nBoxIndex < nNumel)
                    {
                        dCurVal = pdArray[nBoxIndex];
                        dAvg += dCurVal;    
                    }
                    else
                    {
                        QuitWithError("Index out of bounds!", ERR_INDEX_OUT_OF_BOUNDS);
                    }
                }
            }
            dAvg *= dOneOverN;

            if(bIsAdditive)
            {
                pdAverage[yy * nDimension.w + xx] += dAvg * dAvg;
            }   
            else
            {
                pdAverage[yy * nDimension.w + xx] = dAvg * dAvg;
            }
        }
    }
}

