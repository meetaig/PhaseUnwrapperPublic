#include "fileio.h"
/**
 @file      fileIO.c
 @author    Tim Elberfeld
 @date      26.07.2016
 @brief     functions to make file writing and reading easier
 */

/**
@brief      Save a double array as a CSV file
@param[in]  pdArray         -   array to write to CSV file
@param[in]  nDimension      -   height and width of the array
@param[in]  strFilename     -   filename for the csv file (only filename, will be saved in log directory)
@param[in]  strDelimiter    -   delimiter for the values
@param[in]  nPrecision      -   precision for the floating point printing
@date       06.09.2016
@author     Tim Elberfeld
*/
void SaveAsCSV_double(double* pdArray, ArrayDim nDimension, char* strFilename, char* strDelimiter, int32 nPrecision)
{
    char* strFormat = (char*) AllocBytes(LOGBUFF + 1, "char", sizeof(char));
    int32 nAlignment = nPrecision + CSV_ALIGNMENT;
    
    /*  nPrecision decimal places plus 1 decimal point and some extra
        for the delimiter times the length of the line */
    int32 nBytes = nDimension.w * (DBL_MAX_CHAR_COUNT + nAlignment + (int32)strlen(strDelimiter) + 1);
    char* strLine = (char*) AllocBytes(nBytes, "char", sizeof(char));
    sprintf(strFormat, "%%%d.%df%s", nAlignment, nPrecision, strDelimiter); /* make a format string */
    int32 pos = 0;
    int32 nIndex = 0;
    FILE* pFile = MakeOpenLogFile(strFilename, "w");
    
    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {
            nIndex = yy * nDimension.w + xx;
            if(pos >= nBytes)
            {
                WriteErrorLog(LOGBUFF, "string length %d, pos %d", nBytes, pos);
                QuitWithError("string length exceeded", ERR_INVALID_MEMORY_ACCESS);
                break;
            }
            pos += sprintf(&strLine[pos], strFormat, pdArray[nIndex]);
        }
        strLine[pos - strlen(strDelimiter)] = '\0'; /* terminate the string earlier to delete the last delimiter */
        fprintf(pFile,"%s \n", strLine);
        memset(strLine, 0, nBytes);
        pos = 0;
    }
    FreeBytes(strFormat);
    FreeBytes(strLine);
    CloseFile(pFile);
}

/**
@brief      Save an int array as a CSV file
@param[in]  pnArray         -   array to write to CSV file
@param[in]  nDimension      -   height and width of the array
@param[in]  strFilename     -   filename for the csv file (only filename, will be saved in log directory)
@param[in]  strDelimiter    -   delimiter for the values
@date       06.09.2016
@author     Tim Elberfeld
*/
void SaveAsCSV_int32(int32* pnArray, ArrayDim nDimension, char* strFilename, char* strDelimiter)
{
    char* strFormat = (char*) AllocBytes(LOGBUFF + 1, "char", sizeof(char));
    int32 nAlignment = CSV_ALIGNMENT;
    int32 nBytes = nDimension.w * (DBL_MAX_CHAR_COUNT + nAlignment + (int)strlen(strDelimiter));
    char* strLine = (char*) AllocBytes(nBytes + 1, "char", sizeof(char));
    
    sprintf(strFormat, "%%%dd%s", nAlignment, strDelimiter);
    int32 pos = 0;
    int32 nIndex = 0;
    FILE* pFile = MakeOpenLogFile(strFilename, "w");
    
    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {
            nIndex = yy * nDimension.w + xx;
            if(pos > nBytes)
            {
                WriteErrorLog(LOGBUFF, "string length %d, pos %d", nBytes, pos);
                QuitWithError("string length exceeded", ERR_INVALID_MEMORY_ACCESS);
                break;
            }
            pos += sprintf(&strLine[pos], strFormat, pnArray[nIndex]);
        }
        strLine[pos - strlen(strDelimiter)] = '\0'; /* terminate the string earlier to delete the last delimiter */
        fprintf(pFile,"%s \n", strLine);
        memset(strLine, 0, nBytes);
        pos = 0;
    }
    FreeBytes(strFormat);
    FreeBytes(strLine);
    CloseFile(pFile);
}

/**
 @brief      Save an uchar array as a CSV file
 @param[in]  pcArray         -   array to write to CSV file
 @param[in]  nDimension      -   height and width of the array
 @param[in]  strFilename     -   filename for the csv file (only filename, will be saved in log directory)
 @param[in]  strDelimiter    -   delimiter for the values
 @date       06.09.2016
 @author     Tim Elberfeld
 */
void SaveAsCSV_uint8(uint8* pcArray, ArrayDim nDimension, char* strFilename, char* strDelimiter)
{
    char* strFormat = (char*) AllocBytes(LOGBUFF + 1, "char", sizeof(char));
    int32 nAlignment = CSV_ALIGNMENT;
    int32 nBytes = nDimension.w * (DBL_MAX_CHAR_COUNT + nAlignment + (int)strlen(strDelimiter));
    char* strLine = (char*) AllocBytes(nBytes + 1, "char", sizeof(char));
    
    sprintf(strFormat, "%%%dd%s", nAlignment, strDelimiter);
    int32 pos = 0;
    int32 nIndex = 0;
    FILE* pFile = MakeOpenLogFile(strFilename, "w");
    
    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {
            nIndex = yy * nDimension.w + xx;
            if(pos > nBytes)
            {
                WriteErrorLog(LOGBUFF, "string length %d, pos %d", nBytes, pos);
                QuitWithError("string length exceeded", ERR_INVALID_MEMORY_ACCESS);
                break;
            }
            pos += sprintf(&strLine[pos], strFormat, pcArray[nIndex]);
        }
        strLine[pos - strlen(strDelimiter)] = '\0'; /* terminate the string earlier to delete the last delimiter */
        fprintf(pFile,"%s \n", strLine);
        memset(strLine, 0, nBytes);
        pos = 0;
    }
    FreeBytes(strFormat);
    FreeBytes(strLine);
    CloseFile(pFile);
}


/**
@brief 		Save an array as bytecode to a file
@param[in]	pArray 	-	pointer to the array
@param[in]	numel 	-	number of elements in the array
@param[in]	bytes 	-	number of bytes (numel * sizeof(pArray[0]))
@return		<int32>	-	same as CloseFile()
@date 		25.08.2016
@author	 	Tim Elberfeld
*/
int32 SaveAsByteCode(void* pArray, int32 numel, int32 bytes, const char* strFilename)
{
    if(NOT(pArray))
    { 
        QuitWithError("invalid data array", ERR_NULL_POINTER);
        return(ERR_NULL_POINTER);
    }

    FILE* pFile = MakeOpenLogFile(strFilename, "w");
    if(NOT(pFile))
    {
        QuitWithError("", ERR_FILE_OPEN_FAILED);
        return(ERR_FILE_OPEN_FAILED);
    }

    fwrite(pArray, bytes, numel, pFile);
    return(CloseFile(pFile));
	
}

/**
@brief 		open a file, read it byte by byte until eof. Then return the number of bytes read that way
@param[in]	strFilenamePlusPath	-	filename and path of the file to read
@date 		31.08.2016
@author		Tim Elberfeld
*/
int32 ScanFileForNumBytes(char* strFilenamePlusPath)
{
	WriteToLogFile(LOGBUFF, "%s", strFilenamePlusPath);

	FILE* pFile = GetOpenFileHandle(strFilenamePlusPath, "r"); /* always open only for reading! */
	int32 nBytes = 0;
	char* buffer = (char*) AllocBytes(2, "char", sizeof(char));
    if (NOT(buffer))
    {
        QuitWithError("", ERR_ALLOCATION_FAILED);
        return(ERR_ALLOCATION_FAILED);
    }

	while(fread(buffer, sizeof(char), 1, pFile) != 0) /* read one byte after another until this can't be done anymore */
	{
		nBytes++;
	}
	WriteToLogFile(LOGBUFF, "read %d bytes", nBytes);
	FreeBytes(buffer);

    if(CloseFile(pFile))
    {
        QuitWithError("Failed to close file", ERR_FILE_CLOSE_FAILED);
	}
    return(nBytes);
}

/**
@brief 			Read a binary file specified by filename and read a specified number of double values from it
				The memory passed must be already allocated
@param[in]		strFilenamePlusPath	-	filename and path of the file to read
@param[inout]	pdArray				-	pointer to the array to put the file data in
@param[in]		nBytes 				-	number of bytes that were allocated for pdArray and to read from the file
@date 			31.08.2016
@author			Tim Elberfeld
*/
void ReadBytesPreAlloc_double(char* strFilenamePlusPath, double* pdArray, int32 nBytes)
{
	FILE* pFile = GetOpenFileHandle(strFilenamePlusPath, "r"); /* always open only for reading! */
    WriteToLogFile(LOGBUFF, "opened file %s", strFilenamePlusPath);
    
	int32 nElements = nBytes / sizeof(double);
	size_t read = fread(pdArray, sizeof(double), nElements, pFile);
	WriteToLogFile(LOGBUFF, "read %d elements, was supposed to be %d", read, nElements);

	if(read != nElements)
	{
		QuitWithError("Failed to read double array from file", ERR_FILE_READ_FAILED);
	}

	CloseFile(pFile);
}
/**
 @copydoc ReadBytesPreAlloc_double()
*/
void ReadBytesPreAlloc_float(char* strFilenamePlusPath, float* pfArray, int32 nLength)
{
	FILE* pFile = GetOpenFileHandle(strFilenamePlusPath, "r"); /* always open only for reading! */

	int32 nElements = nLength / sizeof(float);
	size_t read = fread(pfArray, sizeof(float), nElements, pFile);
	WriteToLogFile(LOGBUFF, "read %d elements, was supposed to be %d", read, nElements);

	if(read != nElements)
	{
		QuitWithError("Failed to read float array from file", ERR_FILE_READ_FAILED);
	}

	if(IsBigEndian())
	{
		WriteToLogFile(LOGBUFF, "flipping byte order");
		FlipByteOrder(pfArray, nElements);
	}

	CloseFile(pFile);
}
/**
 @copydoc ReadBytesPreAlloc_double()
 */
void ReadBytesPreAlloc_uint8(char* strFilenamePlusPath, uint8* pcArray, int32 nBytes)
{
	FILE* pFile = GetOpenFileHandle(strFilenamePlusPath, "r"); /* always open only for reading! */

	int32 nElements = nBytes;
	size_t read = fread(pcArray, sizeof(uint8), nElements, pFile);
	WriteToLogFile(LOGBUFF, "read %d elements, was supposed to be %d", read, nElements);

	if(read != nElements)
	{
		QuitWithError("Failed to read byte array from file", ERR_FILE_READ_FAILED);
	}

	CloseFile(pFile);
}

/**
@brief 		read a bytes from a file and cast the contents to an <uchar, int, double> array
@param[in]	strFilenamePlusPath	-	location of the file on disk
@date 		31.08.2016
@author		Tim Elberfeld
*/
uint8* ReadBytes_uint8(char* strFilenamePlusPath, int32* rnBytes)
{
    return ReadBytes(strFilenamePlusPath, rnBytes);
}
/**
 @copydoc ReadBytes_uchar()
 */
int32* ReadBytes_int32(char* strFilenamePlusPath, int32* rnBytes)
{
	return (int32*) ReadBytes(strFilenamePlusPath, rnBytes);
}
/**
 @copydoc ReadBytes_uchar()
 */
float* ReadBytes_float(char* strFilenamePlusPath, int32* rnBytes)
{
    return (float*) ReadBytes(strFilenamePlusPath, rnBytes);
}
/**
 @copydoc ReadBytes_uchar()
 */
double* ReadBytes_double(char* strFilenamePlusPath, int32* rnBytes)
{
	return (double*) ReadBytes(strFilenamePlusPath, rnBytes);
}

/**
@brief 			read a file byte by byte and return the data.
@param[in]		strFilenamePlusPath	-	path to the file to read data from
@param[inout]	rnBytes				-	number of bytes that were read from the file
@return 		<void*>				-	pointer to the memory containing the data read from the file
@date 			31.08.2016
@author 		Tim Elberfeld
*/
void* ReadBytes(char* strFilenamePlusPath, int32* rnBytes)
{
    FILE* pFile = GetOpenFileHandle(strFilenamePlusPath, "rb"); /* always open only for reading! */
    uint8* buffer = NULL;
    WriteToLogFile(LOGBUFF, "reading file %s", strFilenamePlusPath);
    
    if(pFile)
    {
        *rnBytes = 0;
        int32 nAllocated = 256;
        buffer = (uint8*) AllocBytes(nAllocated, "uint8", sizeof(uint8));
        
        while(fread(buffer + (*rnBytes), sizeof(uint8), 1, pFile) != 0) /* read one byte after another until this can't be done anymore */
        {
            if((*rnBytes) >= (nAllocated-1))
            {
                buffer = ExtendMemory(buffer, *rnBytes, 2*nAllocated);
                nAllocated *= 2;
            }
            (*rnBytes)++;
        }
        
        if(CloseFile(pFile))
        {
            FreeBytes(buffer);
            QuitWithError("Failed to close file", ERR_FILE_CLOSE_FAILED);
        }
    }
    else
    {
        QuitWithError("Failed to open bytefile", ERR_FILE_OPEN_FAILED);
    }
    
    return((void*)buffer);
}

/**
@brief 			save a double array as a bmp image in the log folder
@param[in]		pdData 			-	data to save
@param[in]		nDimension		-	width and height of the array	
@param[in]		strFilename 	-	filename of the image
@param[in]		colormap		-	which colormap to use
@return 		<int32>			-	error code (0 if success)
@date           04.08.2016
@author 		Tim Elberfeld
*/
int32 SaveAsBMP_double(double* pdData, ArrayDim nDimension, CMAP colormap, const char* strFilename, ...)
{
	int32 nNumel = nDimension.w * nDimension.h; /* scale data according to bit depth  */
	int32 nNumBytes = nNumel * sizeof(double);

    double* pdImgData = (double*)AllocBytes(nNumBytes, "double", sizeof(double)); /* allocate memory and init to zero */
    if(pdImgData == NULL)
    {
        QuitWithError("Could not allocate memory", ERR_ALLOCATION_FAILED);
        return(ERR_ALLOCATION_FAILED);
    }
	
	CopyArray(pdData, nNumBytes, pdImgData, nNumBytes);	/* copy data */
	MapToRange_double(pdImgData, nNumel, 0.0, 255.0);	/* map to 8 bit grayscale */

	BMP* pBMP = BMP_Create((unsigned int)nDimension.w, (unsigned int)nDimension.h, 8); /* make 8 bit rgb bmp */
	BMP_CHECK_ERROR(stderr, -1); /* check if the file was created successfully */

	uint8 pixelval;
	for (int32 yy = 0; yy < nDimension.h; ++yy)
	{
		for (int32 xx = 0; xx < nDimension.w; ++xx)
		{
			pixelval = (uint8)pdImgData[yy * nDimension.w + xx];
			BMP_SetPixelIndex(pBMP, xx, yy, pixelval); /* set pixel */
		}
	}

    /* make the actual filename from the format string */
    va_list argPtr; /* pointer to the argument list passed in '...' */
    va_start(argPtr, strFilename);
    size_t nBufferSize = DEFAULT_BMP_NAME_SIZE;
    char* strFormattedFilename = (char*)calloc(nBufferSize + 1, sizeof(char)); /* + 1 for terminating 0*/
    strFormattedFilename[nBufferSize] = '\0';
    int32 pos = vsprintf(strFormattedFilename, strFilename, argPtr);
    va_end(argPtr);

    WriteToLogFile(LOGBUFF, "saving bitmap image %s", strFormattedFilename);
    SaveBMP(pBMP, strFormattedFilename, colormap);

    FreeBytes(strFormattedFilename);
    FreeBytes(pdImgData);
	BMP_Free(pBMP);
	return(0);
}

/**
@brief          save a int array as a bmp image in the log folder
@param[in]      pnData          -   data to save
@param[in]      nDimension      -   width and height of the array	
@param[in]      colormap        -   which colormap to use
@param[in]      strFilename     -   filename of the image
@param[in]      ...             -   variable argument list
@return         <int32>         -   error code (0 if success)
@date           04.08.2016
@author         Tim Elberfeld
*/
int32 SaveAsBMP_int32(int32* pnData, ArrayDim nDimension, CMAP colormap, const char* strFilename, ...)
{
	int32 nNumel = nDimension.w * nDimension.h;
	int32 nNumBytes = nNumel * sizeof(int);
	int32* pnImgData = (int32*) AllocBytes(nNumBytes, "int32", sizeof(int32)); 

	if(pnImgData == NULL)
    {
        QuitWithError("Could not allocate memory", ERR_ALLOCATION_FAILED);
        return(ERR_ALLOCATION_FAILED);
    }

    /* scale data according to bit depth  */
	CopyArray(pnData, nNumBytes, pnImgData, nNumBytes);	/* copy data */
	MapToRange_int32(pnImgData, nNumel, 0, 255);        /* map to 8 bit grayscale */

	BMP* pBMP = BMP_Create((unsigned int)nDimension.w, (uint32)nDimension.h, 8); /* make 8 bit grayscale bmp */
	BMP_CHECK_ERROR(stderr, -1); /* check if the file was created successfully */

	uint8 pixelval;
	for (int32 yy = 0; yy < nDimension.h; ++yy)
	{
		for (int32 xx = 0; xx < nDimension.w; ++xx)
		{
			pixelval = (uint8)pnImgData[yy * nDimension.w + xx];
			BMP_SetPixelIndex(pBMP, xx, yy, pixelval); /* set pixel */
		}
	}

    /* make the actual filename from the format string */
    va_list argPtr; /* pointer to the argument list passed in '...' */
    va_start(argPtr, strFilename);
    size_t nBufferSize = DEFAULT_BMP_NAME_SIZE;
    char* strFormattedFilename = (char*)calloc(nBufferSize + 1, sizeof(char)); /* + 1 for terminating 0*/
    strFormattedFilename[nBufferSize] = '\0';
    vsprintf(strFormattedFilename, strFilename, argPtr);
    va_end(argPtr);

    WriteToLogFile(LOGBUFF, "saving bitmap image %s", strFormattedFilename);
    SaveBMP(pBMP, strFormattedFilename, colormap);

    FreeBytes(strFormattedFilename); 
    FreeBytes(pnImgData);
	BMP_Free(pBMP);
	return(0);
}

/**
@brief 			save a uint8 array as a bmp image in the log folder
@param[in]		pcData 			-	data to save 	
@param[in]		nDimension		-	width and height of the array
@param[in]		colormap		-	which colormap to use
@param[in]		strFilename 	-	filename of the image
@param[in]      ...             -   variable argument list
@return 		<int32>			-	error code (0 if success)
@date           04.08.2016
@author 		Tim Elberfeld
*/
int32 SaveAsBMP_uint8(uint8* pcData, ArrayDim nDimension, CMAP colormap, const char* strFilename, ...)
{
	int32 nNumBytes = nDimension.w * nDimension.h;
	uint8* pcImgData = (uint8*) AllocBytes(nNumBytes, "uint8", sizeof(uint8)); 
	if (pcImgData == NULL)
	{
        QuitWithError("Could not allocate memory", ERR_ALLOCATION_FAILED);
        return(ERR_ALLOCATION_FAILED);
	}

	CopyArray(pcData, nNumBytes, pcImgData, nNumBytes);	/* copy data */
	MapToRange_uint8(pcImgData, nNumBytes, 0, 255);	/* map to new range */

	BMP* pBMP = BMP_Create((unsigned int)nDimension.w, (uint32)nDimension.h, 8); /* make 8 bit grayscale bmp */
	BMP_CHECK_ERROR(stderr, -1); /* check if the file was created successfully */

	uint8 pixelval;
	for (int32 yy = 0; yy < nDimension.h; ++yy)
	{
		for (int32 xx = 0; xx < nDimension.w; ++xx)
		{
			pixelval = pcImgData[yy * nDimension.w + xx];
			BMP_SetPixelIndex(pBMP, xx, yy, pixelval); /* set pixel */
		}
	}

    /* make the actual filename from the format string */
    va_list argPtr; /* pointer to the argument list passed in '...' */
    va_start(argPtr, strFilename);
    size_t nBufferSize = DEFAULT_BMP_NAME_SIZE;
    char* strFormattedFilename = (char*)calloc(nBufferSize + 1, sizeof(char)); /* + 1 for terminating 0*/
    strFormattedFilename[nBufferSize] = '\0';
    vsprintf(strFormattedFilename, strFilename, argPtr);
    va_end(argPtr);

    WriteToLogFile(LOGBUFF, "saving bitmap image %s", strFormattedFilename);
	SaveBMP(pBMP, strFormattedFilename, colormap);

    FreeBytes(strFormattedFilename);
	FreeBytes(pcImgData);
	BMP_Free(pBMP);
	return(0);
}

/**
@brief      convert a rgb bitmap image to grayscale
@param[in]  pBitmapData     -   data of the bitmap
@param[in]  nDimension      -   height and width of the bitmap
@param[in]  nBitsPerPixel   -   number of bits per pixel (can be 8, 24 or 32)
@return     <uint8*>        -   pointer to converted image memory
@date       08.09.2016
@author     Tim Elberfeld
*/
uint8* RGBToGrayscale(uint8* pBitmapData, ArrayDim nDimension, int nBitsPerPixel)
{
    int nBytesPerPixel = nBitsPerPixel / 8;
    uint8* pcImgData = (uint8*)AllocBytes(nDimension.w * nDimension.h * nBytesPerPixel, "uint8", sizeof(uint8));
    int32 nIndex;
    uint8 r, g, b, gray;

    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {
            nIndex = nBytesPerPixel * (yy * nDimension.w + xx);
            b = pBitmapData[nIndex + 0];
            g = pBitmapData[nIndex + 1];
            r = pBitmapData[nIndex + 2];
            gray = (uint8)	(((double)r * RGB_RED_WEIGHT) + 
            				( (double)g * RGB_GREEN_WEIGHT) + 
            				( (double)b * RGB_BLUE_WEIGHT));
            pcImgData[yy * nDimension.w + xx] = gray;
        }
    }
    return(pcImgData);
}

/**
@brief          Load a bitmap file
@param[in]      strFilename     -   the filename (complete with path)
@param[inout]   rnDimension     -   will hold width and height of the bitmap
@param[inout]   rnBitsPerPixel  -   number of bits per pixel (can be 8, 24 or 32)
@return         <uint8*>        -   pointer to image memory
@date           08.09.2016
@author         Tim Elberfeld
*/
uint8* LoadBMP(char* strFilename, ArrayDim* rnDimension, int32* rnBitsPerPixel)
{
    BMP* pBMP = BMP_ReadFile(strFilename);
    BMP_CHECK_ERROR(stderr, -1);
    
    rnDimension->w = (int32)pBMP->Header.Width;
    rnDimension->h = (int32)pBMP->Header.Height;
    *rnBitsPerPixel = pBMP->Header.BitsPerPixel;
    
    if(*rnBitsPerPixel == 8)
    {
        uint8* pcImgData = (uint8*)AllocBytes(rnDimension->w * rnDimension->h, "uint8", sizeof(uint8));
        if(pcImgData)
        {
            for (int32 yy = 0; yy < rnDimension->h; ++yy)
            {
                for (int32 xx = 0; xx < rnDimension->w; ++xx)
                {
                    BMP_GetPixelIndex(pBMP, xx, yy, &pcImgData[yy * rnDimension->w + xx]);
                }
            }
        }
        return(pcImgData);
    }
    else if(*rnBitsPerPixel == 24 || *rnBitsPerPixel == 32)
    {
        int32 nColorPitch = *rnBitsPerPixel / 8; /* convert to bytes per pixel */
        uint8* pcImgData = (uint8*)AllocBytes(nColorPitch * rnDimension->w * rnDimension->h, "uint8", sizeof(uint8));
        
        if(pcImgData)
        {
            uint8 r = 0, g = 0, b = 0;
            int32 nIndex;
            for (int32 yy = 0; yy < rnDimension->h; ++yy)
            {
                for (int32 xx = 0; xx < rnDimension->w; ++xx)
                {
                    BMP_GetPixelRGB(pBMP, xx, yy, &r, &g, &b);
                    nIndex = nColorPitch * (yy * rnDimension->w + xx);
                    pcImgData[nIndex + 0] = b;
                    pcImgData[nIndex + 1] = g;
                    pcImgData[nIndex + 2] = r;
                }
            }
        }        
        return(pcImgData);
    }
    return(NULL);
}

/**
@brief 			save a bitmap from the passed handle. Apply colormap before that
@param[inout]	pBMP			-	handle to the bitmap structure
@param[in]		strFilename 	-	filename for the image
@param[in]		colormap 		-	colormap to apply before saving
@return 		<int32>			-	error code (0 if success)
@date  			04.08.2016		
@author			Tim Elberfeld
*/
int32 SaveBMP(struct _BMP* pBMP, const char* strFilename, CMAP colormap)
{
	/* make full path */
    char* strFilenamePlusPath =  (char*) AllocBytes(MAX_PATH + 1, "char", sizeof(char));
	if(strFilenamePlusPath == NULL)
    {
        QuitWithError("Could not allocate memory", ERR_ALLOCATION_FAILED);
        return(ERR_ALLOCATION_FAILED);
    }

	GetLogDir(strFilenamePlusPath, MAX_PATH);
	strncat(strFilenamePlusPath, strFilename, strlen(strFilename));
	ApplyColorMap(pBMP, colormap);

	BMP_WriteFile(pBMP, strFilenamePlusPath); /* write bmp to log directoy  */
    FreeBytes(strFilenamePlusPath);
    BMP_CHECK_ERROR(stderr, -2); /* check if the file was written successfully */
    return(0);
}

/**
@brief 			apply colormap to grayscale image
@param[inout]	pBMP		- handle to the bmp structure
@param[in]		colormap 	- colormap type
@date  			04.08.2016
@author			Tim Elberfeld
*/
void ApplyColorMap(struct _BMP* pBMP, CMAP colormap)
{
	uint8* pColorMap;
	switch(colormap)
	{
		case CMAP_GRAY:
			for (int32 ii = 0; ii < CMAP_LENGTH; ++ii)
			{
				BMP_SetPaletteColor(pBMP, ii, ii, ii, ii);
			}
			return; 		
		case CMAP_HOT:					/* colormaps are static arrays that are defined in colormaps.h */
			pColorMap = &CMAP_HOT_A[0];
			break;
		case CMAP_JET:
			pColorMap = &CMAP_JET_A[0];
			break;
		default:
			pColorMap = &CMAP_HOT_A[0];
			break;
	}
	int32 r, g, b;
	for (int32 ii = 0; ii < CMAP_LENGTH; ++ii)
	{
		r = pColorMap[ii * CMAP_PITCH];
		g = pColorMap[ii * CMAP_PITCH + 1];
		b = pColorMap[ii * CMAP_PITCH + 2];
		BMP_SetPaletteColor(pBMP, ii, r, g, b);
	}
}

/**
 @brief     log an allocation
 @param[in] ptr                 -     ptr to the memory that was allocated or freed
 @param[in] nNumBytes           -     number of bytes to be allocated or freed
 @param[in] nBytesPerElement    -     bytes per element of datatype allocated or freed
 @param[in] isFree              -     is the operation a free() or a <m, c>alloc() ?
 @date      31.07.2016
 @author    Tim Elberfeld
 */
void LogAllocation(void* ptr, int32 nNumBytes, int32 nBytesPerElement, bool isFree)
{
    /* log the <m,c>alloc() or free() to the allocations log file */
    WriteToLogFileImp("allocations.txt", __func__, __LINE__,
                      LOGBUFF, "%s memory at %p with %d bytes (%d elements)",
                      isFree ? "freeing" : "allocating", ptr, nNumBytes, nNumBytes / nBytesPerElement);
}

/**
@brief		write a format string to the file specified by strFilename.
			The file must be in the log directory. If there is no such file, it will be created.
@param[in]	strFilename 	-	filename
@param[in]	strCallerFunc	-	name of the function that called this function (see macro in header file)
								Pass NULL to not log the function name and line number 
@param[in]	nLineNumber		-	line from which the function was called. (also see macro in header file)
								Pass negative number to not log the line number!
@param[in]	nBufferSize		-	number of bytes to allocate for the output buffer. This means the number of bytes of the 
								expanded string after parsing the arguments!
@param[in]	strFormat		-	format string
@param[in]	...				-	variable number of arguments to fill the format string.
@return 	<int32>			-	error codes like fclose. (0 for success)
@date		26.07.2016
@author		Tim Elberfeld
*/
int32 WriteToLogFileImp(const char* strFilename, 
                        const char* strCallerFunc, 
                        int32 nLineNumber,
                        size_t nBufferSize, 
                        const char* strFormat, 
                        ...)
{
	FILE* pFile = MakeOpenLogFile(strFilename, NULL);		/* open the file */

	if(pFile != NULL)
	{
        va_list argPtr; /* pointer to the argument list passed in '...' */
        va_start(argPtr, strFormat);
        char* strContent = (char*) calloc(nBufferSize + 1, sizeof(char)); /* + 1 for terminating 0*/
        if (NOT(strContent))
        {
            QuitWithError("", ERR_ALLOCATION_FAILED);
            return(ERR_ALLOCATION_FAILED);
        }
        strContent[nBufferSize] = '\0';
        
		/* append time stamp */
		char* strTimeStamp = (char*) calloc(MAX_PATH + 1, sizeof(char));
        if (NOT(strTimeStamp))
        {
            FreeBytes(strContent);
            QuitWithError("", ERR_ALLOCATION_FAILED);
            return(ERR_ALLOCATION_FAILED);
        }

        MakeTimeStamp(strTimeStamp, MAX_PATH);
        fprintf(pFile, "%s", strTimeStamp);
        free(strTimeStamp);

		vsprintf(strContent, strFormat, argPtr);	/* convert variable argument list into string */
		
		if(strCallerFunc != NULL)					/* only log valid name (duh!) */
		{
			if(nLineNumber >= 0)					/* pass negative number to not log the line number */
			{	
				fprintf(pFile, " ln %4d - ", nLineNumber); /* add caller name and line number} */
			}
			
			fprintf(pFile, "%s() ", strCallerFunc);
		}
		
		fprintf(pFile, "%s", strContent);			/* write string to file */
		puts(strContent); 							/* write string to stdout */
		va_end(argPtr);								/* end using the va_list ptr */
		free(strContent);
        strContent = NULL;
	}
	else
	{
		QuitWithError("Failed to open log file", ERR_FILE_OPEN_FAILED);
        return(ERR_FILE_OPEN_FAILED);
	}

	return(CloseFile(pFile));						/* this already has validity check in it! */
}

/**
@brief      Log a 1d array
@param[in]  strFilename -   name of the file to save the result to
@param[in]  pnArray     -   int array to log
@param[in]  nElements   -   number of elements in the array
@param[in]  strCaller   -   name of the function that called this one
@param[in]  nLineNumber -   line number where the function was called
@return  	<int32>		-	Same as WriteToLogFileImp()
@date       01.09.2016
@author     Tim Elberfeld
*/
int32 LogArray1DImp(const char* strFilename, int32* pnArray, uint32 nElements, const char* strCaller, int32 nLineNumber)
{
    uint32 nDigits = 11;                                           /* assume 11 chars per integer (10 digits and a sign) */
    uint32 nBytes = (int32)(ceil((double)nElements*1.1))*nDigits;    /* allocate enough for 1 row and add a bit of a safety net */
    
    char* strBuff = (char*) calloc(LOGBUFF + 1, sizeof(char));	/* this should be more than enough for one value */
    char* strResult = (char*) calloc(nBytes + 1, sizeof(char));
    
    size_t nCurLen;
    size_t nCumLen = 0;

    ClearToZero(strResult, nBytes); 	/* make the concatenation start from the beginning again */
    for(uint32 xx = 0; xx < nElements; xx++)
    {
        sprintf(strBuff, "%d, ", pnArray[xx]);
        nCurLen = strlen(strBuff);
        nCumLen += nCurLen;
        
        if(nCumLen < nBytes)
        {
            strncat(strResult, strBuff, nCurLen);
        }
        else
        {
            free(strBuff);
            free(strResult);
            QuitWithError("Result buffer full", ERR_INVALID_MEMORY_ACCESS);
            break;
        }
    }

    int32 retval = WriteToLogFileImp(strFilename, strCaller, nLineNumber, LOGBUFF + nBytes, "%s", strResult);
    free(strBuff);
    free(strResult);
    return(retval);
}

/**
@brief		write an array to the log file in a more compressed fashion than just logging every single value
@param[in]	strFilename 	-	filename 
@param[in]	pdArray			-	the array to log
@param[in]	nWidth			-	width of the array
@param[in]	nHeight			-	height of the array
@param[in]	strCaller		-	name of the function that called this one
@param[in]  nLineNumber     -   line number where the function was called
@return 	<int> 			-	error codes like fclose. (0 for success)
@date		26.07.2016
@author		Tim Elberfeld
*/
int LogArray2DImp(const char* strFilename, double* pdArray, int32 nWidth, int32 nHeight, const char* strCaller, int32 nLineNumber)
{
	int32 nDigits = 12; 						/* assume 12 digits per double number -> 8 (or 7 and a sign) digits then a point, then 3 decimals */
	size_t nBytes = (int)(ceil((double)nWidth*1.1))*nDigits;	/* allocate enough for 1 row and add a bit of a safety net */
	
	char* strBuff = (char*)calloc(LOGBUFF + 1, sizeof(char));	/* this should be more than enough for one value */
	char* strResult = (char*)calloc(nBytes + 1, sizeof(char));
	
	int32 k = 0;
    size_t nCurLen;
    size_t nCumLen = 0;
    int32 retval = 0;
    
	for(int32 yy = 0; yy < nHeight; yy++)
	{
		ClearToZero(strResult, nBytes); 	/* make the concatenation start from the beginning again */
		for(int32 xx = 0; xx < nWidth; xx++)
		{
			sprintf(strBuff, "%12.3f", pdArray[k++]);
			nCurLen = strlen(strBuff);
			nCumLen += nCurLen;
			
			if(nCumLen < nBytes)
			{
				strncat(strResult, strBuff, nCurLen);
			}
			else
			{
				free(strBuff);
				free(strResult);
				QuitWithError("Result buffer full", ERR_INVALID_MEMORY_ACCESS);
                break;
			}
		}
		nCumLen = 0;	/* reset the cumulative length */
        retval = WriteToLogFileImp(strFilename, strCaller, nLineNumber, LOGBUFF + nBytes, "%s", strResult);
        
        if(retval != 0)
        {
            break;
        }
	}
	free(strBuff);
	free(strResult);

    return(retval);
}

/**
@brief			make a string containing the current time in the format
				YYYY-MM-DD HH:mm:ss
@param[inout]	strBuffer	-	string buffer for the time stamp
@param[in]		nBufferSize	-	size of the buffer
@date			28.07.2016
@author			Tim Elberfeld
*/
void MakeTimeStamp(char* strBuffer, int32 nBufferSize)
{
    ClearToZero(strBuffer, nBufferSize);
	time_t* currentTime = (time_t*)calloc(1, sizeof(time_t));
    time(currentTime);
	struct tm* timeInfo = localtime(currentTime);
	strftime(strBuffer, nBufferSize - 1, "%F %T :: ", timeInfo);
	free(currentTime);
}

/**
@brief		open or make a log file. (the file will be created if it does not exist yet. 
			It will just be opened if it exitst.
			Changing strOpenMode from the default value, the behaviour will be
			determined by strOpenMode).
@param[in]	strFilename			-	the name of the file to open
@param[in]	strOpenMode			-	mode that should be used to open the file
@return 	<FILE*>				-	handle to the opened file 
@date		26.07.2016
@author		Tim Elberfeld
*/
FILE* MakeOpenLogFile(const char* strFilename, const char* strOpenMode)
{
    if(strFilename)
    {
        char* strFilenamePlusPath = (char*) calloc(MAX_PATH + 1, sizeof(char)); /* make sure the memory is zero initialized */
        if(strFilenamePlusPath)
        {
            GetLogDir(strFilenamePlusPath, MAX_PATH);
            strncat(strFilenamePlusPath, strFilename, strlen(strFilename));
            FILE* pFile = GetOpenFileHandle(strFilenamePlusPath, strOpenMode);
            free(strFilenamePlusPath);
            return(pFile);
        }
    }
    return(NULL);
}

/**
@brief		open a file 
@param[in]	strFilenamePlusPath	-	complete path to the file to open
@param[in]	strOpenMode			-	mode that should be used to open the file
@return 	<FILE*>				-	handle to the file
@date		26.07.2016
@author		Tim Elberfeld
@note		Unfortunately without C++ we can not use fopen_s here... CAUTION!
*/
FILE* GetOpenFileHandle(const char* strFilenamePlusPath, const char* strOpenMode)
{
	if(strOpenMode == NULL)		/* too bad we dont have default arguments in C :( */
	{
        static int isFirstTime = 1;
        if(isFirstTime == 1)
        {
            strOpenMode = "w"; /* overwrite file contents when opening for the first time */
            isFirstTime = 0;
        }
		else
        {
            strOpenMode = "a+";
        }
	}

	return(fopen(strFilenamePlusPath, strOpenMode));
}

/**
@brief		close a file provided a valid file handle
@param[in]	pFile	-	handle to the open file
@return 	<int32>	-	0 for success
						EOF on failure
						EFAULT if invalid file handle
@date		26.07.2016
@author		Tim Elberfeld
*/
int32 CloseFile(FILE* pFile)
{
	if(pFile != NULL)
	{
		fprintf(pFile, "\r\n");	/* append a new line before closing! */
		return(fclose(pFile));
	}

	QuitWithError("Invalid file handle", ERR_INVALID_FILE_HANDLE);
    return(ERR_INVALID_FILE_HANDLE);
}

/**
 @brief         Read next line of an open file
 @param[in]     pFile   -   the file handle
 @param[inout]  rnBytes -   number of bytes actually read
 @return        <char*> -   buffer containing the next line
 @date          21.09.2016
 @author        Tim Elberfeld
 */
char* GetNextLine(FILE* pFile, int32* rnBytes)
{
    if(pFile)
    {
        *rnBytes = 0;
        char* buffer = AllocBytesImp(KiloBytes(5) + 1, "char", sizeof(char), false);
        if (NOT(buffer))
        {
            return(NULL);
        }
        
        while (fread(buffer + (*rnBytes), sizeof(char), 1, pFile) != 0)
        {
            if (((*rnBytes) >= KiloBytes(5)) ||
                (buffer[*rnBytes] == '\n'))
            {
                buffer[*rnBytes] = '\0'; /* replace the newline with \0 termimator */
                (*rnBytes)++;
                return(buffer);
            }
            (*rnBytes)++;
        }
    }
    return(NULL);
}


/**
@brief			get the path to the log directory
@param[inout]	strPath 	-	buffer to hold the path
@param[in]		nBufSize	-	number of bytes in the buffer strPath
@return			string containing the path to the log directory
@date			26.07.2016
@author			Tim Elberfeld
*/
void GetLogDir(char* strPath, int32 nBufSize)
{
    ClearToZero(strPath, nBufSize);
    if(GetWorkingDir(strPath, nBufSize))
	{
#ifdef WIN32
        strncat(strPath, "\\log\\", 5);
#else
        strncat(strPath, "/log/", 5);
#endif
		MakeDir(strPath);
	}
	else
	{
		QuitWithError("Could not get working directory", ERR_DIR_NOT_FOUND);
	}
}
