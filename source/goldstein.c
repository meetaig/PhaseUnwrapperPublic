#include "goldstein.h"
/**
 @file      goldstein.c
 @author    Tim Elberfeld
 @date      27.07.2016
 @brief     Implementation of Goldsteins Branch Cut Algorithm
 
 Algorithm taken from
 D. C. Ghiglia and M. D. Pritt,
 "Two-Dimensional Phase Unwrapping: Theory, Algorithms and Software."
 Wiley Blackwell, May 1998.
 http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html
 original paper:
 R. M. Goldstein, H. A. Zebker, and C. L. Werner,
 “Satellite radar interferometry: Two-dimensional phase unwrapping,”
 Radio Science, vol. 23, no. 4, pp. 713–720, 1988.
 https://dx.doi.org/10.1029/RS023i004p00713
 */

/**
@brief			Make a phase residue map. (used for Goldstein's Algorithm)
@param[in]		pdWrappedPhase      -	the wrapped phase array
@param[in]		nDimension          -	width and height of the phase array
@param[inout]	pcBitflags          -	bitflags array with same size as wrapped phase
@param[inout]	pnResidueMap        -	array to hold the residue map
@param[inout]   ResidueIndexList    -   list of all residues
@return 		<int32>             -	number of residues found during computation
@date			27.07.2016
@author			Tim Elberfeld
*/
int32 MakeResidueMap(double* pdWrappedPhase, ArrayDim nDimension, uint8* pcBitflags,
                     int32* pnResidueMap, IntList* ResidueIndexList)
{
	int32 NumRes=0;
  	double dResidueVal;
  	int32 nIndex;

	if(pcBitflags == NULL)
	{
		QuitWithError("Invalid array passed", ERR_NULL_POINTER);
	}

	double dThresh = 0.01; /* threshold value for residues */

  	for (int32 yy = 0; yy < nDimension.h - 1; yy++)
  	{
    	for (int32 xx = 0; xx < nDimension.w - 1; xx++)
    	{
      		nIndex = yy * nDimension.w + xx;
      		
      		if (test_flag(pcBitflags[nIndex], FLAG_AVOID) ||
      			test_flag(pcBitflags[nIndex + 1], FLAG_AVOID) || 
                test_flag(pcBitflags[nIndex + nDimension.w], FLAG_AVOID) ||
                test_flag(pcBitflags[nIndex + 1 + nDimension.w], FLAG_AVOID))
            {
        		continue; /* ignore this pixel */
      		}

      		dResidueVal =   GradientNormalized(pdWrappedPhase[nIndex + 1],
                                               pdWrappedPhase[nIndex], true) +
                            GradientNormalized(pdWrappedPhase[nIndex],
                                               pdWrappedPhase[nIndex + nDimension.w], true) +
        		   		 	GradientNormalized(pdWrappedPhase[nIndex + 1 + nDimension.w],
                                               pdWrappedPhase[nIndex + 1], true) +
            				GradientNormalized(pdWrappedPhase[nIndex + nDimension.w],
                                               pdWrappedPhase[nIndex + 1 + nDimension.w], true);
                	 		
  			if (dResidueVal > dThresh)
    		{
    			set_flag(pcBitflags[nIndex], FLAG_POS_RES);
    			pnResidueMap[nIndex] = 1;
                if(ResidueIndexList)
                {
                    AppendList(nIndex, ResidueIndexList);
                }
	  			NumRes++;
    		} 
    		else if (dResidueVal < -dThresh)
    		{
    			set_flag(pcBitflags[nIndex], FLAG_NEG_RES);	
    			pnResidueMap[nIndex] = -1;
	  			if(ResidueIndexList)
                {
                    AppendList(nIndex, ResidueIndexList);
                }
	  			NumRes++;
    		}
		}
  	}
  	SaveAsBMP_int32(pnResidueMap, nDimension, CMAP_GRAY, "residuemap.bmp");
  	
  	return(NumRes);
}

/**
@brief 			set FLAG_BORDER at the border pixels
@param[inout]	pcBitflags 		-	bitflags array
@param[in]		nDimension		-	width and height of the arrays
@date 			05.08.2016
@author 		Tim Elberfeld
*/
void MaskBorderPixels(uint8* pcBitflags, ArrayDim nDimension)
{
	WriteToLogFile(LOGBUFF, "Masking border pixels");
	for(int32 yy = 0; yy < nDimension.h; yy++)
	{
		if(yy % (nDimension.h-1) == 0)
		{
			memset(&pcBitflags[yy * nDimension.w], FLAG_BORDER, nDimension.w);
		}
		else 
		{
			pcBitflags[yy * nDimension.w] = FLAG_BORDER;
			pcBitflags[yy * nDimension.w + (nDimension.w-1)] = FLAG_BORDER;
		}
	}
}

/**
@brief  		Remove residue dipoles from an image. Dipoles are residues of opposite charge,
                that lie within one pixel of each other.
				This can be defined in 4 neighborhood or 8 neighborhood:
<pre>
				4 neighborhood 		8 neighborhood
				┌───┬───┬───┐		┌───┬───┬───┐
				│	│ ╳ │	│		│ ╳	│ ╳	│ ╳	│
				├───╆┅┅┅╅───┤		├───╆┅┅┅╅───┤
				│ ╳	┇ ╳ ┇ ╳	│		│ ╳ ┇ ╳	┇ ╳	│
				├───╄┅┅┅╃───┤		├───╄┅┅┅╃───┤
				│	│ ╳ │	│		│ ╳	│ ╳	│ ╳	│
				└───┴───┴───┘		└───┴───┴───┘
				In this case we always use the 8 neighborhood
</pre>
@param[inout]	pnResidueMap 		-	the residue map containing the potential dipoles. 
                                        The map will be modified in the function
@param[inout]	pcBitflags 			-	bitflags array containing ignore flags, branch cuts and other flags.
                                        This function will set branch cuts here
@param[in]		nDimension			-	width and height of the arrays
@param[inout]	nNumResidues 		-	number of residues in the map
@param[inout]	ResidueIndexList	-	list of all residue indices to not be forced to 
                                        visit every pixel in the arrays!
@note 			This can be done more efficiently by just using the 
                residue index list to access the residues and not iterating over every pixel
@date 			01.08.2016
@author 		Tim Elberfeld
*/
void RemoveDipoles(int32* pnResidueMap, uint8* pcBitflags, ArrayDim nDimension,
                   int32* nNumResidues, IntList* ResidueIndexList)
{
	int nCharge;
	int nIndex;
	int nIndexPartner;
	uint8 flag;
	int xxPartner = 0;
	int yyPartner = 0;
	int nChargePartner;
	int nRemoved = 0;
	for (int32 yy = 0; yy < nDimension.h; ++yy)
	{
		for (int32 xx = 0; xx < nDimension.w; ++xx)
		{
			nIndex = yy * nDimension.w + xx;
			nCharge = pnResidueMap[nIndex];

			if(nCharge != 0)
			{
                /* if charge is -1 test for positive and  vice versa */
				flag = (nCharge == -1) ? FLAG_POS_RES : FLAG_NEG_RES;
				nIndexPartner = CheckNeighbors(pcBitflags, xx, yy, nDimension, flag);
				
				if((nIndexPartner != -1) && (nIndexPartner != nIndex))
				{
					ConvertTo2DIndex(nIndexPartner, nDimension.w, &xxPartner, &yyPartner);
					nChargePartner = pnResidueMap[nIndexPartner];

					/* set branch cuts at the removed residues */
					if(ResidueIndexList)
                    {
                        ResidueIndexList->root = DeleteList(nIndex, ResidueIndexList);
                    }
                    
					delete_flag(pcBitflags[nIndex],  (nCharge == -1) ? FLAG_NEG_RES : FLAG_POS_RES);
					set_flag(pcBitflags[nIndex], FLAG_BRANCH_CUT);
					pnResidueMap[nIndex] = 0;
				
                    if(ResidueIndexList)
                    {
                        ResidueIndexList->root = DeleteList(nIndexPartner, ResidueIndexList);
                    }
                    
					delete_flag(pcBitflags[nIndexPartner], flag);
					set_flag(pcBitflags[nIndexPartner], FLAG_BRANCH_CUT);
					pnResidueMap[nIndexPartner] = 0;
					nRemoved++;
				}
			}
		}
	}

	WriteToLogFile(LOGBUFF, "removed %d dipoles (%d Residues)", nRemoved, nRemoved*2);
}

/**
@brief		Check the neighboring pixels around (xx, yy).
            If there is a residue of opposite charge return its index
@param[in]	pcBitflags 	-	bitflags array holding the information of the residues
@param[in]	xx			-	x coordinate of the center residue
@param[in]	yy			-	y coordinate of the center residue
@param[in] 	nDimension	-	width and height of the bitflags array
@return 	<int32>		-	(-1) if no neighbors with the appropriate flag set.
                            If there was one with the flag set then this pixel's index
							is returned.
@date 		05.08.2016
@author 	Tim Elberfeld
*/
int32 CheckNeighbors(uint8* pcBitflags, int32 xx, int32 yy, ArrayDim nDimension, FLAG flag)
{
	int32 aa;
	int32 bb;
	int32 nIndexPartner;

	/* check the direct neighbors first, then the diagonals */
	static int32 aY[] = {-1,  0, 0, 1, -1, -1,  1, 1};
	static int32 aX[] = { 0, -1, 1, 0, -1,  1, -1, 1};
    /* UP, LEFT, RIGHT, DOWN, TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT*/

	for(int32 ii = 0; ii < 8; ii++)
	{
		aa = xx + aY[ii];
		bb = yy + aX[ii];
		nIndexPartner = bb * nDimension.w + aa;

		if(IsValidIndex2D(aa, bb, nDimension))
		{
			if(test_flag(pcBitflags[nIndexPartner], flag))
			{
				return(nIndexPartner);
			}
		}
	}
	return(-1);
}

/**
@brief			Place branch cuts between residues and/or image border
@param[in]		pnResidueMap		-	array containing the residue map
@param[in]		nNumResidues		-	number of residues contained in the map
@param[inout]	pcBitflags 			-	bitflags array
@param[in]		nDimension 			-	width and height of the array
@param[in]		nMaxCutLength 		-	maximum search radius for nearest branch cuts
@param[in]		ResidueIndexList	-	linked list containing the indices of the residues
@date 			28.07.2016
@author			Tim Elberfeld
@todo           update the nearest neighbor query to take the correct minimum distance
*/
void PlaceBranchCuts(   int32* pnResidueMap, int32 nNumResidues, uint8* pcBitflags,
                        ArrayDim nDimension, int32 nMaxCutLength, IntList* ResidueIndexList)
{
    /* ------ nearest neighbor search ------ */
    static int32    nNumDim = 2;
    int32           nFoundResIndex;
    double          dMaxSearchRadius = (double) nMaxCutLength;
    double          dDistNeighbor = 0.0;
    Point           ptCenter = {.x = 0, .y = 0};
    Point           ptFoundRes = {.x = 0, .y = 0};
    KDTree*         ResidueTree = NewKDTree(nNumDim);
    
    InsertResidueCoordsToKDTree(pnResidueMap, nDimension, ResidueTree, ResidueIndexList);
    
    /* ------ residue management ------ */
    double          dDistBorder = 0.0;
    double          dSmallestDist = 0.0;
    int32           nXActive = 0, nYActive = 0;
    int32           nXNearest = 0, nYNearest = 0;
    int32           nTotalCharge = 0;
    int32           nUnbalanced = ResidueIndexList->size;
    Point           ptBest; /*  best candiate point for placing branch cut at the end of the loop */
    Point           ptBorder = {.x = 0, .y = 0};
    Stack*          ActiveList = NewStack(nNumResidues);
    bool            bBreakTwice = false;
    
    while(nUnbalanced > 0)
    {
        for(IntNode* iter = ResidueIndexList->root;
            iter != NULL;
            iter = iter->next)
        {
            if(NOT(test_flag(pcBitflags[iter->x], FLAG_BALANCED)))
            {
                nTotalCharge = pnResidueMap[iter->x]; /* start with the charge of the first residue */
                set_flag(pcBitflags[iter->x], FLAG_ACTIVE);
                PushStack(&ActiveList, iter->x);
            
                for (StackNode* iResActive = ActiveList->root;
                     iResActive != NULL;
                     iResActive = iResActive->next)
                {
                    ConvertTo2DIndex(iResActive->x, nDimension.w, &ptCenter.x, &ptCenter.y);
                    GetNearestBorderPixel(ptCenter.x, ptCenter.y, nDimension, &ptBorder, &dDistBorder);
                    
                    KDTree* tmpTree = CopyKDTree(ResidueTree);
                    dDistNeighbor = 0.0;
                    while(dDistNeighbor < (dDistBorder + BRCUT_BORDER_MARGIN))
                    {
                        dDistNeighbor = QueryNearestResidue(tmpTree, ptCenter.x, ptCenter.y, &ptFoundRes, 1.0, dMaxSearchRadius);
                        nFoundResIndex = ptFoundRes.y * nDimension.w + ptFoundRes.x;
                        
                        if((dDistBorder + BRCUT_BORDER_MARGIN) > dDistNeighbor)
                        {
                            if(NOT(test_flag(pcBitflags[nFoundResIndex], FLAG_ACTIVE)))
                            {
                                if(NOT(test_flag(pcBitflags[nFoundResIndex], FLAG_BALANCED)))
                                {
                                    nTotalCharge += pnResidueMap[nFoundResIndex];
                                    set_flag(pcBitflags[nFoundResIndex], FLAG_BALANCED);
                                    nUnbalanced--;
                                }
                                set_flag(pcBitflags[nFoundResIndex], FLAG_ACTIVE);
                                PushStack(&ActiveList, nFoundResIndex);
                            }
                            
                            BresenhamLine(pcBitflags,
                                          ptCenter.x, ptCenter.y,
                                          ptFoundRes.x, ptFoundRes.y,
                                          nDimension.w, FLAG_BRANCH_CUT, true);
                            SaveAsBMP_uint8(pcBitflags, nDimension, CMAP_GRAY, "bitflag_own.bmp");
                        }
                        else
                        {
                            nTotalCharge = 0;
                            BresenhamLine(  pcBitflags,
                                          ptCenter.x, ptCenter.y,
                                          ptBorder.x, ptBorder.y,
                                          nDimension.w, FLAG_BRANCH_CUT, true);
                            SaveAsBMP_uint8(pcBitflags, nDimension, CMAP_GRAY, "bitflag_own.bmp");
                        }
                        
                        if(nTotalCharge == 0)
                        {
                            bBreakTwice = true;
                            break; /* we need to break out of the PointfArray loop AND the ActiveList loop! */
                        }
                        
                        KDRemove2(tmpTree, (double)ptFoundRes.x, (double)ptFoundRes.y);
                    }
                    
                    FreeKDTree(tmpTree);
                    
                    if(bBreakTwice)
                    {
                        bBreakTwice = false;
                        break; /* again */
                    }
                } /* end for(StackNode* iResActive = ActiveList->root; ...)*/
            } /* end if not balanced */
            
            if(nTotalCharge != 0)
            {
                /* find the residue in ActiveList that is closest to a border pixel */
                dSmallestDist = DBL_MAX;
                
                for(StackNode* iActive = ActiveList->root;
                    iActive != NULL;
                    iActive = iActive->next)
                {
                    GetNearestBorderPixel(nXActive, nYActive, nDimension, &ptBorder, &dDistBorder);
                    if(dDistBorder < dSmallestDist)
                    {
                        dSmallestDist = dDistBorder;
                        
                        ptBest.x = ptBorder.x;
                        ptBest.y = ptBorder.y;
                    }
                }
                /* place branch cut between border pixel and nearest active pixel */
                BresenhamLine(pcBitflags,
                              nXNearest, nYNearest,
                              ptBest.x, ptBest.y,
                              nDimension.w, FLAG_BRANCH_CUT, true);
                SaveAsBMP_uint8(pcBitflags, nDimension, CMAP_GRAY, "bitflag_own.bmp");
            }
            
            MarkInactiveAndBalanced(ActiveList, nDimension, pcBitflags);
            
        } /* end for all residues in ResidueIndexList */
    }
}

/**
 @brief			Place branch cuts between residues and/or image border
                Primitive brute force algorithm
 @param[in]		pnResidueMap		-	array containing the residue map
 @param[in]		nNumResidues		-	number of residues contained in the map
 @param[inout]	pcBitflags 			-	bitflags array
 @param[in]		nDimension 			-	width and height of the array
 @param[in]		nMaxCutLength 		-	maximum search radius for nearest branch cuts
 @date 			28.07.2016
 @author		Tim Elberfeld
 */
void PlaceBranchCutsPrimitive(int32* pnResidueMap, int32 nNumResidues, uint8* pcBitflags,
                              ArrayDim nDimension, int32 nMaxCutLength)
{
    int32   nTotalCharge = 0;
    int32   kk = 0;
    uint8   boxpxl = 0;
    int32   nIndexBox;
    int32   whalf;
    int32   nXActive = 0, nYActive = 0;
    int32   nXNearest = 0, nYNearest = 0;
    int32   nIndexActive = 0;
    Point   ptCenter;
    Point   ptBorder;
    Point   ptBest;
    double  dDistBorder;
    double  dSmallestDist;
    Stack*  ActiveIndices = NewStack(nNumResidues);
    int32   nMinBoxSize = 3;
    int32   nMaxBoxSize = 2 * nMaxCutLength;
    
    WriteToLogFile(LOGBUFF, "starting placing branch cuts");
    
    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for (int32 xx = 0; xx < nDimension.w; xx++)
        {
            kk = yy * nDimension.w + xx;

            if (test_flag(pcBitflags[kk], FLAG_RESIDUE) &&
                NOT(test_flag(pcBitflags[kk], FLAG_BALANCED)))
            {
                set_flag(pcBitflags[kk], FLAG_BALANCED);
                set_flag(pcBitflags[kk], FLAG_ACTIVE);
                nTotalCharge = pnResidueMap[kk];
                
                ClearStack(ActiveIndices); /* delete elements still in the stack */
                PushStack(&ActiveIndices, kk);
                
                for(int32 w = nMinBoxSize; w < nMaxBoxSize; w += 2)
                {
                    whalf = (int32)((double)w * 0.5);
                    for (uint32 ii = 0; ii < ActiveIndices->size; ii++)
                    {
                        nIndexActive = StackElementAt(ActiveIndices, ii); /* get element with index ii */
                        if(nIndexActive != INT_MAX)
                        {
                            ConvertTo2DIndex(nIndexActive, nDimension.w, &ptCenter.x, &ptCenter.y);
                        
                            for (int32 box_y = ptCenter.y - whalf; box_y <= ptCenter.y + whalf; box_y++)
                            {
                                for (int32 box_x = ptCenter.x - whalf; box_x <= ptCenter.x + whalf; box_x++)
                                {
                                    nIndexBox = box_y * nDimension.w + box_x;
                                
                                    if (box_x < 0 ||
                                        box_x >= nDimension.w ||
                                        box_y < 0 ||
                                        box_y >= nDimension.h ||
                                        nIndexBox == nIndexActive)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        boxpxl = pcBitflags[nIndexBox];
                                        if ((box_x == 0) ||
                                            (box_x == (nDimension.w - 1)) ||
                                            (box_y == 0) ||
                                            (box_y == (nDimension.h - 1)) ||
                                            test_flag(pcBitflags[kk], FLAG_BORDER))
                                        {
                                            nTotalCharge = 0;
                                            GetNearestBorderPixel(box_x, box_y, nDimension, &ptBorder, &dDistBorder);
                                        
                                            BresenhamLine(pcBitflags,
                                                          ptCenter.x, ptCenter.y, /* make the branch cut to the actual center of the image! TE_BUG_16102016*/
                                                          ptBorder.x, ptBorder.y,
                                                          nDimension.w, FLAG_BRANCH_CUT, true);
                                        }
                                        else if(test_flag(pcBitflags[nIndexBox], FLAG_RESIDUE) &&
                                                NOT(test_flag(pcBitflags[nIndexBox], FLAG_ACTIVE)))
                                        {
                                            if(NOT(test_flag(pcBitflags[nIndexBox], FLAG_BALANCED)))
                                            {
                                                nTotalCharge += pnResidueMap[nIndexBox];
                                                set_flag(pcBitflags[nIndexBox], FLAG_BALANCED);
                                            }
                                            PushStack(&ActiveIndices, nIndexBox);
                                            set_flag(pcBitflags[nIndexBox], FLAG_ACTIVE);
                                            
                                            BresenhamLine(pcBitflags,
                                                          box_x, box_y,
                                                          ptCenter.x, ptCenter.y,
                                                          nDimension.w, FLAG_BRANCH_CUT, true);
                                        }
                                        
                                        if(nTotalCharge == 0)
                                        {
                                            goto make_inactive;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if(nTotalCharge != 0)
                {
                    /* find the residue in ActiveList that is closest to a border pixel */
                    dSmallestDist = DBL_MAX;
                    for(uint32 active_cur = 0; active_cur < ActiveIndices->size; active_cur++)
                    {
                        nIndexActive = StackElementAt(ActiveIndices, active_cur);
                        ConvertTo2DIndex(nIndexActive, nDimension.w, &nXActive, &nYActive);
                        
                        GetNearestBorderPixel(nXActive, nYActive, nDimension, &ptBorder, &dDistBorder);
                        if(dDistBorder < dSmallestDist)
                        {
                            dSmallestDist = dDistBorder;
                            nXNearest = nXActive;
                            nYNearest = nYActive;
                            ptBest.x = ptBorder.x;
                            ptBest.y = ptBorder.y;
                        }
                    }
                    /* place branch cut between border pixel and nearest active pixel */
                    BresenhamLine(pcBitflags,
                                  nXNearest, nYNearest,
                                  ptBest.x, ptBest.y,
                                  nDimension.w, FLAG_BRANCH_CUT, true);
                }
                
            make_inactive:
                MarkInactiveAndBalanced(ActiveIndices, nDimension, pcBitflags);
            }
        }
    }
}



/**
@brief			query for k nearest neighbors of pixel (nX, nY).
				The result of the query will be the position of the neighbor (stored in ptFoundRes)
				and the distance to said neighbor (return value)
@param[in]		TreeRoot	-	root of the kdtree to use for the query
@param[in]		nX 			-	x coordinate of the residue whos neighbors should be found
@param[in]		nY 			-	y coordinate of the residue whos neighbors should be found
@param[inout]	ptFoundRes	-	point to store the coordinates of the nearest neighbor
@param[in]		nRange 		-	range to limit the search by distance	
@return 		<double>	-	distance to the found residue as a double value
@date 			09.08.2016
@author			Tim Elberfeld
*/
double QueryNearestResidue(KDTree* TreeRoot, int32 nX, int32 nY,
                           Point* ptFoundRes, double dMinDist, double dRange)
{
	double dDist = -1.0;
	static double dResPos[NUM_DIM];
	
	dResPos[0] = (double) nX;
	dResPos[1] = (double) nY;

    Pointf* ptRef = NewPointf(&dResPos[0], NUM_DIM);
    Pointf* ptFound = FindNearestNeighbor(TreeRoot, ptRef, dMinDist, dRange, &dDist);
    
    ptFoundRes->x = (int)ptFound->p[0];
    ptFoundRes->y = (int)ptFound->p[1];
    
    char* strPointf = PointfToString(ptFound);
    WriteToLogFile(LOGBUFF, "nn found: %s with dist %.3f", strPointf, sqrt(dDist));
    free(strPointf);
    
	return(sqrt(dDist));
}


/**
@brief			mark pixels with indices in pnActivePixels as inactive and balanced
@param[in]		ActiveResidues      -	#Stack containing the currently active residue indices
@param[in]		nDimension          -	array dimensions
@param[inout]	pcBitflags          -	bitflags array where the FLAG_ACTIVE flag will be disabled
@return         <int32>             -   number of residues that were set to FLAG_BALANCED
@date 			01.08.2016
@author			Tim Elberfeld
*/
void MarkInactiveAndBalanced(Stack* ActiveResidues, ArrayDim nDimension, uint8* pcBitflags)
{
    int32 nIndex;
    for(uint32 ii = 0; ii < ActiveResidues->size; ii++)
    {
        nIndex = StackElementAt(ActiveResidues, ii);
        if(IsValidIndex(nIndex, nDimension)) /* NOTE: this might be unnecessary, but just to be safe! */
        {
            delete_flag(pcBitflags[nIndex], FLAG_ACTIVE);
        }
	}
}

/**
@brief			insert the residue coordinates into the kd tree TreeRoot
@param[in]		pnResidueMap 		-	residue map
@param[in]		nDimension			-	dimensions (height and width) of the residue map
@param[inout]	TreeRoot			-	root node of the kd tree, where the indices will be inserted
@param[inout]	pnResidueIndices	-	array to hold the 1D indices of the residues
@date 			29.07.2016
@author			Tim Elberfeld
*/
void InsertResidueCoordsToKDTree(int32* pnResidueMap, ArrayDim nDimension,
                                 KDTree* TreeRoot, IntList* ResidueIndexList)
{
	int32 ii = 0;
	int32 xx = 0;
	int32 yy = 0;
	
	for (IntNode* iter = ResidueIndexList->root; iter != NULL ; iter = iter->next)
	{
		if(iter != NULL)
		{			
			ii = iter->x;
			ConvertTo2DIndex(ii, nDimension.w, &xx, &yy);

			if(IsValidIndex2D(xx, yy, nDimension) && ii > 0)
			{
				KDInsert2(TreeRoot, (double)xx, (double)yy);
			}
			else
			{
				WriteErrorLog(LOGBUFF, "Invalid index %d (%d, %d) in list", ii, xx, yy);		
			}
		}
		else
		{
			WriteErrorLog(LOGBUFF, "NULL iterator. list %p, ->root %p, ->end %p, ->size %d",
                        ResidueIndexList, ResidueIndexList->root,
                        ResidueIndexList->end, ResidueIndexList->size);
		}		
	}	
}


/**
@brief			unwrap phase image from branch cuts in pcBitflags
@param[in]		pdWrappedPhase	-	wrapped phase image
@param[in]		pcBitflags 		-	bitflags array containing the branch cut information
@param[in]		nDimension 		-	dimensions of wrapped phase, bitflags and unwrapped phase arrays
@param[inout]	pdUnwrapped		-	unwrapped result phase	
@date 			28.07.2016
@author			Tim Elberfeld
@todo           take out the heatmap
*/
void UnwrapFromBranchCuts(double* pdWrappedPhase, uint8* pcBitflags, ArrayDim nDimension, double* pdUnwrapped)
{
    WriteToLogFile(LOGBUFF, "unwrapping pixels");

    int32 nToGo = (nDimension.h) * (nDimension.w);
    Stack* AdjoinList = NewStack(nToGo / 2);  /* make an empty list to store the pixels that will be visited */
    int32 nIndex;
    
    /* what follows is basically a region labelling */
    double val;
    for (int32 yy = 0; yy < nDimension.h; yy++)
    {
        for(int32 xx = 0; xx < nDimension.w; xx++)
        {
            nIndex = yy * nDimension.h + xx;
            
            if(NOT(test_flag(pcBitflags[nIndex], FLAG_AVOID))) /* enter the "filling" algorithm if valid pixel */
            {
                set_flag(pcBitflags[nIndex], FLAG_UNWRAPPED);
                val = pdWrappedPhase[nIndex];
                pdUnwrapped[nIndex] = val;
                PushStack(&AdjoinList, nIndex);
                
                while(AdjoinList->size > 0)
                {
                    nIndex = PopStack(AdjoinList);
                    UpdateAdjoinList(nIndex, pdWrappedPhase, pdUnwrapped, pcBitflags, nDimension, AdjoinList);
                }
            }
        }
    }

    WriteToLogFile(LOGBUFF, "unwrapping branch cuts");
    /*  NOTE(tim): this could be made more efficient by storing the locations of the branch cuts
        seperately and only looping over those */
    int32 nElements = nDimension.w * nDimension.h;
    Stack* SkippedPixels = NewStack(nElements);

    for (int32 ii = 0; ii < nElements; ii++)
    {
        if(test_flag(pcBitflags[ii], FLAG_BRANCH_CUT))
        {
            if (NOT(test_flag(pcBitflags[ii - 1], FLAG_BRANCH_CUT)))
            {
                pdUnwrapped[ii] = (pdUnwrapped[ii - 1] +
                    GradientNormalized(pdWrappedPhase[ii], pdWrappedPhase[ii - 1], true));
                set_flag(pcBitflags[ii], FLAG_UNWRAPPED);
            }
            else if (NOT(test_flag(pcBitflags[ii + 1], FLAG_BRANCH_CUT)))
            {
                pdUnwrapped[ii] = (pdUnwrapped[ii + 1] +
                    GradientNormalized(pdWrappedPhase[ii], pdWrappedPhase[ii - 1], true));
                set_flag(pcBitflags[ii], FLAG_UNWRAPPED);
            }
            else if (NOT(test_flag(pcBitflags[ii - nDimension.w], FLAG_UNWRAPPED)))
            {
                pdUnwrapped[ii] = (pdUnwrapped[ii - nDimension.w] +
                                   GradientNormalized(pdWrappedPhase[ii], pdWrappedPhase[ii-nDimension.w], true));
                set_flag(pcBitflags[ii], FLAG_UNWRAPPED);
            }
            else if (NOT(test_flag(pcBitflags[ii + nDimension.w], FLAG_UNWRAPPED)))
            {
                pdUnwrapped[ii] = (pdUnwrapped[ii + nDimension.w] +
                    GradientNormalized(pdWrappedPhase[ii], pdWrappedPhase[ii - nDimension.w], true));
                set_flag(pcBitflags[ii], FLAG_UNWRAPPED);
            }
            else
            {
                set_flag(pcBitflags[ii], FLAG_POSTPONED); /* set unwrapping to postponed */
                PushStack(&SkippedPixels, ii); /* save for later */
            }
        }
    }

    /* try unwrapping the pixels that were skipped in the previous step */
    int32 ii;
    while (SkippedPixels->size > 0)
    {
        ii = PopStack(SkippedPixels);

        if (test_flag(pcBitflags[ii], FLAG_POSTPONED)) /* just fill in the missing value with the mean value */
        {
            int32 nX = 0;
            int32 nY = 0;
            ConvertTo2DIndex(ii, nDimension.w, &nX, &nY);
            double sum = 0;
            int32 nn = 0;
            for (int32 yy = nY - 1; yy <= nY + 1; yy++)
            {
                for (int32 xx = nX - 1; xx <= nX + 1; xx++)
                {
                    nIndex = yy * nDimension.w + xx;
                    if(test_flag(pcBitflags[nIndex], FLAG_UNWRAPPED) && (nIndex != ii))
                    {
                        sum += pdUnwrapped[nIndex];
                        nn++;
                    }
                }
            }

            pdUnwrapped[ii] = (sum / (double)nn); 
            delete_flag(pcBitflags[ii], FLAG_POSTPONED); /* remove the postponed flag */
        }
    }

    FreeStack(SkippedPixels);
    FreeStack(AdjoinList);
    WriteToLogFile(LOGBUFF, "finished unwrapping");
}

/**
@brief     		Udpate the adjoin list
@param[in]  	nIndex          -   the index to use as a source to check neighbors
@param[in] 	 	pdWrappedPhase  -   wrapped phase array
@param[inout]	pdUnwrapped		-	unwrapped phase array
@param[inout]	pcBitflags 		-	bitflags array containing the branch cut information
@param[in]		nDimension		-	height and width of the arrays
@param[inout]	AdjoinList		-	list of pixels that are still to be unwrapped
@date       	02.09.2016
@author     	Tim Elberfeld
*/
void UpdateAdjoinList(int32 nIndex,
                      double* pdWrappedPhase,
                      double* pdUnwrapped,
                      uint8* pcBitflags,
                      ArrayDim nDimension,
                      Stack* AdjoinList)
{
    int32 xx, xxn, yy, yyn, nNeighborIndex;
    ConvertTo2DIndex(nIndex, nDimension.w, &xx, &yy);
    
    if(!test_flag(pcBitflags[nIndex], FLAG_UNWRAPPED))
    {
        ConvertTo2DIndex(nIndex, nDimension.w, &xx, &yy);
        WriteToLogFile(LOGBUFF,"(%d %d) not unwrapped", xx, yy);
        PushStack(&AdjoinList, nIndex); /* re insert the point into the adjoin list and postpone unwrapping */
        return;
    }
                                                                                                          
    static int32 nNeighbors = 4;
    static int32 nNeighborX[] = {-1, 1,  0, 0};
    static int32 nNeighborY[] = { 0, 0, -1, 1};
    
    ConvertTo2DIndex(nIndex, nDimension.w, &xx, &yy);

    for(int32 n = 0; n < nNeighbors; n++)
    {
        yyn = (yy + nNeighborY[n]);
        xxn = (xx + nNeighborX[n]);
        nNeighborIndex = yyn * nDimension.w + xxn;
        
        if(!IsValidIndex(nNeighborIndex, nDimension))
        {
            continue; /* don't test if invalid index */
        }
        
        if(!test_flag(pcBitflags[nNeighborIndex], FLAG_AVOID))
        {
            UnwrapPixel(pdWrappedPhase, pdUnwrapped, nIndex, nNeighborIndex, nDimension);
            PushStack(&AdjoinList, nNeighborIndex);
            set_flag(pcBitflags[nNeighborIndex], FLAG_UNWRAPPED);
        }
    }
}

/**
@brief      get the next already unwrapped neighbor of the pixel at nIndex
@param[in]  nIndex      -   the index to find a neighbor from
@param[in]  pcBitflags  -   bitflags array that stores the information which pixels are unwrapped
@param[in]  nDimension  -   height and width of the bitflags array
@return 	<int32>		-	index of next neighbor, or - if none was found (-1)
@note       This uses the 4-neighborhood! in this case it is invalid to use the 8-neighborhood
@date       02.09.2016
@author     Tim Elberfeld
*/
int32 GetNextUnwrappedNeighbor(int nIndex, uint8* pcBitflags, ArrayDim nDimension)
{
    static int32 nNeighbors = 4;
    static int32 nNeighborX[] = { 0, 0, -1, 1};
    static int32 nNeighborY[] = {-1, 1,  0, 0};
    
    int32 nNeighborIndex;
    for(int32 n = 0; n < nNeighbors; n++)
    {
        nNeighborIndex = nIndex + (nNeighborY[n] * nDimension.w) + nNeighborX[n];
        
        if(IsValidIndex(nNeighborIndex, nDimension))
        {
            if(test_flag(pcBitflags[nNeighborIndex], FLAG_UNWRAPPED))
            {
                return nNeighborIndex;
            }
        }
    }
    return(-1);
}


/**
@brief          Unwrap a single pixel
@param[in]      pdWrappedPhase  -   wrapped phase array
@param[inout]   pdWrapped       -   result array to be filled with unwrapped pixel values
@param[in]      nIndex          -   index of the current pixel
@param[in]      nDimension      -   height and width of the arrays
@date           01.09.2016
@author         Tim Elberfeld
*/
void UnwrapPixel(double* pdWrappedPhase, double* pdUnwrapped, int32 nIndex, int32 nNeighborIndex, ArrayDim nDimension)
{
    double dGrad;
    if(nIndex < nNeighborIndex)
    {
        dGrad = - GradientNormalized(pdWrappedPhase[nIndex], pdWrappedPhase[nNeighborIndex], true); /* NOTE(tim): - sign!! */
    }
    else
    {
        dGrad = GradientNormalized(pdWrappedPhase[nNeighborIndex], pdWrappedPhase[nIndex], true);
    }
    double dNewVal = pdUnwrapped[nIndex] + dGrad;
    pdUnwrapped[nNeighborIndex] = dNewVal;
}
