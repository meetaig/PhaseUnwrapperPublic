#include "general.h"
/**
 @file      general.c
 @author    Tim Elberfeld
 @date      25.07.2016
 @brief     general purpose functionality for most algorithm and general programming
*/

/**
 @brief     Simple equality comparison of two floating point numbers.
 This is by no means perfect but is sufficient for the task here.
 For more information about the topic see:
 https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 @param[in] A               -   first number
 @param[in] B               -   second number
 @param[in] dMaxRelativeErr -   maximum relative error for the equality to hold
 where this function is taken from.
 @date      26.09.2016
 @author    Tim Elberfeld
 */
bool AlmostEqualRelative(double A, double B, double dMaxRelativeErr)
{
    double dDiff = fabs(A - B);
    A = fabs(A);
    B = fabs(B);
    
    double dLargest = __max(A, B);
    
    if (dDiff <= dLargest * dMaxRelativeErr)
    {
        return(true);
    }
    
    return(false);
}


/**
 @brief     Get a random double precision floating point number
 in range [0.0, @p dMax].
 @param[in] dMax    -   maximum of desired range
 @date      13.08.2016
 @author    Tim Elberfeld
 */
double GetRandomDouble(double dMax)
{
    return (double)rand()/(double)(RAND_MAX / dMax);
}

/**
 @brief     Linear interpolation function
 @param[in] d1  -   first value
 @param[in] d2  -   second value
 @param[in] t   -   position of the value to return between @p d1 and  @p d2 in interval [0, 1],
 where 0 is @p d1 and 1 is @p d2
 @date      09.09.2016
 @author    Tim Elberfeld
 */
double Lerp(double d1, double d2, double t)
{
    return (1-t)*d1 + t*d2;
}

/**
 @brief          assign a color nLineColor to the array pImage at index nIndex.
 Differentiate between assignment and binary or by boolean bIsFlagValue
 @param[inout]   pImage          -   the image array
 @param[in]      nIndex          -   index at which the color should be assigned
 @param[in]      bIsFlagValue    -   should the pixel be overwritten or should the color
 be treated as a flag value and be combined with the
 original pixel value?
 @param[in]      nLineColor      -   the color to be assigned
 @date           25.07.2016
 @author         Tim Elberfeld
 */
void AssignColor(uint8* pImage, int32 nIndex, bool bIsFlagValue, uint8 nLineColor)
{
    if(bIsFlagValue)
    {
        pImage[nIndex] |= nLineColor;
    }
    else
    {
        pImage[nIndex] = nLineColor;
    }
}

/**
 @brief			Compute the normalized (phase) gradient.
 If bIsWrapped is not 0 (i.e. true) the gradient is computed as wrapped phase gradient.
 The wrapped phase data has to be in an interval -1 to 1, not in the interval -\pi to \pi!
 @param[in]		d1			-	first value
 @param[in]		d2			-	second value
 @param[in]		bIsWrapped 	-	should the gradient value be computed as wrapped phase gradient?
 @date			25.07.2016
 @author			Tim Elberfeld
 */
double GradientNormalized(double d1, double d2, bool bIsWrapped)
{
    double dResult = d1 - d2;
    
    if(bIsWrapped)
    {
        if (dResult > 0.5)
        {
            dResult -= 1.0;
        }
        if (dResult < -0.5)
        {
            dResult += 1.0;
        }
    }
    return dResult;
}

/**
 @brief          convert 1d index to 2d index: ii -> xx, yy
 @param[in]      nIndex      -   1d index
 @param[in]      nRowPitch   -   pitch i.e. width of the array
 @param[inout]   rnX         -   variable to hold the x coordinate
 @param[inout]   rnY         -   variable to hold the y coordinate
 @date           29.07.2016
 @author         Tim Elberfeld
 */
void ConvertTo2DIndex(int32 nIndex, int32 nRowPitch, int32* rnX, int32* rnY)
{
    (*rnX) = (nIndex % nRowPitch);
    (*rnY) = (nIndex / nRowPitch);
}

/**
 @brief		wrap an index by the maximum index
 @param[in]	nIndex 		-	the index
 @param[in]	nMaxIndex	-	the upper bound that would cause the index to wrap
 @return 	the wrapped index, which is garantied to be inside [0, nMaxIndex-1]
 @date		25.07.2016
 @author		Tim Elberfeld
 */
int32 WrapIndex(int32 nIndex, int32 nMaxIndex)
{
    int nWrappedIndex = nIndex;
    if (nIndex < 0)								/* assuming the behavior (-1 % 3 = -1)! */
    {
        nWrappedIndex = nMaxIndex + (nIndex % nMaxIndex);
    }
    else if(nIndex >= nMaxIndex)
    {
        nWrappedIndex = nIndex % nMaxIndex;
    }
    
    return nWrappedIndex;
}

/**
 @brief      2D version of WrapIndex (input is 2D return but value is 1D)
 @param[in]  x           -   x coordinate
 @param[in]  y           -   y coordinate
 @param[in]  nDimension  -   width and height to limit the indices
 @note       this makes a mirrored index instead of wrapping it, like in the original implementation of Flynn
 @date       25.08.2016
 @author     Tim Elberfeld
 */
int32 WrapIndex2D(int32 x, int32 y, ArrayDim nDimension)
{
    int32 xx = x;
    int32 yy = y;
    
    if(x < 0)
    {
        xx = -x;
    }
    else if(xx >= nDimension.w)
    {
        xx = 2 * nDimension.w - 2 - x;
    }
    
    if(y < 0)
    {
        yy = -y;
    }
    else if(yy >= nDimension.h)
    {
        yy = 2 * nDimension.h - 2 - y;
    }
    
    return yy * nDimension.w + xx;
}


/**
 @brief      2D version of IsValidIndex()
 @param[in]  nX          -   x coordinate
 @param[in]  nY          -   y coordinate
 @param[in]  nDimension  -   array dimensions to test against
 @return     <bool>      -   true if the index is within the bounds, false otherwise
 @date       02.08.2016
 @author     Tim Elberfeld
 */
bool IsValidIndex2D(int32 nX, int32 nY, ArrayDim nDimension)
{
    if((nY < 0) ||
       (nY >= nDimension.h) ||
       (nX < 0) ||
       (nX >= nDimension.w))
    {
        return(false);
    }
    return(true);
}

/**
 @brief          test if the index nIndex lies within the boundaries of nDimension
 @param[in]      nIndex      -   the index to test
 @param[in]      nDimension  -   the array dimensions to test against
 @return         <bool>      -   true if the index is within the bounds, false otherwise
 @date           01.08.2016
 @author         Tim Elberfeld
 */
bool IsValidIndex(int32 nIndex, ArrayDim nDimension)
{
    int32 xx;
    int32 yy;
    ConvertTo2DIndex(nIndex, nDimension.w, &xx, &yy);
    
    return(IsValidIndex2D(xx, yy, nDimension));
}

/**
 @brief          calc number of bytes in an array
 @param[in]      nDimension          -   width and height of an array
 @param[in]      nBytesPerElement    -   size in bytes of 1 element
 @return         <uint64>            -   number of bytes in the array
 @date           29.07.2016
 @author         Tim Elberfeld
 */
size_t GetNumBytes(ArrayDim nDimension, int32 nBytesPerElement)
{
    return((uint64)(nDimension.h * nDimension.w) * (uint64)nBytesPerElement);
}

/**
 @brief		return the reciprocal of a number
 @param[in]	the number
 @return     <double>    -   (1.0/n)
 @date		25.07.2016
 @author		Tim Elberfeld
 */
double Reciprocal(double n)
{
    return((fabs(n) > DBL_EPSILON) ? 1.0/n : 0.0);
}

/**
 @brief          swap two 32bit integer values
 @param[in]      a   -   first value
 @param[in]      b   -   second value
 @date           25.07.2016
 @author         Tim Elberfeld
 */
void Swap(int32* a, int32* b)
{
    if(a != b)
    {
        int t = *a;
        *a = *b;
        *b = t;
    }
}

/**
 @brief      test if a rectangle is within the array dimensions
 @param[in]  rect        -   the rectangle
 @param[in]  nDimension  -   height and width to test against
 @return     <bool>      -   true if the rectangle is within the bounds given by @p nDimension,
 false otherwise
 @date       05.08.2016
 @author     Tim Elberfeld
 */
bool RectInBounds(Rectangle rect, ArrayDim nDimension)
{
    if( (rect.left < 0) ||
       (rect.left >= nDimension.w) ||
       (rect.width < 0) ||
       ((rect.left + rect.width) >= nDimension.w) ||
       (rect.top < 0) ||
       (rect.top >= nDimension.h) ||
       (rect.height < 0) ||
       ((rect.top + rect.height) >= nDimension.h))
    {
        return(false);
    }
    return(false);
}

/**
 @brief         make nSteps linearly spaced samples between dStart and dEnd
 @param[in]     dStart      -   start value
 @param[in]     dEnd        -   end value
 @param[in]     dStart      -   number of samples
 @return        <double*>   -   pointer to the sample array (size is 1 by nSteps)
 */
double* LinSpace(double dStart, double dEnd, int nSteps)
{
    double* pdLinspace = (double*)AllocBytes(nSteps*sizeof(double), "double", sizeof(double));
    double dRange = dEnd - dStart;
    double dValueSpacing = dRange/(nSteps-1);
    for (int32 ii = 0; ii < nSteps; ii++)
    {
        pdLinspace[ii] = dValueSpacing * ii;
    }
    return(pdLinspace);
}

/**
@brief          Swap two values in an array
@param[inout]   pdArray -   the array to swap values in
@param[in]      nNumel  -   number of elements in the array
@param[in]      a       -   first index
@param[in]      b       -   second index
@date           08.09.2016
@author         Tim Elberfeld
*/
void SwapInArray(double* pdArray, uint64 nNumel, uint64 a, uint64 b)
{
    if(a < nNumel && a >= 0 && b < nNumel && b >= 0)
    {
        double tmp = pdArray[a];
        pdArray[a] = pdArray[b];
        pdArray[b] = tmp;
    }
}

/**
@brief          compute difference of two double arrays
@param[in]      pdArr1      -   first array
@param[in]      pdArr2      -   second array
@param[in]      nDimension  -   height and width of all the arrays
@param[inout]   pdDiff      -   array to hold the difference
@date           05.09.2016
@author         Tim Elberfeld
*/
void Diff(const double* pdArr1, const double* pdArr2, ArrayDim nDimension, double* pdDiff)
{
    for(int32 ii = 0; ii < (nDimension.h * nDimension.w); ii++)
    {
        pdDiff[ii] = pdArr1[ii] - pdArr2[ii];
    }
}


/**
@brief          Break an integer up into two divisors that are as close in value as possible.
                This can be regarded as the search for a point (a, b) with a and b being integers
                that is closest to the point (sqrt(n), sqrt(n))
@param[in]      n   -   the integer to break up into divisors
@param[inout]   a   -   pointer to the variable to hold divisor 1
@param[inout]   b   -   pointer to the variable to hold divisor 2
@date           01.09.2016
@author         Tim Elberfeld
*/
void NearlySquareDivisor(int32 n, int32* a, int32* b)
{
    int32 nsqrt = (int32)ceil(sqrt(n));
    *a = nsqrt;
    *b = nsqrt;
    int32 result;
    while(1)
    {
        (*b) = (int32)n/(*a);
        result = (*a) * (*b);
        if (result == n)
        {
            return;
        }
        else
        {
            (*a) -= 1;
        }
    }
}

/**
@brief          reverse byte order of a 32bit floating point array
@param[inout]   pfArray     -   array to swap bytes of 
@param[in]      nElements   -   number of elements in the float array
@date           01.09.2016
@author         Tim Elberfeld
*/
void FlipByteOrder(float* pfArray, uint64 nElements)
{
    uint8* floatToConvert;
    uint8* returnFloat;
    float* pfTemp1 = calloc(1, sizeof(float));
    float* pfTemp2 = calloc(1, sizeof(float));;

    for (int32 ii = 0; ii < nElements; ++ii)
    {
        *pfTemp1 = pfArray[ii];
        floatToConvert = (uint8*) pfTemp1;
        returnFloat = (uint8*) pfTemp2;

        /* reverse byte order  */
        returnFloat[0] = floatToConvert[3];
        returnFloat[1] = floatToConvert[2];
        returnFloat[2] = floatToConvert[1];
        returnFloat[3] = floatToConvert[0];    

        pfArray[ii] = *pfTemp2;
    }

    free(pfTemp1);
    free(pfTemp2);
}

/**
@brief      Extend the memory of a certain chunk of data. This allocates new memory and copies the contents
            of the memory chunk to the new memory. The old memory is then free'd.
@param[in]  pOld        -   pointer to the old memory that should be extended
@param[in]  nOldLength  -   number of bytes in pOld
@param[in]  nNewLength  -   new number of bytes to allocate (if this is less than or equal to nOldLength, nothing happens)
@return     <void*>     -   pointer to the new memory
@date       31.08.2016
@author     Tim Elberfeld
*/
void* ExtendMemory(void* pOld, uint32 nOldLength, uint32 nNewLength)
{
    if(pOld == NULL)
    {
        QuitWithError("Tried to extend invalid memory", ERR_NULL_POINTER);
    }

    if(nOldLength >= nNewLength)    
    {
        WriteErrorLog(LOGBUFF, "tried extending memory from %d to %d - ignoring request", nOldLength, nNewLength);
        return pOld;
    }
    
    /* WriteToLogFile(LOGBUFF, "extending memory from %d bytes to %d bytes", nOldLength, nNewLength); */
    /*void* pNew = (void*) AllocBytes(nNewLength, "uint8", sizeof(uint8));
    memcpy(pNew, pOld, nOldLength);*/
    
    void* pNew = realloc(pOld, nNewLength);
    
    /*if(pOld != NULL)
    {
        FreeBytes(pOld);
    }*/
    if(!pNew)
    {
        QuitWithError("failed to extend memory", ERR_ALLOCATION_FAILED);
    }
    
    return(pNew);
}

/**
@brief      Take a float array and copy it into a double array 
@param[in]  pfArray     -   float array to convert
@param[in]  nElements   -   number of elements in pfArray
@return     <double*>   -   same array as input but converted to double format (this will obviously not improve accuracy)   
@date       31.08.2016
@author     Tim Elberfeld
*/
double* ConvertToDouble(float* pfArray, uint64 nElements)
{
    double* pdNew = (double*)AllocBytes(nElements*sizeof(double), "double", sizeof(double));

    for (int32 ii = 0; ii < nElements; ++ii)
    {
        pdNew[ii] = (double) pfArray[ii];
    }

    return(pdNew);
}

/**
@brief          set values at the border of an array to 0
@param[inout]   pdArray     -   double array
@param[in]      nDimension  -   height and width of the array 
@date           25.08.2016
@author         Tim Elberfeld
*/
void SetBorderInvalid(double* pdArray, ArrayDim nDimension)
{
    for(int32 yy = 0; yy < nDimension.h; yy++)
    {
        if(yy % nDimension.h == 0)
        {
            memset(&pdArray[yy * nDimension.w], 0, nDimension.w * sizeof(double));
        }
        else 
        {
            pdArray[yy * nDimension.w] = 0.0;
            pdArray[yy * nDimension.w + (nDimension.w-1)] = 0.0;
        }
    }
}


/**
@brief 			clear an array to 0
@param[in]		pArray      -   pointer to array
@param[in]		nBufferSize -   number of bytes in the array (NOT number of elements!)
@date			25.07.2016
@author			Tim Elberfeld
*/
void ClearToZero(void* pArray, size_t nBufferSize)
{
    if(pArray == NULL)
    {
        QuitWithError("Invalid array pointer", ERR_NULL_POINTER);
    }
    memset((char*)pArray, 0, nBufferSize);
}

/**
@brief          copy the contents from one array to another and test for validity!
@param[in]      pSrc        -   source array
@param[in]      nSourceSize -   number of bytes in memory pointed to by pSrc   
@param[inout]   pDst        -   destination array
@param[in]      nDestSize   -   number of bytes in memory pointed to by pDst
@date           28.07.2016
@author         Tim Elberfeld
*/
void CopyArray(const void* pSrc, size_t nSourceSize, void* pDst, size_t nDestSize)
{
    if(nDestSize != nSourceSize)
    {
        QuitWithError("Source and destination must be same size", ERR_MEMORY_SIZE_MISMATCH);       
    }

    if(pSrc == NULL)
    {
        QuitWithError("Invalid source array", ERR_NULL_POINTER);
    }
    
    if(pDst == NULL)
    {
        QuitWithError("Invalid destination array", ERR_NULL_POINTER);   
    }
    memcpy(pDst, pSrc, nDestSize);
}

/**
@brief          Invert the values in an array, provided having the maximum value
@param[inout]   pdArray -   the array to invert (type: double)
@param[in]      nNumel  -   number of elements in the array
@param[in]      dMaxVal -   the maximum value
@date           27.07.2016
@author         Tim Elberfeld
*/
void Invert(double* pdArray, int32 nNumel, double dMaxVal)
{
    for (int32 ii = 0; ii < nNumel; ii++)
    {
        pdArray[ii] = dMaxVal - pdArray[ii];
    }
}

/**
@brief          Multiply an array by a scalar
@param[inout]   pdArray -   the array
@param[in]      nNumel  -   number of elements in the array
@param[in]      dScalar -   the scalar to multiply by
@date           08.09.2016
@author         Tim Elberfeld
*/
void Multiply(double* pdArray, int32 nNumel, double dScalar)
{
    for (int32 ii = 0; ii < nNumel; ii++)
    {
        pdArray[ii] *= dScalar;
    }
}

/**
@brief      Take a phase array and wrap it to the interval [-PI, PI]
@param[in]  pdArray     -   the phase array
@param[in]  nNumel      -   number of elements in the array
@return     <double*>   -   wrapped phase array
@date       08.09.2016
@author     Tim Elberfeld
*/
double* WrapPhase(double* pdArray, int32 nNumel)
{
    double* pdResult = (double*) AllocBytes(nNumel*sizeof(double), "double", sizeof(double));
    for (int32 ii = 0; ii < nNumel; ii++)
    {
        pdResult[ii] = fmod(pdArray[ii] + PI, TWOPI) - PI;
    }
    return(pdResult);
}


/**
@brief          get nearest pixel to (nX, nY )at the border of an image
@param[in]      nX          -   x coordinate of the reference pixel
@param[in]      nY          -   y coordinate of the reference pixel
@param[in]      nDimension  -   dimensions of the image
@param[inout]   ptBorder    -   border pixel, return value
@param[inout]   dDistance   -   the distance to the pixel that is returned
@date           01.08.2016
@author         Tim Elberfeld
*/
void GetNearestBorderPixel(int32 nX, int32 nY,  ArrayDim nDimension, Point* ptBorder, double* dDistance)
{
    Point pt0 = { .x = nX, .y = 0 };                    /*    ┌── p0 ───┐   */
    Point pt1 = { .x = nX, .y = (nDimension.h - 1) };   /*    p3 ─┼──── p2  */
    Point pt2 = { .x = (nDimension.w - 1), .y = nY };   /*    │   │     │   */
    Point pt3 = { .x = 0, .y = nY };                    /*    └── p1 ───┘   */

    double dDist0;
    double dDist1;

    int32 nHalfWidth = nDimension.w / 2;
    int32 nHalfHeight = nDimension.h / 2;

    if(nX < nHalfWidth)                             /* choices are pt0, pt1, pt3 -> left half */
    {
        if(nY < nHalfHeight)                        /* pt0 or pt3 -> upper left quarter */
        {
            if(nX < nY)
            {
                *dDistance = (double) nX;           /* distance to pt3 */
                *ptBorder = pt3;
            }
            else
            {
                *dDistance = (double) nY;           /* distance to pt0 */
                *ptBorder = pt0;
            }
        }
        else                                        /* p3 or p1 -> lower left quarter */
        {
            dDist1 = (double) (nDimension.h - nY);  /* distance to pt1 */
            if((double) nX < dDist1)
            {
                *dDistance = (double) nX;            /* distance to pt3 */
                *ptBorder = pt3;
            }
            else
            {
                *dDistance = dDist1;
                *ptBorder = pt1;
            }
        }
    }
    else                                            /* choices are pt0, pt1, pt2 -> right half */
    {
        dDist0 = (double) (nDimension.w - nX);      /* distance to pt2 */
        if(nY < nHalfHeight)                        /* pt0 or pt2 -> upper right quarter */
        {

            if(dDist0 < (double) nY)
            {
                *dDistance = dDist0;
                *ptBorder = pt2;
            }
            else
            {
                *dDistance = (double) nY;            /* distance to pt0 */
                *ptBorder = pt0;
            }
        }
        else                                        /* pt1 or pt2 -> lower right quarter */
        {
            dDist1 = (double) (nDimension.h - nY);  /* distance to pt1 */

            if(dDist0 < dDist1)
            {
                *dDistance = dDist0;
                *ptBorder = pt2;
            }
            else
            {
                *dDistance = dDist1;
                *ptBorder = pt1;
            }
        }
    }
}

/**
@brief      Allocate nNumBytes bytes and return pointer to the memory.
            -> call malloc, initialize the memory to 0 and print the number of bytes allocated to a logfile!
            See also: #MACRO AllocBytes()
@param[in]  nNumBytes       - number of bytes to allocate
@param[in]  strType         - string specifying the type for the allocation (malloc only sees bytes)
@param[in]  nTypeSize       - size of the type specified in strType in bytes   
@param[in]  bLogAllocation  - should the allocation be written to a log file?
@date       27.07.2016
@author     Tim Elberfeld
*/
void* AllocBytesImp(uint64 nNumBytes, char* strType, int32 nTypeSize, bool bLogAllocation)
{
    uint64 nNumel = nNumBytes / nTypeSize;
    void* buff = calloc((size_t)nNumel, nTypeSize); /* allocate memory and init to zero */
    if(buff == NULL)
    {
        QuitWithError("Could not allocate memory", ERR_ALLOCATION_FAILED);
        bLogAllocation = false;
        nNumBytes = 0;
    }

    if(bLogAllocation || nNumBytes > LOG_BYTE_THRESH)
    {
        FILE* pFile = MakeOpenLogFile("allocations.txt", NULL);
        fprintf(pFile, "Allocated %llu bytes of type %s, n = %llu. Location %p", nNumBytes, strType, nNumel, buff);
        CloseFile(pFile);
    } 
    
    return(buff);
}

/**
@brief      Free the memory pointed to by ptr and log the memory location to log file before that.
@param[in]  ptr         - the pointer to free
@param[in]  bLogFree    - should this action be written to a log file?
@date       28.07.2016
@author     Tim Elberfeld
*/
void FreeBytesImp(void* ptr, bool bLogFree)
{    
    if(ptr != NULL)
    {
        free(ptr);
    }
#ifdef DEBUG
    else
    {
        QuitWithError("Tried to free NULL ptr", ERR_FREE_FAILED);
    }
#endif 
}

/**
@brief      Crop an array using the dimensions of a #Rectangle
@param[in]  pdArray         -   the array to crop
@param[in]  nDimension      -   dimensions of the array
@param[in]  newDimensions   -   the new dimensions of the cropped image
@return     <double*>        -   pointer to a cropped copy of @p pdArray
@date       10.10.2016
@author     Tim Elberfeld
*/
double* Crop_double(double* pdArray, ArrayDim nDimension, Rectangle newDimensions)
{
    double* pdNewArray = NULL;
    if (RectInBounds(newDimensions, nDimension))
    {
        pdNewArray = (double*) AllocBytes(newDimensions.width * newDimensions.height * sizeof(double), "double", sizeof(double));
        int32 yoff;
        int32 yorig;

        for (int32 yy = newDimensions.top; yy < newDimensions.top + newDimensions.height; yy++)
        {
            yoff = yy * newDimensions.width;
            yorig = yy * nDimension.w + newDimensions.left;
            memcpy(pdNewArray + yoff, pdArray + yorig, newDimensions.width * sizeof(double));
        }
    }
    return(pdNewArray);
}


/**
@brief      Crop an array using the dimensions of a #Rectangle
@param[in]  pnArray         -   the array to crop
@param[in]  nDimension      -   dimensions of the array
@param[in]  newDimensions   -   the new dimensions of the cropped image
@return     <int32*>        -   pointer to a cropped copy of @p pnArray
@date       10.10.2016
@author     Tim Elberfeld
*/
int32*  Crop_int32(int32* pnArray, ArrayDim nDimension, Rectangle newDimensions)
{
    int32* pnNewArray = NULL;
    if (RectInBounds(newDimensions, nDimension))
    {
        pnNewArray = (int32*)AllocBytes(newDimensions.width * newDimensions.height * sizeof(int32), "int32", sizeof(int32));
        int32 yoff;
        int32 yorig;

        for (int32 yy = newDimensions.top; yy < newDimensions.top + newDimensions.height; yy++)
        {
            yoff = yy * newDimensions.width;
            yorig = yy * nDimension.w + newDimensions.left;
            memcpy(pnNewArray + yoff, pnArray + yorig, newDimensions.width * sizeof(int32));
        }
    }
    return(pnNewArray);
}

/**
@brief      Crop an array using the dimensions of a #Rectangle
@param[in]  pcArray         -   the array to crop
@param[in]  nDimension      -   dimensions of the array
@param[in]  newDimensions   -   the new dimensions of the cropped image
@return     <uint8*>        -   pointer to a cropped copy of @p pcArray
@date       10.10.2016
@author     Tim Elberfeld
*/
uint8*  Crop_uint8(uint8* pcArray, ArrayDim nDimension, Rectangle newDimensions)
{
    uint8* pcNewArray = NULL;
    if (RectInBounds(newDimensions, nDimension))
    {
        pcNewArray = (uint8*)AllocBytes(newDimensions.width * newDimensions.height * sizeof(uint8), "uint8", sizeof(uint8));
        int32 yoff;
        int32 yorig;

        for (int32 yy = newDimensions.top; yy < newDimensions.top + newDimensions.height; yy++)
        {
            yoff = yy * newDimensions.width;
            yorig = yy * nDimension.w + newDimensions.left;
            memcpy(pcNewArray + yoff, pcArray + yorig, newDimensions.width);
        }
    }
    return(pcNewArray);
}




/**
@brief          simple bresenham line algorithm. No check for out of bounds!
                If the end or start point are not in the image this will crash!
@param[inout]   pImage          -   Image to be filled with the line.
@param[in]      nX1, nY1        -   coordinates of the starting point
@param[in]      nX2, nY2        -   coordinates of the end point
@param[in]      nWidth          -   width (pixel pitch) of pImage
@param[in]      nLineColor      -   color to set the line to
@param[in]      bIsFlagValue    -   should the pixel be overwritten or should the color
                                    be treated as a flag value and be combined with the
                                    original pixel value?
@param[in]      bDoLog          -   if true, the operation is logged, if FALSE not
@return         @c true if success, @c false if an error occurred.
@date           25.07.2016
@author         Tim Elberfeld
*/
bool BresenhamLineImp(  uint8* pImage, int32 nX1, int32 nY1, int32 nX2, int32 nY2,
                        int32 nWidth, uint8 nLineColor, bool bIsFlagValue, bool bDoLog)
{
    if(nX1 == nX2 && nY1 == nY2)
    {
        return(false);
    }
    
    WriteToLogFile( LOGBUFF, "line from (%3d, %3d) to (%3d, %3d), color %d%s",
                    nX1, nY1, nX2, nY2, nLineColor, bIsFlagValue ? " as flag" : "");
    
    bool bSteep = false;
    int32 dx = abs(nX2 - nX1);
    int32 dy = abs(nY2 - nY1);
    int32 signx = (int32) copysign(1, (nX2 - nX1));
    int32 signy = (int32) copysign(1, (nY2 - nY1));

    if(dy > dx) /* we swap the "fast" direction. The code says x but actually y is used and vice versa */
    {
        bSteep = true;
        Swap(&nX1, &nY1);
        Swap(&dx, &dy);
        Swap(&signx, &signy);
    }

    int32 k = 0;
    int32 error = (2 * dy) - dx;
    for(int32 ii = 0; ii < dx; ii++)
    {
        k = bSteep ? (nX1 * nWidth + nY1) : (nY1 * nWidth + nX1);
        AssignColor(pImage, k, bIsFlagValue, nLineColor);

        while(error >= 0)
        {
            nY1 += signy;
            error -= (2 * dx);
        }

        nX1 += signx;
        error += (2 * dy);
    }

    k = nY2 * nWidth + nX2;
    AssignColor(pImage, k, bIsFlagValue, nLineColor);

    return(true);
}

/**
@brief          set the color of the image array inside the bounds of rect to nColor 
@param[inout]   pImage      -   pointer to image buffer
@param[in]      nDimension  -   width and height of the image
@param[in]      rect        -   the rectangle to fill
@param[in]      nColor      -   color of the rectangle
@date           05.08.2016
@author         Tim Elberfeld
*/
void PaintRect(uint8* pImage, ArrayDim nDimension, Rectangle rect, uint8 nColor)
{
    if(RectInBounds(rect, nDimension))
    {
        int32 nIndex; 
        int32 nRight = rect.left + rect.width; /* right bound of the rectangle */
        int32 nBottom = rect.top + rect.height; /* bottom bound of the rectangle  */
        for (int32 yy = rect.top; yy < nBottom; ++yy)
        {
            for (int32 xx = rect.left; xx < nRight; ++xx)
            {
                nIndex = yy * nDimension.w + xx;
                pImage[nIndex] = nColor; 
            }
        }
    }
}

/**
@brief          same as PaintRect() but will only paint the outline (1px)
@param[inout]   pImage      -   pointer to image buffer
@param[in]      nDimension  -   width and height of the image
@param[in]      rect        -   the rectangle to fill
@param[in]      cColor      -   color of the rectangle
@date           05.08.2016
@author         Tim Elberfeld
*/
void PaintRectBorder(uint8* pImage, ArrayDim nDimension, Rectangle rect, uint8 cColor)
{
    if(RectInBounds(rect, nDimension))
    {
        /* horizontal lines can be set with memset */
        memset(&pImage[rect.top * nDimension.w + rect.left], cColor, rect.width);
        memset(&pImage[(rect.top + rect.height) * nDimension.w + rect.left], cColor, rect.width);

        /* vertical lines */
        int32 nXStart = rect.left;
        int32 nYStart = rect.top + 1;
        int32 nXEnd = nXStart;
        int32 nYEnd = nYStart + (rect.height - 1);
        BresenhamLine(pImage, nXStart, nYStart, nXEnd, nYEnd, nDimension.w, cColor, false);

        nXStart = rect.left + rect.width - 1;         
        nXEnd = nXStart;
        BresenhamLine(pImage, nXStart, nYStart, nXEnd, nYEnd, nDimension.w, cColor, false);
    }
}

/**
@brief      Create a new 3d array data structure (double precision floating point)
@param[in]  nWidth      -   width of the array
@param[in]  nHeight     -   height of the array
@param[in]  nDepth      -   depth of the array
@return     <Array3f*>  -   pointer to the allocated data structure
@date       07.10.2016
@author     Tim Elberfeld
*/
Array3f* NewArray3f(uint32 nWidth, uint32 nHeight, uint32 nDepth)
{
    Array3f* returnArray = (Array3f*)AllocBytes(sizeof(Array3f), "Array3f", sizeof(Array3f));
    size_t nNumel = nWidth * nHeight * nDepth;

    returnArray->w = nWidth;
    returnArray->h = nHeight;
    returnArray->d = nDepth;
    returnArray->numel = nNumel;
    returnArray->data = (double*)AllocBytes(nNumel * sizeof(double), "double", sizeof(double));

    return(returnArray);
}

/**
@brief      Free a Array3f struct
@param[in]  arr -   the array to free
@date       07.10.2016
@author     Tim Elberfeld
*/
void FreeArray3f(Array3f* arr)
{
    if (arr)
    {
        if (arr->data)
        {
            FreeBytes(arr->data);
        }
        FreeBytes(arr);
    }
}

/**
@brief          Write a value at position (x, y, z) to the Array3f struct
@param[inout]   arr     -   the array to fill
@param[in]      x       -   x coordinate
@param[in]      y       -   y coordinate
@param[in]      z       -   z coordinate
@param[in]      value   -   the value to fill in at the specified position
@date           07.10.2016
@author         Tim Elberfeld
*/
void WriteArray3fAt(Array3f* arr, uint32 x, uint32 y, uint32 z, double value)
{
    if ((x < arr->w) && (y < arr->h) && (z < arr->d))
    {
        uint64 nIndex = z * (arr->w * arr->h) + y * arr->w + x;
        if(nIndex < arr->numel)
        {
            arr->data[nIndex] = value;
        }
        else
        {
            WriteToLogFile(LOGBUFF, "invalid index %llu (%d, %d, %d)", nIndex, x, y, z);
        }
    }
}

