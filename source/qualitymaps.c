#include "qualitymaps.h"
/**
 @file      qualitymaps.c
 @author    Tim Elberfeld
 @date		25.07.2016
 @brief     Implementation of several qualitymaps from the book <br>
            D. C. Ghiglia and M. D. Pritt, "Two-Dimensional Phase Unwrapping: Theory, Algorithms and Software." <br>
            Wiley Blackwell, May 1998. <br>
            http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html
 */

/**
@brief 			Compute a quality map for quality guided phase unwrapping
@param[in]		pdWrappedPhase	-	wrapped phase data (in interval -1, 1)
@param[in]		nMode			-	which kind of quality map? See ::QualityMapMode in header file
@param[in]		nBoxSize		-	size of the box (i.e. 3x3) for the averaging or variance computation
@param[in]		nWidth			-	width of the phase data array
@param[in]		nHeight			-	height of the phase data array
@param[inout]	pdQualityMap	-	array to store the quality map. must be same size as pWrappedPhase
@date			25.07.2016
@author			Tim Elberfeld
*/
void MakeQualityMap(double* pdWrappedPhase, int32 nMode, int32 nWidth, int32 nHeight, int32 nBoxSize, double* pdQualityMap)
{
	if(pdWrappedPhase == NULL)
	{
		QuitWithError("Invalid phase array pointer", ERR_NULL_POINTER);
	}
	
	if(pdQualityMap == NULL)
	{
		QuitWithError("Invalid quality map array pointer", ERR_NULL_POINTER);
	}

	bool doRescale = true;
	ArrayDim nDimension = {.w = nWidth, .h = nHeight};
    
	switch(nMode)
	{
		case QMAP_VARIANCE:					    /* phase derivative variance quality map */
		{
			PhaseDerivativeVariance(pdWrappedPhase, nDimension, nBoxSize, pdQualityMap);
			SetBorderInvalid(pdQualityMap, nDimension);
			break;
		}
		case QMAP_PSEUDO_CORR:					/* phase pseudo-correlation quality map */
		{
            PseudoCorr(pdWrappedPhase, nDimension,  nBoxSize, pdQualityMap);
			doRescale = false;					/* this does not need to be inverted, the values already have the correct range */
			break;
		}
		case QMAP_PHASE_GRADIENT:				/* maximum phase gradient quality map */
		{
			MaxPhaseGradient(pdWrappedPhase, nDimension, nBoxSize, pdQualityMap);
			break;
		}
		default:								/* same as QMODE_VARIANCE */
		{
			PhaseDerivativeVariance(pdWrappedPhase, nDimension, nBoxSize, pdQualityMap);
		}
	}

	if(doRescale)
	{
		int nNumel = nDimension.w * nDimension.h;
		MapToRange_double(pdQualityMap, nNumel, 0.0, 1.0); 	/* map to range 0, 1 */
		Invert(pdQualityMap, nNumel, 1.0); 					/* invert  */
	}
}

/**
@brief			Compute the phase derivative variance quality map
@param[in]		pdWrappedPhase	-	the wrapped phase data
@param[in] 		nBoxSize		-	the size of the window for the variance filter (nBoxSize x nBoxSize)
@param[in]		nDimension		-	width and height of the array
@param[inout]	pdQualityMap	-	the array to hold the quality map information
@date 			25.07.2016
@author			Tim Elberfeld
*/
void PhaseDerivativeVariance(double* pdWrappedPhase, ArrayDim nDimension, int nBoxSize, double* pdQualityMap)
{
	bool bIsAdditive = false;
	int64 nNumBytesDbl = GetNumBytes(nDimension, sizeof(double));
	
	double* pdTmp = (double*) AllocBytes(nNumBytesDbl, "double", sizeof(double));  /* allocate a temporary array to store intermediate results */
	
  	/* WriteToLogFile(Bytes(LOGBUFF), "Computing gradient values in x direction"); */
	GradientFilter(pdWrappedPhase, nDimension, DIR_X, true, pdTmp);

  	/* WriteToLogFile(Bytes(LOGBUFF), "Extracting x gradient variances"); */
	VarianceFilter(pdTmp, nDimension, nBoxSize, bIsAdditive, pdQualityMap);	/* result is stored in pdQualityMap */

  	/* WriteToLogFile(LOGBUFF, "Computing gradient values in y direction"); */
  	GradientFilter(pdWrappedPhase, nDimension, DIR_Y, true, pdTmp);
  	
  	bIsAdditive = true;
  	/* WriteToLogFile(LOGBUFF, "Extracting y gradient variances"); */
	VarianceFilter(pdTmp, nDimension, nBoxSize, bIsAdditive, pdQualityMap);	/* result gets added to  pdQualityMap */

  	FreeBytes(pdTmp);															/* free the temporary memory again */
}

/**
@brief			Compute the max phase gradient quality map
@param[in]		pdWrappedPhase	-	the wrapped phase data
@param[in] 		nBoxSize		-	the size of the window for the variance filter (nBoxSize x nBoxSize)
@param[in]		nDimension		-	width and height of the wrapped phase array
@param[inout]	pdQualityMap	-	the array to hold the quality map information
@date 			25.07.2016
@author			Tim Elberfeld
*/
void MaxPhaseGradient(double* pdWrappedPhase, ArrayDim nDimension, int32 nBoxSize, double* pdQualityMap)
{
	bool bIsAdditive = false;
	uint64 nNumBytesDbl = GetNumBytes(nDimension, sizeof(double));

	double* pdTmp = (double*) AllocBytes(nNumBytesDbl, "double", sizeof(double)); /* allocate a temporary array to store intermediate results */

  	/* WriteToLogFile(LOGBUFF, "Computing gradient values in x direction"); */
	GradientFilter(pdWrappedPhase, nDimension, DIR_X, true, pdTmp);

	/* WriteToLogFile(LOGBUFF, "Computing maximum filter for x gradients"); */
  	MaximumFilter(pdTmp, nDimension, nBoxSize, bIsAdditive, pdQualityMap);

  	/* WriteToLogFile(LOGBUFF, "Computing gradient values in y direction"); */
	GradientFilter(pdWrappedPhase, nDimension, DIR_Y, true, pdTmp);
  	
  	bIsAdditive = false;
  	/* WriteToLogFile(LOGBUFF, "Computing maximum filter for y gradients"); */
	MaximumFilter(pdTmp, nDimension, nBoxSize, bIsAdditive, pdQualityMap);
  	
  	FreeBytes(pdTmp);
}

/**
@brief			Compute the pseudo correlation quality map
@param[in]		pdWrappedPhase	-	the wrapped phase data
@param[in] 		nBoxSize		-	the size of the window for the variance filter (nBoxSize x nBoxSize)
@param[in]		nDimension		-	width and height of the wrapped phase array
@param[inout]	pdQualityMap	-	the array to hold the quality map information
@date 			25.07.2016
@author			Tim Elberfeld
*/
void PseudoCorr(double* pdWrappedPhase, ArrayDim nDimension, int32 nBoxSize, double* pdQualityMap)
{
	bool bIsAdditive = false;
 	int32 nNumel = nDimension.w * nDimension.h;
 	uint64 nNumBytesDbl = GetNumBytes(nDimension, sizeof(double));

	double* pdCos = (double*) AllocBytes(nNumBytesDbl, "double", sizeof(double)); /* allocate a temporary array to store cosine values */
	double* pdSin = (double*) AllocBytes(nNumBytesDbl, "double", sizeof(double)); /* allocate a temporary array to store sine values */
	double dVal;

  	/* WriteToLogFile(LOGBUFF, "Extracting cosine and sine values"); */
  	for (int32 kk = 0; kk < nNumel; kk++)
  	{ 
  		dVal = TWOPI * pdWrappedPhase[kk];
    	pdCos[kk] = cos(dVal);
    	pdSin[kk] = sin(dVal);
    }
  	
  	/* WriteToLogFile(LOGBUFF, "Applying squared average filter to cosines"); */
  	SquaredAverageFilter(pdCos, nDimension, nBoxSize, bIsAdditive, pdQualityMap);

  	bIsAdditive = true;
  	/* WriteToLogFile(LOGBUFF, "Applying squared average filter to sines"); */
	SquaredAverageFilter(pdSin, nDimension, nBoxSize, bIsAdditive, pdQualityMap);
  	
  	/*WriteToLogFile(LOGBUFF, "Computing square root");	*/
  	for (int32 kk = 0; kk < nNumel; kk++) /* this cant be left out, because the quality map values must be in [0, 1] */
  	{
    	pdQualityMap[kk] = sqrt(pdQualityMap[kk]);
	}

	FreeBytes(pdCos);
	FreeBytes(pdSin);
}
