#include "utils.h"
/**
@file   utils.c
@brief  utility functions that did not fit into general.c
@date   25.07.2016
@author Tim Elberfeld
*/

/**
@brief 			Quit the program because of an error and return the exit code nErrCode
@param[in]      strFilename -   name of the file where the error occurred
@param[in]      strCaller   -   name of the function that called this function
@param[in]      nLineNumber -   line number in the file, where the error occurred
@param[in]      strMsg      -   a message string
@param[in]		nErrCode	-	exit code
@date			25.07.2016
@author			Tim Elberfeld
*/
void QuitWithErrorImp(const char* strFilename, const char* strCaller, uint32 nLineNumber, const char* strMsg, int32 nErrCode)
{
    if(nErrCode < ERR_ERROR_CODE_COUNT)
    {
        fprintf(stderr, "Error: %s, %s - in %s() ln %d %s \n", ERR_CODE_STRING[nErrCode], strMsg, strCaller, nLineNumber, strFilename);
    }
    else
    {
        fprintf(stderr, "Error: %s - in %s() ln %d %s \n", strMsg, strCaller, nLineNumber, strFilename);
    }
}

/**
@brief		Test endianness of the machine we're on.
@return	 	<bool>	-	@c true if the system is Big Endian
                        @c false if it is Little Endian
@date 		31.08.2016
@author 	Tim Elberfeld
*/
bool IsBigEndian()
{
  int32 x = 1;
  uint8 *y = (uint8*)&x;

  return(((*y + 48) == 1) ? false : true); 
}

/*
@brief          Convert a 'normal' path to a windows style path (changing `/` to '\\')
@param[inout]   The path to convert
@date           06.10.2016
@author         Tim Elberfeld
*/
void ConvertToWindowsStylePath(char* path)
{
    while (*path != '\0')
    {
        *path++ = (*path == '/') ? (*path = '\\') : (*path = '/');
    }
}
