#include "statistics.h"
/**
 @file      statistics.c
 @author    Tim Elberfeld
 @date		05.08.2016
 @brief     functions to calculate statistics like histograms, min, max, stddev, etc.
 */

/**
@brief      Compute the mean squared error of the difference of two arrays with the same size
@param[in]  pdA     -   First array
@param[in]  pdB     -   Second array
@param[in]  nNumel  -   number of elements in both arrays. 
@date       11.10.2016
@author     Tim Elberfeld
*/
double ComputeMSEDiff_double(double* pdA, double* pdB, uint64 nNumel)
{
    double* pdDiff = (double*)AllocBytes(nNumel * sizeof(double), "double", sizeof(double));
    for (uint64 ii= 0; ii < nNumel; ii++)
    {
        pdDiff[ii] = pdA[ii] - pdB[ii];
    }

    double dMSEDiff = ComputeMSError_double(pdDiff, nNumel);
    FreeBytes(pdDiff);
    return(dMSEDiff);
}

/**
@brief      Compute the mean squared error of the difference of two arrays with the same size
@param[in]  pnA     -   First array
@param[in]  pnB     -   Second array
@param[in]  nNumel  -   number of elements in both arrays.
@date       11.10.2016
@author     Tim Elberfeld
*/
double ComputeMSEDiff_int32(int32* pnA, int32* pnB, uint64 nNumel)
{
    int32* pnDiff = (int32*)AllocBytes(nNumel * sizeof(int32), "int32", sizeof(int32));
    for (uint64 ii = 0; ii < nNumel; ii++)
    {
        pnDiff[ii] = pnA[ii] - pnB[ii];
    }

    double dMSEDiff = ComputeMSError_int32(pnDiff, nNumel);
    FreeBytes(pnDiff);
    return(dMSEDiff);
}

/**
@brief      Compute the mean squared error of the difference of two arrays with the same size
@param[in]  pcA     -   First array
@param[in]  pcB     -   Second array
@param[in]  nNumel  -   number of elements in both arrays.
@date       11.10.2016
@author     Tim Elberfeld
*/
double ComputeMSEDiff_uint8(uint8* pcA, uint8* pcB, uint64 nNumel)
{
    uint8* pcDiff = (uint8*)AllocBytes(nNumel * sizeof(uint8), "uint8", sizeof(uint8));
    for (uint64 ii = 0; ii < nNumel; ii++)
    {
        pcDiff[ii] = pcA[ii] - pcB[ii];
    }

    double dMSEDiff = ComputeMSError_uint8(pcDiff, nNumel);
    FreeBytes(pcDiff);
    return(dMSEDiff);
}

/**
@brief      Compute mean squared error of an already normalized array. 
@param[in]  pdArray -   normalized array
@param[in]  nNumel  -   number of elements in the array
@date       10.10.2016
@author     Tim Elberfeld
*/
double ComputeMSError(double* pdArray, uint64 nNumel)
{
    double delta = 0.0;
    double running_mean = 0.0;
    double m2 = 0.0;
    double x;
    for (uint64 ii = 0; ii < nNumel; ii++)
    {
        x = pdArray[ii];
        delta = x - running_mean;
        running_mean += delta / (ii + 1);
        m2 += delta * (x - running_mean);
    }

    double dErr = (nNumel < 2) ? (0.0) : sqrt(m2 / ((double)nNumel- 1)); /* standard deviation */
    return(dErr);
}

 /**
 @brief     Compute  mean squared error value (standard deviation of normalized image with mean subtracted) of a double array
 @param[in] pdArray    -   the array
 @param[in] nNumel     -   number of elements in the array
 @date      10.10.2016
 @author    Tim Elberfeld
 */
double ComputeMSError_double(double* pdArray, uint64 nNumel)
{
    double dMin = 0.0, dMax = 0.0;
    ComputeMinMax_double(pdArray, nNumel, &dMin, &dMax);
    double* pdNormSignal = Normalize_double(pdArray, nNumel, dMin, dMax);

    double dErr = ComputeMSError(pdNormSignal, nNumel);
    FreeBytes(pdNormSignal);
    return(dErr);
}

/**
@brief     Compute mean squared error value (standard deviation of normalized image with mean subtracted) of a int32 array
@param[in] pnArray    -   the array
@param[in] nNumel     -   number of elements in the array
@date      10.10.2016
@author    Tim Elberfeld
*/
double ComputeMSError_int32(int32* pnArray, uint64 nNumel)
{
    int32 nMin = 0, nMax = 0;
    ComputeMinMax_int32(pnArray, nNumel, &nMin, &nMax);
    double* pdNormSignal = Normalize_int32(pnArray, nNumel, nMin, nMax);

    double dErr = ComputeMSError(pdNormSignal, nNumel);
    FreeBytes(pdNormSignal);

    return(dErr);
}

/**
@brief     Compute mean squared error value (standard deviation of normalized image with mean subtracted) of a uint8 array
@param[in] pcArray    -   the array
@param[in] nNumel     -   number of elements in the array
@date      10.10.2016
@author    Tim Elberfeld
*/
double ComputeMSError_uint8(uint8* pcArray, uint64 nNumel)
{
    uint8 cMin = 0, cMax = 0;
    ComputeMinMax_uint8(pcArray, nNumel, &cMin, &cMax);
    double* pdNormSignal = Normalize_uint8(pcArray, nNumel, cMin, cMax);

    double dErr = ComputeMSError(pdNormSignal, nNumel);
    FreeBytes(pdNormSignal);

    return(dErr);
}

/**
 @brief          function to compute a quantile without sorting the whole array
 @param[inout]   pdArray     -   array that will be partially sorted if the value at niQuantile is in the right place
 @param[in]      nNumel      -   number of elements in the array
 @param[in]      niQuantile  -   quantile to compute (i.e. 0.5 for median)
 @date           08.09.2016
 @author         Tim Elberfeld
 */
double ComputeQuantile(double* pdArray, uint64 nNumel, double dQuantile)
{
    uint64 niQuantile = (uint64)round(dQuantile * (double)nNumel); /* which index would the quantile have in a sorted array? */
    
    double* pdArrayCopy = (double*) AllocBytes(nNumel*sizeof(double), "double", sizeof(double));
    CopyArray(pdArray, (size_t)nNumel, pdArrayCopy, (size_t) nNumel);
    
    ComputeQuantileRec(pdArrayCopy, (size_t) nNumel, 0, nNumel-1, niQuantile);
    
    double dQuantileValue = pdArrayCopy[niQuantile];
    FreeBytes(pdArrayCopy);
    return dQuantileValue;
}

/**
@brief          recursive function to compute a quantile. Should only be called by ComputeQuantile()
@param[inout]   pdArray     -   array that will be partially sorted if the value at niQuantile is in the right place.
                                This will reorder the array partially!
@param[in]      nNumel      -   number of elements in the array
@param[in]      niLeft      -   left index for the quantile search
@param[in]      niRight     -   right index for the quantile search
@param[in]      niQuantile  -   index of the quantile in a sorted array
@date           08.09.2016
@author         Tim Elberfeld
*/
void ComputeQuantileRec(double* pdArray, uint64 nNumel, uint64 niLeft, uint64 niRight, uint64 niQuantile)
{
    uint64 ii, jj;
    if (niRight > niLeft)
    {
        ii = niLeft - 1;
        jj = niRight;

        uint64 niPivot = GetMedianPivot(pdArray, nNumel, niLeft, niRight);
        
        do /* search until ii and jj have passed each other */
        {
            do  /* search from the left */
            {
                ii++;
            } while(pdArray[ii] < niPivot);
            
            do  /* search from the right */
            {
                jj--;
            } while ((pdArray[jj] > niPivot) && (jj > 0));
            
            if(ii < jj)
            {
                SwapInArray(pdArray, nNumel, ii, jj);
            }
        } while(ii < jj);
        
        SwapInArray(pdArray, nNumel, ii, niRight);
        if(ii >= niQuantile)
        {
            ComputeQuantileRec(pdArray, nNumel, niLeft, ii - 1, niQuantile);
        }
        else if(ii < niQuantile)
        {
            ComputeQuantileRec(pdArray, nNumel, ii + 1, niRight, niQuantile);
        }
    }
}

/**
@brief      Calculate the index of the median of three elements and return the index for use as pivot in QuickSort()
@param[in]  pdArray     -   the double array to use for the median calculation
@param[in]  nElements   -   number of elements in @p pdArray
@param[in]  niRight     -   right-most index of current search margin
@param[in]  niLeft      -   left-most index of current search margin
@date       04.10.2016
@author     Tim Elberfeld
*/
uint64 GetMedianPivot(const double* pdArray, uint64 nElements, uint64 niLeft, uint64 niRight)
{
    if (niRight < nElements && niLeft < nElements)
    {
        uint64 niPivot = niRight - niLeft / 2;  /* initial pivot is middle index */
        double dMedian = MedianOfThree(pdArray[niLeft], pdArray[niPivot], pdArray[niRight]);

        if (dMedian == pdArray[niRight]) /* change pivot to the index that holds the median of three */
        {
            niPivot = niRight;
        }
        else if (dMedian == pdArray[niLeft])
        {
            niPivot = niLeft;
        }

        return(niPivot);
    }
    return(niRight);
}


/**
@brief          Quicksort a double array
@param[inout]   pdArray     -   the array to sort
@param[in]      nNumel      -   number of elements in the array
@param[in]      niLeft      -   leftmost index for sorting
@param[in]      niRight     -   rightmost index for sorting
@param[inout]   rnCompares  -   will hold number of comparisons made in the algorithm, when finished
@date           08.09.2016
@author         Tim Elberfeld
*/
void QuickSort(double* pdArray, uint64 nNumel, uint64 niLeft, uint64 niRight)
{
    uint64 ii, jj;
    if(niRight > niLeft)
    {
        ii = niLeft - 1;
        jj = niRight;
        uint64 niPivot = GetMedianPivot(pdArray, nNumel, niLeft, niRight);        
        double dMedian = MedianOfThree(pdArray[niLeft], pdArray[niPivot], pdArray[niRight]);
        
        if(dMedian == pdArray[niRight]) /* change pivot to the index that holds the median of three */
        {
            niPivot = niRight;
        }
        else if (dMedian == pdArray[niLeft])
        {
            niPivot = niLeft;
        }

        do /* search until ii and jj have passed each other */
        {
            do	/* search from the left */
            {
                ii++;
            } while(pdArray[ii] < niPivot);
    
            do  /* search from the right */
            {
                jj--;
            } while ((pdArray[jj] < niPivot) && (jj > 0));
    
            if(ii < jj)
            {
                SwapInArray(pdArray, nNumel, ii, jj);
            }
        
        } while(ii < jj);
        
        SwapInArray(pdArray, nNumel, ii, niRight);
        QuickSort(pdArray, nNumel, niLeft, ii - 1);
        QuickSort(pdArray, nNumel, ii + 1, niRight);
    }
}


/**
@brief      do a printout of the statistics contents
@param[in]  statistic   -   the statistic to print out
@date       07.09.2016
@author     Tim Elberfeld
*/
void PrintStats(Stats* statistic)
{
    if(statistic)
    {
        WriteToLogFile(LOGBUFF,
            "numel %llu , min %.3f, max %.3f, mean %.3f, stddev %.3f, var %.3f",
            statistic->numel, statistic->minval, statistic->maxval,
            statistic->mean, statistic->stddev, statistic->variance);
    }
}

/**
@brief      initialize the Stat struct
@param[in]  statistic   -   the struct to init
@date       07.09.2016
@author     Tim Elberfeld
*/
void InitStats(Stats* statistic)
{
    if(statistic)
    {
        statistic->numel = 0;
        statistic->maxval = -DBL_MAX;
        statistic->minval = DBL_MAX;
        statistic->mean = 0.0;
        statistic->variance = 0.0;
        statistic->stddev = 0.0;
    }
}

/**
@brief          Compute several statistics for a double array
@param[in]      pdArray     -   array to compute statistics on
@param[in]      numel       -   number of elements in pdArray
@param[inout]   statistic   -   struct for the statistical features
@param[in]      bDoPrintOut -   if TRUE, the result is printed after computation
@date           07.09.2016
@author         Tim Elberfeld
@note           this can be easily extended using ComputeQuantile() to also include quartile distance, etc.
*/
void ComputeStats_double(double* pdArray, uint64 numel, Stats* statistic , bool bDoPrintOut)
{
    InitStats(statistic);
    statistic->numel = numel;

    if(pdArray == NULL)
    {
        return;
    }
    
    double x = 0.0;
    double sum = 0.0;
    double running_mean = 0.0;
    double m2 = 0.0;
    double delta;
    
    for (uint64 ii = 0; ii < numel; ii++)
    {
        x = pdArray[ii];
        sum += x;
        delta = x - running_mean;
        running_mean += delta / (ii+1);
        m2 += delta * (x - running_mean);
        
        statistic->maxval = __max(statistic->maxval, x);
        statistic->minval = __min(statistic->minval, x);
    }
    
    statistic->variance = numel < 2 ? 0.0 : (m2 / (numel - 1));
    statistic->stddev = sqrt(statistic->variance);
    statistic->mean = sum / numel;
    
    if(bDoPrintOut)
    {
        PrintStats(statistic);
    }
}
/**
 @copydoc ComputeStats_double
 */
void ComputeStats_int32(int32* pnArray, uint64 numel, Stats* statistic , bool bDoPrintOut)
{
    InitStats(statistic);
    statistic->numel = numel;
    
    if(pnArray == NULL)
    {
        return;
    }
    
    double x = 0.0;
    double sum = 0.0;
    double running_mean = 0.0;
    double m2 = 0.0;
    double delta;
    
    for (uint64 ii = 0; ii < numel; ii++)
    {
        x = pnArray[ii];
        sum += x;
        delta = x - running_mean;
        running_mean += delta / (ii+1);
        m2 += delta * (x - running_mean);
        
        statistic->maxval = __max(statistic->maxval, x);
        statistic->minval = __min(statistic->minval, x);
    }
    
    statistic->variance = numel < 2 ? 0.0 : (m2 / (numel - 1));
    statistic->stddev = sqrt(statistic->variance);
    statistic->mean = sum / numel;
    
    if(bDoPrintOut)
    {
        PrintStats(statistic);
    }
}
/**
 @copydoc ComputeStats_double
*/
void ComputeStats_uint8(uint8* pcArray, uint64 numel, Stats* statistic , bool bDoPrintOut)
{
    InitStats(statistic);
    statistic->numel = numel;
    
    if(pcArray == NULL)
    {
        return;
    }
    
    double x = 0.0;
    double sum = 0.0;
    double running_mean = 0.0;
    double m2 = 0.0;
    double delta;
    
    for (uint64 ii = 0; ii < numel; ii++)
    {
        x = pcArray[ii];
        sum += x;
        delta = x - running_mean;
        running_mean += delta / (ii+1);
        m2 += delta * (x - running_mean);
        
        statistic->maxval = __max(statistic->maxval, x);
        statistic->minval = __min(statistic->minval, x);
    }
    
    statistic->variance = numel < 2 ? 0.0 : (m2 / (numel - 1));
    statistic->stddev = sqrt(statistic->variance);
    statistic->mean = sum / numel;
    
    if(bDoPrintOut)
    {
        PrintStats(statistic);
    }
}

/**
@brief      write the histogram to disk as a csv file. It will be placed in the log/ directory.
            No checks for correct file ending! this has to be done before.
@param[in]  pHisto      -   the histogram to export
@param[in]  strFilename -   filename of the csv file
@return     <int>       -   error code (0 if success)
@date       09.08.2016
@author     Tim Elberfeld
*/
int SaveHistogramAsCSV(Histogram* pHisto, const char* strFilename)
{
    /* Say we have on average 1024 * 1024 px in an image.  */
    /* Then the largest value in a bin can be 1048576 which has 7 digits. */
    /* 12 digits, where 3 are decimals should be enough then */
    /* --> assume 12 digits per double number -> 8 (or 7 and a sign) digits then a point, then 3 decimals */
    int32 nDigits = 12;                      
    int32 nBytes = (int)(ceil((double)pHisto->bins * 1.2)) * (nDigits + 2); /* account for the separator */
    int32 nCurLenData = 0;
    int32 nCurLenLabel = 0;

    static char strSeparator[] = ",";
    char* strLineBufferData = (char*) calloc(nBytes, sizeof(char));
    char* strLineBufferLabel = (char*) calloc(nBytes, sizeof(char));
    
    for (uint32 ii = 0; ii < pHisto->bins; ++ii)
    {
        nCurLenData += sprintf(strLineBufferData + nCurLenData, "%12.3f", pHisto->data[ii]);
        nCurLenLabel += sprintf(strLineBufferLabel + nCurLenLabel, "%12.3f", pHisto->labels[ii]);

        if(ii < (pHisto->bins-1)) /* add a separator for all but the last value */
        {
            nCurLenData += sprintf(strLineBufferData + nCurLenData, "%s ", strSeparator);
            nCurLenLabel += sprintf(strLineBufferLabel + nCurLenLabel, "%s ", strSeparator);
        }

        if((nCurLenData > nBytes) || (nCurLenLabel > nBytes))
        {
            QuitWithError("exceeded string length", ERR_INVALID_MEMORY_ACCESS);
        }
    }
    
    /* write values to file */
    FILE* pFile = MakeOpenLogFile(strFilename, "w"); /* overwrite if file with same name already exists */
    if(pFile != NULL)
    {
        fprintf(pFile, "%s \r\n", strLineBufferData);           
        fprintf(pFile, "%s", strLineBufferLabel);
    }

    free(strLineBufferData);
    free(strLineBufferLabel);
    return CloseFile(pFile);
}

/**
@brief          set labels (bin boundaries) of the histogram
@param[inout]   pHisto      -   the histogram
@param[in]      dBucketSize -   size (width) of the individual buckets
@date           07.08.2016
@author         Tim Elberfeld       
*/
void SetHistogramLabels(Histogram* pHisto, double dBucketSize)
{
    for (uint32 ii = 0; ii < pHisto->bins; ++ii)
    {
        pHisto->labels[ii] = ii * dBucketSize + pHisto->srcmin; 
    }
}

/**
@brief          internal function to apply all the flags that are set after computing it
@param[inout]   pHisto  - pointer to histogram struct
@date           07.08.2016
@author         Tim Elberfeld
*/
void ApplyFlags(Histogram* pHisto)
{
    if(test_flag(pHisto->flags, FLAG_CUMUL))
    {
        /* WriteToLogFile(LOGBUFF, "Converting to cumulative histogram"); */
        MakeCumulative(pHisto);
    }
    
    if(test_flag(pHisto->flags, FLAG_LOG))
    {
        /* WriteToLogFile(LOGBUFF, "Converting to logarithmic histogram"); */
        MakeLogarithmic(pHisto);
    } 

    if(test_flag(pHisto->flags, FLAG_NORM))
    {
        /* WriteToLogFile(LOGBUFF, "normalizing"); */
        MakeLogarithmic(pHisto);  
    }
}

/**
@brief      Compute a cumulative histogram from a "normal" histogram.
            This allocates a new histogram, which must be freed after use by calling
            FreeHisto(pHisto) as ususal.
@param[in]  pHisto  -   the histogram to convert.
@date       07.08.2016
@author     Tim Elberfeld
*/
void NormalizeHistogram(Histogram* pHisto)
{
    if(!test_flag(pHisto->flags, FLAG_NORM))
    {
        for (uint32 ii = 0; ii < pHisto->bins; ++ii)
        {
            pHisto->data[ii] = (pHisto->data[ii] - pHisto->hmin) / pHisto->hmax;
        }
        pHisto->hmin = 0.0;
        pHisto->hmax = 1.0;
        set_flag(pHisto->flags, FLAG_NORM);
    }

}

/**
@brief      Compute a cumulative histogram from a "normal" histogram.
@param[in]  pHisto  -   the histogram to convert.
@date       07.08.2016
@author     Tim Elberfeld
*/
void MakeCumulative(Histogram* pHisto)
{
    if(test_flag(pHisto->flags, FLAG_CUMUL))
    {
        pHisto->hmin = pHisto->data[0];      /* minimum value is by default the first value in the data array */
        double dCurVal;
        for (uint32 ii = 1; ii < pHisto->bins; ++ii)
        {
            dCurVal = pHisto->data[ii];
            pHisto->data[ii] = pHisto->data[ii-1] + dCurVal;
        }

        pHisto->hmax = pHisto->data[pHisto->bins - 1]; /* last one is always the largest value in a histogram */
        set_flag(pHisto->flags, FLAG_CUMUL);
    } 
}

/**
@brief          convert to logarithmic histogram
@param[inout]   pHisto  -   histogram to convert   
@date           07.08.2016
@author         Tim Elberfeld
*/
void MakeLogarithmic(Histogram* pHisto)
{
    if(!test_flag(pHisto->flags, FLAG_LOG))
    {
        /* recompute the min and max values */
        pHisto->hmin = DBL_MAX;
        pHisto->hmax = -DBL_MAX;
        for (uint32 ii = 0; ii < pHisto->bins; ++ii)
        {
            if(pHisto->data[ii] > 0) /* must be greater than 0 to be a valid operation */
            {
                pHisto->data[ii] = log(pHisto->data[ii]);
                pHisto->hmin = __min(pHisto->data[ii], pHisto->hmin);
                pHisto->hmax = __max(pHisto->data[ii], pHisto->hmax);
            } 
        }
        set_flag(pHisto->flags, FLAG_LOG);
    }
}

/**
@brief          clear the data in a histogram
@param[inout]   pHisto  -   the histogram to reset
                            this does not change the number of bins, nor will it free the memory.
                            It will reset the min and max fields and set the elements in the data field to zero.
                            Flags will not be reset
@date           07.08.2016
@author         Tim Elberfeld
*/
void ClearHistogram(Histogram* pHisto)
{
    int32 nNumBytes = pHisto->bins * sizeof(double);
    memset(pHisto->data, 0, nNumBytes);
    memset(pHisto->labels, 0, nNumBytes);
    
    /* reset min, mac and flags */
    pHisto->hmin = DBL_MAX;
    pHisto->hmax = -DBL_MAX; 
    pHisto->srcmin = DBL_MAX;
    pHisto->srcmax = -DBL_MAX;
}

/**
@brief      save a histogram as a bitmap image
@param[in]  pHisto          - the histogram to convert to an image
@param[in]  strFilename     - the filename for the bitmap
@param[in]  colormap        - colormap to use
@return     <int32>         - error code (0 if success)
@date       05.08.2016
@author     Tim Elberfeld
*/  
int32 SaveHistogramAsBMP(Histogram* pHisto, const char* strFilename, CMAP colormap)
{
    WriteToLogFile(LOGBUFF, "Saving histogram: bins %d, hmin %.3f, hmax %.3f", pHisto->bins, pHisto->hmin, pHisto->hmax);
    int32 nBarHeight;                                             /* bar height (will be calculated per bar obviously) */
    int32 nBarWidth = MIN_BAR_WIDTH;                              /* width of the bars in pixels */
    int32 nBarSpacing = MIN_BAR_SPACING;                          /* px between the bars */
    int32 nImgWidth;

    do
    {
        nImgWidth = (nBarWidth + nBarSpacing) * pHisto->bins;   /* recompute the image width until its at least 512px */
        
        if(nImgWidth < MIN_IMG_WIDTH)
        {
            (nBarWidth <= MAX_BAR_WIDTH) ? nBarWidth++ : nBarSpacing++; /* increase bar width until maximum, then increase spacing */
        }
    }
    while(nImgWidth < MIN_IMG_WIDTH);

    int32 nMaxBarHeight = 256;                                    /* maximum bar height */
    uint8 nColor = 1;                                   /* color of the bars (background is black) */

    uint8 * pImgData = (uint8 *) calloc(nImgWidth * nMaxBarHeight, sizeof(uint8));
    Rectangle curBar;
    ArrayDim nDimension = {.w = nImgWidth, .h = nMaxBarHeight};

    WriteToLogFile(LOGBUFF, "histogram flags %s %s %s",
                            test_flag(pHisto->flags, FLAG_NORM) ? "FLAG_NORM":"",
                            test_flag(pHisto->flags, FLAG_LOG) ? "FLAG_LOG":"",
                            test_flag(pHisto->flags, FLAG_CUMUL) ? "FLAG_CUMUL":"");

    for (uint32 ii = 0; ii < pHisto->bins; ++ii)
    {
        /* calculate height for bar */
        if(test_flag(pHisto->flags, FLAG_NORM))
        {

            nBarHeight = (int32)round((double)nMaxBarHeight * pHisto->data[ii]);  /* if the histogram is normalzed, no fancy conversion needed */
        }
        else
        {
            nBarHeight = (int32)round(((double)nMaxBarHeight / pHisto->hmax) * pHisto->data[ii]); 
        }
        
        
        if(nBarHeight >= nMaxBarHeight) /* if the maximum bin index was surpassed */
        {
            nBarHeight = (nMaxBarHeight - 1);
        }

        curBar.top = ((nMaxBarHeight-1) - nBarHeight);
        curBar.left = (ii * (nBarWidth + nBarSpacing));
        curBar.width = nBarWidth;
        curBar.height = __max(nBarHeight, 1);

        nColor = ((nColor + 2) % 254) + 1;

        PaintRect(pImgData, nDimension, curBar, nColor);
        PaintRectBorder(pImgData, nDimension, curBar, 255);   
    }

    BMP* pBMP = BMP_Create((uint32)nImgWidth, (uint32)nMaxBarHeight, 8); /* make 8 bit rgb bmp */
    BMP_CHECK_ERROR(stderr, -1); /* check if the file was created successfully */

    for (int32 yy = 0; yy < nMaxBarHeight; ++yy)
    {
        for (int32 xx = 0; xx < nImgWidth; ++xx)
        {
            BMP_SetPixelIndex(pBMP, xx, yy, pImgData[yy * nImgWidth + xx]); /* set pixel */
        }
    }
    
    int32 nErrCode = SaveBMP(pBMP, strFilename, colormap);
    free(pImgData);
    BMP_Free(pBMP);
    return nErrCode;
}


/**
@brief      print the values in a histogram with bin index and bounds
@param[in]  pHisto - the histogram
@date       07.08.2016
@author     Tim Elberfeld
*/
void PrintHistogram(Histogram* pHisto)
{
    for (uint32 ii = 0; ii < pHisto->bins; ++ii)
    {
        WriteToLogFile(LOGBUFF, "bin %4d: ]%.2f %.2f] val %.2f", 
                        ii, pHisto->labels[__max(ii-1, 0)], pHisto->labels[ii], pHisto->data[ii]);
    }
}

/**
@brief          Map values in a double array to range
@param[in]      pdArray     -   the array
@param[in]      nDimension  -   width and height of the array  
@param[inout]   pHisto      -   pointer to the histogram. The histogram is initialized already with a pointer to the 
                                histogram data, than can hold pHisto->bins elements of type int.     
@date           05.08.2016
@author         Tim Elberfeld
*/
void ComputeHistogram_double(double* pdArray, ArrayDim nDimension, Histogram* pHisto)
{
    double dMin = 0;
    double dMax = 0;
    uint64 nNumel = nDimension.w * nDimension.h;
    ComputeMinMax_double(pdArray, nNumel, &dMin, &dMax);

    pHisto->srcmin = dMin;
    pHisto->srcmax = dMax;

    double dRange = dMax - dMin;
    double dBucketSize = dRange / (double)(pHisto->bins - 1);
    
    uint32 nBin = 0;
    double array_min_adjusted = 0;
    double new_index_raw = 0;
    if(dBucketSize > 0.0)
    {
        for (uint64 ii = 0; ii < nNumel; ++ii)
        {
            array_min_adjusted = (pdArray[ii] - dMin);
            new_index_raw = array_min_adjusted / dBucketSize;
            nBin = (int32)round(new_index_raw);

            if(nBin >= pHisto->bins)
            {
                nBin = (pHisto->bins - 1);
            }
            UpdateHistogram(pHisto, nBin);
        }
        ApplyFlags(pHisto);
        SetHistogramLabels(pHisto, dBucketSize);
    }
    else
    {
        WriteErrorLog(LOGBUFF, "Too many bins -> bucket size too small");
    }
    PrintHistogramStats(pHisto);
}
/**
 @copydoc ComputeHistogram_double()
 */
void ComputeHistogram_int32(int32* pnArray, ArrayDim nDimension, Histogram* pHisto)
{
    int32 nMin;
    int32 nMax;
    int32 nNumel = nDimension.w * nDimension.h;
    ComputeMinMax_int32(pnArray, nNumel, &nMin, &nMax);

    pHisto->srcmin = (double)nMin;
    pHisto->srcmax = (double)nMax;

    double dRange = (double)(nMax - nMin);
    double dBucketSize = dRange / (double)(pHisto->bins - 1);

    uint32 nBin;
    if(dBucketSize > 0.0)
    {
        for (int32 ii = 0; ii < nNumel; ++ii)
        {
            nBin = (int32)round((double)(pnArray[ii] - nMin) / dBucketSize);
            
            if(nBin >= pHisto->bins)
            {
                nBin = (pHisto->bins - 1);
            } 
            
            UpdateHistogram(pHisto, nBin);
        }
        ApplyFlags(pHisto);
        SetHistogramLabels(pHisto, dBucketSize);
    } 
    else
    {
        WriteErrorLog(LOGBUFF, "Too many bins -> bucket size too small");
    }
}
/**
 @copydoc ComputeHistogram_double()
 */
void ComputeHistogram_uint8(uint8* pcArray, ArrayDim nDimension, Histogram* pHisto)
{
    uint8 cMin;
    uint8 cMax;
    int32 nNumel = nDimension.w * nDimension.h;
    ComputeMinMax_uint8(pcArray, nNumel, &cMin, &cMax);

    pHisto->srcmin = (double)cMin;
    pHisto->srcmax = (double)cMax;

    double dRange = (double)(cMax - cMin);
    double dBucketSize = dRange / (double)(pHisto->bins - 1);

    uint32 nBin;
    if(dBucketSize > 0.0 || (pHisto->bins == 256))
    {
        for (int32 ii = 0; ii < nDimension.w * nDimension.h; ++ii)
        {
            if(pHisto->bins == 256)
            {
                nBin = pcArray[ii]; /* use actual value as bin index */
            }
            else
            {
                nBin = (int32)round((double)(pcArray[ii] - cMin) / dBucketSize);    
            }
            
            if(nBin >= pHisto->bins)
            {
                nBin = (pHisto->bins - 1);
            }

            UpdateHistogram(pHisto, nBin);
        }

        if(pHisto->bins != 256)
        {
            SetHistogramLabels(pHisto, dBucketSize);
        }
        else
        {
            for (int32 ii = 0; ii < 256; ++ii)
            {
                pHisto->labels[ii] = ii;
            }
        }

        ApplyFlags(pHisto);
    }
    else
    {
        WriteErrorLog(LOGBUFF, "Too many bins -> bucket size too small");
    }
}

/**
@brief      print the members of the histogram struct
@param[in]  pHisto  -   pointer to the struct
@date       25.08.2016
@author     Tim Elberfeld  
*/
void PrintHistogramStats(Histogram* pHisto)
{
    WriteToLogFile(LOGBUFF, 
        "histo %p, bins %d, srcmin %.2f, srcmax %.2f, min %.2f, max %.2f", 
        pHisto,
        pHisto->bins, 
        pHisto->srcmin,
        pHisto->srcmax,
        pHisto->hmin,
        pHisto->hmax);
    WriteToLogFile(LOGBUFF,
        "flags%s%s%s",
        test_flag(pHisto->flags, FLAG_NORM) ? " FLAG_NORM " : "",
        test_flag(pHisto->flags, FLAG_LOG) ? "FLAG_LOG " : "",
        test_flag(pHisto->flags, FLAG_CUMUL) ? "FLAG_CUMUL " : "");
}

/**
@brief          update the histogram at bin nBinIndex. Record the min and max while we're at it
@param[inout]   pHisto      -   pointer to the histogram struct
@param[in]      nBinIndex   -   index where to increase the value
@date           05.08.2016
@author         Tim Elberfeld
*/
void UpdateHistogram(Histogram* pHisto, uint32 nBinIndex)
{
    if(nBinIndex >= 0 && nBinIndex < pHisto->bins)
    {
        pHisto->data[nBinIndex]++;
        pHisto->hmin = __min(pHisto->hmin, pHisto->data[nBinIndex]);
        pHisto->hmax = __max(pHisto->hmax, pHisto->data[nBinIndex]);
    }
    else
    {
        WriteErrorLog(LOGBUFF, "invalid index %d / bins %d", 
            nBinIndex, pHisto->bins);
        QuitWithError("invalid bin index", ERR_INDEX_INVALID);
    }   
}

/**
@brief      make an empty histogram struct
@param[in]  nBins           -    number of bins. Memory will be allocated appropriately
@param[in]  flags           -    flag values (if there are any). Pass FLAG_EMPTY, if no flags should be set
@return     <Histogram*>    -    allocated memory for histogram
@date       05.08.2016
@author     Tim Elberfeld
*/
Histogram* NewHistogram(int32 nBins, FLAG flags)
{
    Histogram* pHisto = (Histogram*) calloc(1, sizeof(Histogram));
    pHisto->hmin = DBL_MAX;
    pHisto->hmax = -DBL_MAX;
    pHisto->data = (double*) calloc(nBins, sizeof(double));
    pHisto->labels = (double*) calloc(nBins, sizeof(double));
    pHisto->bins = nBins;
    pHisto->flags = flags; 
    return pHisto;
}

/**
@brief      free the memory of a histogram
@param[in]  pHisto  -   pointer to the memory occupied by the histogram.
@date       05.08.2016
@author     Tim Elberfeld
*/
void FreeHistogram(Histogram* pHisto)
{
    if (pHisto != NULL)
    {
        if(pHisto->data != NULL)
        {
            free(pHisto->data);
        }
        if(pHisto->labels != NULL)
        {
            free(pHisto->labels);
        }
        free(pHisto);
    }
}

/**
@brief			Map values in a double array to range
@param[in]		pdArray	-	the array
@param[in]		nNumel	-	number of elements in the array
@param[in]		dMinVal	-	new minimum value
@param[in]		dMaxVal	-	new maximum value
@date			25.07.2016
@author			Tim Elberfeld
*/
void MapToRange_double(double* pdArray, uint64 nNumel, double dMinVal, double dMaxVal)
{	
    double dCurMinVal = 0;
    double dCurMaxVal = 0;
    ComputeMinMax_double(pdArray, nNumel, &dCurMinVal, &dCurMaxVal);
    double dOldRange = dCurMaxVal - dCurMinVal;

    double dNewRange = dMaxVal - dMinVal;

    for(int32 ii = 0; ii < nNumel; ii++)
	{
		pdArray[ii] = (pdArray[ii] - dCurMinVal)*Reciprocal(dOldRange) * dNewRange + dMinVal;  
	}
}
/**
 @copydoc MapToRange_double()
 */
void MapToRange_int32(int32* pnArray, uint64 nNumel, int32 nMinVal , int32 nMaxVal)
{   
    int32 nCurMinVal = 0;
    int32 nCurMaxVal = 0;
    ComputeMinMax_int32(pnArray, nNumel, &nCurMinVal, &nCurMaxVal);
    
    double dOldRange = (double) (nCurMaxVal - nCurMinVal);
    double dNewRange = (double) (nMaxVal - nMinVal);

    int32 nCur = 0;
    for(int32 ii = 0; ii < nNumel; ii++)
    {
        nCur = (int32)round(((double)(pnArray[ii] - nCurMinVal)/dOldRange) * dNewRange + (double)nMinVal); 
        pnArray[ii] = nCur;
    }
}
/**
 @copydoc MapToRange_double()
 */
void MapToRange_uint8(uint8* pcArray, uint64 nNumel, uint8 cMinVal , uint8 cMaxVal)
{   
    uint8 cCurMinVal = 255;
    uint8 cCurMaxVal = 0;
    ComputeMinMax_uint8(pcArray, nNumel, &cCurMinVal, &cCurMaxVal);
    
    double dOldRange = (double) (cCurMaxVal - cCurMinVal);
    double dNewRange = (double) (cMaxVal - cMinVal);

    double dNewVal;
    for(int32 ii = 0; ii < nNumel; ii++)
    {
        dNewVal = (((double)(pcArray[ii] - cCurMinVal)/dOldRange) * dNewRange + (double)cMinVal);
        pcArray[ii] = (uint8)round(dNewVal);  
    }
}

/**
@brief			Compute minimum and maximum value of a double array
@param[in]		pdArray	-	the array
@param[in]		nNumel	-	number of elements in the array
@param[inout]	dMinVal	-	Variable for the minimum value
@param[inout]	dMaxVal	-	Variable for the maximum value
@date			25.07.2016
@author			Tim Elberfeld
*/
void ComputeMinMax_double(double* pdArray, uint64 nNumel, double* dMinVal, double* dMaxVal)
{
	*dMaxVal = -DBL_MAX;
	*dMinVal = DBL_MAX;
	double dVal = 0;

	for(uint64 ii = 0; ii < nNumel; ii++)
	{
		dVal = pdArray[ii];
		*dMinVal = __min(dVal, *dMinVal);
		*dMaxVal = __max(dVal, *dMaxVal);
	}
}
/**
@copydoc ComputeMinMax_double()
*/
void ComputeMinMax_int32(int32* pnArray, uint64 nNumel, int32* nMinVal, int32* nMaxVal)
{
    *nMaxVal = -INT_MAX;
    *nMinVal = INT_MAX;
    int32 nVal = 0;

    for(uint64 ii = 0; ii < nNumel; ii++)
    {
        nVal = pnArray[ii];
        *nMinVal = __min(nVal, *nMinVal);
        *nMaxVal = __max(nVal, *nMaxVal);
    }
}
/**
 @copydoc ComputeMinMax_double()
 */
void ComputeMinMax_uint8(uint8* pcArray, uint64 nNumel, uint8* cMinVal, uint8* cMaxVal)
{
    *cMaxVal = 0;
    *cMinVal = 255;
    uint8 cVal = 0;
    
    for(uint64 ii = 0; ii < nNumel; ii++)
    {
        cVal = pcArray[ii];
        *cMinVal = __min(cVal, *cMinVal);
        *cMaxVal = __max(cVal, *cMaxVal);
    }
}

/**
@brief      normalize the data of an array and return the result as a double array
@param[in]  p[d, n, c]Array     -   the array to normalize
@param[in]  nElements           -   number of elements in the array
@param[in]  [d, n, c]Min        -   minimum for the normalization
@param[in]  [d, n, c]Max        -   maximum for the normalization
@date       01.09.2016
@author     Tim Elberfeld
*/
double* Normalize_double(const double* pdArray, uint64 nNumel, double dMin, double dMax)
{
    if(pdArray == NULL)
    {
        return NULL;
    }
    
    double* pdNew = (double*) AllocBytes(nNumel*sizeof(double), "double", sizeof(double));
    
    double dRange = dMax - dMin;
    for (uint64 ii = 0; ii < nNumel; ii++)
    {
        pdNew[ii] = pdArray[ii] / dRange - dMin;
    }
    
    return pdNew;
}
/**
 @copydoc Normalize_double()
 */
double* Normalize_int32(const int32* pnArray, uint64 nNumel, int32 nMin, int32 nMax)
{
    if(pnArray == NULL)
    {
        return(NULL);
    }
    
    double* pdNew = (double*) AllocBytes(nNumel*sizeof(double), "double", sizeof(double));
    
    double dRange = (double)nMax - (double)nMin;
    for (uint64 ii = 0; ii < nNumel; ii++)
    {
        pdNew[ii] = (double)pnArray[ii] / dRange - (double)nMin;
    }
    
    return(pdNew);
}
/**
 @copydoc Normalize_double()
 */
double* Normalize_uint8(const uint8* pcArray, uint64 nNumel, uint8 cMin, uint8 cMax)
{
    if(pcArray == NULL)
    {
        return(NULL);
    }
    
    double* pdNew = (double*) AllocBytes(nNumel*sizeof(double), "double", sizeof(double));
    
    double dRange = (double)cMax - (double)cMin;
    for (uint64 ii = 0; ii < nNumel; ii++)
    {
        pdNew[ii] = (double)pcArray[ii] / dRange - (double)cMin;
    }
    
    return(pdNew);
}
