/**
 @file      datagen.c
 @brief     functions to generate data for phase unwrapping
 @date      13.09.2016
 @author    Tim Elberfeld
 */
#include "datagen.h"

/**
 @brief     generate a ramp in an array with values ranging from -dValRange/2 to dValRange/2.
 
 */
double* GenRamp(ArrayDim nDimension, double dValRange, RampMode mode)
{
    return(NULL);
    /*
    double* pdRamp = AllocBytes(GetNumBytes(nDimension, sizeof(double)), "double", sizeof(double));
    
    double dStep = dValRange / (double)nDimension.h;
    double* dRampLine = LinSpace_double(-(dValRange*0.5), dValRange*0.5, nDimension.h);
    int nEnd1 = -1;
    int nEnd2 = -1;
    switch (mode)
    {
        case RAMP_SINGLE:
            break;
        case RAMP_SHEARED:
            nEnd1 = nDimension.w * 0.5;
            break;
        case RAMP_DOUBLE_SHEARED:
            nEnd1 = nDimension.w * 0.5;
            nEnd2 = nDimension.w;
            break;
        default:
            QuitWithError("Ramp mode not supported", ERR_INVALID_PARAM);
            break;
    }
    for (int yy = 0; yy < nDimension.h; yy++)
    {
        if(nEnd1 == -1)
        {
            if(nEnd2 == -1)
            {
                
            }
        }
        else
        {
        }
    }*/
}

/**
 @brief      generate spiral shear example phase
 @param[in]  nDimension  -   shape of the image
 @param[in]  dValueRange -   value range of the ramp in the spiral
 @param[in]  dRadius     -   parameter for the spiral (how many rotations the spiral does)
 @return     <double*>   -   example phase image
 @date       05.07.2016
 @author     Tim Elberfeld
 */
double* GenSpiralShear(ArrayDim nDimension, double dValueRange, double dRadius)
{
    
    double* pdResult = (double*) AllocBytes(GetNumBytes(nDimension, sizeof(double)), "double", sizeof(double));
    uint8* pcResult = (uint8*) AllocBytes(GetNumBytes(nDimension, sizeof(uint8)), "uint8", sizeof(uint8));
    
    Point ptCenter = {nDimension.w /2, nDimension.h / 2};   /* center of the image */
    Point ptPrev1 = ptCenter;                               /*   prevpos is to store the last painted position */
    Point ptPrev2 = ptCenter;
    Point ptBorder = {.x = 0, .y = 0};
    bool bPaint1 = true;                                    /* the routine paints 2 spirals that are shifted by 180 deg */
    bool bPaint2 = true;                                    /* of one of the spirals hits the border, it will not be painted further */
    
    uint8 cColor = 1;
    double dAngleStep = 0.1;                                /* increase in angle */
    double dMaxAngle = 5*TWOPI;
    int32 nNumAngles = (int32)round(dMaxAngle / dAngleStep);
    double angle;
    int32 x1, x2, y1, y2;
    double dDist = 0.0;
    double dFirstArgX, dFirstArgY;
    
    for(int32 ii = 0; ii < nNumAngles; ii++)
    {
        angle = ii * dMaxAngle;
        dFirstArgX = ptCenter.x + angle * dRadius;
        dFirstArgY = ptCenter.y + angle * dRadius;
        
        x1 = (int32)floor(dFirstArgX * cos(angle));
        y1 = (int32)floor(dFirstArgY * sin(angle));
        
        if(bPaint1)
        {
            if(IsValidIndex2D(x1, y1, nDimension))
            {
                BresenhamLine(pcResult, ptPrev1.x, ptPrev1.y, x1, y1, nDimension.w, cColor, false);
                ptPrev1.x = x1;
                ptPrev1.y = y1;
            }
            else
            {
                GetNearestBorderPixel(ptPrev1.x, ptPrev1.y, nDimension, &ptBorder, &dDist);
                BresenhamLine(pcResult, ptBorder.x, ptBorder.y, ptPrev1.x, ptPrev1.y, nDimension.w, cColor, false);
                bPaint1 = false;
            }
        }
        
        x2 = (int32)floor(dFirstArgX * cos(angle + PI)); /* shifted by 180 deg */
        y2 = (int32)floor(dFirstArgY * sin(angle + PI));
        
        if(bPaint2)
        {
            if(IsValidIndex2D(x2, y2, nDimension))
            {
                BresenhamLine(pcResult, ptPrev2.x, ptPrev2.y, x2, y2, nDimension.w, cColor, false);
                ptPrev2.x = x2;
                ptPrev2.y = y2;
            }
            else
            {
                GetNearestBorderPixel(ptPrev2.x, ptPrev2.y, nDimension, &ptBorder, &dDist);
                BresenhamLine(pcResult, ptBorder.x, ptBorder.y, ptPrev2.x, ptPrev2.y, nDimension.w, cColor, false);
                bPaint2 = false;
            }
        }
        
        if(!bPaint1 && !bPaint2)
        {
            break;     /* finish the loop prematurely if both spirals touch the image border */
        }
    }
    
    /* now fill one of the spiral arms with a ramp */
    Point ptSeed = {.x = 1, .y = 1};
    Stack* ToFill = FloodFillList(pcResult, nDimension, ptSeed, cColor);
    
    double* pdRamp = GenRamp(nDimension, dValueRange, RAMP_SINGLE);
    
    for (StackNode* iter = ToFill->root; iter != NULL; iter = iter->next)
    {
        pdResult[iter->x] = pdRamp[iter->x];
        pcResult[iter->x] = cColor;
    }
    uint64 nNumel = GetNumBytes(nDimension, 1);
    for (int32 ii = 0; ii < nNumel; ii++)
    {
        if(pcResult[ii] != cColor)
        {
            pdResult[ii] = (dValueRange * 0.5) - pdRamp[ii];
        }
    }
    
    FreeBytes(pdRamp);
    FreeBytes(pcResult);
    FreeStack(ToFill);
    return(pdResult);
}
