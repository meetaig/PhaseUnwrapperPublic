#include "test.h"
/**
@file   test.c
@brief  functions to test functionality of the PhaseUnwrapper module
@date   29.08.2016
@author Tim Elberfeld
*/

/**
 @brief     Print out the size in bytes of most used datatypes
 @return    <bool>  -   always true
 @date      20.09.2016
 @author    Tim Elberfeld
 */
bool Test_PrintDatatypes()
{
    WriteToLogFile(LOGBUFF, "------- built in datatypes -------");
    WriteToLogFile(LOGBUFF, "uint8*      -> %5d bytes", sizeof(uint8*));
    WriteToLogFile(LOGBUFF, "uint8       -> %5d bytes", sizeof(uint8));
    WriteToLogFile(LOGBUFF, "uint16*     -> %5d bytes ", sizeof(uint16*));
    WriteToLogFile(LOGBUFF, "uint16      -> %5d bytes ", sizeof(uint16));
    WriteToLogFile(LOGBUFF, "uint32*     -> %5d bytes ", sizeof(uint32*));
    WriteToLogFile(LOGBUFF, "uint32      -> %5d bytes ", sizeof(uint32));
    WriteToLogFile(LOGBUFF, "int8*       -> %5d bytes", sizeof(int8*));
    WriteToLogFile(LOGBUFF, "int8        -> %5d bytes", sizeof(int8));
    WriteToLogFile(LOGBUFF, "int16*      -> %5d bytes ", sizeof(int16*));
    WriteToLogFile(LOGBUFF, "int16       -> %5d bytes ", sizeof(int16));
    WriteToLogFile(LOGBUFF, "int32*      -> %5d bytes ", sizeof(int32*));
    WriteToLogFile(LOGBUFF, "int32       -> %5d bytes ", sizeof(int32));
    WriteToLogFile(LOGBUFF, "char*       -> %5d bytes", sizeof(char*));
    WriteToLogFile(LOGBUFF, "char        -> %5d bytes", sizeof(char));
    WriteToLogFile(LOGBUFF, "float*      -> %5d bytes", sizeof(float*));
    WriteToLogFile(LOGBUFF, "float       -> %5d bytes", sizeof(float));
    WriteToLogFile(LOGBUFF, "double*     -> %5d bytes", sizeof(double));
    WriteToLogFile(LOGBUFF, "double      -> %5d bytes", sizeof(double));
    WriteToLogFile(LOGBUFF, "------- own datatypes -------");
    WriteToLogFile(LOGBUFF, "Point       -> %5d bytes ", sizeof(Point));
    WriteToLogFile(LOGBUFF, "Rectangle   -> %5d bytes ", sizeof(Rectangle));
    WriteToLogFile(LOGBUFF, "ArrayDim    -> %5d bytes ", sizeof(ArrayDim));
    WriteToLogFile(LOGBUFF, "Stack       -> %5d bytes ", sizeof(Stack));
    WriteToLogFile(LOGBUFF, "StackNode   -> %5d bytes ", sizeof(StackNode));
    WriteToLogFile(LOGBUFF, "IntList     -> %5d bytes ", sizeof(IntList));
    WriteToLogFile(LOGBUFF, "IntNode     -> %5d bytes ", sizeof(IntNode));
    WriteToLogFile(LOGBUFF, "KDNode      -> %5d bytes ", sizeof(KDNode));
    WriteToLogFile(LOGBUFF, "KDTree      -> %5d bytes ", sizeof(KDTree));
    WriteToLogFile(LOGBUFF, "Hyperrect   -> %5d bytes ", sizeof(Hyperrect));
    WriteToLogFile(LOGBUFF, "Hypersphere -> %5d bytes ", sizeof(Hypersphere));
    
    return(true);
}


/**
@brief	test if the bresenham line algorithm works correctly by creating an image with 254 lines (line number is the color of the line) 
		in a circle
@return <bool> - @c true if the test went successfully, @c false otherwise
@date 	29.08.2016
@author	Tim Elberfeld
*/
bool Test_Bresenham()
{
	WriteToLogFile(LOGBUFF, "testing correctness of bresenham algorithm implementation");
	ArrayDim nDimension = {.w = 512, .h = 512};
	int32 nBytes = nDimension.w * nDimension.h;

	uint8* pImg = (uint8*) AllocBytes(nBytes, "uint8", sizeof(uint8));

	int32 nNumLines = 254;
	double dRadius = 200.0;
	double anglestep = TWOPI / (double)nNumLines;

	Point ptCenter = {.x = nDimension.w / 2, .y = nDimension.h / 2};

	int32 x2 = 0;
	int32 y2 = 0;
	double angle;
	int32 nColor = 1;
	for (int32 i = 0; i < nNumLines; ++i)
	{
		angle = (double)i * anglestep;
		x2 = ptCenter.x + (int32) floor(cos(angle) * dRadius);
		y2 = ptCenter.y + (int32) floor(sin(angle) * dRadius);

		BresenhamLineImp(pImg, ptCenter.x, ptCenter.y, x2, y2, nDimension.w, nColor, false, true);
		nColor++;
	}

	SaveAsBMP_uint8(pImg, nDimension, CMAP_GRAY, "TestBresenham.bmp");
	FreeBytes(pImg);
	return(true); /* how can this test be evaluated better? */
}

#ifndef NUM_DIM
#define NUM_DIM 2
#endif 
/**
@brief		general test case for the kdtree -> adding residues generated from the input phase and then removing them again
@param[in]	pdWrappedPhase	-	array containing the wrapped phase data
@param[in]	nWidth 			-	width of the array
@param[in]	nHeight 		-	height of the array
@return 	<bool>			-	@c true if the test passed, @c false otherwise	
@date 		30.08.2016
@author		Tim Elberfeld
*/
bool Test_KdTree_GeneralTestCase(double* pdWrappedPhase, int32 nWidth, int32 nHeight)
{	
	ArrayDim nDimension = {.w = nWidth, .h = nHeight};
	int32 nBytes = nDimension.w * nDimension.h;
	uint8* pcBitflags = (uint8*) AllocBytes(nBytes, "uint8", sizeof(uint8));
	int32* pnResidueMap = (int32*) AllocBytes(nBytes * sizeof(int32), "int32", sizeof(int32));

	IntList* pResidueIndexList = NewLinkedList();
	MakeResidueMap(pdWrappedPhase, nDimension, pcBitflags, pnResidueMap, pResidueIndexList);

	/* now insert the residues into the tree. */
    KDTree* TreeRoot = NewKDTree(NUM_DIM);

	int32 xx;
	int32 yy;
	for (IntNode* iter = pResidueIndexList->root; iter != NULL; iter = iter->next)
	{
		ConvertTo2DIndex(iter->x, nDimension.w, &xx, &yy);
		KDInsert2(TreeRoot,(double)xx, (double)yy);
	}

	/* now remove them again */
	for (IntNode* iter = pResidueIndexList->root; iter != NULL; iter = iter->next)
	{
		ConvertTo2DIndex(iter->x, nDimension.w, &xx, &yy);
		KDRemove2(TreeRoot, (double)xx, (double)yy);
	}
    
	bool bRetVal = (TreeRoot->root == NULL);

	FreeKDTree(TreeRoot);
	FreeList(pResidueIndexList);
	FreeBytes(pnResidueMap);
	FreeBytes(pcBitflags);

	return(bRetVal); /* this returns true if root is NULL, as it should be  */
}

/**
@brief 	Simple test case for a bug that caused the find_min funciton to return the wrong node
@return <bool>	-	@c true if the tree is empty after inserting and then deleting the nodes
					@c false  otherwise
@date	29.08.2016
@author	Tim Elberfeld
*/
bool Test_KdTree_FindMinBug()
{
/* #define SAVE_GRAPHS 1 */

	WriteToLogFile(LOGBUFF, "testing for find_min bug");
	int32 nDim = NUM_DIM;
	KDTree* TreeRoot = NewKDTree(nDim);	/* construct the kd tree  */
	
	double pos[] = {  2.0,  95.0,
					125.0, 292.0,
					130.0,  58.0,
					 15.0, 215.0,
					 75.0, 123.0 };
	int32 nNumel = sizeof(pos) / (sizeof(pos[0])*nDim);

	for(int32 ii = 0; ii < nNumel; ii++)
	{
		KDInsert2(TreeRoot, pos[ii * nDim], pos[ii * nDim + 1]);
	}

#ifdef SAVE_GRAPHS
	KDTreeToDotFile(TreeRoot, "original_tree.dot");

	char* strFilename = (char*) AllocBytes(256, "char", sizeof(char));
#endif
	for (int32 ii = 0; ii < nNumel; ++ii)
	{
		KDRemove2(TreeRoot, pos[nDim * ii], pos[nDim*ii + 1]);
#ifdef SAVE_GRAPHS
		sprintf(strFilename, "tree_removed%d.dot", ii + 1);
		KDTreeToDotFile(TreeRoot, strFilename);
#endif
	}
#ifdef SAVE_GRAPHS
	KDTreeToDotFile(TreeRoot, "tree_removed_all.dot");
#endif

	bool bRetVal = false;
	if(TreeRoot->root == NULL) 	/* the delete operation was successful if the tree is now empty */
	{
		bRetVal = true;
	}
#ifdef SAVE_GRAPHS
	FreeBytes(strFilename);
#endif

	FreeKDTree(TreeRoot); 			/* free kd tree memory */
	return(bRetVal);
}


/**
 @brief     Test nearest neighbor searching
 @date      15.09.2016
 @author    Tim Elberfeld
 */
bool Test_KdTree_FindNearestNeighbor()
{
    int32 nDim = NUM_DIM;
    KDTree* TreeRoot = NewKDTree(NUM_DIM);	/* construct the kd tree  */
    double dMax = 300;
    double pos[] = {GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax),
                    GetRandomDouble(dMax), GetRandomDouble(dMax)};
    int32 nNumel = sizeof(pos) / (sizeof(pos[0])*nDim);
    for(int32 ii = 0; ii < nNumel; ii++)
    {
        KDInsert2(TreeRoot, pos[ii * nDim], pos[ii * nDim + 1]);
    }
    
    double* pdDistances = (double*) AllocBytes(nNumel*nNumel*sizeof(double), "double", sizeof(double));
    for (int32 ii = 0; ii < (nNumel); ii++)
    {
        for(int32 jj = 0; jj < nNumel; jj++)
        {
            pdDistances[ii*nNumel+jj] = sqrt(EucDistSq(&pos[nDim*ii], &pos[nDim*jj], nDim));
        }
    }
    
    double dDist = 0;
    Pointf* ptRef = NewEmptyPointf(nDim);
    int32 counter = 0;
    for (int32 ii = 0; ii < nNumel; ii++)
    {
        CopyArray(&pos[nDim*ii], nDim*sizeof(double), ptRef->p, nDim*sizeof(double));
        Pointf* ptNN = FindNearestNeighbor(TreeRoot, ptRef, 1.0, 200.0, &dDist);
        dDist = sqrt(dDist);
        
        for(int32 jj = 0; jj < nNumel; jj++)
        {
            if((pdDistances[ii*nNumel+jj] == dDist) &&
               SamePos(ptNN->p, &pos[nDim*jj], ptNN->dim))
            {
                counter++;
            }
        }
        FreePointf(ptNN);
    }
    FreePointf(ptRef);
    
    return(counter == nNumel);
}
#undef NUM_DIM

#ifndef nint
#define nint(x) ((x)>0. ? (int)((x)+0.5) : (int)((x)-0.5))
#endif
/**
 @brief     test if nint() macro and round produce the same output
 @param[in] numel   -   number of random numbers to test with
 @return    <bool>  -   @c true if every test was successful, @c false otherwise
 @date      12.09.2016
 @author    Tim Elberfeld
 */
bool Test_Nint(int32 numel)
{
    double randnum;
    double randnum_nint;
    double randnum_round;
    double dMax = 1;
    double dMin = -1;
    WriteToLogFile(LOGBUFF, "testing equality of nint() and round() with %d random numbers", numel);
    for (int32 ii = 0; ii < numel; ii++)
    {
        randnum = ((double)rand() / (double)(RAND_MAX/(dMax-dMin))) + dMin;
        
        randnum_nint = nint(randnum);
        randnum_round = round(randnum);
        if(randnum_nint != randnum_round)
        {
            WriteToLogFile(LOGBUFF, "randnum %.4f, nint: %.4f, round: %.4f", randnum, randnum_nint, randnum_round);
            return(false);
        }
    }
    return(true);
}

/**
 @brief     Test the performance of different quality maps
 @param[in] pdWrapped       -   wrapped phase
 @param[in] pdOriginal      -   original phase
 @param[in] nDimension      -   height and width of the phase arrays
 @param[in] nMinBox         -   minimum box size for computation of quality map
 @param[in] nMaxBox         -   maximum  "   "    "       "      "     "     "
 @param[in] strDataDumpFile -   filename for the csv file of the test results
 @return    <bool>          -   always true
 @date      20.09.2016
 @author    Tim Elberfeld
 */
bool Test_QualityMapPerformance(double* pdWrapped, double* pdOriginal, ArrayDim nDimension, int32 nMinBox, int32 nMaxBox, char* strDataDumpFileName)
{
    uint64 nBytesQualMap = GetNumBytes(nDimension, sizeof(double));
    static int32 nNumQualMaps = 3;
    static int32 nNumNorms = 5; /* L0, L1 and L2 norm and MeasureUnwrappingSuccess(), ComputeRMSError_double() */
    int32 nNumBoxSizes = (((nMaxBox - nMinBox) / 2) + 1);
    
    Array3f* pData = NewArray3f(nNumQualMaps, nNumNorms, nNumBoxSizes);
    double* pdPhaseVariance = (double*)AllocBytes(nBytesQualMap, "double", sizeof(double));
    double* pdUnwrapped = (double*)AllocBytes(nBytesQualMap, "double", sizeof(double));
    double* pdPseudoCorr = (double*)AllocBytes(nBytesQualMap, "double", sizeof(double));
    double* pdMaxGrad = (double*)AllocBytes(nBytesQualMap, "double", sizeof(double));

    int32 nBoxSizeIndex = 0;
    int32 nQualMapIndex = 0;
    for (int32 nCurrentBoxSize = nMinBox; nCurrentBoxSize <= nMaxBox; nCurrentBoxSize += 2)
    {
        nQualMapIndex = 0;
        MakeQualityMap(pdWrapped, QMAP_VARIANCE, nDimension.w, nDimension.h, nCurrentBoxSize, pdPhaseVariance);
        FlynnUnwrap(pdWrapped, nDimension.w, nDimension.h, pdPhaseVariance, pdUnwrapped);
        SaveAsBMP_double(pdPhaseVariance, nDimension, CMAP_GRAY, "phase_variance_%d.bmp", nCurrentBoxSize);
        SaveAsBMP_double(pdUnwrapped, nDimension, CMAP_GRAY, "unwrapped_phase_variance_%d.bmp", nCurrentBoxSize);

        WriteArray3fAt(pData, nQualMapIndex, 0, nBoxSizeIndex, L0Norm(pdUnwrapped, pdWrapped, pdPhaseVariance, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 1, nBoxSizeIndex, L1Norm(pdUnwrapped, pdWrapped, pdPhaseVariance, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 2, nBoxSizeIndex, L2Norm(pdUnwrapped, pdWrapped, pdPhaseVariance, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 3, nBoxSizeIndex, MeasureUnwrappingSuccess(pdUnwrapped, QMAP_VARIANCE, nDimension.w, nDimension.h)); 
        WriteArray3fAt(pData, nQualMapIndex, 4, nBoxSizeIndex, ComputeMSEDiff_double(pdUnwrapped, pdOriginal, nDimension.w * nDimension.h));
        
        nQualMapIndex++;
       
        MakeQualityMap(pdWrapped, QMAP_PSEUDO_CORR, nDimension.w, nDimension.h, nCurrentBoxSize, pdPseudoCorr);
        FlynnUnwrap(pdWrapped, nDimension.w, nDimension.h, pdPseudoCorr, pdUnwrapped);
        SaveAsBMP_double(pdPseudoCorr, nDimension, CMAP_GRAY, "pseuo_corr_%d.bmp", nCurrentBoxSize);
        SaveAsBMP_double(pdUnwrapped, nDimension, CMAP_GRAY, "unwrapped_pseuo_corr_%d.bmp", nCurrentBoxSize);
        
        WriteArray3fAt(pData, nQualMapIndex, 0, nBoxSizeIndex, L0Norm(pdUnwrapped, pdWrapped, pdPseudoCorr, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 1, nBoxSizeIndex, L1Norm(pdUnwrapped, pdWrapped, pdPseudoCorr, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 2, nBoxSizeIndex, L2Norm(pdUnwrapped, pdWrapped, pdPseudoCorr, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 3, nBoxSizeIndex, MeasureUnwrappingSuccess(pdUnwrapped, QMAP_VARIANCE, nDimension.w, nDimension.h));
        WriteArray3fAt(pData, nQualMapIndex, 4, nBoxSizeIndex, ComputeMSEDiff_double(pdUnwrapped, pdOriginal, nDimension.w * nDimension.h));

        nQualMapIndex++;
      
        MakeQualityMap(pdWrapped, QMAP_PHASE_GRADIENT, nDimension.w, nDimension.h, nCurrentBoxSize, pdMaxGrad);
        FlynnUnwrap(pdWrapped, nDimension.w, nDimension.h, pdMaxGrad, pdUnwrapped);
        SaveAsBMP_double(pdMaxGrad, nDimension, CMAP_GRAY, "max_grad_%d.bmp", nCurrentBoxSize);
        SaveAsBMP_double(pdUnwrapped, nDimension, CMAP_GRAY, "unwrapped_max_grad_%d.bmp", nCurrentBoxSize);
        
        WriteArray3fAt(pData, nQualMapIndex, 0, nBoxSizeIndex, L0Norm(pdUnwrapped, pdWrapped, pdMaxGrad, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 1, nBoxSizeIndex, L1Norm(pdUnwrapped, pdWrapped, pdMaxGrad, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 2, nBoxSizeIndex, L2Norm(pdUnwrapped, pdWrapped, pdMaxGrad, nDimension));
        WriteArray3fAt(pData, nQualMapIndex, 3, nBoxSizeIndex, MeasureUnwrappingSuccess(pdUnwrapped, QMAP_VARIANCE, nDimension.w, nDimension.h));
        WriteArray3fAt(pData, nQualMapIndex, 4, nBoxSizeIndex, ComputeMSEDiff_double(pdUnwrapped, pdOriginal, nDimension.w * nDimension.h));
        
        nBoxSizeIndex++;
    }

    ArrayDim nDataSizeTotal = {.w = pData->w * pData->h, .h = pData->d};
    SaveAsCSV_double(pData->data, nDataSizeTotal, strDataDumpFileName, CSV_DELIM_COMMA, 10);
    
    FreeBytes(pdPhaseVariance);
    FreeBytes(pdPseudoCorr);
    FreeBytes(pdMaxGrad);
    FreeBytes(pdUnwrapped);
    FreeArray3f(pData);
    return(true);
}



