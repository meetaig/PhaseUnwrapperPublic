/**
@file       linked_list.c
@author     Tim Elberfeld
@date       27.07.2016
@brief      simple implementation of a doubly linked list
 
List structure with no garantied contiguous memory. Removing nodes from any position only reassigns pointers and therefore is really fast.
*/
#include "linked_list.h"

/**
 @brief         Compute pairwise distances of points
 @param[in]     PointList   -   list of indices (as 1D index)
 @param[in]     nDimension  -   height and width of the array (to convert the 1D to 2D indices)
 @param[inout]  rnElements  -   number of elements allocated for the return array
 @return        <double*>   -   array containing the pairwise euclidean distances
 @date          16.09.2016
 @author        Tim Elberfeld
 */
double* ComputePairwiseDistances(IntList* PointList, ArrayDim nDimension, int32* rnElements)
{
    int32 nNumel = PointList->size;
    double* pdDistances = AllocBytes(nNumel*nNumel*sizeof(double), "double", sizeof(double));
    
    int32 diffx, diffy;
    IntNode* iter = PointList->root;
    IntNode* iter2;
    int32 x1, x2, y1, y2;
    double dDist;
    
    for (int32 ii = 0;
         ii < nNumel;
         ii++, iter = iter->next)
    {
        ConvertTo2DIndex(iter->x, nDimension.w, &x1, &y1);
        iter2 = iter;
        for (int jj = ii; jj < nNumel; jj++, iter2 = iter2->next)
        {
            ConvertTo2DIndex(iter2->x, nDimension.w, &x2, &y2);
            diffx = (x1 - x2);
            diffy = (y1 - y2);
            dDist = (double)(diffx * diffx) + (double)(diffy * diffy);
            
            pdDistances[ii*nNumel+jj] = sqrt(dDist);
            if(ii != jj)
            {
                pdDistances[jj*nNumel+ii] = sqrt(dDist);
            }
        }
    }
    return(pdDistances);
}


/**
@brief      Copy the contents of an IntList and return the copy
@param[in]  other           -   the list to copy
@return     <IntList*>     -   newly allocated list that is an exact copy of "other" 
@date       05.09.2016
@author     Tim Elberfeld
*/
IntList* CopyList(IntList* other)
{
    if(other == NULL)
    {
        return(NULL);
    }
    
    IntList* NewList = NewLinkedList();
    
    for (IntNode* iter = other->root; iter != NULL; iter = iter->next)
    {
        AppendList(iter->x, NewList);
    }
    
    return(NewList);
}

/**
@brief      make a new empty list. result must be freed after usage using FreeList()!
@return     pointer to a newly allocated IntList struct
@date       01.08.2016
@author     Tim Elberfeld
*/
IntList* NewLinkedList()
{
    IntList* list = AllocBytes(sizeof(IntList), "IntList", sizeof(IntList));
    list->root = NULL;
    list->end = NULL;
    list->size = 0;
    /* WriteToLogFile(LOGBUFF, "empty list %p", list); */
    return list;
}

/**
@brief          initialize list with an array of values
@param[inout]   LinkedList  -   the list to initialize
@param[in]      pnValues    -   array containing the values to initialize the listl
@param[in]      nNumel      -   number of elements in pnArray
@date           31.07.2016
@author         Tim Elberfeld
*/
void InitList(IntList* LinkedList, int32* pnValues, int32 nNumel)
{
    if(LinkedList == NULL)
    {
        QuitWithError("Invalid list ptr!", ERR_NULL_POINTER);
    }
    
    if(pnValues == NULL)
    {
        QuitWithError("Invalid value array ptr", ERR_NULL_POINTER);
    }

    /* write to allocation log file */
    LogAllocation(LinkedList, nNumel*sizeof(IntNode), sizeof(IntNode), false);
	for(int32 ii = 0; ii < nNumel; ii++)
	{
        AppendList(pnValues[ii], LinkedList);
    }
}

/**
 @brief         Log an IntNode object to the logfile
 @param[in]     node    - the node to log
 @param[in]     strCaller   -   name of the function that called this one
 @param[in]     nLineNumber -   line number where the function was called 
 @date          31.07.2016
 @author        Tim Elberfeld
 */
void LogNodeImp(IntNode* node, const char* strCaller, uint32 nLineNumber)
{
    if(node != NULL)
    {
        WriteToLogFileImp(LOGFILE, strCaller, nLineNumber, LOGBUFF, 
            "node address %p, node.x = %d, node.prev = %p, node.next = %p",node, node->x, node->prev, node->next);
    }
}


/**
@brief      Test if the element nValue is in the list or not
@param[in]  nValue      -   value to test for
@param[in]  LinkedList  -   the list to scan
@return     <bool>      -   @c true if a node with x value nValue is in the list,
                            @c false otherwise.
@note       
@date       02.09.2016
@author     Tim Elberfeld
*/
bool ElementInList(int32 nValue, IntList* LinkedList)
{
    for(IntNode* iter = LinkedList->root; iter != NULL; iter = iter->next)
    {
        if(iter->x == nValue)
        {
            return(true);
        }
    }
    return(false);
}

/**
@brief		insert an element at target position
@param		nValue		-	the value to insert
@param		LinkedList 	-	the list
@param		nTargetPos	-	target position
@note       O(n)
@author 	Tim Elberfeld
@date		29.07.2016
*/
void InsertList(int32 nValue, IntList* LinkedList, int32 nTargetPos)
{
    IntNode* newNode = (IntNode*)AllocBytesImp(sizeof(IntNode), "IntNode", sizeof(IntNode), false);
    newNode->x = nValue;
    newNode->next = NULL;
    newNode->prev = NULL;
    
	bool bInserted = false;
	
	if(LinkedList->root == NULL)		/* empty list */
	{
        AppendList(nValue, LinkedList);
		return;	
	}

	if(nTargetPos == 0)					/* insert at the beginning (root) */
	{
        /* WriteToLogFile(LOGBUFF, "added element to beginning"); */
        LogNode(newNode);
        
        IntNode* prevroot = LinkedList->root;
		newNode->next = prevroot;
		prevroot->prev = newNode;
		LinkedList->root = newNode;
		LinkedList->size++;
		return;
	}
    else if(nTargetPos == LinkedList->size - 1) /* append instead of insert */
    {
        AppendList(nValue, LinkedList);
        return;
    }

    int32 nPos = 0;
    for(IntNode* current = LinkedList->root; current != NULL; current = current->next)
	{
		if(nPos == nTargetPos)
		{
            /* WriteToLogFile(LOGBUFF, "adding node %d at %d between %p and %p", newNode->x, nTargetPos, current, current->next); */
            
            if(current->next != NULL)
            {
                IntNode* afterCur = current->next;
                afterCur->prev = newNode;
                
                newNode->prev = current;
                newNode->next = afterCur;
                
                current->next = newNode;
                
                LogNode(newNode->prev);
                LogNode(newNode);
                LogNode(newNode->next);
            }
            else
            {
                assert(current == LinkedList->end);     /* current->next is NULL, when we are at the end */
                
                newNode->prev = current;
                current->next = newNode;
            }
            bInserted = true;
            LinkedList->size++;
            break;
		}
        nPos++;
	}
    
    LogList(LinkedList);
    
	if(!bInserted)
	{
		WriteErrorLog(LOGBUFF, "Failed to insert %d into linked list", nValue);
	}
}

/**
 @brief     log contents of a list
 @param[in] LinkedList  -   list to log
 @param[in] strCaller   -   name of the function that called this one
 @param[in] nLineNumber -   line number where the function was called 
 @date      01.08.2016
 @author    Tim Elberfeld
 */
void LogListImp(IntList* LinkedList, const char* strCaller, uint32 nLineNumber)
{   
    if(LinkedList != NULL)
    {
        WriteToLogFileImp(LOGFILE, strCaller, nLineNumber, LOGBUFF, "logging list %p of size %d", LinkedList, LinkedList->size);
        int32 ii = 0;
        for (IntNode* current = LinkedList->root; current != NULL; current = current->next)
        {
            if(current != NULL)
            {  
                LogNodeImp(current, strCaller, nLineNumber);
            }
            else if(ii < LinkedList->size)
            {
                WriteErrorLog(LOGBUFF, "encountered NULL after %d / %d", ii, LinkedList->size);
            }
            ii++;
        }
    }
    else
    {
        QuitWithError("invalid list pointer!", ERR_NULL_POINTER);
    }
}

/**
@brief		delete an item from a list
@param[in]	nValue      -	the value to delete from the list
                            If this value is not unique in the list, the first one that is
                            encountered will be removed and all other occurrences stay in the list!
@param[in]  LinkedList  -   the list to delete the node with value nValue from
@return     <IntNode*> -   pointer to the new root node
@mod        01.09.2016  -   changed return type from <void> to <IntNode*>
@date		31.07.2016
@author		Tim Elberfeld
*/
IntNode* DeleteList(int32 nValue, IntList* LinkedList)
{
    bool bFoundNode = false;
    IntNode* toDeleteNode = NULL;
    for(IntNode* current = LinkedList->root; current != NULL; current = current->next)
    {
        if(current->x == nValue)
        {
            toDeleteNode = current;
            bFoundNode = true;
            break;
        }
    }
    
    if(bFoundNode)
    {
        if(toDeleteNode == LinkedList->root &&
           toDeleteNode == LinkedList->end)
        {
            LinkedList->end = NULL;
            LinkedList->root = NULL;
        }
        else if(toDeleteNode == LinkedList->root)
        {
            LinkedList->root = toDeleteNode->next;
            LinkedList->root->prev = NULL;
        }
        else if (toDeleteNode == LinkedList->end)
        {
            LinkedList->end = toDeleteNode->prev;
            LinkedList->end->next = NULL;
        }
        else
        {
            IntNode* A = toDeleteNode->prev;
            IntNode* C = toDeleteNode->next;
            A->next = C;
            C->prev = A;
        }
        
        if(toDeleteNode != NULL)
        {
            /* WriteToLogFile(LOGBUFF, "deleting %d from list", toDeleteNode->x); */
            toDeleteNode->next = NULL;
            toDeleteNode->prev = NULL;
            FreeBytesImp(toDeleteNode, false);
            LinkedList->size--;
        }
    }
    return(LinkedList->root);
}

/**
@brief          pop the last element (IntList.end)from the list and return its value
@param[inout]   LinkedList  -   the list
@return         <int32>     -   value of the item that was popped
                                if the list is empty -INT_MAX is returned!
@date           02.09.2016
@author         Tim Elberfeld
*/
int32 PopList(IntList* LinkedList)
{
    int32 nRetVal = -INT_MAX;
    if(LinkedList->end)
    {
        nRetVal = LinkedList->end->x;
        LinkedList->root = DeleteList(nRetVal, LinkedList);
        /* WriteToLogFile(LOGBUFF, "popped element %d, new size %d", nRetVal, LinkedList->size); */
    }
    return nRetVal;
}


/**
@brief			append an item to the end of the list
@param[in]		nValue		-	the value to append
@param[inout]	LinkedList	-	the list to append the new item to
@date			29.07.2016
@author			Tim Elberfeld	
*/
void AppendList(int32 nValue, IntList* LinkedList)
{
    IntNode* newNode = (IntNode*) AllocBytesImp(sizeof(IntNode), "IntNode", sizeof(IntNode), false);
    newNode->x = nValue;
    
    if(LinkedList->end == NULL && LinkedList->root == NULL)    /* empty list */
    {
        LinkedList->root = newNode;
        LinkedList->end = newNode;
        LinkedList->size++;
    }
    else
    {
        newNode->prev = LinkedList->end;
        LinkedList->end->next = newNode;
        LinkedList->end = newNode;
        LinkedList->size++;
    }
}

/**
 @brief     free the memory acquired for a list
 @param[in] LinkedList  -   the list to free
 @date      31.07.2016
 @author    Tim Elberfeld
 */
void FreeList(IntList* LinkedList)
{
    if(LinkedList->root != NULL && LinkedList->end != NULL)    /* otherwise the list is empty */
    {
        for(IntNode* cur = LinkedList->root; cur != NULL; cur = cur->next)
        {
            if(cur->prev)
            {
                FreeBytesImp(cur->prev, false);
                cur->prev = NULL;
            }
        }
    }
    
    FreeBytes(LinkedList); /* finally free the list */
}
