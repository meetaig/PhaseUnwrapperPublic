#include "unwrap.h"
/**
 @file      unwrap.c
 @brief     implementation of the unwrapping algorithms. 
            Functionality of the two algorithms can be found in goldstein.c and flynn.c respectively
 @author    Tim Elberfeld
 @date      27.07.2016
 */

/**
 @brief         Calculate a error measure gauging the success of the unwrapping.
                This will make a quality map of the unwrapped phase and calculate the sum of it.
                A perfect unwrapping woudld be achieved when the sum is equal to the number of elements,
                or in this case to 1.0 because the number of elements of the phase array will be used as a
                normalization factor.
 @param[in]     pdUnwrapped     -   unwrapped phase data
 @param[in]     mode            -   quality map for the weights of the phase gradients.
 @param[in]     nWidth          -   width of the unwrapped phase
 @param[in]     nHeight         -   height of the unwrapped phase
 @date          13.09.2016
 @author        Tim Elberfeld
 */
double MeasureUnwrappingSuccess(double* pdUnwrapped, QualityMapMode mode, int32 nWidth, int32 nHeight)
{
    if(pdUnwrapped == NULL)
    {
        QuitWithError("Invalid unwrapped phase" , ERR_NULL_POINTER);
    }
    
    double* pdQualityMap = AllocBytes(nWidth * nHeight * sizeof(double), "double", sizeof(double));
    MakeQualityMap(pdUnwrapped, mode, nWidth, nHeight, DEFAULT_BOX_SIZE, pdQualityMap);
    
    int32 nNumel = nWidth * nHeight;
    double dNormFactor = Reciprocal((double)nNumel); /* one over number of elements */
    
    double dSum = 0.0;
    for (int32 ii = 0; ii < nNumel; ii++)
    {
        dSum += pdQualityMap[ii];
    }
    
    FreeBytes(pdQualityMap);
    
    return(dSum * dNormFactor);
}

/**
 @brief     Compute the L0 norm of a wrapped and unwrapped phase pair. For more information see MeasureUnwrappingSuccess() <br>
                
            The formula to calculate the error measure is
            \f[
            \epsilon = \frac{1}{MN} \sum^{M-2}_{i = 0} \sum^{N-1}_{j = 0} w^{x}_{i, j} \vert \Phi_{i+1, j} - \Phi_{i, j} - \Delta^{x}_{i, j} \vert^{p} + \frac{1}{MN} \sum^{M-1}_{i = 0} \sum^{N-2}_{j = 0} w^{y}_{i, j} \vert \Phi_{i, j+1} - \Phi_{i, j} - \Delta^{y}_{i, j} \vert^{p},
            \f]
            where \f$w^{x}_{i, j} = \min(w_{i, j}, w_{i +1, j})\f$ and \f$w^{y}_{i, j} = \min(w_{i, j}, w_{i, j+1})\f$
            are the weights of the wrapped phase gradients in either direction at \f$(i, j)\f$. \f$\Delta^{x}\f$ and
            \f$\Delta^{y}\f$ are the corresponding wrapped phase gradients in x and y direction,
            \f$\Phi\f$ is the unwrapped phase data and \f$p\f$ is the exponent of the \f$L^{p}\f$ norm used for the computation.
 
            Code is simplified using the assumptions:
            \f[ -n^{0} = -1 \\
                 n^{0} = 1 \f]
            Citation from Ghiglia et al. " p = 0 produces a count of the number of samples at
            which the  gradients of the unwrapped solution do not match those of the wrapped phase"
 @param[in] pdUnwrapped -   unwrapped phase data
 @param[in] pdWrapped   -   wrapped phase data
 @param[in] pdWeights   -   weights for the wrapped phase gradients
 @param[in] nDimension  -   height and width of the arrays
 @return    <double>    -   computed L0 norm
 @date      13.09.2016
 @author    Tim Elberfeld
 */
double L0Norm(double* pdUnwrapped, double* pdWrapped, double* pdWeights, ArrayDim nDimension)
{
    uint64 nNumel = nDimension.w * nDimension.h;
    uint32 nNoMatchX = 0, nNoMatchY = 0;
    uint64 nTotalNoMatch = 0;
    double dWrappedGradX = 0.0, dWrappedGradY = 0.0;
    double dUnwrappedGradX = 0.0, dUnwrappedGradY = 0.0;
    double dNormalizationFactor = Reciprocal((double)nNumel);
    uint32 nIndex;
    
    for (int32 yy = 0; yy < (nDimension.h - 1); yy++)
    {
        for (int32 xx = 0; xx < (nDimension.w - 1); xx++)
        {
            nIndex = yy * nDimension.w + xx;
            nNoMatchX = 0;
            nNoMatchY = 0;
            if(xx < (nDimension.w - 2))
            {
                dUnwrappedGradX = fabs(pdUnwrapped[nIndex + 1] - pdUnwrapped[nIndex]);
                dWrappedGradX = fabs(GradientNormalized(pdWrapped[nIndex + 1], pdWrapped[nIndex], true));
                
                if(NOT(AlmostEqualRelative(dUnwrappedGradX, dWrappedGradX, REL_ERR_DBL_DEFAULT)))
                {
                    nNoMatchX++;
                }
            }
            
            if(yy < (nDimension.h - 2))
            {
                dUnwrappedGradY = fabs(pdUnwrapped[nIndex + nDimension.w] - pdUnwrapped[nIndex]);
                dWrappedGradY = fabs(GradientNormalized(pdWrapped[nIndex + nDimension.w], pdWrapped[nIndex], true));
                
                if(NOT(AlmostEqualRelative(dUnwrappedGradY, dWrappedGradY, REL_ERR_DBL_DEFAULT)))
                {
                    nNoMatchY++;
                }
            }
            
            nTotalNoMatch += __max(nNoMatchX, nNoMatchY);
        }
    }
    
    return(dNormalizationFactor * (double)nTotalNoMatch);
}

/**
 @brief     Compute the L1 norm of a wrapped and unwrapped phase pair. For more information see MeasureUnwrappingSuccess() <br>
            
            The formula to calculate the error measure is
            \f[
            \epsilon = \frac{1}{MN} \sum^{M-2}_{i = 0} \sum^{N-1}_{j = 0} w^{x}_{i, j} \vert \Phi_{i+1, j} - \Phi_{i, j} - \Delta^{x}_{i, j} \vert^{p} + \frac{1}{MN} \sum^{M-1}_{i = 0} \sum^{N-2}_{j = 0} w^{y}_{i, j} \vert \Phi_{i, j+1} - \Phi_{i, j} - \Delta^{y}_{i, j} \vert^{p},
            \f]
            where \f$w^{x}_{i, j} = \min(w_{i, j}, w_{i +1, j})\f$ and \f$w^{y}_{i, j} = \min(w_{i, j}, w_{i, j+1})\f$
            are the weights of the wrapped phase gradients in either direction at \f$(i, j)\f$. \f$\Delta^{x}\f$ and
            \f$\Delta^{y}\f$ are the corresponding wrapped phase gradients in x and y direction,
            \f$\Phi\f$ is the unwrapped phase data and \f$p\f$ is the exponent of the \f$L^{p}\f$ norm used for the computation.
 
            Code is simplified using the assumptions:
            \f$ n^{1} = n \f$

 @param[in] pdUnwrapped -   unwrapped phase data
 @param[in] pdWrapped   -   wrapped phase data
 @param[in] pdWeights   -   weights for the wrapped phase gradients
 @param[in] nDimension  -   height and width of the arrays
 @return    <double>    -   computed L1 norm
 @date      13.09.2016
 @author    Tim Elberfeld
 */
double L1Norm(double* pdUnwrapped, double* pdWrapped, double* pdWeights, ArrayDim nDimension)
{
    int32 nNumel = nDimension.w * nDimension.h;
    double dEpsX = 0.0, dEpsY = 0.0;
    double dPhaseDiffX, dPhaseDiffY;
    double dNormalizationFactor = Reciprocal(nNumel);
    int32 nIndex;
    
    for (int32 yy = 0; yy < (nDimension.h - 1); yy++)
    {
        for (int32 xx = 0; xx < (nDimension.w - 1); xx++)
        {
            nIndex = yy * nDimension.w + xx;
            if(xx < (nDimension.w - 2))
            {
                dPhaseDiffX = pdUnwrapped[nIndex + 1] - pdUnwrapped[nIndex];
                dEpsX += pdWeights[nIndex] *
                    fabs((dPhaseDiffX - GradientNormalized(pdWrapped[nIndex + 1], pdWrapped[nIndex], true)));
            }
            if(yy < (nDimension.h - 2))
            {
                dPhaseDiffY = pdUnwrapped[nIndex + nDimension.w] - pdUnwrapped[nIndex];
                dEpsY += pdWeights[nIndex] *
                    fabs((dPhaseDiffY - GradientNormalized(pdWrapped[nIndex + nDimension.w], pdWrapped[nIndex], true)));
            }
        }
    }
    
    return(dNormalizationFactor * dEpsX + dNormalizationFactor * dEpsY);
}

/**
 @brief     Compute the L2 norm of a wrapped and unwrapped phase pair.
            
            The formula to calculate the error measure is
            \f[
            \epsilon = \frac{1}{MN} \sum^{M-2}_{i = 0} \sum^{N-1}_{j = 0} w^{x}_{i, j} \vert \Phi_{i+1, j} - \Phi_{i, j} - \Delta^{x}_{i, j} \vert^{p} + \frac{1}{MN} \sum^{M-1}_{i = 0} \sum^{N-2}_{j = 0} w^{y}_{i, j} \vert \Phi_{i, j+1} - \Phi_{i, j} - \Delta^{y}_{i, j} \vert^{p},
            \f]
            where \f$w^{x}_{i, j} = \min(w_{i, j}, w_{i +1, j})\f$ and \f$w^{y}_{i, j} = \min(w_{i, j}, w_{i, j+1})\f$
            are the weights of the wrapped phase gradients in either direction at \f$(i, j)\f$. \f$\Delta^{x}\f$ and
            \f$\Delta^{y}\f$ are the corresponding wrapped phase gradients in x and y direction,
            \f$\Phi\f$ is the unwrapped phase data and \f$p\f$ is the exponent of the \f$L^{p}\f$ norm used for the computation.
 
 @param[in] pdUnwrapped -   unwrapped phase data
 @param[in] pdWrapped   -   wrapped phase data
 @param[in] pdWeights   -   weights for the wrapped phase gradients
 @param[in] nDimension  -   height and width of the arrays
 @return    <double>    -   computed L2 norm
 @date      13.09.2016
 @author    Tim Elberfeld
 */
double L2Norm(double* pdUnwrapped, double* pdWrapped, double* pdWeights, ArrayDim nDimension)
{
    int32 nNumel = nDimension.w * nDimension.h;
    double dEpsX = 0.0, dEpsY = 0.0;
    double dPhaseDiffX, dPhaseDiffY;
    double dNormalizationFactor = Reciprocal(nNumel);
    int32 nIndex;
    
    double dDiffTermX = 0.0, dDiffTermY = 0.0;
    
    for (int32 yy = 0; yy < (nDimension.h - 1); yy++)
    {
        for (int32 xx = 0; xx < (nDimension.w - 1); xx++)
        {
            nIndex = yy * nDimension.w + xx;
            if(xx < (nDimension.w - 2))
            {
                dPhaseDiffX = pdUnwrapped[nIndex + 1] - pdUnwrapped[nIndex];
                dDiffTermX = dPhaseDiffX - GradientNormalized(pdWrapped[nIndex + 1], pdWrapped[nIndex], true);
                dEpsX += pdWeights[nIndex] * fabs(dDiffTermX * dDiffTermX);
            }
            
            if(yy < (nDimension.h - 2))
            {
                dPhaseDiffY = pdUnwrapped[nIndex + nDimension.w] - pdUnwrapped[nIndex];
                dDiffTermY = dPhaseDiffY - GradientNormalized(pdWrapped[nIndex + nDimension.w], pdWrapped[nIndex], true);
                dEpsY += pdWeights[nIndex] * fabs(dDiffTermY * dDiffTermY);
            }
        }
    }
    
    return(dNormalizationFactor * sqrt(dEpsX) + dNormalizationFactor * sqrt(dEpsY));
}

/**
@brief			Unwrap the phase using Goldstein's Branch Cut Algorithm
@param[in]		pdWrappedPhase	-	the wrapped phase array
@param[in]		nWidth			-	width of the phase array
@param[in]		nHeight			-	height of the phase array
@param[in]		nMaxCutLength	-	maximum length of the branch cuts that will be used as guide for the unwrapping
@param[inout]	pdUnwrapped		-	array to hold the unwrapped phase after the algorithm is finished
@date			27.07.2016
@author			Tim Elberfeld
*/
void GoldsteinUnwrap(double* pdWrappedPhase, int nWidth, int nHeight, int nMaxCutLength, bool bRemoveDipoles, double* pdUnwrapped)
{
	if(pdWrappedPhase == NULL)
	{
		QuitWithError("Invalid wrapped phase array" , ERR_NULL_POINTER);
	}
	if (pdUnwrapped == NULL)
	{
		QuitWithError("Invalid unwrapped phase array. Maybe allocation failed?" , ERR_NULL_POINTER);	
	}

	/* -------- allocate memory -------- */
	ArrayDim nDimension ={.w = nWidth, .h = nHeight};
    uint64 nBytes = GetNumBytes(nDimension, sizeof(uint8));
	uint8* pcBitflags = (uint8*) AllocBytes(nBytes, "uint8", sizeof(uint8));
	MaskBorderPixels(pcBitflags, nDimension); /* set border flag */
    
    uint64 nBytesInt = nBytes * sizeof(int32);
    int32* pnResidueMap = (int32*) AllocBytes(nBytesInt, "int32", sizeof(int32));
	
	/* -------- identify residues -------- */
	int32 nNumResidues = MakeResidueMap(pdWrappedPhase, nDimension, pcBitflags, pnResidueMap, NULL);
	WriteToLogFile(LOGBUFF, "found %d residues", nNumResidues);

    if(bRemoveDipoles)			/* update bitflags array with micro branch cuts if dipoles are to be removed */
	{
        RemoveDipoles(pnResidueMap, pcBitflags, nDimension, &nNumResidues, NULL);
    }
    
    /* -------- place phase integration barriers -------- */
    PlaceBranchCutsPrimitive(pnResidueMap, nNumResidues, pcBitflags, nDimension, nMaxCutLength);

    /* -------- unwrap and return -------- */
    UnwrapFromBranchCuts(pdWrappedPhase, pcBitflags, nDimension, pdUnwrapped);

    FreeBytes(pnResidueMap);
    FreeBytes(pcBitflags);
}

/**
@brief			Unwrap the phase using Flynn's Minimum Discontinuity Algorithm
@param[in]		pdWrappedPhase	-	the wrapped phase array
@param[in]		nWidth			-	width of the phase array
@param[in]		nHeight			-	height of the phase array
@param[in]		pdQualityMap	-	quality map to guide the unwrapping. See qualitymaps.c/.h for details 
@param[inout]	pdUnwrapped		-	array to hold the unwrapped phase after the algorithm is finished
@date			27.07.2016
@author			Tim Elberfeld
*/
void FlynnUnwrap(double* pdWrappedPhase, int32 nWidth, int32 nHeight, double* pdQualityMap, double* pdUnwrapped)
{
	if(pdWrappedPhase == NULL)
	{
		QuitWithError("Invalid phase array" , ERR_NULL_POINTER);
	}
	
	if(pdQualityMap == NULL)
	{
		QuitWithError("Invalid quality map array" , ERR_NULL_POINTER);	
	}
	
	if (pdUnwrapped == NULL)
	{
		QuitWithError("Invalid unwrapped phase array" , ERR_NULL_POINTER);	
	}
	
	ArrayDim nDimension = {.w = nWidth, .h = nHeight};
	ArrayDim nJumpCountDim = {.w = nWidth + 1, .h = nHeight + 1};

	int32 nNumel = nJumpCountDim.w * nJumpCountDim.h;
	int32 nNumBytesInt = nNumel * sizeof(int32);		/* number of bytes to allocate for the jump counts and wrap counts */
	
	/* allocate memory for temporary storage */
	int32* pnHorizontalJump = (int32*)AllocBytes(nNumBytesInt, "int32", sizeof(int32));
	int32* pnVerticalJump = (int32*)AllocBytes(nNumBytesInt, "int32", sizeof(int32));

	ComputeJumpCounts(pdWrappedPhase, nDimension, pnHorizontalJump, pnVerticalJump);
	ScanNodes(nJumpCountDim, pnHorizontalJump, pnVerticalJump, pdQualityMap);
	UnwrapFromJumpCounts(pdWrappedPhase, nDimension, pnHorizontalJump, pnVerticalJump, pdUnwrapped);

	FreeBytes(pnVerticalJump);
	FreeBytes(pnHorizontalJump);
}
