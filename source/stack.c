#include "stack.h"
/**
 @file      stack.c
 @author    Tim Elberfeld
 @date		07.09.2016
 @brief     Simple implementation of a stack structure with garantied coniguous memory. <br>
            Similar to @c std::array in c++ the stack grows by a factor of 2 each time an element is
            added and additional space is needed.
 */

/**
@brief          Make an empty Stack and pre-allocate nNumNodes
@param[in]      nNumNodes   -   number of nodes to pre-allocate. If this value is lower than INITIAL_NODE_COUNT,
                                allocates INITIAL_NODE_COUNT nodes instead.
@return         <Stack*>    -   pointer to the new Stack object
@date           07.09.2016
@author         Tim Elberfeld
*/
Stack* NewStack(size_t nNumNodes)
{
    Stack* newStack = (Stack*) AllocBytes(sizeof(Stack),"Stack", sizeof(Stack));
    
    newStack->root = NULL;
    newStack->end = NULL;
    newStack->size = 0;
    
    nNumNodes = __max(nNumNodes, INITIAL_NODE_COUNT);
    newStack->node_pool = (StackNode*) AllocBytes(nNumNodes*sizeof(StackNode), "StackNode", sizeof(StackNode));
    newStack->allocated = nNumNodes;
    
    return newStack;
}

/**
@brief          Push new element to end of Stack
@param[inout]   stack   -   pointer to pointer to the Stack
@param[in]      nValue  -   value to append to the Stack
@date           07.09.2016
@author         Tim Elberfeld
*/
void PushStack(Stack** stack, int32 nValue)
{
    if(*stack)
    {
        if((*stack)->size == (*stack)->allocated)
        {
            Stack* newStack = ExtendStack((*stack));
            FreeStack((*stack));
            *stack = newStack;
            newStack = NULL;
        }
        
        StackNode* newNode = &(*stack)->node_pool[(*stack)->size++];
        newNode->x = nValue;
        
        if((*stack)->end == NULL && (*stack)->root == NULL) /* empty stack */
        {
            newNode->next = NULL;
            newNode->prev = NULL;
            (*stack)->end = newNode;
            (*stack)->root = newNode;
        }
        else                                /* non empty stack */
        {
            newNode->next = NULL;
            newNode->prev = (*stack)->end;
            (*stack)->end->next = newNode;
            (*stack)->end = newNode;
        }
    }
}


/**
 @brief     Access StackNode with index directly, instead of going through one by one
 @param[in] s       -   the stack to access
 @param[in] nIndex  -   the index of the node to access 
 @return    <int32> -   The payload StackNode::x of node with index @p nIndex.
                        return value is INT_MAX if @p s or @p nIndex were invalid
 @date
 @author    Tim Elberfeld
 */
int32 StackElementAt(Stack* s, uint32 nIndex)
{
    if(s)
    {
        if(s->size > nIndex)
        {
            return(s->node_pool[nIndex].x);
        }
    }
    return(INT_MAX);
}

/**
@brief          Pop last element from stack and return it
@param[inout]   stack   -   pointer to the stack
@return         <int>   -   payload of the element that was popped
@date           07.09.2016
@author         Tim Elberfeld
*/
int PopStack(Stack* stack)
{
    int32 retval = -INT_MAX;
    if(stack != NULL)
    {
        if(stack->root != NULL)
        {
            StackNode* toDelete = &(stack->node_pool[(--(stack->size))]);
            
            if(stack->end != NULL)
            {
                stack->end = toDelete->prev;
                if(toDelete->prev != NULL)
                {
                    stack->end->next = NULL;
                }
            }
            
            if(toDelete == stack->root)
            {
                stack->root = NULL;
            }
            
            toDelete->next = NULL;
            toDelete->prev = NULL;
            
            retval = toDelete->x;
            toDelete->x = 0;
        }
    }
    return(retval);
}

/**
@brief      extend the stack memory.
            Creates a new stack that is an exact copy of the input parameter,
            but with double the space allocated in Stack::node_pool.
@param[in]  stack       -   the stack to extend
@return     <Stack*>    -   pointer to the newly allocated stack
@date       07.09.2016
@author     Tim Elberfeld
*/
Stack* ExtendStack(Stack* stack)
{
    if(stack == NULL)
    {
        QuitWithError("Invalid Stack passed", ERR_NULL_POINTER);
    }
    
    Stack* newStack = NewStack(stack->allocated * 2);
    WriteToLogFile(LOGBUFF, "extended stack from %d to %d", stack->allocated, newStack->allocated);

    for (StackNode* iter = stack->root; iter != NULL; iter = iter->next)
    {
        PushStack(&newStack, iter->x); /* NOTE(tim): PushStack() calls ExtendStack(),
                                        but this should be fine because we allocated twice as much space for newStack */
    }
    
    return newStack;
}

/**
 @brief         clear contents of a stack but leave memory allocated
 @param[inout]  stack   -   the stack to clear
 @date          15.09.2016
 @author        Tim Elberfeld
 */
void ClearStack(Stack* s)
{
    s->root = NULL;
    s->end = NULL;
    memset(s->node_pool, 0, s->allocated * sizeof(StackNode)); /* not sizeof(Stack), but sizeof(StackNode)!! TE_BUG_16102016 */
    s->size = 0;
}

/**
@brief      free the memory occupied by the stack
@param[in]  stack   -   the Stack to destroy
@date       07.09.2016
@author     Tim Elbefeld
*/
void FreeStack(Stack* stack)
{
    if(stack)
    {
        if(stack->node_pool)
        {
            FreeBytes(stack->node_pool);
        }
        FreeBytes(stack);
    }
}