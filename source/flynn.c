#include "flynn.h"
/** 
 @file      flynn.c
 @author    Tim Elberfeld
 @date      27.07.2016
 @brief     implementation of Flynn's Minimum Discontinuity Algorithm.

 Algorithm taken from
 D. C. Ghiglia and M. D. Pritt, "Two-Dimensional Phase Unwrapping: Theory, Algorithms and Software."
 Wiley Blackwell, May 1998.
 http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html
 original paper:
 T. J. Flynn, “Two-dimensional phase unwrapping with minimum weighted discontinuity,”
 Journal of the Optical Society of America A, vol. 14, no. 10, pp. 2692–2701, October 1997.
 https://dx.doi.org/10.1364/JOSAA.14.002692
 */

/**
@brief			unwrapping step of Flynn's Minimum Discontinuity Algorithm
@param[in]		pdWrappedPhase		-	wrapped phase array
@param[in]		nDimension			-	width and height of the phase array
@param[in]		pnVerticalJump  	-	vertical jump counts
@param[in]		pnHorizontalJump	-	horizontal jump counts
@param[inout]	pdUnwrapped			-	array to hold the unwrapped phase values
@date 			28.07.2016
@author			Tim Elberfeld
*/
void UnwrapFromJumpCounts(double* pdWrappedPhase, ArrayDim nDimension,
                          const int32* pnHorizontalJump, const int32* pnVerticalJump, double* pdUnwrapped)
{
  	WriteToLogFile(LOGBUFF, "Unwrapping phase from wrap counts, dim: %d %d", nDimension.w, nDimension.h);
  	
    ArrayDim nJumpCountDim = {.w = nDimension.w + 1, .h = nDimension.h + 1};
  	double dPhaseDiff;
  	int32 nJumpOld;
  	size_t nBytes = GetNumBytes(nDimension, sizeof(double));
  	CopyArray(pdWrappedPhase, nBytes, pdUnwrapped, nBytes);	/* copy the contents of the wrapped phase to unwrapped phase array */

    int32 yy = 0;
  	for (int32 xx = 0; xx < (nDimension.w - 1); xx++)			/* this only iterates over the first row */
  	{
    	dPhaseDiff =   pdUnwrapped[yy * nDimension.w + (xx + 1)] -
                       pdUnwrapped[yy * nDimension.w + xx] + EPS; /* yy is zero here! */

    	nJumpOld = (int32) round(dPhaseDiff);
    	pdUnwrapped[yy * nDimension.w + (xx + 1)]
                    += (double)(pnVerticalJump[(yy + 1) * nJumpCountDim.w + (xx + 1)] - nJumpOld);
  	}

    for (int32 yy = 0; yy < (nDimension.h - 1); yy++)
  	{ 
    	for (int32 xx = 0; xx < nDimension.w; xx++)
    	{
      		dPhaseDiff = pdUnwrapped[(yy + 1) * nDimension.w + xx] - pdUnwrapped[yy * nDimension.w + xx] + EPS;
      		nJumpOld = (int32) round(dPhaseDiff);

      		pdUnwrapped[(yy + 1) * nDimension.w + xx]
                        += (double)(pnHorizontalJump[(yy + 1) * nJumpCountDim.w + (xx + 1)] - nJumpOld);
    	}
  	}
}

/**
@brief			Compute jump counts from wrapped phase. Jump counts are used for Flynn's Minimum Discontinuity Algorithm
@param[in]		pdWrappedPhase      -	the wrapped phase array
@param[in]		nDimension          -	width and height of the phase array
@param[inout]	pnHorizontalJump    -	array for the horizontal jump counts.
                                        Note that the jumpcounts must be 1 greater than the phase array in both x and y direction
@param[inout]	pnVerticalJump      -	array for the vertical jump counts.
                                        Note that the jumpcounts must be 1 greater than the phase array in both x and y direction
@note 			Comment from the original code by Ghiglia et al., 
                variable names changed to fit the function definition:
				<pre>
                * Compute phase jump counts.
   				* If dx(i,j) and dy(i,j) represent the wrapped derivatives
   				* pdWrappedPhase(i+1,j) - pdWrappedPhase(i,j) and pdWrappedPhase(i,j+1) - pdWrappedPhase(i,j),
   				* respectively, then the jump count pnHorizontalJump(i,j) corresponds to
   				* dy(i-1,j-1), and pnVerticalJump(i,j) corresponds to dx(i-1,j-1).
                </pre>
                Explanation:
                <pre>
				This is because the jump counts are "in between" the phase pixels like so:
				x   x   x   x   x
				  o───o───o───o───o
 				x │ x │ x │ x │ 
 				  o───o───o───o─
				x │ x │ x │ x 
				  o───o 
				x │ 
				where 'x' are the places where the jump counts go and 'o' are the phase "pixels".
                </pre>
@date			27.07.2016
@author			Tim Elberfeld
*/
void ComputeJumpCounts(double* pdWrappedPhase, ArrayDim nDimension, int32* pnHorizontalJump, int32* pnVerticalJump)
{
    /* WriteToLogFile(LOGBUFF, "Computing intial jump counts", nDimension.w, nDimension.h); */
  	double dPhaseDiffH = 0.0;
  	double dPhaseDiffV = 0.0;
  	ArrayDim nJumpCountDim = {.w = nDimension.w + 1, .h = nDimension.h + 1};
    
    for (int32 yy = 1; yy < nJumpCountDim.h; yy++)      /* NOTE: we start at 1 for yy, not at 0,  */
    {                                                   /*       but we go to the width of the jump counts, */
                                                        /*       so one more than the phase array! */
        for (int32 xx = 1; xx < nJumpCountDim.w; xx++)  /* NOTE: we also start at 1 for xx! */
        {
            if (yy < nDimension.h)  /*  horizontal jump counts only go from y = 1 to (height-1), */
            {                       /*  where height is the jump count height */
                /*  xx is shifted to the left so the phase will
                    not be accessed at an invalid position */
                dPhaseDiffH = 	pdWrappedPhase[yy * nDimension.w + (xx - 1)] -
                                pdWrappedPhase[(yy - 1) * nDimension.w + (xx - 1)] + EPS;
                pnHorizontalJump[yy * nJumpCountDim.w + xx] = (int32) round(dPhaseDiffH);
            }
            /*  vertical jump counts only go from x = 1 to (width-1),
                where width is the jump count width */
            if (xx < nDimension.w)
            {
                /* similar to above, yy is shifted up by 1! */
                dPhaseDiffV = pdWrappedPhase[(yy - 1) * nDimension.w + xx] -
                    pdWrappedPhase[(yy - 1) * nDimension.w + (xx - 1)] + EPS;
                pnVerticalJump[yy * nJumpCountDim.w + xx] = (int32)round(dPhaseDiffV);
            }
        }
    }
}

/**
@brief          Step 2 of Flynn's Minimum Discontinuity Algorithm
                Compute the constant factors for the multiplication of the wrapped phase with 2\pi.
                Those so called wrap counts are related to the jump counts that are computed in
                #ComputeJumpCounts() in the following way: <p>
                Jump counts can be converted into the wrap counts and the other way around.
                If the wrap count c_{m, n} (meaning for the pixel with coordinates y = m, x = n)
                is incremented, the vertical jump count v_{m, n} and horizontal jump count h_{m, n}
                are incremented, while v_{m - 1, n} and h_{m, n - 1} are decremented.
                <pre>

                | 0 |_1_| 2 |_1_| 0 |   Wrap counts in 1D
                        ___............ 2
                    ___|   |___........ 1
                ___|            |___ .. 0
                |                   |   Unwrapped signal according to those wrap counts
                </pre>
@param[in]		nJumpCountDim		-	width and height of the jump counts arrays
                                        (CAUTION: NOT the phase array!)
@param[inout]	pnHorizontalJump	-	array for the horizontal jump counts, see ComputeJumpCounts()
@param[inout]	pnVerticalJump 		-	array for the vertical jump counts, see ComputeJumpCounts()
@param[in]		pdQualityMap		-	the quality map to guide the creation of new jump/wrap counts.
@attention      The quality map array is 1 smaller in width and height than 
                @p pnHorizontalJumpCount and @p pnVerticalJumpCount
@note 			Both jump count arrays will be modified during the optimization process
                and will be used for the unwrapping step.
@date 			28.07.2016
@author			Tim Elberfeld									
*/
void ScanNodes(ArrayDim nJumpCountDim, int32* pnHorizontalJump, int32* pnVerticalJump, double* pdQualityMap)
{
	WriteToLogFile(LOGBUFF, "Starting wrap count optimization - scanning nodes, dim: %d %d", nJumpCountDim.w, nJumpCountDim.h);
	uint64 nBytesInt = GetNumBytes(nJumpCountDim, sizeof(int32));
	int32* pnWrapCounts = (int32*)AllocBytes(nBytesInt, "int32", sizeof(int32));

    int32 nNumel = nJumpCountDim.w * nJumpCountDim.h;
    int32 nNewLoops = 0;
	int32 nNewEdges  = 0;
	
    ArrayDim nPhaseDim = {.w = nJumpCountDim.w - 1, .h = nJumpCountDim.h - 1};
    
	/* allocate temp storage for holding bitflags */
    uint64 nBytesUInt8 = GetNumBytes(nJumpCountDim, sizeof(uint8));
    uint8* pcBitflags = (uint8*)AllocBytes(nBytesUInt8, "uint8", sizeof(uint8));

    uint8* pcBitflagsWork = pcBitflags;         /* save pointer for the init loop */
    for (int32 ii = 0; ii < nNumel; ii++) 		/* init the bitflags */
    {
    	set_flag(*pcBitflagsWork++, FLAG_NOW);
    }
    pcBitflagsWork = NULL; 						/* just to be safe! */

    int32 nIter = 0;
 	do
  	{
  		nNewLoops = 0;                          /* reset new edge and loop count */
		nNewEdges = 0;
        
        AddLeftToRight(pcBitflags, pnWrapCounts,
                       pnVerticalJump, pnHorizontalJump, pdQualityMap,
                       nPhaseDim, &nNewEdges, &nNewLoops);
        AddTopToBottom(pcBitflags, pnWrapCounts,
                       pnVerticalJump, pnHorizontalJump, pdQualityMap,
                       nPhaseDim, &nNewEdges, &nNewLoops);
        AddRightToLeft(pcBitflags, pnWrapCounts,
                       pnVerticalJump, pnHorizontalJump, pdQualityMap,
                       nPhaseDim, &nNewEdges, &nNewLoops);
        AddBottomToTop(pcBitflags, pnWrapCounts,
                       pnVerticalJump, pnHorizontalJump, pdQualityMap,
                       nPhaseDim, &nNewEdges, &nNewLoops);
	   
	    WriteToLogFile(LOGBUFF, "New edges: %8d  New loops: %8d", nNewEdges, nNewLoops);
	    
	    nNumel = nJumpCountDim.w * nJumpCountDim.h;
	    for (int32 ii = 0; ii < nNumel; ii++)
	    {
        	if(test_flag(pcBitflags[ii], FLAG_LATER))
        	{
        	  	set_flag(pcBitflags[ii], FLAG_NOW);
        	}
        	else
        	{
          		delete_flag(pcBitflags[ii], FLAG_ANYTIME);
        	}
      	}
        nIter++;
	}
	while (nNewEdges > 0 && nIter < 1000);

	FreeBytes(pnWrapCounts);
    FreeBytes(pcBitflags);
}

/**
 * @brief          Add left to right edges to the wrap count tree
 * @param[inout]   pcBitflags          -    bitflags array holding information about postponed nodes, etc.
 * @param[inout]   pnWrapCounts        -    wrap count nodes (see ScanNodes())
 * @param[inout]   pnVerticalJump      -    vertical jump counts (see ComputeJumpCounts() for more info)
 * @param[inout]   pnHorizontalJump    -    horizontal jump counts (see ComputeJumpCounts() for more info)
 * @param[in]      pdQualityMap        -    quality map to guide the tree creation
 * @param[in]      nPhaseDim           -    height and width of the original phase array
 * @param[inout]   rnNewEdges          -    pointer to integer holding information about the
 *                                          new edges that were added in this function
 * @param[inout]   rnNewLoops          -    pointer to integer holding information about the
 *                                          new loops that were added in this function
 * @date           09.08.2016
 * @author         Tim Elberfeld
 */
void AddLeftToRight(uint8* pcBitflags, int32* pnWrapCounts,
                    int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim,
                    int32* rnNewEdges, int32* rnNewLoops)
{  
    ArrayDim    nJumpCountDim = {.w = nPhaseDim.w + 1, .h = nPhaseDim.h + 1};
    bool        bFoundLoop = false;
    Point       ptNext;
    Point       ptCur;
    Point       ptLast;
    int32       nValueIncrease;
    int32       nWeight;
    int32       nValueChange;
    
    for (int32 yy = 0; yy <= (nPhaseDim.h - 1); yy++) /* loop over all but the last row */
    {
        ptCur.y = yy;
        ptNext.y = yy + 1;

        for (int32 xx = 0; xx <= nPhaseDim.w; xx++)
        { 
            ptNext.x = xx;
            ptCur.x = xx;
            
            if(IsValidIndex2D(xx, yy, nPhaseDim) && IsValidIndex2D(ptNext.x, ptNext.y, nPhaseDim))
            {
                if (test_flag(pcBitflags[yy * nJumpCountDim.w + xx], FLAG_NOW) ||
                    test_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x], FLAG_NOW))
                {
                    if (ptNext.y <= 1 || ptNext.x <= 0)
                    {
                        nValueIncrease = (int32) -copysign(1, -pnVerticalJump[ptNext.y * nJumpCountDim.w + ptNext.x]);
                    }
                    else
                    {
                        /* compute the weight as integer weight */
                        nWeight = (int32)(1.0 +
                                          LARGE_INTEGER * pdQualityMap[ptNext.y * nPhaseDim.w + ptNext.x]);
                        nValueIncrease = (int32)-copysign(nWeight,
                                                   -pnVerticalJump[ptNext.y * nJumpCountDim.w + ptNext.x]);
                    }
                
                    nValueChange = pnWrapCounts[ptCur.y * nJumpCountDim.w + ptCur.x] +
                                        nValueIncrease - pnWrapCounts[ptNext.y * nJumpCountDim.w + ptNext.x];
                
                    if (nValueChange > 0)
                    {	/*  revise values in subtree of ptNext and check for loop */
                        (*rnNewEdges)++;
                        ptLast.x = xx;
                        ptLast.y = yy;
                        bFoundLoop = false;
                    
                        ChangeExten(ptNext, ptLast, &bFoundLoop,
                                    nValueChange, pnWrapCounts, pcBitflags, nPhaseDim);
                    
                        if (bFoundLoop)
                        {
                            ApplyElementaryOperation(ptNext, ptCur, pcBitflags, pnWrapCounts, 
                                pnVerticalJump, pnHorizontalJump, nJumpCountDim);
                            ChangeValue(ptNext, -nValueChange, pnWrapCounts, pcBitflags, nJumpCountDim);
                            (*rnNewLoops)++;
                        }
                        else
                        {
                            /* add edge and remove edges to ptNext  */
                            set_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_RIGHT);
                        
                            if (ptNext.y < nPhaseDim.h)
                            {
                                delete_flag(pcBitflags[(ptNext.y + 1) * nJumpCountDim.w + ptNext.x], FLAG_LEFT);
                            }
                            
                            if (ptNext.x < nPhaseDim.w)
                            {
                                delete_flag(pcBitflags[ptNext.y * nJumpCountDim.w + (ptNext.x + 1)], FLAG_UP);
                            }
                              
                            if (ptNext.x > 0)
                            {
                                delete_flag(pcBitflags[ptNext.y * nJumpCountDim.w + (ptNext.x - 1)], FLAG_DOWN);
                            }
                        }
                    }
                }
            }
        }
    }
}

/**
 * @brief          Add top to bottom edges to the wrap count tree
 * @param[inout]   pcBitflags          -   bitflags array holding information about postponed nodes, etc.
 * @param[inout]   pnWrapCounts        -   wrap count nodes (see #ScanNodes)
 * @param[inout]   pnVerticalJump      -   vertical jump counts (see #ComputeJumpCounts for more info)
 * @param[inout]   pnHorizontalJump    -   horizontal jump counts (see #ComputeJumpCounts for more info)
 * @param[in]      pdQualityMap        -   quality map to guide the tree creation
 * @param[in]      nPhaseDim           -   height and width of the original phase array
 * @param[inout]   rnNewEdges          -   pointer to integer holding information about the
                                            new edges that were added in this function
 * @param[inout]   rnNewLoops          -   pointer to integer holding information about the
                                            new loops that were added in this function
 * @date           09.08.2016
 * @author         Tim Elberfeld
 */
void AddTopToBottom(uint8* pcBitflags, int32* pnWrapCounts,
                    int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim,
                    int32* rnNewEdges, int32* rnNewLoops)
{
    ArrayDim    nJumpCountDim = {.w = nPhaseDim.w + 1, .h = nPhaseDim.h + 1};
    bool        bFoundLoop = false;
    Point       ptNext;
    Point       ptCur;
    Point       ptLast;
    int32       nValueIncrease;
    int32       nWeight;
    int32       nValueChange;

    for (int32 yy = 0; yy <= nPhaseDim.h; yy++)
    {
        ptCur.y = yy;
        ptNext.y = yy;
            
        for (int32 xx = 0; xx <= (nPhaseDim.w - 1); xx++) /* loop over all but the last column */
        {
            ptCur.x = xx;
            ptNext.x = xx + 1;
            
            if(IsValidIndex2D(xx, yy, nPhaseDim) && IsValidIndex2D(ptNext.x, ptNext.y, nPhaseDim))
            {
                if (test_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_NOW) ||
                    test_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x], FLAG_NOW))
                {
                    if (ptNext.y <= 0 || ptNext.x <= 1) 
                    {
                        nValueIncrease = (int32)-copysign(1, pnHorizontalJump[ptNext.y * nJumpCountDim.w + ptNext.x]);
                    }
                    else
                    {
                        nWeight = (int32)(1.0 +
                                          LARGE_INTEGER * pdQualityMap[ptNext.y * nPhaseDim.w + ptNext.x]);
                        nValueIncrease = (int32)-copysign(nWeight,
                                                   pnHorizontalJump[ptNext.y * nJumpCountDim.w + ptNext.x]);
                    }
                    
                    nValueChange = pnWrapCounts[ptCur.y * nJumpCountDim.w + ptCur.x] +
                                        nValueIncrease - pnWrapCounts[ptNext.y * nJumpCountDim.w + ptNext.x];
                    
                    if (nValueChange > 0)
                    {
                        (*rnNewEdges)++;
                        ptLast.x = ptCur.x;
                        ptLast.y = ptCur.y;
                        bFoundLoop = false;
                        ChangeExten(ptNext, ptLast, &bFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nPhaseDim);
                        
                        if (bFoundLoop)
                        {
                            ApplyElementaryOperation(ptNext, ptCur, pcBitflags, pnWrapCounts, pnVerticalJump, pnHorizontalJump, nJumpCountDim);
                            ChangeValue(ptNext, -nValueChange, pnWrapCounts, pcBitflags, nJumpCountDim);
                            (*rnNewLoops)++;
                        }
                        else
                        {
                            set_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_DOWN);
                            
                            if (ptNext.y < nPhaseDim.h)
                            {
                                delete_flag(pcBitflags[(ptNext.y + 1) * nJumpCountDim.w + ptNext.x], FLAG_LEFT);
                            }
                            
                            if (ptNext.y > 0)
                            {
                                delete_flag(pcBitflags[(ptNext.y - 1) * nJumpCountDim.w + ptNext.x], FLAG_RIGHT);
                            }
                            
                            if (ptNext.x < nPhaseDim.w)
                            {
                                delete_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x + 1], FLAG_UP);
                            }	
                        }
                    }
                }
            }
        }
    }
}

/**
* @brief          Add right to left edges to the wrap count tree
* @param[inout]   pcBitflags          -   bitflags array holding information about postponed nodes, etc.
* @param[inout]   pnWrapCounts        -   wrap count nodes (see #ScanNodes)
* @param[inout]   pnVerticalJump      -   vertical jump counts (see #ComputeJumpCounts for more info)
* @param[inout]   pnHorizontalJump    -   horizontal jump counts (see #ComputeJumpCounts for more info)
* @param[in]      pdQualityMap        -   quality map to guide the tree creation
* @param[in]      nPhaseDim           -   height and width of the original phase array
* @param[inout]   rnNewEdges          -   pointer to integer holding information about the
*                                         new edges that were added in this function
* @param[inout]   rnNewLoops          -   pointer to integer holding information about the
*                                         new loops that were added in this function
* @date           09.08.2016
* @author         Tim Elberfeld
*/
void AddRightToLeft(uint8* pcBitflags, int32* pnWrapCounts,
                    int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim,
                    int32* rnNewEdges, int32* rnNewLoops)
{
    ArrayDim    nJumpCountDim = {.w = nPhaseDim.w + 1, .h = nPhaseDim.h + 1};
    bool        bFoundLoop = false;
    Point       ptNext;
    Point       ptCur;
    Point       ptLast;
    int32       nValueIncrease;
    int32       nWeight;
    int32       nValueChange;
    
    for (int32 yy = nPhaseDim.h; yy >= 1; yy--) /* loop over all but the first row */
    {
        ptNext.y = yy - 1;
        ptCur.y = yy;
        
        for (int32 xx = nPhaseDim.w; xx >=0; xx--)
        {
            ptNext.x = xx;
            ptCur.x = xx;
            
            if(IsValidIndex2D(xx, yy, nPhaseDim) && IsValidIndex2D(ptNext.x, ptNext.y, nPhaseDim))
            {
                if (test_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_NOW) ||
                    test_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x], FLAG_NOW))
                {
                    /*if (ptCur.y <= 1 || ptCur.y >= nPhaseDim.h  ||
                        ptCur.x <= 0 || ptCur.x >= nPhaseDim.w)*/
                    if (ptCur.y <= 1 || ptCur.x <= 0)
                    {
                        nValueIncrease = (int32)-copysign(1, pnVerticalJump[ptCur.y * nJumpCountDim.w + ptCur.x]);
                    }
                    else
                    {
                        nWeight = (int32) (1.0 + LARGE_INTEGER * pdQualityMap[ptCur.y * nPhaseDim.w + ptCur.x]);
                        nValueIncrease = (int32)-copysign(nWeight, pnVerticalJump[ptCur.y * nJumpCountDim.w + ptCur.x]);
                    }
                    
                    nValueChange = pnWrapCounts[ptCur.y * nJumpCountDim.w + ptCur.x] +
    		      	  					nValueIncrease - pnWrapCounts[ptNext.y * nJumpCountDim.w + ptNext.x];
                    
                    if (nValueChange > 0)
                    {
                        (*rnNewEdges)++;
                        ptLast.x = ptCur.x;
                        ptLast.y = ptCur.y;
                        bFoundLoop = false;
                        ChangeExten(ptNext, ptLast, &bFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nPhaseDim);
                        
                        if (bFoundLoop)
                        {
                            ApplyElementaryOperation(ptNext, ptCur,
                                                     pcBitflags, pnWrapCounts,
                                                     pnVerticalJump, pnHorizontalJump, nJumpCountDim);
                            ChangeValue(ptNext, -nValueChange, pnWrapCounts, pcBitflags, nJumpCountDim);
                            (*rnNewLoops)++;
                        }
                        else
                        {
                            set_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_LEFT);
                            
                            if (ptNext.y > 0)
                            {
                                delete_flag(pcBitflags[(ptNext.y - 1) * nJumpCountDim.w + ptNext.x], FLAG_RIGHT);
                            }
                            
                            if (ptNext.x < nPhaseDim.w)
                            {
                                delete_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x + 1], FLAG_UP);
                            }
                            
                            if (ptNext.x > 0) 
                            {
                                delete_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x - 1], FLAG_DOWN);
                            }
                        }
                    }
                }
            }
        }	
    }
}

/**
 * @brief          Add bottom to top edges to the wrap count tree
 * @param[inout]   pcBitflags          -   bitflags array holding information about postponed nodes, etc.
 * @param[inout]   pnWrapCounts        -   wrap count nodes (see #ScanNodes)
 * @param[inout]   pnVerticalJump      -   vertical jump counts (see #ComputeJumpCounts for more info)
 * @param[inout]   pnHorizontalJump    -   horizontal jump counts (see #ComputeJumpCounts for more info)
 * @param[in]      pdQualityMap        -   quality map to guide the tree creation
 * @param[in]      nPhaseDim           -   height and width of the original phase array
 * @param[inout]   rnNewEdges          -   pointer to integer holding information about the
 *                                         new edges that were added in this function
 * @param[inout]   rnNewLoops          -   pointer to integer holding information about the
 *                                         new loops that were added in this function
 * @date           09.08.2016
 * @author         Tim Elberfeld
 */
void AddBottomToTop(uint8* pcBitflags, int32* pnWrapCounts,
                    int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim,
                    int32* rnNewEdges, int32* rnNewLoops)
{
    ArrayDim    nJumpCountDim = {.w = nPhaseDim.w + 1, .h = nPhaseDim.h + 1};
    bool        bFoundLoop = false;
    Point       ptNext;
    Point       ptCur;
    Point       ptLast;
    int32       nValueIncrease;
    int32       nWeight;
    int32       nValueChange;
    
    for (int32 yy = nPhaseDim.h; yy >= 0; yy--)
    {
        ptCur.y = yy;
        ptNext.y = yy;
        
        for (int32 xx = nPhaseDim.w; xx >= 1; xx--)
        {
            ptCur.x = xx;
            ptNext.x = xx - 1;
            
            if(IsValidIndex2D(xx, yy, nPhaseDim) && IsValidIndex2D(ptNext.x, ptNext.y, nPhaseDim))
            {
                if (test_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_NOW) ||
                    test_flag(pcBitflags[ptNext.y * nJumpCountDim.w + ptNext.x], FLAG_NOW))
                {
                    /*if (ptCur.y <= 0 || ptCur.y >= nPhaseDim.h ||
                        ptCur.x <= 1 || ptCur.x >= nPhaseDim.w) */
                    if (ptCur.y <= 0 || ptCur.x <= 1)
                    {
                        nValueIncrease = (int32)-copysign(1, -pnHorizontalJump[ptCur.y * nJumpCountDim.w + ptCur.x]);
                    }
                    else
                    {
                        nWeight = (int32)(1.0 + LARGE_INTEGER * pdQualityMap[ptCur.y * nPhaseDim.w + ptCur.x]);
                        nValueIncrease = (int32)-copysign(nWeight, -pnHorizontalJump[ptCur.y * nJumpCountDim.w + ptCur.x]);
                    }
                    
                    nValueChange = pnWrapCounts[ptCur.y * nJumpCountDim.w + ptCur.x] +
                                        nValueIncrease - pnWrapCounts[ptNext.y * nJumpCountDim.w + ptNext.x];
                    
                    if (nValueChange > 0)
                    {
                        (*rnNewEdges)++;
                        ptLast.x = xx;
                        ptLast.y = yy;
                        bFoundLoop = false;
                        ChangeExten(ptNext, ptLast, &bFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nPhaseDim);
                        
                        if (bFoundLoop)
                        {
                            ApplyElementaryOperation(ptNext, ptCur, pcBitflags, pnWrapCounts, pnVerticalJump, pnHorizontalJump, nJumpCountDim);
                            ChangeValue(ptNext, -nValueChange, pnWrapCounts, pcBitflags, nJumpCountDim);
                            (*rnNewLoops)++;
                        }
                        else
                        {
                            set_flag(pcBitflags[ptCur.y * nJumpCountDim.w + ptCur.x], FLAG_UP);
                            
                            if (ptNext.y < nPhaseDim.h)
                            {
                                delete_flag(pcBitflags[(ptNext.y + 1) * nJumpCountDim.w + ptNext.x], FLAG_LEFT);
                            }
                            
                            if (ptNext.y > 0)
                            {
                                delete_flag(pcBitflags[(ptNext.y - 1) * nJumpCountDim.w + ptNext.x], FLAG_RIGHT);
                            }
                            
                            if (ptNext.x > 0) 
                            {
                                delete_flag(pcBitflags[ptNext.y * nJumpCountDim.w + (ptNext.x - 1)], FLAG_DOWN);
                            }
                        }
                    }
                }    
            }
        }
    }
}

/**
 @brief         increment or decrement the jump counts at the node from @p ptFrom to @p ptTo
 @param[in]     ptFrom              -   start position of the node
 @param[in]     ptTo                -   end position of the node
 @param[inout]  pnVerticalJump      -   vertical jump count array
 @param[inout]  pnHorizontalJump    -   horizontal jump count array
 @param[in]     nJumpCountDim       -   height and width of the jump count arrays
 @date          09.09.2016
 @author        Tim Elberfeld
 */
void ChangeJump(Point ptFrom, Point ptTo, int32* pnVerticalJump, int32* pnHorizontalJump, ArrayDim nJumpCountDim)
{
    if (ptFrom.y > ptTo.y) 					/* Remove edge from last node to base of loop  */
    {
        pnVerticalJump[ptFrom.y * nJumpCountDim.w + ptFrom.x]--;
    }
    else if (ptFrom.y < ptTo.y)
    {
        pnVerticalJump[ptTo.y * nJumpCountDim.w + ptTo.x]++;
    }
    else if (ptFrom.x > ptTo.x)
    {
        pnHorizontalJump[ptFrom.y * nJumpCountDim.w + ptFrom.x]++;
    }
    else if (ptFrom.x < ptTo.x)
    {
        pnHorizontalJump[ptTo.y * nJumpCountDim.w + ptTo.x]--;
    }
}

/**
 @brief         Update edge information at ptTip
 @param[inout]  ptTip               -   the current position in the tree we are traversing
 @param[inout]  pcBitflags          -   bitflags array containing information about edge direction
 @param[inout]  pnVerticalJump      -   vertical jump count array
 @param[inout]  pnHorizontalJump    -   horizontal jump count array
 @param[in]     nJumpCountDim       -   height and width of the jump count arrays
 @date          09.09.2016
 @author        Tim Elberfeld
 */
void UpdateEdge(Point* ptTip, uint8* pcBitflags, int32* pnVerticalJump, int32* pnHorizontalJump, ArrayDim nJumpCountDim)
{
    if ((ptTip->y > 0) && test_flag(pcBitflags[(ptTip->y - 1) * nJumpCountDim.w + ptTip->x], FLAG_RIGHT))
    {
        pnVerticalJump[ptTip->y * nJumpCountDim.w + ptTip->x]--;
        delete_flag(pcBitflags[(ptTip->y - 1) * nJumpCountDim.w + ptTip->x], FLAG_RIGHT);
        ptTip->y--;
    }
    else if(ptTip->y < (nJumpCountDim.h - 1) &&  test_flag(pcBitflags[(ptTip->y + 1) * nJumpCountDim.w + ptTip->x], FLAG_LEFT))
    {
        pnVerticalJump[(ptTip->y + 1) * nJumpCountDim.w + ptTip->x]++;
        delete_flag(pcBitflags[(ptTip->y + 1) * nJumpCountDim.w + ptTip->x], FLAG_LEFT);
        ptTip->y++;
    }
    else if ((ptTip->x > 0) && test_flag(pcBitflags[ptTip->y * nJumpCountDim.w + (ptTip->x - 1)], FLAG_DOWN))
    {
        pnHorizontalJump[ptTip->y * nJumpCountDim.w + ptTip->x]++;
        delete_flag(pcBitflags[ptTip->y * nJumpCountDim.w + (ptTip->x - 1)], FLAG_DOWN);
        ptTip->x--;
    }
    else if ((ptTip->x < nJumpCountDim.w) && test_flag(pcBitflags[ptTip->y * nJumpCountDim.w + (ptTip->x + 1)], FLAG_UP))
    {
        pnHorizontalJump[ptTip->y * nJumpCountDim.w + (ptTip->x + 1)]--;
        delete_flag(pcBitflags[ptTip->y * nJumpCountDim.w + (ptTip->x + 1)], FLAG_UP);
        ptTip->x++;
    }
    else
    {
        QuitWithError("broken loop", ERR_FLYNN_BROKEN_LOOP);
    }
}

/**
@brief			apply an improving elementary operation (a loop) from the wrap counts
@param[in] 		ptBase				-	base point of the loop
@param[in] 		ptLast				-	last point of the loop
@param[inout] 	pcBitflags 			-	array holding edge flags
@param[inout] 	pnWrapCounts		-	wrap counts array
@param[inout] 	pnVerticalJump  	- 	vertical jump counts
@param[inout] 	pnHorizontalJump  	- 	horizontal jump counts 				
@param[in]		nJumpCountDim 		-	height and width of the arrays (1 more than phase array!)
@date 			01.08.2016
@author			Tim Elberfeld
*/
void ApplyElementaryOperation(  Point ptBase,
                                Point ptLast,
				                uint8 *pcBitflags,
				                int32* pnWrapCounts, 
				                int32* pnVerticalJump, 
				                int32* pnHorizontalJump, 
				                ArrayDim nJumpCountDim)
{
    if(!IsValidIndex2D(ptBase.x, ptBase.y, nJumpCountDim) || !IsValidIndex2D(ptLast.x, ptLast.y, nJumpCountDim))
    {
        WriteErrorLog(LOGBUFF, "points out of bounds: base (%d %d), last (%d %d), nDim: (%d %d)",
                      ptBase.x, ptBase.y, ptLast.x, ptLast.y, nJumpCountDim.w, nJumpCountDim.h);
        return;
    }
    
	ChangeJump(ptBase, ptLast, pnVerticalJump, pnHorizontalJump, nJumpCountDim);
	
	/* Back up over loop: remove edges and change subtree values */
	Point ptTip = {.x = ptLast.x, .y = ptLast.y};
	int32 nValueShift = 0;	
	do
	{
		nValueShift = -pnWrapCounts[ptTip.y * nJumpCountDim.w + ptTip.x];

	  	ChangeValue(ptTip, nValueShift, pnWrapCounts, pcBitflags, nJumpCountDim);
        
        /* remove edge direction information */
        UpdateEdge(&ptTip, pcBitflags, pnVerticalJump, pnHorizontalJump, nJumpCountDim);

	} while ((ptTip.y != ptBase.y) || (ptTip.x != ptBase.x));
}

/**
@brief			Add 'nValueChange' to the values of all nodes of the tree rooted at ptRoot. 
				Set a flag if the tree includes ptLast. Make ptRoot child of the next iteration.
@param[in]  	ptRoot 			-	root of the tree to scan
@param[in]		ptLast			-	last point 
@param[inout]	rbFoundLoop		-	will be true if a loop was encountered during one of the recursive iterations
@param[in]		nValueChange	-	increase of the pnWrapCounts array at position ptRoot
@param[inout]	pnWrapCounts	-	wrap counts array
@param[inout]	pcBitflags 		-	array holding the bitflags
@param[in]      nDimension      -   height and width of the phase array
@date 			01.08.2016
@author			Tim Elberfeld
*/
void ChangeExten(	Point ptRoot, Point ptLast, bool* rbFoundLoop, int32 nValueChange, int32* pnWrapCounts,
    		     	uint8* pcBitflags, ArrayDim nDimension)
{
    if( !IsValidIndex2D(ptRoot.x, ptRoot.y, nDimension) ||
        !IsValidIndex2D(ptLast.x, ptLast.y, nDimension))
    {
        WriteErrorLog(LOGBUFF, "index out of bounds: ptRoot (%d, %d), ptLast (%d, %d), nDimension (%d %d)",
            ptRoot.x, ptRoot.y, ptLast.x, ptLast.y, nDimension.w, nDimension.h);
        return;
    }
	Point ptNext;
    
  	*rbFoundLoop |= ((ptRoot.y == ptLast.y) && 	/* 'or' bFoundLoop with the result of this iteration. */
  					(ptRoot.x == ptLast.x));	/* This means it will be true if there was a loop in any of the previous iterations */
  													
  	uint8 child_node = pcBitflags[ptRoot.y * (nDimension.w + 1) + ptRoot.x];

  	if (test_flag(child_node, FLAG_LEFT))
  	{
    	ptNext.x = ptRoot.x;
    	ptNext.y = ptRoot.y - 1;
    	ChangeExten(ptNext, ptLast, rbFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nDimension);
  	}

  	if (test_flag(child_node, FLAG_RIGHT))
  	{
    	ptNext.x = ptRoot.x;
    	ptNext.y = ptRoot.y + 1; 
    	ChangeExten(ptNext, ptLast, rbFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nDimension);
  	}

  	if (test_flag(child_node, FLAG_UP))
  	{
   		ptNext.x = ptRoot.x - 1;
    	ptNext.y = ptRoot.y;
	    ChangeExten(ptNext, ptLast, rbFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nDimension);
  	}
  
  	if (test_flag(child_node, FLAG_DOWN))
  	{
    	ptNext.x = ptRoot.x + 1;
    	ptNext.y = ptRoot.y;
    	ChangeExten(ptNext, ptLast, rbFoundLoop, nValueChange, pnWrapCounts, pcBitflags, nDimension);
  	}

  	pnWrapCounts[ptRoot.y * (nDimension.w + 1) + ptRoot.x] += nValueChange;
  	set_flag(pcBitflags[ptRoot.y * (nDimension.w + 1) + ptRoot.x], FLAG_ANYTIME);
}

/**
@brief			Add @p nValueShift (< 0 !) to the values of all nodes of the tree rooted at @p ptRoot.
				If the change would make @p pnWrapCounts[@p ptRoot] < 0,
				make @p pnWrapCounts 0 instead  and split the node from its parent.
				Make @p ptRoot child in the next iteration.
@param[in]		ptRoot			-	the root for the tree to change
@param[in]		nValueShift		-	value to add to pnWrapCounts[ptRoot]
@param[inout]	pnWrapCounts	-	wrap counts array
@param[inout]	pcBitflags 		-	bitflags array containing the edge information
@param[in]		nJumpCountDim   -	height and width of pnWrapCounts and pcBitflags
@date 			01.08.2016
@author			Tim Elberfeld
 */
void ChangeValue(Point ptRoot, int32 nValueShift, int32* pnWrapCounts, uint8* pcBitflags, ArrayDim nJumpCountDim)
{
    if(!IsValidIndex2D(ptRoot.x, ptRoot.y, nJumpCountDim))
    {
        WriteErrorLog(LOGBUFF, "point out of bounds: root (%d %d), nDim: (%d %d)",
                      ptRoot.x, ptRoot.y, nJumpCountDim.w, nJumpCountDim.h);
        return;
    }
    
  	if ((pnWrapCounts[ptRoot.y * nJumpCountDim.w + ptRoot.x] + nValueShift) < 0)
  	{
   		nValueShift = -pnWrapCounts[ptRoot.y * nJumpCountDim.w + ptRoot.x];

    	if (ptRoot.y > 0)
    	{
    	     delete_flag(pcBitflags[(ptRoot.y - 1) * nJumpCountDim.w + ptRoot.x], FLAG_RIGHT);
    	}

   		if (ptRoot.y < (nJumpCountDim.h-1))
   		{
   			delete_flag(pcBitflags[(ptRoot.y + 1) * nJumpCountDim.w + ptRoot.x], FLAG_LEFT);
   		}
    	
    	if (ptRoot.x > 0)
    	{
    		delete_flag(pcBitflags[ptRoot.y * nJumpCountDim.w + (ptRoot.x - 1)], FLAG_DOWN);
    	}
    	
    	if (ptRoot.x < (nJumpCountDim.w - 1))
    	{
    		delete_flag(pcBitflags[ptRoot.y * nJumpCountDim.w + (ptRoot.x + 1)], FLAG_UP);
    	}
  	}
  	
  	uint8 child_node = pcBitflags[ptRoot.y * nJumpCountDim.w + ptRoot.x];
  	Point ptNext;

  	if(test_flag(child_node, FLAG_LEFT))
  	{
  		ptNext.x = ptRoot.x;
  		ptNext.y = ptRoot.y - 1;
  		ChangeValue(ptNext, nValueShift, pnWrapCounts, pcBitflags, nJumpCountDim);
  	}
  	
  	if (test_flag(child_node, FLAG_RIGHT))
  	{
  		ptNext.x = ptRoot.x;
  		ptNext.y = ptRoot.y + 1;
  		ChangeValue(ptNext, nValueShift, pnWrapCounts, pcBitflags, nJumpCountDim);
  	}

  	if (test_flag(child_node, FLAG_UP)) 
  	{
  		ptNext.x = ptRoot.x - 1;
  		ptNext.y = ptRoot.y;
  		ChangeValue(ptNext, nValueShift, pnWrapCounts, pcBitflags, nJumpCountDim);
  	}

  	if (test_flag(child_node, FLAG_DOWN))
  	{
  		ptNext.x = ptRoot.x + 1;
  		ptNext.y = ptRoot.y;
  		ChangeValue(ptNext, nValueShift, pnWrapCounts, pcBitflags, nJumpCountDim);
  	}

  	set_flag(pcBitflags[ptRoot.y * nJumpCountDim.w + ptRoot.x], FLAG_ANYTIME);
  	pnWrapCounts[ptRoot.y * nJumpCountDim.w + ptRoot.x] += nValueShift;
}
