#include "kdtree.h"
/**
 @file      kdtree.c
 @brief     Simple implementation of a kd-tree. 
            Supports adding and removing nodes, nearest neighbor search
            and tracks bounding box of points contained in tree.
 @date      13.08.2016
 @author    Tim Elberfeld
 */

/**
 @brief     Find all neighbors to a point @p ptRef that are within range @p dRange
 @param[in] Tree            -   the tree to search in
 @param[in] ptRef           -   reference #Pointf for the search
 @param[in] dRange          -   Radius of the search
 @return    <PointfArray*>  -   pointer to result structure
 @date      19.09.2016
 @author    Tim Elberfeld
 */
PointfArray* FindAllInRange(KDTree* Tree, Pointf* ptRef, double dRange)
{
    int32 nNumPoints = INITIAL_POINTF_COUNT;
    PointfArray* pResult = NewPointfArray(nNumPoints, ptRef->dim);
    
    if(Tree->dim == ptRef->dim)
    {
        Hypersphere* sphere = NewHypersphere(ptRef, dRange);
        int32 nNumFound = FindInRangeRec(Tree->root, sphere, Tree->BB, &pResult, 0);
        pResult->size = nNumFound;
    }
    
    return(pResult);
}

/**
 @brief     Find nodes in range of a #Hypersphere recursively
            Implemented using lecture notes:
            <pre>
            David Mount, “Lecture notes: CMSC 420: Data structures,” 
            University of Maryland, Dept. of Computer Science, 2001,
            https://www.cs.umd.edu/users/mount/420/Lects/420lects.pdf ,retrieved 13.08.2016
            </pre>
 @param[in] root    -   root of the current subtree
 @param[in] sphere  -   the #Hypersphere representing the range
 @param[in] BB      -   bounding #Hyperrect of the current subtree
 @param[in] pResult -   pointer-to-pointer to the #PointfArray result structure
 @date      19.09.2016
 @author    Tim Elberfeld
 */
int32 FindInRangeRec(KDNode* root, Hypersphere* sphere, Hyperrect* BB, PointfArray** pResult, int32 nDepth)
{
    if(root == NULL)
    {
        return(0);
    }
    if(HyperrectDisjointFrom(BB, sphere))
    {
        return(0);
    }
    if(HyperrectInHypersphere(BB, sphere))
    {
        return(root->size);
    }
    
    int32 nCount = 0;
    if(PointfInHypersphere(root->pos, sphere))
    {
        nCount++;
    }
    int32 nCountLeft = 0;
    int32 nCountRight = 0;
    int32 cd = nDepth % root->pos->dim;
        
    Hyperrect* BBLeft = CopyHyperrect(BB);
    Hyperrect* BBRight = CopyHyperrect(BB);
    TrimHyperrect(BBLeft, root->pos, cd, TRIM_RIGHT);
    TrimHyperrect(BBRight, root->pos, cd, TRIM_LEFT);
    
    nCountLeft += FindInRangeRec(root->left, sphere, BBLeft, pResult, nDepth + 1);
    if(nCountLeft > 0)
    {
        AddToPointfArray(pResult, root->left);
    }
    
    nCountRight += FindInRangeRec(root->right, sphere, BBRight, pResult, nDepth + 1);
    if(nCountRight > 0)
    {
        AddToPointfArray(pResult, root->left);
    }
    return(nCount + nCountLeft + nCountRight);
}

/**
 @brief     Add a node and all its subnodes to the #PointfArray.
            If the memory is not sufficient, extend it
 @param[in] pArray      -   pointer-to-pointer to the array to fill
 @param[in] NodesToAdd  -   root of the subtree to add to the array
 @date      19.09.2016
 @author    Tim Elberfeld
 */
void AddToPointfArray(PointfArray** pArray, KDNode* NodesToAdd)
{
    if(NodesToAdd)
    {
        memcpy((*pArray)->points[(*pArray)->cursor].p, NodesToAdd->pos->p, NodesToAdd->pos->dim * sizeof(double));
        (*pArray)->cursor++;
        if((*pArray)->cursor >= (*pArray)->allocated)
        {
            PointfArray* pTmp = CopyPointfArray(*pArray, true);
            FreePointfArray(*pArray);
            *pArray = pTmp;
        }
    
        AddToPointfArray(pArray, NodesToAdd->left);
        AddToPointfArray(pArray, NodesToAdd->right);
    }
}

/**
 @brief         find _the_ nearest neighbor of @p ptRef in @p Tree
 @param[in]     Tree        -   the tree to search in
 @param[in]     ptRef       -   point to search the neighbor of
 @param[in]     dMinDist    -   minimum distance for the neighbor
 @param[in]     dMaxDist    -   maximum distance for the neighbor, 
                                set to -1 to allow infinite distance
 @param[inout]  rdDistSq    -   will hold the squared distance to the nearest neighbor that was found
                                after call is done
 @date          15.09.2016
 @author        Tim Elberfeld
 */
Pointf* FindNearestNeighbor(KDTree* Tree, Pointf* ptRef, double dMinDist, double dMaxDist, double* rdDistSq)
{
    assert(Tree->dim > 0);
    assert(Tree->dim == ptRef->dim);
    *rdDistSq = DBL_MAX; /* best distance */
    Pointf* ptBest = NewEmptyPointf(ptRef->dim);
    Hyperrect* BB = CopyHyperrect(Tree->BB);
    FindNNRec(Tree->root, ptRef, dMinDist*dMinDist, dMaxDist*dMaxDist, &ptBest, rdDistSq, BB, 0);
    return(ptBest);
}

/**
 @brief         find _the_ nearest neighbor of @p ptRef in current subtree @p root
 @param[in]     root        -   root node of the current subtree
 @param[in]     ptRef       -   point to search the neighbor of
 @param[in]     dMinDistSq  -   minimum squared distance for the neighbor
 @param[in]     dMaxDistSq  -   maximum squared distance for the neighbor,
                                set to -1 to allow infinite distance
 @param[inout]  dBestDistSq -   best squared distance encountered so far
 @param[inout]  ptBest      -   pointer-to-pointer to #Pointf with the best distance so far
 @param[in]     BB          -   (hyper) bounding box of the current search space
 @param[in]     nDepth      -   current recursion depth
 @date          15.09.2016
 @author        Tim Elberfeld
 */
void FindNNRec(KDNode* root, Pointf* ptRef, double dMinDistSq, double dMaxDistSq,
               Pointf** ptBest, double* dBestDistSq, Hyperrect* BB, int32 nDepth)
{
    if(root == NULL)
    {
        return;
    }
    
    Pointf* vertex = ClosestHyperrectVertex(ptRef, BB);
    double dDistSqToHyperrect = EucDistSq(ptRef->p, vertex->p, ptRef->dim);
    FreePointf(vertex);
    if(dDistSqToHyperrect > (*dBestDistSq))
    {
        /*  search space does not contain any points that are
            closer than the best already found */
        return;
    }
    
    double dDistSq = EucDistSq(ptRef->p, root->pos->p, ptRef->dim);
    if((dDistSq <= (*dBestDistSq)) && (dDistSq < dMaxDistSq) && (dDistSq > dMinDistSq))
    {
        memcpy((*ptBest)->p, root->pos->p, root->pos->dim * sizeof(double));
        *dBestDistSq = dDistSq;
    }

    int32 curdim = nDepth % BB->dim;
    Hyperrect* BBLeft = CopyHyperrect(BB);
    Hyperrect* BBRight = CopyHyperrect(BB);
    TrimHyperrect(BBLeft, root->pos, curdim, TRIM_RIGHT); /* cut off right part */
    TrimHyperrect(BBRight, root->pos, curdim, TRIM_LEFT); /* cut off left part */

    if(ptRef->p[curdim] < root->pos->p[curdim])
    {
        FindNNRec(root->left, ptRef, dMinDistSq, dMaxDistSq, ptBest, dBestDistSq,
                    BBLeft, nDepth + 1);

        FindNNRec(root->right, ptRef, dMinDistSq, dMaxDistSq, ptBest, dBestDistSq,
                    BBRight, nDepth + 1);
    }
    else
    {
        FindNNRec(root->right, ptRef, dMinDistSq, dMaxDistSq, ptBest, dBestDistSq,
                    BBRight, nDepth + 1);

        FindNNRec(root->left, ptRef, dMinDistSq, dMaxDistSq, ptBest, dBestDistSq,
                    BBLeft, nDepth + 1);
    }
}

/**
* @brief        Set the Hyperrect::topleft field to the same value as @p p,
*               effectively trimming off a piece using a hyperplane with the 
*               split coordinate @p cd.
*               <pre>
*               Explanation in 2D. Trim dimension is p[0]. x marks the
*               area that is trimmed off
*      Hyperrect::topleft  ╬───┬───────┐   ╬───┬───────┐
*                          │ x │       │   │   │ x x x │
*                          │ x ╬ p[0]  │   │   ╬p[0] x │
*                          │ x │       │   │   │ x x x │
*                          │ x │       │   │   │ x x x │
*                          └───┴───────╬   └───┴───────╬ Hyperrect::bottomright
*                            TRIM_LEFT       TRIM_RIGHT
*               </pre>
* @param[inout] rect    -   the #Hyperrect to trim
* @param[in]    p       -   the point to trim the rectangle with
* @param[in]    cd      -   dimension to regard for the trim
* @param[in]    side    -   either ::TRIM_LEFT or ::TRIM_RIGHT    
* @date         15.09.2016
* @author       Tim Elberfeld
*/
void TrimHyperrect(Hyperrect* rect, Pointf* p, int32 cd, TrimSide side)
{
    if(rect->dim == p->dim)
    {
        if(side == TRIM_RIGHT)
        {
            rect->bottomright->p[cd] = p->p[cd];
        }
        else if(side == TRIM_LEFT)
        {
            rect->topleft->p[cd] = p->p[cd];
        }
    }
}

/**
 @brief     Test if a #Pointf @p ptRef lies within a #Hypersphere @p sphere
 @param[in] ptRef   -   reference point
 @param[in] sphere  -   the #Hypersphere
 @return    <bool>  -   @c true if @p ptRef lies within @p sphere. @c false otherwise.
 @date      19.09.2016
 @author    Tim Elberfeld
 */
bool PointfInHypersphere(Pointf* ptRef, Hypersphere* sphere)
{
    return(EucDistSq(ptRef->p, sphere->pos->p, ptRef->dim) < sphere->radiussq);
}

/**
 @brief     Find the vertex of a #Hyperrect @p rect that is closest to @p ptRef
            In theory we would need all vertices of the #Hyperrect to be able 
            to do that. But we can also just construct this vertex by always taking
            the coordinate with the smaller difference to that coordinate in @p ptRef
 
 @param[in] ptRef       -   the reference point
 @param[in] rect        -   the hyperrect
 @return    <Pointf*>   -   #Pointf* of the vertex. (Must be free'd)
 @date      16.09.2016
 @author    Tim Elberfeld
 */
Pointf* ClosestHyperrectVertex(Pointf* ptRef, Hyperrect* rect)
{
    Pointf* ptBest = NULL;
    if(rect->dim == ptRef->dim)
    {
        ptBest = NewEmptyPointf(ptRef->dim);
        Pointf* a = rect->topleft;
        Pointf* b = rect->bottomright;
        double diffa;
        double diffb;
        for (int32 ii = 0; ii < ptRef->dim; ii++)
        {
            diffa = fabs(a->p[ii] - ptRef->p[ii]);  /* always choose the coordinate */
            diffb = fabs(b->p[ii] - ptRef->p[ii]);  /*  with the smallest absolute difference */
            
            ptBest->p[ii] = diffa < diffb ? a->p[ii] : b->p[ii];
        }
    }
    else
    {
        WriteErrorLog(LOGBUFF, "Rect and point do not have same dimensionality!");
    }
    return(ptBest);
}

/**
@brief      Copy a #Hyperrect
@param[in]  other           -   the #Hyperrect to copy
@return     <Hyperrect*>    -   the copy of @p other
@date       15.09.2016
@author     Tim Elberfeld
*/
Hyperrect* CopyHyperrect(Hyperrect* other)
{
    return(NewHyperrect(other->topleft->p, other->bottomright->p, other->dim));
}

/**
 @brief     Copy a #Hyphersphere
 @param[in] other           -   the #Hypersphere to copy
 @return    <#Hypersphere*> -   copy of @p other
 @date      19.09.2016
 @author    Tim Elberfeld
 */
Hypersphere* CopyHypersphere(Hypersphere* other)
{
    return(NewHypersphere(other->pos, other->radius));
}

/**
 @brief     Copy a #PointfArray and maybe extend the memory of the copy
 @param[in] other           -   the array to copy
 @param[in] bExtendMemory   -   if @c true, the PointfArray::allocated field will be double of that of @p other,
                                meaning that at least another @p other.size #Pointf can be added to the array
 @date      19.09.2016
 @author    Tim Elberfeld
 */
PointfArray* CopyPointfArray(PointfArray* other, bool bExtendMemory)
{
    PointfArray* newArray;
    
    int32 nNumPoints = (bExtendMemory ? (other->size * 2) : (other->size));
    newArray = NewPointfArray(nNumPoints, other->dim);
    int32 nBytesPerPos = sizeof(double) * other->dim;
    
    for (int32 ii = 0; ii < other->size; ii++)
    {
        memcpy(newArray->points[ii].p, other->points[ii].p, nBytesPerPos);
    }
    
    return(newArray);
}

/**
 @brief     Test if a #Hyperrect is completely enclosed in a #Hypersphere
 @param[in] rect    -   the #Hyperrect
 @param[in] sphere  -   the #Hypersphere
 @return    <bool>  -   @c true if @p rect is completely enclosed by @p sphere. @c false otherwise
 @date      19.09.2016
 @author    Tim Elberfeld
 */
bool HyperrectInHypersphere(Hyperrect* rect, Hypersphere* sphere)
{
    bool isEnclosed = true;
    if((EucDistSq(rect->bottomright->p, sphere->pos->p, sphere->pos->dim) > sphere->radiussq) ||
       (EucDistSq(rect->topleft->p, sphere->pos->p, sphere->pos->dim) > sphere->radiussq))
    {
        isEnclosed = false;
    }
    return(isEnclosed);
}

/**
 @brief     test if a #Hyperrect does NOT overlap with a #Hypersphere
 @param[in] rect    -   the #Hyperrect
 @param[in] sphere  -   the #Hypersphere
 @return    <bool>  -   @c true if @p rect has an overlap with @p sphere. @c false otherwise
 @todo      Extend this to more than 2D... For now there is not enough time.
 @date      19.09.2016
 @author    Tim Elberfeld
 */
bool HyperrectDisjointFrom(Hyperrect* rect, Hypersphere* sphere)
{
    if(sphere->pos->dim != rect->dim || rect->dim != 2)
    {
        return(true);
    }
    double* pdPoint = (double*)calloc(rect->dim, sizeof(double)); /* we only need this to work for 2D now. */
    
    bool noOverlap = true;
    if((EucDistSq(rect->bottomright->p, sphere->pos->p, sphere->pos->dim) > sphere->radiussq) ||
       (EucDistSq(rect->topleft->p, sphere->pos->p, sphere->pos->dim) < sphere->radiussq))
    {
        noOverlap  = false;
    }
    
    pdPoint[0] = rect->bottomright->p[0];
    pdPoint[1] = rect->topleft->p[1];
    if(EucDistSq(pdPoint, sphere->pos->p, rect->dim) < sphere->radiussq)
    {
        noOverlap = false;
    }

    pdPoint[0] = rect->topleft->p[0];
    pdPoint[1] = rect->bottomright->p[1];
    if(EucDistSq(pdPoint, sphere->pos->p, rect->dim) < sphere->radiussq)
    {
        noOverlap = false;
    }

    return(noOverlap);
}

/**
 @brief     Copy a #Pointf
 @param[in] other       -   the #Pointf to copy
 @return    <#Pointf*>  -   pointer to newly allocated #Pointf
 @date      15.09.2016
 @author    Tim Elberfeld
 */
Pointf* CopyPointf(Pointf* other)
{
    Pointf* pt = NewPointf(other->p, other->dim);
    return(pt);
}

/**
 @brief     Copy a #KDNode 
 @param[in] other       -   the #KDNode to copy
 @return    <KDNode*>   -   the copied data struture
 @date      18.09.2016
 @author    Tim Elberfeld
 */
KDNode* CopyKDNode(KDNode* other)
{
    KDNode* newNode = NULL;
    if(other)
    {
        newNode = (KDNode*) calloc(1, sizeof(KDNode));
        newNode->pos = CopyPointf(other->pos);
        newNode->left = CopyKDNode(other->left);
        newNode->right = CopyKDNode(other->right);
        newNode->size = other->size;
    }
    return(newNode);
}

/**
 @brief     Copy a #KDTree
 @param[in] other       -   the #KDTree to copy
 @return    <KDTree*>   -   the copied data struture
 @date      18.09.2016
 @author    Tim Elberfeld
 */
KDTree* CopyKDTree(KDTree* other)
{
    KDTree* newTree = NULL;
    if(other)
    {
        newTree = (KDTree*) calloc(1, sizeof(KDTree));
        newTree->BB = CopyHyperrect(other->BB);
        newTree->dim = other->dim;
        newTree->size = other->size;
        newTree->root = CopyKDNode(other->root);
    }
    return(newTree);
}

/**
@brief      Calculate the squared euclidean distance of two 
            double arrays of coordinates
@param[in]  a       -   first point's coordinates
@param[in]  b       -   second point's coordinates
@param[in]  nDim    -   number of coordinates in @p a and @p b
@date       15.09.2016
@author     Tim Elberfeld
*/
double EucDistSq(double* a, double* b, int32 nDim)
{
    double distsq = 0.0;
    double diff;
    for (int32 ii = 0; ii < nDim; ii++)
    {
        diff = a[ii] - b[ii];
        distsq += diff * diff;
    }
    return distsq;
}

/**
 @brief     Construct a new #KDTree struct
 @param[in] nNumDim     -   number of dimensions of the kdtree
 @return    <KDTree*>   -   pointer to the newly allocated kdtree
 @date      13.08.2016
 @author    Tim Elberfeld
 */
KDTree* NewKDTree(int32 nDim)
{
    KDTree* Tree    = (KDTree*) calloc(1, sizeof(KDTree));
    Tree->size      = 0;
    Tree->dim       = nDim;
    Tree->root      = NULL;
    double* pdPos   = (double*) calloc(nDim * 2, sizeof(double));
    
    for (int32 ii = 0; ii < 2*nDim; ii++)
    {
        pdPos[ii] = ii < nDim ? DBL_MAX : -DBL_MAX;
    }
    
    Tree->BB        = NewHyperrect(pdPos, pdPos + nDim, nDim);
    free(pdPos);
    return(Tree);
}

/**
 @brief     Construct a new #KDNode struct
 @param[in] ptPos       -   position information of the node
 @return    <KDNode*>   -   pointer to the newly allocated kdnode
 @date      13.08.2016
 @author    Tim Elberfeld
 */
KDNode* NewKDNode(Pointf* ptPos)
{
    KDNode* newNode = (KDNode*) calloc(1, sizeof(KDNode));
    newNode->pos    = CopyPointf(ptPos);
    newNode->size   = 0;
    newNode->left   = NULL;
    newNode->right  = NULL;
    return(newNode);
}

/**
 @brief     Make a new #Pointf struct
 @param[in] pdPos       -   coordinate information
 @param[in] nDim        -   number of dimensions (also number of @c doubles in pdPos)
 @return    <Pointf*>   -   pointer to newly allocated Pointf
 @date      15.09.2016
 @author    Tim Elberfeld
 */
Pointf* NewPointf(double* pdPos, int32 nDim)
{
    Pointf* retpt = NewEmptyPointf(nDim);
    memcpy(retpt->p, pdPos, nDim * sizeof(double));
    return(retpt);
}

/**
 @brief     Make a new 2D #Pointf
 @param[in] dX  -   x coordinate
 @param[in] dY  -   y coordinate
 @date      19.09.2016
 @author    Tim Elberfeld
 */
Pointf* NewPointf2(double dX, double dY)
{
    Pointf* retpt = NewEmptyPointf(2);
    retpt->p = (double*) calloc(2, sizeof(double));
    retpt->p[0] = dX;
    retpt->p[0] = dY;
    retpt->dim = 2;
    return(retpt);
}

/**
 @brief     Make a new empty #Pointf struct
 @param[in] nDim        -   number of dimensions (also number of @c doubles in pdPos)
 @return    <Pointf*>   -   pointer to newly allocated Pointf
 @date      15.09.2016
 @author    Tim Elberfeld
 */
Pointf* NewEmptyPointf(int32 nDim)
{
    Pointf* retpt   = (Pointf*) calloc(1, sizeof(Pointf));
    retpt->dim      = nDim;
    retpt->p        = (double*) calloc(nDim, sizeof(double));
    return(retpt);
}

/**
 @brief     Allocate space for a new (empty) #PointfArray
 @param[in] nNumPointf  -   number of points to allocate
 @param[in] nDim        -   number of dimensions of the points
 @date      19.09.2016
 @author    Tim Elberfeld
 */
PointfArray* NewPointfArray(int32 nNumPointf, int32 nDim)
{
    PointfArray* pResult = (PointfArray*) calloc(1, sizeof(PointfArray));
    
    pResult->dim = nDim;
    pResult->size = 0;
    pResult->allocated = __max(nNumPointf, INITIAL_POINTF_COUNT);
    int32 nBytesPerPos = sizeof(double) * nDim;
    
    pResult->points = (Pointf*) calloc(pResult->allocated, sizeof(Pointf));
    
    for (int ii = 0; ii < pResult->allocated; ii++)
    {
        pResult->points[ii].dim = nDim;
        pResult->points[ii].p = (double*)AllocBytes(nBytesPerPos, "double", sizeof(double));
    }
    
    return(pResult);
}


/**
@brief      Make a new #Hyperrect struct
@param[in]  topleft         -   pointer to coordinate information for top left point
@param[in]  bottomright     -   pointer to coordinate information for bottom right point
@param[in]  nDim            -   number of dimensions of the coordinate
@return     <Hyperrect*>    -   pointer to newly allocated Hyperrect
@date       15.09.2016
@author     Tim Elberfeld
*/
Hyperrect* NewHyperrect(double* topleft, double* bottomright, int32 nDim)
{
    Hyperrect* retrect      = (Hyperrect*) calloc(1, sizeof(Hyperrect));
    retrect->topleft        = NewPointf(topleft, nDim);
    retrect->bottomright    = NewPointf(bottomright, nDim);
    retrect->dim            = nDim;
    return(retrect);
}

/**
 @brief     Make a new #Hypersphere struct
 @param[in] ptOrigin    -   the origin of the #Hypersphere
 @param[in] dRadius     -   radius of the #Hypersphere
 @date      19.09.2106
 @author    Tim Elberfeld
 */
Hypersphere* NewHypersphere(Pointf* ptOrigin, double dRadius)
{
    Hypersphere* newHypersphere = (Hypersphere*) calloc(1, sizeof(Hypersphere));
    newHypersphere->pos = CopyPointf(ptOrigin);
    newHypersphere->radius = dRadius;
    newHypersphere->radiussq = dRadius * dRadius;
    return(newHypersphere);
}

/**
 @brief     Clean up the Pointf struct memory
 @param[in] p   -   #Pointf to free
 @date      15.09.2016
 @author    Tim Elberfeld
 */
void FreePointf(Pointf* p)
{
    if(p)
    {
        if(p->p)
        {
            free(p->p); /* free coordinate information */
        }
        free(p); /* free point structure */
    }
}

/**
 @brief     Free the memory allocated for a #PointfArray
 @param[in] p   -   #PointfArray to free
 @date      19.09.2016
 @author    Tim Elberfeld
 */
void FreePointfArray(PointfArray* p)
{
    if(p)
    {
        if(p->points)
        {
            for (int32 ii = 0; ii < p->allocated; ii++)
            {
                if(p->points[ii].p)
                {
                    free(p->points[ii].p);
                }
            }
            free(p->points);
        }
        free(p);
    }
}

/**
@brief      Free #KDNode recursively. This will delete _all subtrees_
@param[in]  h   -   the hyperrect
@date       15.09.2016
@author     Tim Elberfeld
*/
void FreeKDTree(KDTree* t)
{
    if(t)
    {
        FreeKDNode(t->root);
        FreeHyperrect(t->BB);
        free(t);
    }
}

/**
@brief      Free #KDNode recursively. This will delete _all subtrees_
@param[in]  h   -   the hyperrect
@date       15.09.2016
@author     Tim Elberfeld
*/
void FreeKDNode(KDNode* n)
{
    if(n)
    {
        FreeKDNode(n->right);
        FreeKDNode(n->left);
        FreePointf(n->pos);
        free(n);
    }
}

/**
@brief      Free memory that was allocated for #Hyperrect
@param[in]  h   -   the hyperrect
@date       15.09.2016
@author     Tim Elberfeld
*/
void FreeHyperrect(Hyperrect* h)
{
    if(h)
    {
        FreePointf(h->topleft);
        FreePointf(h->bottomright);
    }
}    

/**
 @brief     Test if two Pointf's are the same
 @param[in] p1      -   first position
 @param[in] p2      -   second position
 @param[in] nDim    -   number of dimensions of the coordinates
 @return    <bool>  -   @c true if the points are the same <br>
                        @c false otherwise
 @date      13.08.2016
 @author    Tim Elberfeld
 */
bool SamePos(double* p1, double* p2, int32 nDim)
{
    bool bRetVal = true;
    for (int32 ii = 0; ii < nDim; ii++)
    {
        if(p1[ii] != p2[ii])
        {
            bRetVal = false;
            break;
        }
    }
    return(bRetVal);
}

/**
 @brief         Update bounding box of KDTree
 @param[inout]  Tree    -   the tree to update
 @param[in]     ptPos   -   the position to update the bounding box with
 @date          13.08.2016
 @author        Tim Elberfeld
 @todo          Udpate bounding box also when a node is removed!
 */
void UdpateMinMax(KDTree* Tree, Pointf* ptPos)
{
    if(ptPos->dim == Tree->dim)
    {
        Pointf* topleft = Tree->BB->topleft;  /* copy the pointers to make the code more readable */
        Pointf* bottomright = Tree->BB->bottomright; 
        for (int32 ii = 0; ii < Tree->dim; ii++)
        {
            topleft->p[ii]      = __min(topleft->p[ii], ptPos->p[ii]);
            bottomright->p[ii]  = __max(bottomright->p[ii], ptPos->p[ii]);
        }
    }
}

/**
 @brief     Insert a 2D point into the @p Tree
 @param[in] Tree    -   the tree to add the node to
 @param[in] dX      -   x coordinate
 @param[in] dY      -   y coordinate
 @date      15.09.2016
 @author    Tim Elberfeld
 */
void KDInsert2(KDTree* Tree, double dX, double dY)
{
    Pointf* ptPos = NewEmptyPointf(2);
    ptPos->p[0] = dX;
    ptPos->p[1] = dY;
    KDInsert(Tree, ptPos);
    FreePointf(ptPos);
}

/**
 @brief         Insert a new node into a tree
 @param[inout]  Tree    -   the tree to insert the node in
 @param[in]     ptPos   -   the position of the node to be added
 @date          13.08.2016
 @author        Tim Elberfeld
 */
void KDInsert(KDTree* Tree, Pointf* ptPos)
{
    assert(Tree->dim > 0);
    assert(ptPos->dim == Tree->dim);
    bool bAdded = false;
    Tree->root = KDInsertRec(Tree->root, ptPos, &bAdded, Tree->dim, 0);
    
    if(bAdded)
    {
        Tree->size++;
        UdpateMinMax(Tree, ptPos);
    }
}

/**
 @brief         recursively insert into the tree
 @param[in]     CurNode     -   root node of the current subtree
 @param[in]     ptPos       -   the position of the node to be added
 @param[in]     rbAdded     -   Will be set to @c true of a node was added.
                                Value will stay unchanged if no node was added.
 @param[in]     nDim        -   number of dimensions of the tree
 @param[in]     nDepth      -   current recursion depth
 @return        <KDNode*>   -   new root of the current subtree
 @date          13.08.2016
 @author        Tim Elberfeld
 */
KDNode* KDInsertRec(KDNode* CurNode, Pointf* ptPos, bool* rbAdded, int32 nDim, int32 nDepth)
{
    if(CurNode == NULL)
    {
        *rbAdded = true;
        return NewKDNode(ptPos);
    }
    
    int32 cd = nDepth % nDim;
    
    if (SamePos(ptPos->p, CurNode->pos->p, ptPos->dim))
    {
        /* do nothing, just return the input node */
    }
    else if (ptPos->p[cd] < CurNode->pos->p[cd])
    {
        bool bAdded = false;
        CurNode->left = KDInsertRec(CurNode->left, ptPos, &bAdded, nDim, nDepth + 1);
        if(bAdded)
        {
            *rbAdded |= true;
            CurNode->left->size++;
        }
    }
    else
    {
        bool bAdded = false;
        CurNode->right = KDInsertRec(CurNode->right, ptPos, &bAdded,  nDim, nDepth + 1);
        if(bAdded)
        {
            *rbAdded |= true;
            CurNode->right->size++;
        }
    }
    
    return(CurNode);
}

/**
 @brief         Find the minimum point in a subtree
 @param[in]     CurNode     -   root of the current subtree
 @param[in]     nDir        -   coordinate to use to compare for FindMin()
 @param[in]     nDim        -   number of dimensions of the tree
 @param[in]     nDepth      -   current recursion depth
 @return        <KDNode*>   -   the minimum node of the subtree that was passed
 @date          13.08.2016
 @author        Tim Elberfeld
 */
KDNode* FindMin(KDNode* CurNode, int32 nDir, int32 nDim, int32 nDepth)
{
    assert(nDim > 0);
    return FindMinRec(CurNode, nDir, nDim, nDepth);
}

/**
 @brief         Find the minimum point in a subtree (recursively)
 @param[in]     CurNode     -   root of the current subtree
 @param[in]     nDir        -   coordinate to use to compare for FindMin()
 @param[in]     nDim        -   number of dimensions of the tree
 @param[in]     nDepth      -   current recursion depth
 @return        <KDNode*>   -   the minimum node of the subtree that was passed
 @date          13.08.2016
 @author        Tim Elberfeld
 */
KDNode* FindMinRec(KDNode* CurNode, int32 nDir, int32 nDim, int32 nDepth)
{
    if(!CurNode)
    {
        return NULL;
    }
    if(CurNode->left == NULL && CurNode->right == NULL)
    {
        return CurNode; // is leaf node
    }
    
    int32 curdim = nDepth % nDim;
    /*  if the current dimension is the same as the dimension that we search the 
        minimum in we only need to search left. otherwise both subtrees
        are candidates for the search */
    if(curdim == nDir)
    {
        if(CurNode->left == NULL)
        {
            return(CurNode); /* no smaller node in tree */
        }
        else
        {
            /* left subtree is populated -> we need to go deeper */
            return FindMinRec(CurNode->left, nDir, nDim, nDepth + 1);
        }
    }
    /*  we have to search both subtrees and find
        the smallest value compared to the current node */
    KDNode* left_sub = FindMinRec(CurNode->left, nDir, nDim, nDepth + 1);
    KDNode* right_sub = FindMinRec(CurNode->right, nDir, nDim, nDepth + 1);
    
    return(MinOfThree(CurNode, left_sub, right_sub, nDir));
}

/**
 @brief         Find the smalles of three nodes with respect to coordinate nDir
 @param[in]     a           -   first node
 @param[in]     b           -   second node
 @param[in]     c           -   third node
 @param[in]     nDir        -   coordinate to use to compare
 @return        <KDNode*>   -   pointer to the node that is the smallest
 @date          13.08.2016
 @author        Tim Elberfeld
 */
KDNode* MinOfThree(KDNode* a, KDNode* b, KDNode* c, int32 nDir)
{
    if(a)
    {
        KDNode* result = a;
        if(b)
        {
            if(b->pos->p[nDir] < result->pos->p[nDir])
            {
                result = b;
            }
        }
        if(c)
        {
            if(c->pos->p[nDir] < result->pos->p[nDir])
            {
                result = c;
            }
        }
        return result;
    }
    QuitWithError("First node can't be invalid", ERR_NULL_POINTER);
    return(NULL); /* this is just here so that the compiler does not complain */
}

/**
 @brief     Remove a node from the kdtree
 @param[in] Tree    -   the tree to remove the node from
 @param[in] ptPos   -   the position of the node to remove
 @date      13.08.2016
 @author    Tim Elberfeld
 */
void KDRemove(KDTree* Tree, Pointf* ptPos)
{
    bool bRemoved = false;
    Tree->root = KDRemoveRec(Tree->root, ptPos, &bRemoved, Tree->dim, 0);
    if(bRemoved)
    {
        Tree->size--;
    }
}

/**
 @brief         Remove a 2D point from the tree
 @param[inout]  Tree    -   the tree to remove the node from
 @param[in]     dX      -   x coordinate
 @param[in]     dY      -   y coordinate
 @date          15.09.2016
 @author        Tim Elberfeld
 */
void KDRemove2(KDTree* Tree, double dX, double dY)
{
    if(Tree->dim == 2)
    {
        Pointf* ptPos = NewEmptyPointf(2);
        ptPos->p[0] = dX;
        ptPos->p[1] = dY;
        KDRemove(Tree, ptPos);
        FreePointf(ptPos);
    }
}

/**
 @brief         Remove a node from the kdtree recursively
                there are 4 possible cases:
                -#  node is invalid (NULL)
                -#  leaf node: just remove
                -#  one child (left(2a) or right(2b))
                -#  two children: search both subtrees
 @param[in]     CurNode     -   root of the current subtree
 @param[in]     ptPos       -   the position of the node to remove
 @param[inout]  rbRemoved   -   Will be set to @c true if a node was added.
                                Value is unchanged when no node was added
 @param[in]     nDim        -   number of dimensions of the tree
 @param[in]     nDepth      -   current recursion depth
 @return        <KDNode*>   -   the new root node of the subtree that was passed
 @date          13.08.2016
 @author        Tim Elberfeld
 */
KDNode* KDRemoveRec(KDNode* CurNode, Pointf* ptPos, bool* rbRemoved, int32 nDim, int32 nDepth)
{    
    if(CurNode == NULL)
    {
        return(NULL); /* case 0, nothing happens */
    }
    else
    {
        int32 cd = nDepth % nDim;
        if(SamePos(CurNode->pos->p, ptPos->p, ptPos->dim))
        {
            if(CurNode->right != NULL) /* case 2b*/
            {
                KDNode* node_min = FindMin(CurNode->right, cd, nDim, nDepth + 1);
                CurNode->pos = node_min->pos;
                bool bRemoved = false;
                CurNode->right = KDRemoveRec(CurNode->right, node_min->pos, &bRemoved, nDim, nDepth + 1);
                if(bRemoved && CurNode->right)
                {
                    *rbRemoved |= true;
                    CurNode->right->size--;
                }
            }
            else if(CurNode->left != NULL) /* case 2a */
            {
                KDNode* node_min = FindMin(CurNode->left, cd, nDim, nDepth + 1);
                CurNode->pos = node_min->pos;
                
                /* swap subtrees */
                bool bRemoved = false;
                CurNode->right = KDRemoveRec(CurNode->left, node_min->pos, &bRemoved, nDim, nDepth + 1);
                CurNode->left = NULL;
                if(bRemoved && CurNode->right)
                {
                    *rbRemoved |= true;
                    CurNode->right->size--;
                }
            }
            else    /* case 1, just delete node */
            {
                *rbRemoved = true;
                free(CurNode);
                return(NULL);
            }
        }
        else
        {
            if(ptPos->p[cd] < CurNode->pos->p[cd]) /* go left */
            {
                bool bRemoved = false;
                CurNode->left = KDRemoveRec(CurNode->left, ptPos, &bRemoved, nDim, nDepth + 1);
                if(bRemoved && CurNode->left)
                {
                    *rbRemoved |= true;
                    CurNode->left->size--;
                }
            }
            else    /* go right */
            {
                bool bRemoved = false;
                CurNode->right = KDRemoveRec(CurNode->right, ptPos, &bRemoved, nDim, nDepth + 1);
                if(bRemoved && CurNode->right
                   )
                {
                    *rbRemoved |= true;
                    CurNode->right->size--;
                }
            }
        }
    }
    
    return(CurNode);
}


/**
 @brief     write kd tree to a <a href=http://www.graphviz.org/content/dot-language>dot</a> 
            file, to visualize the structure
 @param[in] Tree        - the tree to visualize
 @param[in] strFilename - name of the text file that will be created
 @date      10.08.2016
 @author    Tim Elberfeld
 */
void KDTreeToDotFile(KDTree* Tree, const char* strFilename)
{
    if(Tree)
    {
        FILE* pFile = MakeOpenLogFile(strFilename, "w");
        
        fprintf(pFile, "%s", "digraph d { \n"); /* print opening statement for the graph in dot language */
        fprintf(pFile, "rankdir=UD \n graph [splines=ortho] \n"); /* set some options */
        
        /* traverse the tree and print the nodes */
        int32* num = (int32*) calloc(1, sizeof(int32));  /* make this a unique location to make
                                                            sure numbers can't occur twice */
        *num = 0;
        char* strRoot = KDNodeToString(Tree->root, num);
        KDNodeLabelToFile(pFile, Tree->root, strRoot, "white");
        KDNodesToFile(pFile, Tree->root, "root", num);
        
        if(strRoot)
        {
            free(strRoot);
        }
        if(num)
        {
            free(num);
        }
        fprintf(pFile,"%s", "}");            /* close the digraph environment */
        CloseFile(pFile);
    }
}

/**
 @brief 	write a node and the subtree of the node to a .dot file
 @param[in]	pFile			-	handle to the open file
 @param[in]	nodes 			-	the node to write to the file
 @param[in]	strParentname	-	name of the parent to write the relationship between
                                node and parent to the file
 @param[in]	num 			-	number of the node (to generate the name string)
 @date 		10.08.2016
 @author	Tim Elberfeld
 */
void KDNodesToFile(FILE* pFile, KDNode* node, const char* strParentname, int32* num)
{
    static const char* strColorRight = "cornflowerblue";
    static const char* strColorLeft = "coral";
    if(node && pFile)
    {
        char* strLeft = NULL;
        char* strRight = NULL;
        
        if(node->left)
        {
            (*num)++;
            strLeft = KDNodeToString(node->left, num);
            KDNodeLabelToFile(pFile, node->left, strLeft, strColorLeft);
            fprintf(pFile, "%s -> %s \n", strParentname, strLeft);
        }
        
        if(node->right)
        {
            (*num)++;
            strRight = KDNodeToString(node->right, num); /* name of the current node */
            KDNodeLabelToFile(pFile, node->right, strRight, strColorRight);
            fprintf(pFile, "%s -> %s \n", strParentname, strRight);
        }
        
        if(strLeft)
        {
            KDNodesToFile(pFile, node->left, strLeft, num);
            free(strLeft);
        }
        if(strRight)
        {
            KDNodesToFile(pFile, node->right, strRight, num);
            free(strRight);
        }
    }
}

/**
 @brief 	Write the node label definition to the .dot file.
            This makes a label for the node containing the 2d position.
            A color will also be added. For a root node with position (1.0, 2.0)
            the string written to the file will look like this:
            <pre>
            root [label="(1.000, 2.000)", style="filled", fillcolor="white"]
            </pre>
 @param[in]	pFile 		-	handle to the open file
 @param[in] node 		- 	the node whose information to write to the file
 @param[in] strName 	-	name of the node (i.e. node1234)
 @param[in] strColor 	-	color to give to the node
 @date 		10.08.2016
 @author	Tim Elberfeld
 */
void KDNodeLabelToFile(FILE* pFile, KDNode* node, const char* strName, const char* strColor)
{
    if(node)
    {
        char* strPos = KDNodePositionToString(node);
        fprintf(pFile, "%s [label=\"(%s)\", style=\"filled\", fillcolor=\"%s\"] \n",
                strName, strPos, strColor);
        if(strPos)
        {
            FreeBytes(strPos);
        }
    }
    else
    {
        fprintf(pFile, "empty \n");
    }
}

/**
 @brief      convert the position of a node into a string
 @param[in]  node    -   the node
 @return     <char*> -   the string that was created. (memory has to be free'd!)
 @date       15.09.2016
 @author     Tim Elberfeld
 */
char* KDNodePositionToString(KDNode* node)
{
    int nBytes = node->pos->dim * (DBL_MAX_CHAR_COUNT + 2); /* chars per number plus 2 chars for a space and the comma */
    char* strPos = (char*) AllocBytes(nBytes, "char", sizeof(char));
    int nCursor = 0;
    
    for(int32 ii = 0; ii  < node->pos->dim; ii++)
    {
        nCursor += sprintf(strPos + nCursor, "%.2f, ", node->pos->p[ii]);
    }
    return(strPos);
}

/**
 @brief 	make a node name string to input to NodeLabelToFile()
 @param[in]	node 	-	the node
 @param[in]	num 	-	number of the node.
 @return 	<char*>	-	name of the node
 @note 		if num is 0 the name returned will be "root".
 @date 		10.08.2016
 @author	Tim Elberfeld
 */
char* KDNodeToString(KDNode* node, int32* num)
{
    char* strName = (char*) calloc(MAX_PATH + 1, sizeof(char));
    if(*num == 0)
    {
        sprintf(strName, "%s","root");
    }
    else
    {
        sprintf(strName, "node%d", *num);
    }
    return(strName);
}

/**
 @brief     Logging function for #Pointf s
 @param[in] pt  -   the point to log
 @date      16.09.2016
 @author    Tim Elberfeld
 */
void LogPointf(Pointf* pt)
{
    char* strPoint = PointfToString(pt);
    WriteToLogFile((int)strlen(strPoint) + 8, "point (%s)", strPoint);
    free(strPoint);
}

/**
 @brief     Make a string out of the #Pointf position
 @param[in] pt  -   the point to convert
 @date      16.09.2016
 @author    Tim Elberfeld
 */
char* PointfToString(Pointf* pt)
{
    int nBytes = pt->dim * (DBL_MAX_CHAR_COUNT + 6);
    char* strPoint = (char*) calloc(nBytes, sizeof(char));
    int pos = 0;
    for (int ii = 0; ii < pt->dim; ii++)
    {
        pos += sprintf(strPoint + pos, "%.3f, ", pt->p[ii]);
    }
    strPoint[pos+1] = '\0';
    return(strPoint);
}
