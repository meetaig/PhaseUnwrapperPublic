/**
 @file      timer.c
 @brief     implementation of simple timer structure to measure code performance.
 @author    Tim Elberfeld
 @date      21.09.2016
 */

#include "timer.h"

/**
 @brief         Initialize the timer struct
 @param[inout]  timer   -   the timer structure
 @date          21.09.2016
 @author        Tim Elberfeld
 */
void InitTimer(Timer* timer)
{
    timer->start_t = 0;
    timer->end_t = 0;
    timer->secs_start = 0.0;
    timer->secs_end = 0.0;
}

/**
 @brief         Start the timer (fill Timer::start_t and Timer::secs_start)
 @param[inout]  timer   -   the timer structure
 @date          21.09.2016
 @author        Tim Elberfeld
 */
void StartTimer(Timer* timer)
{
    timer->start_t = clock();
    timer->secs_start = (double)timer->start_t / CLOCKS_PER_SEC;
}

/**
 @brief         End the timer (fill Timer::end_t and Timer::secs_end)
 @param[inout]  timer   -   the timer structure
 @date          21.09.2016
 @author        Tim Elberfeld
 */
void EndTimer(Timer* timer)
{
    timer->end_t = clock();
    timer->secs_end = (double)timer->end_t / CLOCKS_PER_SEC;
}

/**
 @brief     returns the time elapsed
 @param[in] timer       -   the timer structure
 @return    <double>    -   time elapsed in milliseconds
 */
double GetTimeElapsed(Timer* timer)
{
    if(timer)
    {
        return(1000 * (timer->secs_end - timer->secs_start));
    }
    return(-1.0);
}

/**
 @brief     returns the time elapsed
 @param[in] timer       -   the timer structure
 @return    <double>    -   time elapsed in milliseconds
 */
uint64 GetCyclesElapsed(Timer* timer)
{
    if(timer)
    {
        return((uint64)(timer->end_t - timer->start_t));
    }
    return(0);
}