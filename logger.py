# -*- coding: utf-8 -*-
#
# !/usr/bin/env python
#
# :brief:   logging module to output log files
#
# :date:    18.04.2016
# :author:  Tim Elberfeld
#
# :usage:   customLog = logger.get_custom_logger('filename.log')
#           customLog.error(msg)        # log an error message
#           customLog.debug(msg)        # log a debug message
#           ...

import os
import logging
from pprint import pprint
import numpy as np
import matplotlib.image as mpimg



def get_custom_logger(filename='default.log'):
    """
    :brief:             get a customized logger class that has all the formatting done already
    :param filename:    filename of the logfile
    :return:            customized instance of logging
    :date:              18.04.2016
    :author:            Tim Elberfeld
    """
    logger = logging.getLogger('CustomLogger')
    logger.setLevel(logging.DEBUG)              # all messages will be logged

    logger.handlers = []                        # if there are handlers in the logger that we don't want, delete them
    logger.addHandler(LogHandler(filename))     # add the custom handler if it is not already there

    return logger


class LogHandler(logging.FileHandler):
    def __init__(self, filename):
        logdir = get_log_dir() + filename
        logging.FileHandler.__init__(self, logdir)

        fmt = '%(asctime)s %(levelname)s ln %(lineno)d %(funcName)s()  %(message)s'
        fmt_date = '%d-%m-%Y %T'
        formatter = logging.Formatter(fmt, fmt_date)
        self.setFormatter(formatter)


def get_log_dir():
    """
    :brief:     Get the project directory and make sure there is a subfolder called 'log'.
                Return the path to this folder
    :return:    Path to logging directory
    :date:      18.04.2016
    :author:    Tim Elberfeld
    """
    cwd = os.getcwd()
    logpath = (cwd + '/log/')

    if not os.path.isdir(logpath):      # make the directory if it not exists
        os.mkdir(logpath)

    return logpath


# ----------- debug tools -----------
def fullprint(*args, **kwargs):         # print function that makes the output print everything
    oldopt = np.get_printoptions()
    np.set_printoptions(threshold=10000)
    pprint(*args, **kwargs)
    np.set_printoptions(**oldopt)


def log_image(img, name="default.png", cmap="hot"):
    """
    :brief:         simple function to save arrays as images
    :param img:     the image to save
    :param name:    name of the image in the log directory
    :param cmap:    colormap specifier, as usual in matplotlib
    :return:        nothing
    :date:          29.06.2016
    :author:        Tim Elberfeld
    """
    mpimg.imsave(get_log_dir() + name, img, cmap=cmap)


def print_array_stats(array, name = '', printfun=None):

    msg = name + " mean {0:.3f} std {1:.3f} min {2:.3f} max {3:.3f}".format(
        np.mean(array),
        np.std(array),
        np.min(array),
        np.max(array))

    if printfun is None:
        print(msg)
    else:
        printfun(msg)


def clear_log_dir(verbose=False):
    """
    :brief:         simple function to delete everything contained in the log directory
    :param verbose: if set to True, function will print what it deleted
    :return:        nothing
    """
    logdir = get_log_dir()

    logcontent = os.listdir(logdir)  # returns list of everything that is contained in log/

    print "deleting the following files from logdir:"

    for file in logcontent:
        filepath = logdir + file
        print file
        os.remove(filepath)

    print " "
    print " "


def save_np_array(array, filename):
    """
    :brief:             save a numpy array as raw data in .npy format in the log directoy
    :param array:       the array to save
    :param filename:    name of the file
    :return:            nothing
    :date:              25.07.2016
    :author:            Tim Elberfeld
    """
    path = get_log_dir() + filename
    np.save(path, array)
