#ifndef __QUALITY_MAPS_H
#define __QUALITY_MAPS_H
/**
 @file      qualitymaps.h
 @brief		Interface for the quality maps functions. For descriptions of the functions, see qualitymaps.c
 @date		25.07.2016
 @author	Tim Elberfeld
 */

#include "utils.h"
#include "fileIO.h"
#include "statistics.h"
#include "general.h"
#include "imageprocessing.h"

typedef enum QualityMapMode /*! enum for the different qualitymaps that are implemented */
{
	QMAP_VARIANCE			= 0x00, /*!< PhaseDerivativeVariance() */
	QMAP_PSEUDO_CORR		= 0x01, /*!< PseudoCorr() */
	QMAP_PHASE_GRADIENT 	= 0x02  /*!< MaxPhaseGradient() */
} QualityMapMode;

#define DEFAULT_BOX_SIZE 3  /* default is a 3x3 pixel area */

void MakeQualityMap(double* pdWrappedPhase, int32 nMode, int32 nWidth, int32 nHeight, int32 nBoxSize, double* pdQualityMap);
void PhaseDerivativeVariance(double* pdWrappedPhase, ArrayDim nDimensions, int32 nBoxSize, double* pdQualityMap);
void MaxPhaseGradient(double* pdWrappedPhase, ArrayDim nDimensions, int32 nBoxSize, double* pdQualityMap);
void PseudoCorr(double* pdWrappedPhase, ArrayDim nDimensions, int32 nBoxSize, double* pdQualityMap);
#endif /* __QUALITY_MAPS_H */
