#ifndef __STATISTICS_H
#define __STATISTICS_H
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include "linked_list.h"
#include "utils.h"
#include "fileIO.h"
#include "general.h"

/**
@file       statistics.h
@brief		Interface for statistics functions for phase unwrapping. 
			For descriptions of the functions, see statistics.c
@date		25.07.2016
@author		Tim Elberfeld
*/

/* magic values for histogram plotting in bitmaps */
#define MIN_IMG_WIDTH 	512
#define MIN_BAR_WIDTH 	3
#define MAX_BAR_WIDTH 	50
#define MIN_BAR_SPACING 1

#define FLAG_EMPTY		0x00		/* empty flag (no bits set)  */
#define FLAG_LOG 		0x01		/* is the histogram is supposed to be logarithmic? */
#define FLAG_CUMUL 		0x02		/* is the histogram a cumulative one? */
#define FLAG_NORM		0x04		/* is the histogram normalized? */

typedef struct _HISTOGRAM
{
    double* data;                   /* holds the data of the histogram */
    double* labels;                 /* holds labels of the bins */
    double srcmin;                  /* minimum value of the source data that produced the histogram */
    double srcmax;                  /* maximum value "  "   "       "   "       "       "   "       */
    double hmax;                    /* max value */
    double hmin;                    /* min value */
    uint32 bins;                    /* number of bins (and therefore number of elemens in data and labels) */
    FLAG flags;                     /*  space for flags. Can be  */
                                    /*  -   FLAG_CUMUL for cumulative histogram	*/
                                    /*  -   FLAG_LOG for logarithmic values */
                                    /*  -   FLAG_NORM for normalized values */
                                    /* the flags can be present at the same time (i.e. if(FLAG_CUMUL && FLAG_NORM) means a normalized cumulative histogram) */
} Histogram;

typedef struct _STATS
{
    double  variance;               /* variance */
    double  stddev;                 /* standard deviation */
    double  mean;                   /* mean value */
    double  minval;                 /* min value -> name change to resolve ambiguity with min/max macro*/
    double  maxval;                 /* max value */
    uint64  numel;                  /* number of values that were used to compute above stats */
} Stats;

#define MedianOfThree(a, b, c) (((a)-(b))*((b)-(c)) > -1 ? (b) : (((a)-(b))*((a)-(c)) < 1 ? (a) : (c)))

/* logging functions */
int32       SaveHistogramAsCSV(Histogram* pHisto, const char* strFilename);
int32       SaveHistogramAsBMP(Histogram* pHisto, const char* strFilename, CMAP colormap);
void        PrintHistogram(Histogram* pHisto);
void        PrintHistogramStats(Histogram* pHisto);

/* allocation and deallocation */
Histogram*  NewHistogram(int32 nBins, FLAG flags);
void        FreeHistogram(Histogram* pHisto);

/* modification of the histogram data */
void        MakeLogarithmic(Histogram* pHisto);
void        MakeCumulative(Histogram* pHisto);
void        NormalizeHistogram(Histogram* pHisto);
void        ClearHistogram(Histogram* pHisto);
void        UpdateHistogram(Histogram* pHisto, uint32 nBinIndex);

void        ComputeHistogram_double(double* pdArray, ArrayDim nDimension, Histogram* pHisto);
void        ComputeHistogram_int32(int32* pnArray, ArrayDim nDimension, Histogram* pHisto);
void        ComputeHistogram_uint8(uint8* pcArray, ArrayDim nDimension, Histogram* pHisto);

/* statistics computation */
void        ComputeStats_double(double* pdArray, uint64 numel, Stats* statistic , bool bDoPrintOut);
void        ComputeStats_int32(int32* pdArray, uint64 numel, Stats* statistic , bool bDoPrintOut);
void        ComputeStats_uint8(uint8* pdArray, uint64 numel, Stats* statistic , bool bDoPrintOut);

void        InitStats(Stats* statistic);

/* internal functions */
void        ApplyFlags(Histogram* pHisto);
void        SetHistogramLabels(Histogram* pHisto, double dBucketSize);

/* multi purpose statistics functions */
double      ComputeQuantile(double* pdArray, uint64 nNumel, double dQuantile);
void        ComputeQuantileRec(double* pdArray, uint64 nNumel, uint64 niLeft, uint64 niRight, uint64 niQuantile);
void        QuickSort(double* pdArray, uint64 nNumel, uint64 niLeft, uint64 niRight);
uint64      GetMedianPivot(const double* pdArray, uint64 nElements, uint64 niLeft, uint64 niRight);

double      ComputeMSError_double(double* pdArray, uint64 nNumel);
double      ComputeMSError_int32(int32* pnArray, uint64 nNumel);
double      ComputeMSError_uint8(uint8* pcArray, uint64 nNumel);

double      ComputeMSEDiff_double(double* pdA, double* pdB, uint64 nNumel);
double      ComputeMSEDiff_int32(int32* pdA, int32* pdB, uint64 nNumel);
double      ComputeMSEDiff_uint8(uint8* pdA, uint8* pdB, uint64 nNumel);

double*     Normalize_double(const double* pdArray, uint64 nNumel, double dMin, double dMax);
double*     Normalize_int32(const int32* pnArray, uint64 nNumel, int32 nMin, int nMax);
double*     Normalize_uint8(const uint8* pcArray, uint64 nNumel, uint8 cMin, uint8 cMax);

void        ComputeMinMax_double(double* pdArray, uint64 nNumel, double* dMinVal, double* dMaxVal);
void        ComputeMinMax_int32(int32* pnArray, uint64 nNumel, int32* nMinVal, int32* nMaxVal);
void        ComputeMinMax_uint8(uint8* pcArray, uint64 nNumel, uint8* cMinVal, uint8* cMaxVal);

void        MapToRange_double(double* pArray, uint64 nNumel, double dMinVal, double dMaxVal);
void        MapToRange_int32(int32* pnArray, uint64 nNumel, int32 nMinVal, int32 nMaxVal);
void        MapToRange_uint8(uint8* pcArray, uint64 nNumel, uint8 cMinVal, uint8 cMaxVal);

#endif
