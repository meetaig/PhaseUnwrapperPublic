#ifndef __FILEPATHS_H
#define __FILEPATHS_H

#ifdef _MSC_VER
#define LENA_PATH           "C:/Users/Tim/Desktop/PhaseUnwrapper2/PhaseUnwrapper/lena.bmp"
#define GHIGLIA_DATA_PATH   "E:/Daten/Dropbox/Dropbox/Uni/MasterThesis/code/phase/data"
#define TEST_DATA_PATH      "E:/Daten/Dropbox/Dropbox/Uni/MasterThesis/test_data/"
#else
#define LENA_PATH           "/Users/Tim/Dropbox/Uni/MasterThesis/PythonProjects/PhaseUnwrapper_Clean/lena.bmp"
#define GHIGLIA_DATA_PATH   "/Users/Tim/Dropbox/Uni/MasterThesis/code/phase/data/"
#define TEST_DATA_PATH      "/Users/Tim/Dropbox/Uni/MasterThesis/test_data/"
#endif


#endif /* FILEPATHS_H */
