/**
 @file      kdtree.h
 @brief     Simple implementation of a kd-tree. For more information about functions, see kdtree.c
 @date      13.08.2016
 @author    Tim Elberfeld
 */
#ifndef __KD_TREE_H
#define __KD_TREE_H
#include "utils.h"
#include "fileIO.h"
#include <string.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define INITIAL_POINTF_COUNT    64  /* number of #Pointf to allocate by default */

typedef struct _POINTF          /** simple floating-point Point structure */
{
    double* p;                  /**< pointer to coordinate data */
    int32   dim;                /**< number of dimensions in @c p (has dim*sizeof(double) bytes of memory allocated */
} Pointf;

typedef struct _POINTF_ARRAY    /** an array of #Pointf */
{
    Pointf* points;             /**< pointer to the #Pointf array */
    union                       /** these are the same but for code clarity can be accessed by different names */
    {
        int32 cursor;           /**< index of the next #Pointf that is invalid */
        int32 size;             /**< number of valid  #Pointf in PointfArray::points */
    };
    int32 dim;                  /**< number of dimensions of the #Pointf in the array */
    int32 allocated;            /**< number of #Pointf allocated for PointfArray::points */
} PointfArray;

typedef struct _HYPER_RECT      /** hyper rectangle */
{
    Pointf* topleft;            /**< top left point (name from 2D, but is equivalent in k dimensions) */
    Pointf* bottomright;        /**< bottom right point (name from 2D, but is equivalent in k dimensions) */
    int32   dim;                /**< number of dimensions of the hyperrect */
} Hyperrect;

typedef struct _HYPER_SPHERE    /** hyper sphere (helper for range search */
{
    Pointf* pos;                /**< the position of the sphere's origin */
    double radius;              /**< radius of the sphere */
    double radiussq;            /**< squared radius of the sphere (save on multiplication for range queries)*/
} Hypersphere;


typedef struct _KD_NODE         /** node for the #KDTree */
{
    Pointf* pos;                /**< node position in k dimensions */
    int32 size;                 /**< number of nodes in all subtrees of this node (including itself) */
    struct _KD_NODE* left;      /**< left subtree */
    struct _KD_NODE* right;     /**< right subtree */
} KDNode;

typedef struct _KDTREE          /** KD Tree structure */
{
    KDNode* root;               /**< root node of the tree */
    Hyperrect* BB;              /**< hyper rectangle with bounding box information */
    int32 size;                 /**< number of nodes in the tree */
    int32 dim;                  /**< number of dimensions of the tree */
} KDTree;

typedef enum _TRIM_SIDE         /** enum for TrimHyperrect() */
{
    TRIM_LEFT,                  /**< trim on the left side of the #Hyperrect */
    TRIM_RIGHT                  /**< trim on the right side of the #Hyperrect */
} TrimSide;


/* -------- "constructors" and "destructors" -------- */
KDTree*         NewKDTree(int32 nNumDim);
KDNode*         NewKDNode(Pointf* ptPos);
Pointf*         NewPointf(double* pdPos, int32 nDim);
Pointf*         NewPointf2(double dX, double dY);
Pointf*         NewEmptyPointf(int32 nDim);
PointfArray*    NewPointfArray(int32 nNumPoints, int32 nDim);
Hypersphere*    NewHypersphere(Pointf* ptOrigin, double dRadius);
Hyperrect*      NewHyperrect(double* topleft, double* bottomright, int32 nNumDim);

void            FreeKDTree(KDTree* t);
void            FreeKDNode(KDNode* n);
void            FreePointf(Pointf* p);
void            FreePointfArray(PointfArray* p);
void            FreeHypersphere(Hypersphere* s);
void            FreeHyperrect(Hyperrect* h);

Pointf*         CopyPointf(Pointf* other);
PointfArray*    CopyPointfArray(PointfArray* other, bool bExtendMemory);
Hyperrect*      CopyHyperrect(Hyperrect* other);
Hypersphere*    CopyHypersphere(Hypersphere* other);
KDNode*         CopyKDNode(KDNode* other);
KDTree*         CopyKDTree(KDTree* other);

/* -------- Struct modification -------- */
void            AddToPointfArray(PointfArray** pArray, KDNode* NodesToAdd);
void            TrimHyperrect(Hyperrect* rect, Pointf* p, int32 cd, TrimSide side);
void            UdpateMinMax(KDTree* Tree, Pointf* ptPos);

void            KDRemove2(KDTree* Tree, double dX, double dY);
void            KDRemove(KDTree* Tree, Pointf* ptPos);
KDNode*         KDRemoveRec(KDNode* CurNode, Pointf* ptPos, bool* rbRemoved, int32 nDim, int32 nDepth);

void            KDInsert2(KDTree* Tree, double dX, double dY);
void            KDInsert(KDTree* Tree, Pointf* ptPos);
KDNode*         KDInsertRec(KDNode* CurNode, Pointf* ptPos, bool* rbAdded, int32 nDim, int32 nDepth);

/* -------- get information  -------- */
Pointf*         ClosestHyperrectVertex(Pointf* ptRef, Hyperrect* rect);
bool            HyperrectInHypersphere(Hyperrect* rect, Hypersphere* sphere);
bool            HyperrectDisjointFrom(Hyperrect* rect, Hypersphere* sphere);
bool            PointfInHypersphere(Pointf* ptRef, Hypersphere* sphere);
bool            SamePos(double* p1, double* p2, int32 nDim);
double          EucDistSq(double* a, double* b, int32 nDim);

/* -------- KDTree nearest neighbor queries  -------- */
PointfArray*    FindAllInRange(KDTree* Tree, Pointf* ptRef, double dRange);
int32           FindInRangeRec(KDNode* root, Hypersphere* sphere, Hyperrect* BB, PointfArray** pResult, int32 nDepth);

Pointf*         FindNearestNeighbor(KDTree* Tree, Pointf* ptRef,
                                    double dMinDist, double dMaxDist, double* rdDistSq);
void            FindNNRec(KDNode* root, Pointf* ptRef, double dMinDist, double dMaxDist,
                          Pointf** ptBest, double* dBestDistSq, Hyperrect* BB, int32 nDepth);

KDNode*         FindMin(KDNode* CurNode, int32 nDir, int32 nDim, int32 nDepth);
KDNode*         FindMinRec(KDNode* CurNode, int32 nDir, int32 nDim, int32 nDepth);
KDNode*         MinOfThree(KDNode* a, KDNode* b, KDNode* c, int32 dir);

/* -------- debugging/logging functions -------- */
void            KDTreeToDotFile(KDTree* Tree, const char* strFilename);
void            KDNodesToFile(FILE* pFile, KDNode* node, const char* parentname, int32* num);
void            KDNodeLabelToFile(FILE* pFile, KDNode* node, const char* strName, const char* strColor);
void            LogPointf(Pointf* pt);

char*           KDNodeToString(KDNode* node, int32* num);
char*           KDNodePositionToString(KDNode* node);
char*           PointfToString(Pointf* pt);


#endif /* __KD_TREE_H */
