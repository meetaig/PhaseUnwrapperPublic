#ifndef __UTILS_H
#define __UTILS_H
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>		
#include <stdlib.h>
#ifdef _MSC_VER
#ifndef _CRT_SECURE_NO_WARNINGS /* disable warnings for now, better solution would be to fix it, but that is a lot of work */
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#endif

#define __STDC_FORMAT_MACROS /* proper format specifiers for 64 bit integer types */
#include <inttypes.h>

/**
 @file      utils.h
 @brief     defines, structs and enums for easier data handling and cleaner code.
 @date      25.07.2016
 @author    Tim Elberfeld
*/

typedef uint8_t     uint8;
typedef uint16_t    uint16;
typedef uint32_t    uint32;
typedef uint64_t    uint64;

typedef int8_t      int8;
typedef int16_t     int16;
typedef int32_t     int32;
typedef int64_t     int64;

typedef enum _ERROR_CODES       /** enum for the error codes used in the project */
{
	ERR_SUCCESS 				= 0x00, /**< general success code */
    
	/* FILE IO */
	ERR_INVALID_FILE_HANDLE		= 0x01, /**< invalid file handle encountered */
	ERR_FILE_CLOSE_FAILED		= 0x02, /**< failed to close file */
	ERR_FILE_READ_FAILED 		= 0x03, /**< failed to read file */
	ERR_FILE_OPEN_FAILED		= 0x04, /**< failed to open file */
	ERR_DIR_NOT_FOUND			= 0x05, /**< did not find directory */
	
    /* MEMORY MANAGEMENT */
	ERR_ALLOCATION_FAILED		= 0x06, /**< allocation request failed */
	ERR_FREE_FAILED				= 0x07, /**< could not free memory */
	ERR_MEMORY_SIZE_MISMATCH	= 0x08, /**< memory sizes are not the same */
	
    /* MEMORY ACCESS */
	ERR_INDEX_OUT_OF_BOUNDS 	= 0x09, /**< index accessing invalid element */
	ERR_INDEX_INVALID			= 0x0A, /**< index invalid */
	ERR_INVALID_MEMORY_ACCESS	= 0x0B, /**< invalid memory access */
	ERR_NULL_POINTER			= 0x0C, /**< null pointer encountered */
    
	/* MISC */
	ERR_UNIT_TEST_FAILED		= 0x0D, /**< failed unit test */
	ERR_INVALID_PARAM			= 0x0E, /**< invalid parameter value */
    ERR_NOT_IMPLEMENTED         = 0x0F, /**< function not implemented */

    /* ALGORITHM SPECIFIC */
	ERR_FLYNN_BROKEN_LOOP		= 0x10, /**< broken loop encountered in Flynn's algorithm */
	ERR_INVALID_QUERY_RESULT	= 0x11, /**< kd query result invalid */
	ERR_KD_DIMENSION_ZERO		= 0x12,  /**< kd tree has invalid dimensionality 0 */

    ERR_ERROR_CODE_COUNT        = 0x13
} ErrorCodes;

static const char* ERR_CODE_STRING[] =
{
    "",
    "Invalid file handle",
    "Could not close file handle",
    "Could not read file",
    "Could not open file",
    "Directory not found",
    "Allocation request failed",
    "Could not free memory",
    "Buffer sizes don't match",
    "Index out of bounds",
    "Invalid index",
    "Invalid memory access",
    "NULL pointer encountered",
    "Failed unit test",
    "Invalid parameter",
    "Functionality not implemented",
    "Flynn: broken loop encountered",
    "KDTree: query returned invalid result",
    "KDTree: invalid dimensionality 0",
};

typedef struct _POINT   /** simple point structure */
{
	int32 x;              /**< X coordinate */
	int32 y;              /**< Y coordinate */
} Point;

typedef struct _ARRAY_DIM   /** structure for collection dimensions of arrays */
{
	int32 h;          /**< height */
	int32 w;          /**< width */
} ArrayDim;

typedef struct _RECTANGLE   /** simple rectangle structure */
{
	int32 top;        /**< starting y coordinate */
	int32 left;       /**< starting x coordinate */
	int32 height;     /**< height, end y is computed using top + height */
	int32 width;      /**< width, end x is computed using left + width */
} Rectangle;

typedef struct _ARRAY_3F
{
    uint32 w;
    uint32 h;
    uint32 d;
    size_t numel;
    double* data;
} Array3f;

/* numerical constants */
#ifndef PI
#define PI (3.141592653589793)
#endif

#ifndef NEGPI
#define NEGPI (-3.14159265358979)
#endif

#ifndef TWOPI
#define TWOPI (6.283185307179586)
#endif

#ifndef HALFPI	
#define HALFPI (1.570796326794896)
#endif

#ifndef TAU
#define TAU TWOPI
#endif

#ifndef FLAG
#define FLAG uint8
#endif

#ifndef NOT
#define NOT(expression) (!(expression))
#endif

/* for easier allocation */ 
#define Bytes(x) x
#define KiloBytes(x) 1024 * Bytes((x))
#define MegaBytes(x) 1024 * KiloBytes((x))
#define GigaBytes(x) 1024 * MegaBytes((x))

#define LOG_BYTE_THRESH KiloBytes(1)    /* arbitrary threshold to log the allocation every time instead of respecting the bLogAllocation flag  */

#define set_flag(x, flag) ((x) |= (flag))
#define delete_flag(x, flag) ((x) &= (~(flag)))
#define test_flag(x, flag) ((x) & (flag))

#ifndef __max
#define __max(a,b) \
        ({  __typeof__ (a) _a = (a); \
       		__typeof__ (b) _b = (b); \
       		_a > _b ? _a : _b; })
#endif

#ifndef __min
#define __min(a,b) \
        ({  __typeof__ (a) _a = (a); \
        	__typeof__ (b) _b = (b); \
        	_a < _b ? _a : _b; })
#endif

#define QuitWithError(arg_str, arg_errcode) QuitWithErrorImp((__FILE__), (__func__), (__LINE__), (arg_str), (arg_errcode))

void QuitWithErrorImp(const char* strFilename, const char* strCaller, uint32 nLineNumber, const char* strMsg, int32 nErrCode);
bool IsBigEndian();
void ConvertToWindowsStylePath(char* path);

#endif /* __UTILS_H */
