#ifndef __GENERAL_H
#define __GENERAL_H
#include "utils.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>		/* DBL_EPSILON */
#include <limits.h>
#include "fileio.h"
#ifndef _MSC_VER
#include <tgmath.h>		/* type-generic copysign() macro */
#endif


/**
@file       general.h
@brief		Interface for general purpose functions for phase unwrapping. 
			For descriptions of the functions, see general.c
@date		25.07.2016
@author		Tim Elberfeld
*/

#define REL_ERR_DBL_DEFAULT (2 * DBL_EPSILON)

/* renaming for allocation and free to hide bLog<Action> */
/**
 @def   Define to reduce number of parameters for #AllocBytesImp
 */
#define AllocBytes(arg_numbytes, arg_strtype, arg_typesize) AllocBytesImp((arg_numbytes), (arg_strtype), (arg_typesize), (true))
/**
 @def   Define to reduce the number of arguments in FreeBytesImp
 */
#define FreeBytes(arg_ptr) FreeBytesImp((arg_ptr), (true))
/**
 @def   Define to reduce the number of arguments in BresenhamLineImp
 */
#define BresenhamLine(arg_imgptr, arg_x1, arg_y1, arg_x2, arg_y2, arg_width, arg_color, arg_isflag) \
		BresenhamLineImp((arg_imgptr), (arg_x1), (arg_y1), (arg_x2), (arg_y2), (arg_width), (arg_color), (arg_isflag), 0)

/* -- memory handling -- */
size_t  GetNumBytes(ArrayDim nDimension, int32 nBytesPerElement);
void*   ExtendMemory(void* pOld, uint32 nOldLength, uint32 nNewLength);
void    FlipByteOrder(float* pfArray, uint64 nElements);
double* ConvertToDouble(float* pfArray, uint64 nElements);
void	ClearToZero(void* pArray, size_t nBufferSize);
void 	CopyArray(const void* pSrc, size_t nSourceSize, void* pDst, size_t nDestSize);
void*	AllocBytesImp(uint64 nNumBytes, char* strType, int32 nTypeSize, bool bLogAllocation);
void 	FreeBytesImp(void* ptr, bool bLogFree);

/* -- useful numerical features -- */
bool    AlmostEqualRelative(double A, double B, double dMaxRelativeErr);
double  Lerp(double d1, double d2, double t);
double  GetRandomDouble(double dMax);
double* LinSpace(double dStart, double dEnd, int32 nSteps);
double* WrapPhase(double* pdArray, int32 nNumel);
void    Multiply(double* pdArray, int32 nNumel, double dScalar);
void    SwapInArray(double* pdArray, uint64 nNumel, uint64 a, uint64 b);
void    Diff(const double* pdArr1, const double* pdArr2, ArrayDim nDimension, double* pdDiff);
void    NearlySquareDivisor(int32 n, int32* a, int32* b);
void 	Invert(double* pdArray, int32 nNumel, double dMaxVal);
void    GetNearestBorderPixel(int nX, int nY,  ArrayDim nDimension, Point* ptBorder, double* dDistance);

/* index magic */
bool    RectInBounds(Rectangle rect, ArrayDim nDimension);
bool    IsValidIndex(int32 nIndex, ArrayDim nDimension);
bool    IsValidIndex2D(int32 nX, int32 nY, ArrayDim nDimension);
int32   WrapIndex2D(int32 x, int32 y, ArrayDim nDimension);
int32   WrapIndex(int32 nIndex, int32 nMaxIndex);
void    ConvertTo2DIndex(int32 nIndex, int32 nRowPitch, int32* rnX, int32* rnY);

/* misc */
double* Crop_double(double* pdArray, ArrayDim nDimension, Rectangle newDimensions);
int32*  Crop_int32(int32* pnArray, ArrayDim nDimension, Rectangle newDimensions);
uint8*  Crop_uint8(uint8* pcArray, ArrayDim nDimension, Rectangle newDimensions);
void    Swap(int32* a, int32* b);
double  Reciprocal(double n);
double  GradientNormalized(double d1, double d2, bool bIsWrapped);
void    SetBorderInvalid(double* pdArray, ArrayDim nDimension);
void 	PaintRect(uint8* pImage, ArrayDim nDimension, Rectangle rect, uint8 nColor);
void 	PaintRectBorder(uint8* pImage, ArrayDim nDimension, Rectangle rect, uint8 nColor);
bool 	BresenhamLineImp(uint8* pImage, int32 nX1, int32 nY1, int32 nX2, int32 nY2, int32 nWidth,
                         uint8 nLineColor, bool bIsFlagValue, bool bLog);
void    AssignColor(uint8* pImage, int32 nIndex, bool bIsFlagValue, uint8 nLineColor);

/* Array3f */
Array3f* NewArray3f(uint32 nWidth, uint32 nHeight, uint32 nDepth);
void FreeArray3f(Array3f* arr);
void WriteArray3fAt(Array3f* arr, uint32 x, uint32 y, uint32 z, double value);

#endif /* __GENERAL_H */
