#ifndef __LINKED_LIST_H
#define __LINKED_LIST_H
#include <assert.h>
#include <stdlib.h>
#include "utils.h"
#include "general.h"

/**
@file       linked_list.h
@brief		Interface for linked list structures and functions. 
			For descriptions of the functions, see linked_list.c
@date		27.07.2016
@author		Tim Elberfeld
*/

typedef struct INT_NODE
{
  int32 x;                  /* payload */
  struct INT_NODE* next;    /* pointer to next node */
  struct INT_NODE* prev;    /* pointer to previous node */
} IntNode;

typedef struct INT_LIST
{
	struct INT_NODE* root;      /* start node of the list */
	struct INT_NODE* end;       /* end node of the list */
	int32 size;                 /* number of valid nodes */
} IntList;

#define LogNode(arg_node) LogNodeImp((arg_node), (__func__), (__LINE__))
#define LogList(arg_list) LogListImp((arg_list), (__func__), (__LINE__))

IntList*    NewLinkedList();
void        InitList(IntList* LinkedList, int32* pnValues, int32 nNumel);
void        FreeList(IntList* LinkedList);
void        AppendList(int32 nValue, IntList* LinkedList);
int32       PopList(IntList* LinkedList);
IntNode*    DeleteList(int32 nValue, IntList* LinkedList);
void        InsertList(int32 nValue, IntList* LinkedList, int32 nTargetPos);
bool        ElementInList(int32 nValue, IntList* LinkedList);

/* LOGGING UTILS */
double*     ComputePairwiseDistances(IntList* PointList, ArrayDim nDimension, int32* rnElements);
void        LogNodeImp(IntNode* node, const char* strCaller, uint32 nLineNumber);
void        LogListImp(IntList* LinkedList, const char* strCaller, uint32 nLineNumber);

#endif
/* __LINKED_LIST_H */
