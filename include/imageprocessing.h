#ifndef __IMAGE_PROCESSING_H
#define __IMAGE_PROCESSING_H
#include "stack.h"
#include "utils.h"
#include "general.h"
#include "fileIO.h"

/**
@file       imageprocessing.h
@brief		Interface for image processing functions. 
			For descriptions of the functions, see imageprocessing.c
@date		25.07.2016
@author		Tim Elberfeld
*/

typedef enum _DIRECTION     /** enum for direction of filters (i.e. GradientFilter() or MedianFilter1D()  */
{
    DIR_X	= 0x01, /**< X direction */
    DIR_Y 	= 0x02  /**< Y direction */
} Direction;

void    MedianFilter1D(uint8* pImg, ArrayDim nDimension, Direction nDirection, uint8* pResult);
void    GradientFilter(double* pdArray, ArrayDim nDimension, Direction nDirection, bool bIsWrapped, double* pdGradient);
void    VarianceFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdVariance);
void    MaximumFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdMaximum);
void    AverageFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdAverage);
void    SquaredAverageFilter(double* pdArray, ArrayDim nDimension, int32 nBoxSize, bool bIsAdditive, double* pdAverage);

void    FloodFill(uint8* pcImg, ArrayDim nDimension, Point ptSeed, uint8 cBorderColor, uint8 cFillColor);
Stack*  FloodFillList(uint8* pcImg, ArrayDim nDimension, Point ptSeed, uint8 cBorderColor);

uint8*  ResizeImage(uint8* pcImg, ArrayDim nDimension, double dScaling, ArrayDim* nNewDimension);

#endif
