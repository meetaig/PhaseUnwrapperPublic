/**
 @file      timer.h
 @brief     Interface for simple timer structure to measure code performance. 
            For implementation details see timer.c
 @author    Tim Elberfeld
 @date      21.09.2016
 */
#ifndef __TIMER_H
#define __TIMER_H
#include "utils.h"
#include <stdio.h>
#include <time.h>

typedef struct _TIMER
{
    clock_t start_t;
    clock_t end_t;
    double  secs_start;
    double  secs_end;
} Timer;

void    InitTimer(Timer* timer);
void    StartTimer(Timer* timer);
void    EndTimer(Timer* timer);
double  GetTimeElapsed(Timer* timer);
uint64 GetCyclesElapsed(Timer* timer);

#endif /* TIMER_H */
