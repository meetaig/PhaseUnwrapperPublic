#ifndef __FLYNN_H
#define __FLYNN_H
#include <assert.h>
#include "utils.h"
#include "fileIO.h"

/**
@file       flynn.h
@brief		Interface for functions for Flynn's Minimum Discontinuity Algorithm. For descriptions of the functions, see flynn.c
@date		27.07.2016
@author		Tim Elberfeld
*/

/* directions for the edges in OptimizeWrapCounts */
#define FLAG_DOWN  		0x01	/* 0000 0001 */
#define FLAG_UP     	0x02	/* 0000 0010 */
#define FLAG_RIGHT 		0x04	/* 0000 0100 */
#define FLAG_LEFT   	0x08	/* 0000 1000 */

/* flags for when to change the value at a specific pixel */
#define FLAG_NOW  		0x10	/* 0001 0000 */
#define FLAG_LATER		0x20	/* 0010 0000 */	
#define FLAG_ANYTIME	(FLAG_NOW | FLAG_LATER)

#ifndef LARGE_INTEGER
#define LARGE_INTEGER 25500		/* large integer to get appropriate weights from the algorithm */
#endif

#ifndef EPS
#define EPS 0.000001        	/* this is the original epsilon value Flynn used in his implementation */
#endif

void ScanNodes(ArrayDim nJumpCountDim, int32* pnHorizontalJump, int32* pnVerticalJump, double* pdQualityMap);
void AddLeftToRight(uint8* pcBitflags, int32* pnWrapCounts, int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim, int32* rnNewEdges, int32* rnNewLoops);
void AddTopToBottom(uint8* pcBitflags, int32* pnWrapCounts, int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim, int32* rnNewEdges, int32* rnNewLoops);
void AddRightToLeft(uint8* pcBitflags, int32* pnWrapCounts, int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim, int32* rnNewEdges, int32* rnNewLoops);
void AddBottomToTop(uint8* pcBitflags, int32* pnWrapCounts, int32* pnVerticalJump, int32* pnHorizontalJump,
                    double* pdQualityMap, ArrayDim nPhaseDim, int32* rnNewEdges, int32* rnNewLoops);

void ComputeJumpCounts(double* pdWrappedPhase, ArrayDim nDimension, int32* pnHorizontalJump, int32* pnVerticalJump);
void ComputeJumpCountsOriginal(double* pdWrappedPhase, int32* pnHJump, int32* pnVJump, ArrayDim nDimension);
void UnwrapFromJumpCounts(double* pdWrappedPhase, ArrayDim nDimension,
                          const int32* pnHorizontalJump, const int32* pnVerticalJump, double* pdUnwrapped);
void ApplyElementaryOperation(	Point ptBase, Point ptLast, uint8* pcBitflags, int32* pnWrapCounts,
					int32* pnVerticalJump, int32* pnHorizontalJump, ArrayDim nJumpCountDim);
void ChangeExten(	Point ptRoot,  Point ptLast, bool* rbFoundLoop, int32 nValueChange, int32* pnWrapCounts, 
					uint8* pcBitflags, ArrayDim nJumpCountDim);
void ChangeValue(Point ptRoot, int32 nValueChange, int32* pnWrapCounts, uint8* pcBitflags, ArrayDim nJumpCountDim);
void ChangeJump(Point ptFrom, Point ptTo, int32* pnVerticalJump, int32* pnHorizontalJump, ArrayDim nJumpCountDim);

#endif /* __FLYNN_H */
