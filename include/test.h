#ifndef __TEST_H
#define __TEST_H
#include <math.h>
#include <assert.h>
#include "utils.h"
#include "fileIO.h"
#include "general.h"
#include "imageprocessing.h"
#include "linked_list.h"
#include "unwrap.h"
#include "goldstein.h"
#include "flynn.h"
#include "qualitymaps.h"

/**
@file       test.h
@brief		Interface for testing algorithms implemented for phase unwrapping. 
			For descriptions of the functions, see test.c
			
			Unwrapping algorithms taken from
			D. C. Ghiglia and M. D. Pritt, "Two-Dimensional Phase Unwrapping: Theory, Algorithms and Software."
			Wiley Blackwell, May 1998.
			http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html

@date		29.08.2016
@author		Tim Elberfeld
*/

bool Test_Nint(int32 numel);
bool Test_Bresenham();
bool Test_PrintDatatypes();
bool Test_KdTree_FindMinBug();
bool Test_KdTree_FindNearestNeighbor();
bool Test_KdTree_GeneralTestCase(double* pdWrappedPhase, int32 nWidth, int32 nHeight);
bool Test_QualityMapPerformance(double* pdWrapped, double* pdOriginal, ArrayDim nDimension, int32 nMinBox, int32 nMaxBox, char* strDataDumpFileName);

#endif /* __TEST_H */
