#ifndef __FILE_IO_H
#define __FILE_IO_H
/**
@file       fileIO.h
@brief		Interface for the file IO functions. For descriptions of the functions, see fileIO.c
@date		25.07.2016
@author		Tim Elberfeld
*/
#ifdef _MSC_VER
#ifndef _CRT_SECURE_NO_WARNINGS /* disable warnings for now, better solution would be to fix it, but that is a lot of work */
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#endif

#include <time.h>
#include <stdarg.h>
#include "general.h"
#include "colormap.h"
#include "qdbmp.h"
#include "utils.h"
#include "statistics.h"

#define LOGGING_ENABLED 1

/* macros to automatically insert calling function name and line number */
#ifdef LOGGING_ENABLED
#define WriteErrorLog(arg_buffsize, arg_formatstr, ...)\
        WriteToLogFileImp((ERRLOGFILE), (__func__), (__LINE__), (arg_buffsize), (arg_formatstr), ##__VA_ARGS__)

#define WriteToLogFile(arg_buffsize, arg_formatstr, ...)\
        WriteToLogFileImp((LOGFILE), (__func__), (__LINE__), (arg_buffsize), (arg_formatstr), ##__VA_ARGS__)

#define LogArray2D(arg_arrptr, arg_width, arg_height)\
        LogArray2DImp((LOGFILE), (arg_arrptr), (arg_width), (arg_height), (__func__), (__LINE__))
#define LogArray1D(arg_arrptr, arg_elements)\
        LogArray1DImp((LOGFILE), (arg_arrptr), (arg_elements), (__func__), (__LINE__))
#else
#define WriteErrorLog(arg_buffsize, arg_formatstr, ...) 
#define WriteToLogFile(arg_buffsize, arg_formatstr, ...)
#define LogArray2D(arg_arrptr, arg_width, arg_height)
#define LogArray1D(arg_arrptr, arg_elements)
#endif
/* (hopefully) platform independent directory creation */
#if defined(_WIN32) || defined(WIN32) 	/* this should be defined under windows, regardless of 64 or 32 bit*/
	#include <direct.h>
	#include <sys/stat.h>
	#define GetWorkingDir _getcwd
	#define MakeDir(str) _mkdir(str)
#else									/* unix based system */
	#include <unistd.h>
	#include <sys/stat.h>
	#define GetWorkingDir getcwd
	#define MakeDir(str) mkdir(str, 0777)
#endif

#define DBL_MAX_CHAR_COUNT 310  /*  number of digits in double precision floating point
                                    is at most 310 for (-DBL_MAX), not counting the decimal
                                    point and averything after that */

/* arbitrary constant for calling WriteToLogFile */
#define LOGBUFF 	(DBL_MAX_CHAR_COUNT + 100)  /*  number of bytes to allocate for logging buffer by default.
                                                    Usage: WriteToLogFile(LOGBUFF, 'format_string', 'va_arg'); */
#define LOGFILE 	"general_log.txt"		/* all purpose log file */
#define ERRLOGFILE 	"errors.txt"			/* log file for errors that are not that grave that the program needs to be stopped */

/* macro magic for printf'ing bytes as binary */
#define BYTE_TO_BINARY_PATTERN "%d%d%d%d%d%d%d%d"
#define BYTE_TO_BINARY(byte)   \
        (byte & 0x80 ? 1 : 0), \
        (byte & 0x40 ? 1 : 0), \
        (byte & 0x20 ? 1 : 0), \
        (byte & 0x10 ? 1 : 0), \
        (byte & 0x08 ? 1 : 0), \
        (byte & 0x04 ? 1 : 0), \
        (byte & 0x02 ? 1 : 0), \
        (byte & 0x01 ? 1 : 0)

#ifndef MAX_PATH
#define MAX_PATH 512
#endif

#ifndef DEFAULT_BMP_NAME_SIZE
#define DEFAULT_BMP_NAME_SIZE 512
#endif 

#define CSV_ALIGNMENT 10        /*  min width of the padding for the csv printing */
#define CSV_DELIM_COMMA         ", "
#define CSV_DELIM_TAB           "\t"

#define RGB_RED_WEIGHT          0.299
#define RGB_GREEN_WEIGHT        0.587
#define RGB_BLUE_WEIGHT         0.114

/* functionality to be able to read bytefiles from cython (requires the memory to be allocated by cython) */
int32   ScanFileForNumBytes(char* strFilenamePlusPath);
void    ReadBytesPreAlloc_double(char* strFilenamePlusPath, double* pdArray, int32 nLength);
void    ReadBytesPreAlloc_float(char* strFilenamePlusPath, float* pfArray, int32 nLength);
void    ReadBytesPreAlloc_uint8(char* strFilenamePlusPath, uint8* pcArray, int32 nLength);

/* now for the actual function definitions */
uint8*  ReadBytes_uint8(char* strFilenamePlusPath, int32* rnBytes);
int32*  ReadBytes_int32(char* strFilenamePlusPath, int32* rnBytes);
float*  ReadBytes_float(char* strFilenamePlusPath, int32* rnBytes);
double* ReadBytes_double(char* strFilenamePlusPath, int32* rnBytes);
void*   ReadBytes(char* strFilenamePlusPath, int32* rnBytes);
int32   SaveAsByteCode(void* pArray, int32 numel, int32 bytes, const char* strFilename);

int32   WriteToLogFileImp(const char* strFilename, const char* strCallerFunc, int32 nLineNumber, size_t nBufferSize, const char* strFormat, ...);

int32   LogArray2DImp(const char* strFilename, double* pdArray, int32 nWidth, int32 nHeight, const char* strCaller, int32 nLineNumber);
int32   LogArray1DImp(const char* strFilename, int32* pnArray, uint32 nElements, const char* strCaller, int32 nLineNumber);
void    LogAllocation(void* ptr, int32 nNumBytes, int32 nBytesPerElement, bool isFree);

/* export of different file types */
void    SaveAsCSV_double(double* pdArray, ArrayDim nDimension, char* strFilename, char* strDelimiter, int nPrecision);
void    SaveAsCSV_int32(int32* pnArray, ArrayDim nDimension, char* strFilename, char* strDelimiter);
void    SaveAsCSV_uint8(uint8* pcArray, ArrayDim nDimension, char* strFilename, char* strDelimiter);

uint8*  LoadBMP(char* strFilename, ArrayDim* rnDimension, int32* rnBitsPerPixel);
uint8*  RGBToGrayscale(uint8* pBitmapData, ArrayDim nDimension, int32 nBitsPerPixel);

int32   SaveAsBMP_double(double* pdData, ArrayDim nDimension, CMAP colormap, const char* strFilename, ...);
int32   SaveAsBMP_int32(int32* pnData, ArrayDim nDimension, CMAP colormap, const char* strFilename, ...);
int32   SaveAsBMP_uint8(uint8* pcData, ArrayDim nDimension, CMAP colormap, const char* strFilename, ...);
int32   SaveBMP(struct _BMP* pBMP, const char* strFilename, CMAP colormap);
void    ApplyColorMap(struct _BMP* pBMP, CMAP colormap);

FILE*   MakeOpenLogFile(const char* strFilename, const char* strOpenMode);
FILE*   GetOpenFileHandle(const char* strFilenamePlusPath, const char* strOpenMode);
int32   CloseFile(FILE* pFile);
char*   GetNextLine(FILE* pFile, int32* rnBytes);
void    GetLogDir(char* strPath, int32 nBufSize);
void    MakeTimeStamp(char* strBuffer, int32 nBufferSize);

#endif /* __FILE_IO_H */
