#ifndef __UNWRAP_H
#define __UNWRAP_H
#include "float.h"
#include <assert.h>
#include "imageprocessing.h"
#include "general.h"
#include "goldstein.h"
#include "flynn.h"
#include "qualitymaps.h"

/**
@file       unwrap.h
@brief		Interface for main unwrapping functionality functions for phase unwrapping.
			For descriptions of the functions, see unwrap.c
			Algorithms taken from
			D. C. Ghiglia and M. D. Pritt, "Two-Dimensional Phase Unwrapping: Theory, Algorithms and Software."
			Wiley Blackwell, May 1998.
			http://eu.wiley.com/WileyCDA/WileyTitle/productCd-0471249351.html

@date		27.07.2016
@author		Tim Elberfeld
*/

void GoldsteinUnwrap(double* pdWrappedPhase, int nWidth, int nHeight, int nMaxCutLength, bool bRemoveDipoles, double* pdUnwrapped);
void FlynnUnwrap(double* pdWrappedPhase, int nWidth, int nHeight, double* pdQualityMap, double* pdUnwrapped);

double MeasureUnwrappingSuccess(double* pdUnwrapped, QualityMapMode mode, int32 nWidth, int32 nHeight);
double L0Norm(double* pdUnwrapped, double* pdWrapped, double* pdWeights, ArrayDim nDimension);
double L1Norm(double* pdUnwrapped, double* pdWrapped, double* pdWeights, ArrayDim nDimension);
double L2Norm(double* pdUnwrapped, double* pdWrapped, double* pdWeights, ArrayDim nDimension);

#endif
/* __UNWRAP_H */
