#ifndef __GOLDSTEIN_H
#define __GOLDSTEIN_H
#include "utils.h"
#include "general.h"
#include "linked_list.h"
#include "stack.h"
#include "kdtree.h"
#include "statistics.h"
#include "fileIO.h"

/**
@file       goldstein.h
@brief		Interface for Goldstein unwrapping functions.
			For descriptions of the functions, see goldstein.c
@date		25.07.2016
@author		Tim Elberfeld
*/

#define NUM_DIM 2 		/* number of dimensions for the positions in kdtree */

#define FLAG_POS_RES 	0x01                        /* 0000 0001 */
#define FLAG_NEG_RES	0x02                        /* 0000 0010 */
#define FLAG_BALANCED	0x04                        /* 0000 0100 */
#define FLAG_ACTIVE 	0x08                        /* 0000 1000 */
#define FLAG_UNWRAPPED 	0x10                       	/* 0001 0000 */
#define FLAG_BORDER 	0x20                        /* 0010 0000 */
#define FLAG_BRANCH_CUT 0x40                      	/* 0100 0000 */
#define FLAG_POSTPONED  0x80                        /* 1000 0000 */

#define FLAG_PROCESSED 	(FLAG_BALANCED | FLAG_ACTIVE)
#define FLAG_RESIDUE	(FLAG_NEG_RES | FLAG_POS_RES)
#define FLAG_AVOID		(FLAG_BORDER | FLAG_BRANCH_CUT | FLAG_UNWRAPPED)
#define FLAG_IGNORE		FLAG_AVOID

#define BRCUT_BORDER_MARGIN 3.0     /*  margin for border pixels. This is so that if there
                                        are residues that are only a little bit farther away
                                        than a border pixel, these residues will be picked
                                        instead of the border pixel */

int32 	MakeResidueMap(double* pdWrappedPhase, ArrayDim nDimension,
                       uint8* pcBitflags, int32* pnResidueMap, IntList* ResidueIndexList);
void 	PlaceBranchCuts(int* pnResidueMap, int nNumResidues, uint8* pcBitflags,
						ArrayDim nDimension, int32 nMaxCutLength, IntList* ResidueIndexList);
void 	PlaceBranchCutsPrimitive(int* pnResidueMap, int nNumResidues, uint8* pcBitflags,
                                 ArrayDim nDimension, int32 nMaxCutLength);
void 	InsertResidueCoordsToKDTree(int32* pnResidueMap, ArrayDim nDimension,
                                    KDTree* TreeRoot, IntList* ActiveResidueList);
void 	RemoveDipoles(int32* pnResidueMap, uint8* pcBitflags, ArrayDim nDimension,
                      int32* nNumResidues, IntList* ResidueIndexList);
void 	MarkInactiveAndBalanced(Stack* ActiveResidues, ArrayDim nDimension, uint8* pcBitflags);
void 	MaskBorderPixels(uint8* pcBitflags, ArrayDim nDimension);
int32 	CheckNeighbors(uint8* pcBitflags, int32 xx, int32 yy, ArrayDim nDimension, FLAG flag);
double  QueryNearestResidue(KDTree* TreeRoot, int32 nX, int32 nY, Point* ptFoundRes, double dMindist, double dRange);
void    UnwrapFromBranchCuts(double* pdWrappedPhase, uint8* pcBitflags, ArrayDim nDimension, double* pdUnwrapped);
void    UpdateAdjoinList(int32 nIndex, double* pdWrappedPhase, double* pdUnwrapped,
                         uint8* pcBitflags, ArrayDim nDimension, Stack* AdjoinList);
int32   GetNextUnwrappedNeighbor(int32 nIndex, uint8* pcBitflags, ArrayDim nDimension);
void    UnwrapPixel(double* pdWrappedPhase, double* pdUnwrapped, int32 nIndex, int32 nNeighborIndex, ArrayDim nDimension);

#endif /* __GOLDSTEIN_H */
