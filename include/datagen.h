#ifndef __DATAGEN_H
#define __DATAGEN_H
/**
 @file      datagen.h
 @brief     interface for data generation functions, for detailed description see datagen.c
 @date      13.09.2016
 @author    Tim Elberfeld
 */
#include <stdio.h>
#include "stack.h"
#include "imageprocessing.h"
#include "general.h"
#include "utils.h"

typedef enum _RAMP_MODE     /** modes that GenRamp() supports */
{
    RAMP_SINGLE         = 0x00, /**< single ramp over the whole image */
    RAMP_SHEARED        = 0x01, /**< half the image is like RAMP_SINGLE, the other half is flat */
    RAMP_DOUBLE_SHEARED = 0x02  /**< half the image is a rising ramp the other half is a falling ramp */
} RampMode;

double* GenRamp(ArrayDim nDimension, double dValRange, RampMode mode);
double* GenSpiralShear(ArrayDim nDimension, double dValueRange, double dRadius);

#endif /* __DATAGEN_H */
