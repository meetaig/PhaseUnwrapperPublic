//
//  stack.h
#ifndef __STACK_H
#define __STACK_H
/**
 @file      stack.h
 @author    Tim Elberfeld
 @date		07.09.2016
 @brief     for detailed description see stack.c
 */

#include <stdio.h>
#include "general.h"
#include "fileIO.h"

#define INITIAL_NODE_COUNT 64   /* number of nodes to allocate initially in every case when calling NewStack()*/

typedef struct _STACK_NODE      /** node of the stack */
{
    int32 x;                    /**< payload */
    struct _STACK_NODE* next;   /**< next node */
    struct _STACK_NODE* prev;   /**< previous node */
} StackNode;

typedef struct _STACK       /** Stack structure */
{
    StackNode* root;        /**< pointer to starting node (root) of the stack */
    StackNode* end;         /**< pointer to last element (end/tail) of the stack */
    StackNode* node_pool;   /**< pointer to contiguous node memory */
    size_t size;            /**< number of elements in the stack */
    size_t allocated;       /**< number of nodes allocated in Stack::node_pool */
} Stack;

Stack*  NewStack(size_t nNumNodes);
void    PushStack(Stack** s, int32 nValue);
int32   PopStack(Stack* s);
Stack*  ExtendStack(Stack* s);
void    ClearStack(Stack* s);
void    FreeStack(Stack* s);
int32   StackElementAt(Stack* s, uint32 nIndex);

#endif /* stack_h */
