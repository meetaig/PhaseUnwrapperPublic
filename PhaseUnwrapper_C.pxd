cimport cython

cdef extern from "math.h":
    int abs(int i)

cdef extern from "qualitymaps.h":
    int MakeQualityMap( double* pWrappedPhase, int nMode, int nBoxSize,
                        int nWidth, int nHeight, double* pdQualityMap)


cdef extern from "unwrap.h":
    void GoldsteinUnwrap(double* pdWrappedPhase,
                        int nWidth, int nHeight,
                        int nMaxCutLength, unsigned char bRemoveDipoles,
                        double* pdUnwrapped);

    void FlynnUnwrap(   double* pdWrappedPhase,
                        int nWidth, int nHeight,
                        double* pdQualityMap,
                        double* pdUnwrapped)

cdef extern from "fileIO.h":
    void ReadBytesPreAlloc_uint8(char* strFilenamePlusPath, unsigned char* pcArray, int nLength);
    void ReadBytesPreAlloc_float(char* strFilenamePlusPath, float* pfArray, int nLength);
    void ReadBytesPreAlloc_double(char* strFilenamePlusPath, double* pdArray, int nLength);
    int SaveAsByteCode(void* pArray, int numel, int bytes, const char* strFilename);
    int ScanFileForNumBytes(char* strFilenamePlusPath);	