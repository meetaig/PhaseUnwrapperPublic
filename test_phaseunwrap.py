import unittest
import datagenerator
import general
import numpy as np
import logger
import matplotlib.pyplot as plt
from PhaseUnwrapper_C import test_kdtree_findmin, test_bresenham, test_kdtree_generalcase


class TestPhaseUnwrapping(unittest.TestCase):
    """
    unit testing class for the phase unwrapping methods
    """

    def test_kdtree_findmin(self):
        self.assertTrue(test_kdtree_findmin())

    def test_kdtree_general(self):
        phaseimg = datagenerator.gen_spiral_shear((512, 512), 6*np.pi, 20)
        wrapped = general.wrapping_operator(phaseimg)
        wrapped = general.normalize(wrapped, -np.pi, np.pi)
        self.assertTrue(test_kdtree_generalcase(wrapped))

        img, wrapped = datagenerator.get_wrapped_lena(6*np.pi)
        wrapped = general.normalize(wrapped, -np.pi, np.pi)
        self.assertTrue(test_kdtree_generalcase(wrapped))

    def test_bresenham(self):
        self.assertTrue(test_bresenham())

    def test_border_coordinate_routine(self):
        self.assertTrue(self.border_coordinate_routine("odd"))
        self.assertTrue(self.border_coordinate_routine("even"))


    def border_coordinate_routine(self, mode):
        """
        :brief:         test border coordinate list construction
        :param mode:    can be 'even' or 'odd' or 'both'.
                        -   'even' tests coordinate list with even side length (i.e. 2, 4, 6, ...)
                        -   'odd' tests coordinate list with odd side length (i.e. 3, 5, 7, ...)
                        -   'both' tests both of the above cases
                        output of the method is a figure with a plot of an image containing all the border coordinates
                        produced with a specific k filled with this k value. This sould result in an upside down pyramid
                        in both cases, if the code is correct.
        :return:        nothing. If something went wrong in the routines, errors are raised
        :date:          25.06.2016
        :author:        Tim Elberfeld
        """
        lower_bound = 0

        if mode.lower() == "odd":
            imsize = (513, 513)
            lower_bound = 3
        elif mode.lower() == "even":
            imsize = (512, 512)
            lower_bound = 2
        else:
            raise ValueError("Unknown mode {0}".format(mode))

        upper_bound = imsize[0] - 1
        center = np.asarray((imsize[1] // 2, imsize[0] // 2))
        img = np.zeros(imsize)
        img[center] = 1
        for k in range(lower_bound, upper_bound+2, 2):
            plist = general.border_offset_list(k, center)
            img[plist] = 1

        if np.sum(img) == np.prod(img.shape):
            return True
        else:
            return False


if __name__ == '__main__':
    unittest.main()